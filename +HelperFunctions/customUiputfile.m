function [filename, pathname, filterindex] = customUiputfile(varargin)
applicationSettings = ApplicationSettings.Instance();
currentDirectory = applicationSettings.CurrentFileLocation;
varargin{3} = currentDirectory;
[filename, pathname, filterindex] = uiputfile(varargin{:});

% if ~isequal(filename, 0)
%     applicationSettings.CurrentFileLocation = pathname;
% end
    
end