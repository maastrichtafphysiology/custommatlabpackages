function c = mySetxor(a,b)
%#eml
%SETXOR Set exclusive-or.
%   SETXOR(A,B) when A and B are vectors returns the values that are
%   not in the intersection of A and B.  The result will be sorted.
%   A and B can be cell arrays of strings.
%
%   SETXOR(A,B,'rows') when A are B are matrices with the same number
%   of columns returns the rows that are not in the intersection
%   of A and B.
%
%   [C,IA,IB] = SETXOR(...) also returns index vectors IA and IB such
%   that C is a sorted combination of the elements of A(IA) and B(IB)
%   (or A(IA,:) and B(IB,:)).
%
%   See also UNIQUE, UNION, INTERSECT, SETDIFF, ISMEMBER.

%   Copyright 1984-2009 The MathWorks, Inc.
%   $Revision: 1.19.4.5 $  $Date: 2009/09/28 20:28:09 $

%   Cell array implementation in @cell/setxor.m
% % emlmex mySetxor -eg {emlcoder.egs(0,[1 100]), emlcoder.egs(0,[1 100])} -report

numelA = numel(a);
numelB = numel(b);

c = reshape([a([]);b([])],1,0);

% Handle empty arrays.

if (numelA == 0 || numelB == 0)
    % Predefine outputs to be of the correct type.
    if (numelA == 0)
        c = unique(b);
    else
        c = unique(a);
    end
    
    % General handling.
    
else
    % Sort if unsorted.  Only check this for long lists.
    checksortcut = 1000;
    
    if numelA <= checksortcut || ~(issorted(a))
        a = sort(a);
    end
    
    if numelB <= checksortcut || ~(issorted(b))
        b = sort(b);
    end
    
    % Find members of the XOR set.
    tfa = ~ismember(a, b);
    tfb = ~ismember(b, a);
    
    % a(tfa) now contains all members of A which are not in B
    % b(tfb) now contains all members of B which are not in A
    c = unique([a(tfa), b(tfb)]);    % Remove duplicates from XOR list.
end