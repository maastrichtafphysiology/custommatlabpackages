function xy = fastcov(x,y)
%#codegen
%Fast COV Covariance matrix.
%   COV(X), if X is a vector, returns the variance.  For matrices,
%   where each row is an observation, and each column a variable,
%   COV(X) is the covariance matrix.  DIAG(COV(X)) is a vector of
%   variances for each column, and SQRT(DIAG(COV(X))) is a vector
%   of standard deviations. COV(X,Y), where X and Y are matrices with
%   the same number of elements, is equivalent to COV([X(:) Y(:)]).
% % To generate MEX file:
% % emlmex fastcov -eg {emlcoder.egs(0,[400 1]), emlcoder.egs(0,[400 1])} -report


  data = [x(:) y(:)];
  m = size(data, 1);
  xc = bsxfun(@minus, data, sum(data,1) / m);
  xy = (xc' * xc)/(m - 1);
end
