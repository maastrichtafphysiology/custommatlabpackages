% fastcov: fast covariance computation
emlmex fastcov -eg {emlcoder.egs(0,[400 1]), emlcoder.egs(0,[400 1])}

% set operations: union, intersection, xor
emlmex myUnion -eg {emlcoder.egs(0,[1 100]), emlcoder.egs(0,[1 100])};
emlmex myIntersect -eg {emlcoder.egs(0,[1 100]), emlcoder.egs(0,[1 100])};
emlmex mySetxor -eg {emlcoder.egs(0,[1 100]), emlcoder.egs(0,[1 100])};