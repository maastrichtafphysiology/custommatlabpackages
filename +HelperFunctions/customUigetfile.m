function [filename, pathname, filterindex] = customUigetfile(varargin)
applicationSettings = ApplicationSettings.Instance();
currentDirectory = applicationSettings.CurrentFileLocation;
varargin{3} = currentDirectory;
[filename, pathname, filterindex] = uigetfile(varargin{:});

if ~isequal(filename, 0)
%     applicationSettings.CurrentFileLocation = pathname;
    if ischar(filename)
        applicationSettings.CurrentFileLocation = fullfile(pathname, filename);
    end
    if iscell(filename)
        applicationSettings.CurrentFileLocation = fullfile(pathname, filename{1});
    end
end
    
end