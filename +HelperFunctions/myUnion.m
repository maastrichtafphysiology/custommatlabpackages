function [c,ia,ib] = myUnion(a,b)
%#eml
%UNION  Set union.
%   UNION(A,B) for vectors A and B, returns the combined values of the two
%   vectors with no repetitions. MATLAB sorts the results. A and B can be
%   cell arrays of strings.
%
%   UNION(A,B,'rows') for matrices A are B that have the same number of
%   columns, returns the combined rows from the two matrices with no
%   repetitions. MATLAB ignores the 'rows' flag for all cell arrays.
%
%   [C,IA,IB] = UNION(...) also returns index vectors IA and IB such
%   that C is a sorted combination of the elements A(IA) and B(IB)
%   (or A(IA,:) and B(IB,:)).
%
%   Class support for inputs A,B:
%      float: double, single
%
%   See also UNIQUE, INTERSECT, SETDIFF, SETXOR, ISMEMBER.

%   Copyright 1984-2004 The MathWorks, Inc.
%   $Revision: 1.20.4.5 $  $Date: 2008/03/13 17:32:07 $

%   Cell array implementation in @cell/union.m
% % emlmex myUnion -eg {emlcoder.egs(0,[1 100]), emlcoder.egs(0,[1 100])} -report

numelA = numel(a);
numelB = numel(b);
nOut = nargout;

% Handle empty: no elements.

if (numelA == 0 || numelB == 0)
    
    if (numelB == 0)
        % Call UNIQUE on one list if the other is empty.
        [c, ia] = unique(a);
        ib = zeros(1,0);
    else
        [c, ib] = unique(b);
        ia = zeros(1,0);
    end
    
    % General handling.
    
else    
    if nOut <= 1
        % Call UNIQUE to do all the work.
        c = unique([a b]);
    else
        [c,ndx] = unique([a b]);
        % Indices determine whether an element was in A or in B.
        d = ndx > numelA;
        ia = ndx(~d);
        ib = ndx(d)-numelA;
    end
end