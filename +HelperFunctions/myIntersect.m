function c = myIntersect(a,b)
%#eml
%INTERSECT Set intersection.
%   INTERSECT(A,B) for vectors A and B, returns the values common to the
%   two vectors. MATLAB sorts the results.  A and B can be cell arrays of
%   strings.
%
%   INTERSECT(A,B,'rows') for matrices A and B that have the same number of
%   columns, returns the rows common to the two matrices. MATLAB ignores
%   the 'rows' flag for all cell arrays.
%
%   [C,IA,IB] = INTERSECT(A,B) also returns index vectors IA and IB
%   such that C = A(IA) and C = B(IB).
%
%   [C,IA,IB] = INTERSECT(A,B,'rows') also returns index vectors IA and IB
%   such that C = A(IA,:) and C = B(IB,:).
%
%   Class support for inputs A,B:
%      float: double, single
%
%   See also UNIQUE, UNION, SETDIFF, SETXOR, ISMEMBER.

%   Copyright 1984-2009 The MathWorks, Inc.
%   $Revision: 1.21.4.11 $  $Date: 2009/09/28 20:28:06 $

%   Cell array implementation in @cell/intersect.m
% % emlmex myIntersect -eg {emlcoder.egs(0,[1 100]), emlcoder.egs(0,[1 100])} -report


numelA = numel(a);
numelB = numel(b);

c = reshape([a([]);b([])],0,1);    % Predefined to determine class of output

% Handle empty: no elements.

if (numelA == 0 || numelB == 0)
    
    % Do Nothing
    
elseif (numelA == 1)
    
    % Scalar A: pass to ISMEMBER to determine if A exists in B.
    [tf,pos] = ismember(a,b);
    if any(tf)
        c = a;
    end
    
elseif (numelB == 1)
    
    % Scalar B: pass to ISMEMBER to determine if B exists in A.
    [tf,pos] = ismember(b,a);
    if any(tf)
        c = b;
    end
    
else % General handling.
    
    % Switch to sort shorter list.
    
    if numelA < numelB
        a = sort(a);
        
        [tf,pos] = ismember(b,a);     % TF lists matches at positions POS.
        
        where = zeros(size(a));       % WHERE holds matching indices
        where(pos(tf)) = find(pos);   % from set B, 0 if unmatched.
        tfs = where > 0;              % TFS is logical of WHERE.
        
        % Create intersection list.
        ctemp = a(tfs);
    else
        b = sort(b);
        
        [tf,pos] = ismember(a,b);     % TF lists matches at positions POS.
        
        where = zeros(size(b));       % WHERE holds matching indices
        where(pos(tf)) = find(pos);   % from set B, 0 if unmatched.
        tfs = where > 0;              % TFS is logical of WHERE.
        
        % Create intersection list.
        ctemp = b(tfs);
    end
    
    c = cast(ctemp,class(c));
end
end
