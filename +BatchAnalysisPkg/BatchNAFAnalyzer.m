classdef BatchNAFAnalyzer < BatchAnalysisPkg.BatchAnalyzer
    properties
        HighpassFilter
        ComputedParameters
        SpectralComputation
        SampleEntropy
        FWaveDistanceThreshold
    end
    
    methods
        function self = BatchNAFAnalyzer()
            self = self@BatchAnalysisPkg.BatchAnalyzer;
            
            self.HighpassFilter = struct(...
                'Enabled', true,...
                'Frequency', 1);
            
            self.ComputedParameters = struct(...
                'SpectralParameters', true,...
                'SampleEntropy', true,...
                'FWaveAmplitude', true);
            
            self.SpectralComputation = struct(...
                'NumberOfFFTPoints', 2^10,...
                'WindowLength', 4,...
                'FFTOverlap', 0.5,...
                'DFRange', [3, 12],...
                'NumberOfPeaks', 2,...
                'PeakRange', 1,...
                'FundamentalFrequency', false,...
                'FundamentalFrequencyThreshold', 0.75);
            
            self.SampleEntropy = struct(...
                'samples', 3,...
                'tolerance', 0.35);
            
            self.FWaveDistanceThreshold = 100;
        end
        
        function result = Compute(self, dataList)
            % dataList: a list of QRST filenames
            filenameList = dataList;
            numberOfFiles = numel(filenameList);
            
            result = cell(numberOfFiles, 1);
            patientIDs = cell(numberOfFiles, 1);
            spectra = cell(numberOfFiles, 1);
            fileChannelLabels = cell(numberOfFiles, 1);
            
            self.UpdateTextProgress({' '});
            self.UpdateTextProgress({['%%%% ', datestr(datetime('now')), ' %%%%']});
            self.UpdateTextProgress({'Computing AF complexity parameters...'});
            
            %             parfor fileIndex = 1:numberOfFiles
            for fileIndex = 1:numberOfFiles
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                self.UpdateTextProgress({['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']});
                patientIDs{fileIndex} = shortFilename;
                fileData = load(filename);
                ecgData = fileData.cancelledEcgData;
                numberOfChannels = ecgData.GetNumberOfChannels();
                currentLabels = deblank(ecgData.ElectrodeLabels);
                fileChannelLabels{fileIndex} = currentLabels(:)';
                
                % Highpass filter of 1Hz
                if self.HighpassFilter.Enabled
                    nyquistFrequency = ecgData.SamplingFrequency / 2;
                    [b, a] = cheby2(3, 20,...
                        self.HighpassFilter.Frequency / nyquistFrequency, 'high');
                    ecgData.Data = filtfilt(b, a, ecgData.Data);
                end
                
                % Remove first and last 10% (with a maximum of 1 second)
                time = ecgData.GetTimeRange();
                recordingLength = time(end) - time(1);
                analysisStartTime = min([time(1) + 1, time(1) + recordingLength * 0.1]);
                analysisEndTime = max([time(end) - 1, time(end) - recordingLength * 0.1]);
                validTime = time >= analysisStartTime & time <= analysisEndTime;
                ecgData.Data = ecgData.Data(validTime, :);
                
                % Complexity parameter settings
                complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                complexityCalculator.DFParameters = self.SpectralComputation;
                complexityCalculator.DFParameters.WindowLength = floor(complexityCalculator.DFParameters.WindowLength * ecgData.SamplingFrequency);
                
                % Sample Entropy
                complexityCalculator.SampleEntropyParameters = self.SampleEntropy;
                
                % F-wave amplitude
                complexityCalculator.FWaveDistanceThreshold = self.FWaveDistanceThreshold;
                
                % Spectral parameters (DF, OI & SE)
                if self.ComputedParameters.SpectralParameters
                    spectralParameters = complexityCalculator.ComputeSpectralParameters(ecgData);
                else
                    spectralParameters = struct(...
                        'DominantFrequency', NaN(numberOfChannels, 1),...
                        'OrganizationIndex', NaN(numberOfChannels, 1),...
                        'RegularityIndex', NaN(numberOfChannels, 1),...
                        'SpectralEntropy', NaN(numberOfChannels, 1),...
                        'psd', NaN(numberOfChannels, 1));
                end
                
                % Sample entropy
                if self.ComputedParameters.SampleEntropy && self.ComputedParameters.SpectralParameters
                    [~, dfSampleEntropy] = complexityCalculator.ComputeSampleEntropy(ecgData,...
                        1:ecgData.GetNumberOfChannels, spectralParameters.DominantFrequency, 250);
                else
                    dfSampleEntropy = NaN(numberOfChannels, 1);
                end
                
                % F-wave amplitude & dispersion (with and without QRS intervals)
                if self.ComputedParameters.FWaveAmplitude
                    % with QRS
                    [fWaveAmplitudes, fWaveIndices, fWavePeakValleyIndices] =...
                        complexityCalculator.ComputeFWaveAmplitudes(ecgData);
                    fWaveMedian = cellfun(@median, fWaveAmplitudes);
                    fWaveDispersion = cellfun(@(x)...
                        diff(prctile(x, [25, 75])) / sum(prctile(x, [25, 75])),...
                        fWaveAmplitudes);
                    
                    % only TQ
                    qrsRange = [0.05, 0.05];
                    validFWaveIndices = cellfun(@(x, y)...
                        BatchAnalysisPkg.BatchNAFAnalyzer.GetValidFWaves(...
                        ecgData,...
                        x, y, fileData.rWaveIndices, qrsRange),...
                        fWaveIndices, fWavePeakValleyIndices, 'uniformOutput', false);
                    
                    fWaveMedianTQ = cellfun(@(x, y) median(x(y)), fWaveAmplitudes, validFWaveIndices);
                    fWaveDispersionTQ = cellfun(@(x, y)...
                        diff(prctile(x(y), [25, 75])) / sum(prctile(x(y), [25, 75])),...
                        fWaveAmplitudes, validFWaveIndices);
                else
                    fWaveMedian = NaN(numberOfChannels, 1);
                    fWaveDispersion = NaN(numberOfChannels, 1);
                    fWaveMedianTQ = NaN(numberOfChannels, 1);
                    fWaveDispersionTQ = NaN(numberOfChannels, 1);
                end
                
                % R-R intervals
                rrIntervals = diff(sort(fileData.rWaveIndices));
                rrIntervalMean = mean(rrIntervals ./ ecgData.SamplingFrequency);
                rrIntervalSD = std(rrIntervals ./ ecgData.SamplingFrequency);
                
                % Concatenate parameters
                result{fileIndex} = [...
                    spectralParameters.DominantFrequency;...
                    spectralParameters.OrganizationIndex;...
                    spectralParameters.RegularityIndex;...
                    spectralParameters.SpectralEntropy;...
                    dfSampleEntropy;...
                    fWaveMedian;...
                    fWaveDispersion;...
                    fWaveMedianTQ;...
                    fWaveDispersionTQ;...
                    rrIntervalMean;...
                    rrIntervalSD;...
                    recordingLength]';
                
                spectra{fileIndex} = spectralParameters.psd;
            end
            
            % Structure result output
            channelLabels = unique(horzcat(fileChannelLabels{:}))';
            channelLabels = channelLabels(:)';
            DFLabels = strcat('Dominant_Frequency', '_', channelLabels);
            OILabels = strcat('Organization_Index', '_', channelLabels);
            RILabels = strcat('Regularity_Index', '_', channelLabels);
            SELabels = strcat('Spectral_Entropy', '_', channelLabels);
            SampEnLabels = strcat('Sample_Entropy', '_', channelLabels);
            FWALabels = strcat('F_wave_Amplitude', '_', channelLabels);
            FWADispersionLabels = strcat('F_wave_Amplitude_Dispersion', '_', channelLabels);
            FWATQLabels = strcat('F_wave_Amplitude_TQ', '_', channelLabels);
            FWADispersionTQLabels = strcat('F_wave_Amplitude_Dispersion_TQ', '_', channelLabels);
            parameterLabels = horzcat(DFLabels, OILabels, RILabels, SELabels,...
                SampEnLabels, FWALabels, FWADispersionLabels, FWATQLabels, FWADispersionTQLabels,...
                'RR_Mean', 'RR_SD', 'Length');
            
            collectedResults = NaN(numberOfFiles, numel(parameterLabels));
            for fileIndex = 1:numberOfFiles
                currentLabels = fileChannelLabels{fileIndex};
                currentLabels = currentLabels(:)';
                DFLabels = strcat('Dominant_Frequency', '_', currentLabels);
                OILabels = strcat('Organization_Index', '_', currentLabels);
                RILabels = strcat('Regularity_Index', '_', currentLabels);
                SELabels = strcat('Spectral_Entropy', '_', currentLabels);
                SampEnLabels = strcat('Sample_Entropy', '_', currentLabels);
                FWALabels = strcat('F_wave_Amplitude', '_', currentLabels);
                FWADispersionLabels = strcat('F_wave_Amplitude_Dispersion', '_', currentLabels);
                FWATQLabels = strcat('F_wave_Amplitude_TQ', '_', currentLabels);
                FWADispersionTQLabels = strcat('F_wave_Amplitude_Dispersion_TQ', '_', currentLabels);
                currentParameterLabels = horzcat(DFLabels, OILabels, RILabels,...
                    SELabels, SampEnLabels,...
                    FWALabels, FWADispersionLabels, FWATQLabels, FWADispersionTQLabels,...
                    'RR_Mean', 'RR_SD', 'Length');
                
                [validParameters, parameterIndices] = ismember(currentParameterLabels, parameterLabels);
                currentResult = result{fileIndex};
                
                collectedResults(fileIndex, parameterIndices) = currentResult(validParameters);
            end
            
            resultTable = array2table(collectedResults, 'VariableNames', parameterLabels,...
                'RowNames', patientIDs);
            
            result = struct(...
                'ResultTable', resultTable,...
                'Spectra', {spectra});
            
            self.UpdateTextProgress({'Done'});
        end
    end
    
    methods (Static)
        function validFWaveIndices = GetValidFWaves(ecgData,...
                fWavePeakIndices, fWaveValleyIndices, rWaveIndices, qrsRange)
            
            qrstRangeSamples = round(qrsRange * ecgData.SamplingFrequency);
            qtIntervals = [rWaveIndices - qrstRangeSamples(1), rWaveIndices +  qrstRangeSamples(2)];
            
            validPeakIndices = ~any(bsxfun(@ge, fWavePeakIndices, qtIntervals(:, 1)') &...
                bsxfun(@le, fWavePeakIndices, qtIntervals(:, 2)'), 2);
            
            validValleyIndices = ~any(bsxfun(@ge, fWaveValleyIndices, qtIntervals(:, 1)') &...
                bsxfun(@le, fWaveValleyIndices, qtIntervals(:, 2)'), 2);
            
            validFWaveIndices = validPeakIndices & validValleyIndices;
        end
    end
end