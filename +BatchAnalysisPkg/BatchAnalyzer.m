classdef BatchAnalyzer < handle
    properties
        ResultTextHandle
    end
    
    methods
        function self = BatchAnalyzer()
        end
    end
    
    methods (Abstract)
        result = Compute(self, dataList);
    end
    
    methods (Access = protected)
        function UpdateTextProgress(self, textString, replacePreviousLine)
            if nargin < 3
                replacePreviousLine = false;
            end
            
            if ishandle(self.ResultTextHandle)
                resultText = get(self.ResultTextHandle, 'string');
                if replacePreviousLine
                    resultText(end) = textString;
                else
                    resultText = vertcat(resultText, textString);
                end
                set(self.ResultTextHandle, 'string', resultText);
            else
                disp(textString);
            end
            
            pause(eps);
        end
    end
end
