classdef BatchWavemapAnalysisPanel < BatchAnalysisPkg.BatchAnalysisPanel
    properties
        Analyzer
        
        TabGroup
        SettingsPanel
        LogPanel
        LogText
        
        Results
        ResultPanel 
    end
    
    properties (Access = private)
        TabSelectionDropDown
        ResultTabGroup
        WaveInfoTab
        WaveInfoPanel
        PropertyMapTab
        PropertyMapPanel
        ConductionMapTab
        ConductionMapPanel
        DissociationTab
        DissociationMapPanel
        
        FractionationAmplitude
        FractionationSlope
        FractionationCloseThreshold
        
        MinimalConductionVelocity
        MergeOverlapThreshold
        
        MinimumWaveSize
        VelocityRangeStartEdit
        VelocityRangeEndEdit
    end
    
    methods
        function self = BatchWavemapAnalysisPanel(position)
            self = self@BatchAnalysisPkg.BatchAnalysisPanel(position);
            self.Analyzer = BatchAnalysisPkg.BatchWavemapAnalyzer();
        end
        
        function Create(self, parentHandle)
            Create@BatchAnalysisPkg.BatchAnalysisPanel(self, parentHandle);
            
            uicontrol('parent', self.FileListPanel,...
                'style', 'pushbutton',...
                'units', 'normalized',...
                'string', 'Load result',...
                'callback', @self.LoadResult,...
                'position', [.05 .1 .1 .2]);
            
            self.CreateTabGroup();
        end
    end
    
    methods (Access = protected)
        function AddFilesToFileListBox(self, varargin)
            [filenames, pathname] = uigetfile( ...
                '*_DD.mat','Deflection detection-files (*_DD.mat)', ...
                'Select Deflection Detection (DD) files', ...
                'MultiSelect', 'on');
            if isnumeric(filenames), return; end
            
            if ischar( filenames)
                filenames = cellstr(filenames);
            end
            
            fileList = get(self.FileListBox, 'string');
            
            for i = 1:length(filenames)
                fullPath = [pathname filenames{i}];
                if all(~strcmp(fullPath, fileList))
                    fileList = [fileList; cellstr(fullPath)]; %#ok<*AGROW>
                end
            end
            
            set(self.FileListBox, 'string', fileList);
            set(self.FileListBox, 'value', numel(fileList));
        end
        
        function RemoveFilesFromFileListBox(self, varargin)
            selectedFile = get(self.FileListBox, 'value');
            fileList = get(self.FileListBox, 'string');
            
            if isempty(fileList), return; end
            
            fileList(selectedFile) = [];
            
            set(self.FileListBox, 'value', 1);
            set(self.FileListBox, 'string', fileList);
        end
        
        function Compute(self, varargin)
            filenameList = get(self.FileListBox, 'string');
            self.Results = cell(size(filenameList, 1), 1);
            
            if isempty(filenameList), return; end
            
            self.Results = self.Analyzer.Compute(filenameList);
            
%             for fileIndex = 1:numel(filenameList)
%                 filename = filenameList{fileIndex};
%                 [~, shortFilename] = fileparts(filename) ;
%                 disp(['Processing: ', shortFilename, '...']);
%                 
%                 ddData = load(filename);
%                 
%                 self.Results{fileIndex} = self.Analyzer.Compute(ddData);
%                 self.Results{fileIndex}.shortFilename = shortFilename;
%                 self.Results{fileIndex}.filename = filename;
%             end
%             disp('Done');
%             self.Results = [self.Results{:}];

            self.InitializeResultPanel();
        end
        
        function CreateTabGroup(self)
            self.TabGroup = uitabgroup(...
                'parent', self.ControlHandle,...
                'position', [0 0 1 .8]);
            
            self.SettingsPanel = uitab(self.TabGroup, 'title', 'Parameter settings');
            self.CreateSettingsPanel();
            
            self.LogPanel = uitab(self.TabGroup, 'title', 'Log');
            
            self.LogText = uicontrol('parent', self.LogPanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 1],...
                'style', 'edit',...
                'enable', 'inactive',...
                'min', 1, 'max', 10,...
                'string', '',...
                'backgroundColor', [1,1,1],...
                'horizontalAlignment', 'left');
            uicontrol(...
                'parent', self.LogPanel,...
                'units', 'normalized',...
                'position', [.95 0 .05 .05],...
                'style', 'pushbutton',...
                'string', 'Clear',...
                'callback', @self.ClearLog);
            self.Analyzer.ResultTextHandle = self.LogText;
            
            self.CreateResultPanel();
        end
        
        function CreateResultPanel(self)
%             if ishandle(self.ResultPanel)
%                 delete(self.ResultPanel);
%             end
            self.ResultPanel = uitab(self.TabGroup, 'title', 'Results');
            
            self.TabSelectionDropDown = uicontrol(...
                'parent', self.ResultPanel,...
                'style', 'popup',...
                'units', 'normalized',...
                'position', [.05 .925 .2 .05],...
                'string', 'No files',...
                'callback', @self.ShowSelectedResult);
            
            uicontrol('parent', self.ResultPanel,...
                'style', 'pushbutton',...
                'units', 'normalized',...
                'string', 'Save result as .mat',...
                'callback', @self.SaveResult,...
                'position', [.3 .925 .2 .05]);
            
            uicontrol('parent', self.ResultPanel,...
                'style', 'pushbutton',...
                'units', 'normalized',...
                'string', 'Export result to .csv',...
                'callback', @self.ExportResult,...
                'position', [.55 .925 .2 .05]);
            
            self.ResultTabGroup = uitabgroup(...
                'parent', self.ResultPanel,...
                'position', [0 0 1 .9]);
            
            self.WaveInfoTab = uitab(self.ResultTabGroup, 'title', 'Wave info');
            self.WaveInfoPanel = BatchAnalysisPkg.BatchWaveInfoPanel([0 0 1 1]);
            self.WaveInfoPanel.Create(self.WaveInfoTab);
            self.WaveInfoPanel.Show();
            
            self.PropertyMapTab = uitab(self.ResultTabGroup, 'title', 'Property maps');
            self.PropertyMapPanel = BatchAnalysisPkg.BatchPropertyMapPanel([0 0 1 1]);
            self.PropertyMapPanel.Create(self.PropertyMapTab);
            self.PropertyMapPanel.Show();
            
            self.ConductionMapTab = uitab(self.ResultTabGroup, 'title', 'Conduction maps');
            self.ConductionMapPanel = BatchAnalysisPkg.BatchConductionMapPanel([0 0 1 1]);
            self.ConductionMapPanel.Create(self.ConductionMapTab);
            self.ConductionMapPanel.Show();
            
            self.DissociationTab = uitab(self.ResultTabGroup, 'title', 'Dissociation');
            self.DissociationMapPanel = BatchAnalysisPkg.BatchDissociationPanel([0 0 1 1]);
            self.DissociationMapPanel.Create(self.DissociationTab);
            self.DissociationMapPanel.Show();
        end
        
        function CreateSettingsPanel(self)
            self.CreateFractionationSettings();
            self.CreateWavemapSettings();
            self.CreateWaveComplexitySettings();
        end
        
        function CreateFractionationSettings(self)
            fractionationDetectionPanel = uipanel(...
                'parent', self.SettingsPanel,...
                'title', 'Fractionation quantification',...
                'units', 'normalized',...
                'position', [0 .7 1/3 .2]);
            
            textHeight = 0.3;
            textSpacing = 0.3;
            labelWidth = 0.6;
            editWidth = 0.2;
            
            uicontrol('parent', fractionationDetectionPanel,...
                'style', 'text',...
                'units', 'normalized',...
                'string', 'Amplitude (%)',...
                'horizontalAlignment', 'left',...
                'position', [.1, 1-textSpacing, labelWidth, textHeight]);
            self.FractionationAmplitude = uicontrol('parent', fractionationDetectionPanel,...
                'style', 'edit',...
                'units', 'normalized',...
                'tag', 'amplitude',...
                'string', num2str(self.Analyzer.FractionationThresholds.amplitude),...
                'callback', @self.SetFractionationParameter,...
                'position', [.1 + labelWidth, 1-textSpacing, editWidth, textHeight]);
            
            uicontrol('parent', fractionationDetectionPanel,...
                'style', 'text',...
                'units', 'normalized',...
                'string', 'Slope (%)',...
                'horizontalAlignment', 'left',...
                'position', [.1, 1-2*textSpacing, labelWidth, textHeight])
            self.FractionationSlope = uicontrol('parent', fractionationDetectionPanel,...
                'style', 'edit',...
                'units', 'normalized',...
                'tag', 'slope',...
                'string', num2str(self.Analyzer.FractionationThresholds.slope),...
                'callback', @self.SetFractionationParameter,...
                'position', [.1 + labelWidth, 1-2*textSpacing, editWidth, textHeight]);
            
            uicontrol('parent', fractionationDetectionPanel,...
                'style', 'text',...
                'units', 'normalized',...
                'string', 'Minimum distance to local deflection (seconds)',...
                'horizontalAlignment', 'left',...
                'position', [.1, 1-3*textSpacing, labelWidth, textHeight])
            self.FractionationCloseThreshold = uicontrol('parent', fractionationDetectionPanel,...
                'style', 'edit',...
                'units', 'normalized',...
                'tag', 'close',...
                'string', num2str(self.Analyzer.FractionationThresholds.close),...
                'callback', @self.SetFractionationParameter,...
                'position', [.1 + labelWidth, 1-3*textSpacing, editWidth, textHeight]);
        end
        
        function CreateWavemapSettings(self)
            wavemapPanel = uipanel(...
                'parent', self.SettingsPanel,...
                'title', 'Wavefront reconstruction',...
                'units', 'normalized',...
                'position', [0 .4 1/3 .2]);
            
            textHeight = 0.45;
            textSpacing = 0.45;
            labelWidth = 0.6;
            editWidth = 0.2;
            
            uicontrol('parent', wavemapPanel,...
                'style', 'text',...
                'units', 'normalized',...
                'string', 'Minimal conduction velocity (mm/ms)',...
                'horizontalAlignment', 'left',...
                'position', [.1, 1-textSpacing, labelWidth, textHeight]);
            self.MinimalConductionVelocity = uicontrol('parent', wavemapPanel,...
                'style', 'edit',...
                'units', 'normalized',...
                'tag', 'minimalConductionVelocity',...
                'string', num2str(self.Analyzer.WavemapSettings.minimalConductionVelocity),...
                'callback', @self.SetWavemapParameter,...
                'position', [.1 + labelWidth, 1-textSpacing, editWidth, textHeight]);
            
            uicontrol('parent', wavemapPanel,...
                'style', 'text',...
                'units', 'normalized',...
                'string', 'Merge overlap threshold (%)',...
                'horizontalAlignment', 'left',...
                'position', [.1, 1-2*textSpacing, labelWidth, textHeight])
            self.MergeOverlapThreshold = uicontrol('parent', wavemapPanel,...
                'style', 'edit',...
                'units', 'normalized',...
                'tag', 'mergeOverlapThreshold',...
                'string', num2str(self.Analyzer.WavemapSettings.mergeOverlapThreshold),...
                'callback', @self.SetWavemapParameter,...
                'position', [.1 + labelWidth, 1-2*textSpacing, editWidth, textHeight]);
        end
        
        function CreateWaveComplexitySettings(self)
            waveAnalysisPanel = uipanel(...
                'parent', self.SettingsPanel,...
                'title', 'Wave analysis',...
                'units', 'normalized',...
                'position', [0 .1 1/3 .2]);
            
            textHeight = 0.45;
            textSpacing = 0.45;
            labelWidth = 0.6;
            editWidth = 0.2;
            
            uicontrol('parent', waveAnalysisPanel,...
                'style', 'text',...
                'units', 'normalized',...
                'string', 'Minimum wave size (electrodes)',...
                'horizontalAlignment', 'left',...
                'position', [.1, 1-textSpacing, labelWidth, textHeight]);
            self.MinimumWaveSize = uicontrol('parent', waveAnalysisPanel,...
                'style', 'edit',...
                'units', 'normalized',...
                'tag', 'minimumWaveSize',...
                'string', num2str(self.Analyzer.WaveAnalysisThresholds.minimumWaveSize),...
                'callback', @self.SetWavemapAnalysisParameter,...
                'position', [.1 + labelWidth, 1-textSpacing, editWidth, textHeight]);
            
            uicontrol('parent', waveAnalysisPanel,...
                'style', 'text',...
                'units', 'normalized',...
                'string', 'Velocity range (mm/ms)',...
                'horizontalAlignment', 'left',...
                'position', [.1, 1-2*textSpacing, labelWidth, textHeight]);
            self.VelocityRangeStartEdit = uicontrol('parent', waveAnalysisPanel,...
                'style', 'edit',...
                'units', 'normalized',...
                'string', num2str(self.Analyzer.WaveAnalysisThresholds.velocityRange(1)),...
                'callback', @self.SetVelocityRangeStart,...
                'position', [.1 + labelWidth, 1-2*textSpacing, editWidth / 2, textHeight]);
            self.VelocityRangeEndEdit = uicontrol('parent', waveAnalysisPanel,...
                'style', 'edit',...
                'units', 'normalized',...
                'string', num2str(self.Analyzer.WaveAnalysisThresholds.velocityRange(end)),...
                'callback', @self.SetVelocityRangeEnd,...
                'position', [.1 + labelWidth + editWidth / 2, 1-2*textSpacing, editWidth / 2, textHeight]);
        end
        
        function InitializeResultPanel(self)
            filenameList = {self.Results.shortFilename};
            set(self.TabSelectionDropDown, 'string', filenameList);
        end
        
        function ShowSelectedResult(self, varargin)
            selectedResultIndex = get(self.TabSelectionDropDown, 'value');
            if selectedResultIndex < 1 || selectedResultIndex > numel(self.Results)
                return;
            end
            
            self.ShowWaveInfoTab(self.Results(selectedResultIndex));
            self.ShowPropertyMapTab(self.Results(selectedResultIndex));
            self.ShowConductionMapTab(self.Results(selectedResultIndex));
            self.ShowDissociationTab(self.Results(selectedResultIndex));
        end
        
        function ShowWaveInfoTab(self, result)
            self.WaveInfoPanel.SetData(result.waves);
        end
        
        function ShowPropertyMapTab(self, result)
            self.PropertyMapPanel.SetMapData(result.maps, result.electrodePositions);
        end
        
        function ShowConductionMapTab(self, result)
            self.ConductionMapPanel.SetData(result.conduction, result.electrodePositions);
        end
        
        function ShowDissociationTab(self, result)
            self.DissociationMapPanel.SetData(result.waves, result.electrodePositions);
        end
        
        function SaveResult(self, varargin)
            if isempty(self.Results), return; end
            
            [filename, pathname] = HelperFunctions.customUiputfile('*.mat', 'Save result as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            result = self.Results; %#ok<NASGU>
            analyzer = self.Analyzer; %#ok<NASGU>
            save(filename, 'result', 'analyzer');
        end
        
        function LoadResult(self, varargin)
            [filename, pathname] = HelperFunctions.customUigetfile('*.mat', 'Select batch result file');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            resultData = load(filename);
            if isfield(resultData, 'result')
                self.Results = resultData.result;
                
                set(self.FileListBox, 'string', {self.Results.filename});
                set(self.FileListBox, 'value', 1);
                
                self.InitializeResultPanel();
                
                if isfield(resultData, 'analyzer')
                    self.Analyzer = resultData.analyzer;
                    delete(get(self.SettingsPanel, 'Children'));
                    self.CreateSettingsPanel();
                end
            else
                disp('Invalid batch result file: does not contain result field.');
            end
        end
        
        function ExportResult(self, varargin)
            if isempty(self.Results), return; end
            
            [filename, pathname] = HelperFunctions.customUiputfile('*.csv', 'Export result as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            outputFilename = fullfile(pathname, filename);
            
            % create a dataset and export
            filenames = {self.Results.shortFilename}';
            validFiles = ~cellfun(@isempty, {self.Results.duration});
            
            percentiles = [5, 50, 95];
            
            % wave information          
            waveInfo = vertcat(self.Results.waves);
            numberOfWaves = vertcat(waveInfo.numberOfWaves);
            numberOfBTWaves = vertcat(waveInfo.numberOfBT);
            
            waveSizeMean = cellfun(@mean, {waveInfo.waveSizes}');
            waveSizeSD = cellfun(@std, {waveInfo.waveSizes}');
            waveSizePercentiles = cellfun(@(x) prctile(x, percentiles), {waveInfo.waveSizes}',...
                'uniformOutput', false);
            waveSizePercentiles = vertcat(waveSizePercentiles{:});
            
            velocityMean = cellfun(@mean, {waveInfo.waveVelocity}');
            velocitySD = cellfun(@std, {waveInfo.waveVelocity}');
            velocityPercentiles = cellfun(@(x) prctile(x, percentiles), {waveInfo.waveVelocity}',...
                'uniformOutput', false);
            velocityPercentiles = vertcat(velocityPercentiles{:});
            
            mapInfo = vertcat(self.Results.maps);
            % fractionation
            fractionationMean = cellfun(@mean, {mapInfo.FractionationIndices}');
            fractionationMedian = cellfun(@median, {mapInfo.FractionationIndices}');
            
            % dissociation
            dissociationMean = cellfun(@mean, {mapInfo.MaxDissociation}');
            dissociationMedian = cellfun(@median, {mapInfo.MaxDissociation}');
            
            if isfield(waveInfo, 'ElectrodeDissociation')
                dissociationPercentiles = cellfun(@(x) ...
                    prctile(BatchAnalysisPkg.BatchDissociationPanel.ComputeUniqueDissociation(x), percentiles),...
                    {waveInfo.ElectrodeDissociation}, 'uniformOutput', false);
                dissociationPercentiles = vertcat(dissociationPercentiles{:});
            else
                dissociationPercentiles = NaN(numel(dissociationMean), 3);
            end
            
            % AFCL
            afclMean = cellfun(@mean, {waveInfo.AFCL}');
            afclSD = cellfun(@std, {waveInfo.AFCL}');
            afclPercentiles = cellfun(@(x) prctile(x, percentiles), {waveInfo.AFCL}',...
                'uniformOutput', false);
            afclPercentiles = vertcat(afclPercentiles{:});
            
            % recording length
            recordingLength = vertcat(self.Results.duration);
            
            resultTable = table(recordingLength,...
                numberOfWaves, numberOfBTWaves,....
                waveSizeMean, waveSizeSD,...
                waveSizePercentiles(:, 1), waveSizePercentiles(:, 2), waveSizePercentiles(:, 3),...
                afclMean, afclSD,...
                afclPercentiles(:, 1), afclPercentiles(:, 2), afclPercentiles(:, 3),...
                velocityMean, velocitySD,...
                velocityPercentiles(:, 1), velocityPercentiles(:, 2), velocityPercentiles(:, 3),...
                fractionationMean, fractionationMedian,...
                dissociationMean, dissociationMedian,...
                dissociationPercentiles(:, 1), dissociationPercentiles(:, 2), dissociationPercentiles(:, 3),...
                'VariableNames', {'Recording_Length',...
                'Number_of_Waves', 'Number_of_BT_Waves',...
                'Wave_Size_Mean', 'Wave_Size_SD',...
                'Wave_Size_P05', 'Wave_Size_P50', 'Wave_Size_P95',...
                'AFCL_Mean', 'AFCL_SD',...
                'AFCL_P05', 'AFCL_P50', 'AFCL_P95',...
                'Velocity_Mean', 'Velocity_SD',...
                'Velocity_P05', 'Velocity_P50', 'Velocity_P95',...
                'Fractionation_Index_Mean', 'Fractionation_Index_Median', 'Maximum_Dissociation_Mean', 'Maximum_Dissociation_Median',...
                'Maximum_Dissociation_P05', 'Maximum_Dissociation_P50', 'Maximum_Dissociation_P95'},...
                'RowNames', filenames(validFiles));
            
            writetable(resultTable, outputFilename, 'WriteRowNames', true);
            
%             outputData = dataset({filenames, 'filenames'},...
%                 {recordingLength, 'Recording_Length'},...
%                 {numberOfWaves, 'Number_of_Waves'}, {numberOfBTWaves, 'Number_of_BT_Waves'},...
%                 {waveSizeMean, 'Wave_Size_Mean'}, {waveSizeMedian, 'Wave_Size_Median'},...
%                 {afclMean, 'AFCL_Mean'}, {afclMedian, 'AFCL_Median'},...
%                 {velocityMean, 'Velocity_Mean'}, {velocityMedian, 'Velocity_Median'},...
%                 {fractionationMean, 'Fractionation_Index_Mean'}, {fractionationMedian, 'Fractionation_Index_Median'},...
%                 {dissociationMean, 'Maximum_Dissociation_Mean'}, {dissociationMedian, 'Maximum_Dissociation_Median'},...
%                 {dissociationPercentiles(:, 1), 'Maximum_Dissociation_P05'}, {dissociationPercentiles(:, 2), 'Maximum_Dissociation_P50'}, {dissociationPercentiles(:, 3), 'Maximum_Dissociation_P95'});
%             
%             export(outputData, 'file', outputFilename, 'delimiter', ',');
        end
        
        function SetFractionationParameter(self, source, varargin)
            parameter = get(source, 'Tag');
            self.Analyzer.FractionationThresholds.(parameter) =...
                str2double(get(source, 'string'));
        end
        
        function SetWavemapParameter(self, source, varargin)
            parameter = get(source, 'Tag');
            self.Analyzer.WavemapSettings.(parameter) =...
                str2double(get(source, 'string'));
        end
        
        function SetWavemapAnalysisParameter(self, source, varargin)
            parameter = get(source, 'Tag');
            self.Analyzer.WavemapSettings.(parameter) =...
                str2double(get(source, 'string'));
        end
        
        function SetDFRangeStart(self, source, varargin)
            self.Analyzer.WaveAnalysisThresholds.velocityRange(1) =...
                str2double(get(source, 'string'));
        end
        
        function SetDFRangeEnd(self, source, varargin)
            self.Analyzer.WaveAnalysisThresholds.velocityRange(end) =...
                str2double(get(source, 'string'));
        end
        
        function ClearLog(self, source, varargin)
            set(self.LogText, 'string', '');
        end
    end
end