classdef BatchWaveInfoPanel < UserInterfacePkg.CustomPanel
    properties
        WaveInfo
    end
    
    methods
        function self = BatchWaveInfoPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
        end
        
        function SetData(self, waveData)
            self.WaveInfo = waveData;        
            self.ShowWaveInfo();
        end
    end
    
    methods (Access = private)
        function ShowWaveInfo(self)
            pValues = [5 50 95];
            waveNumbersPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .5 1 .5]);
            axesHandles = NaN(1, 2);
            
            % Wave type distribution
            axesHandles(1) = subplot(1,3,1, 'parent', waveNumbersPanel);
            numberOfWaves = self.WaveInfo.numberOfWaves;
            numberOfBT = self.WaveInfo.numberOfBT;
            numberOfPeripheral = numberOfWaves - numberOfBT;
            
            pie(axesHandles(1), [numberOfPeripheral, numberOfBT], {'Peripheral', 'Breakthrough'});
            title(axesHandles(1), 'Wave types');
            text('units', 'normalized', 'position', [0 0 0], 'parent', axesHandles(1), 'string', ['Total: ', num2str(numberOfWaves),...
                ' , Peripheral: ', num2str(numberOfPeripheral),...
                ' , Breakthrough: ', num2str(numberOfBT)]);
            
            % Wave size distribution
            axesHandles(2) = subplot(1,3,2, 'parent', waveNumbersPanel);
            sizeBins = 1:5:max(self.WaveInfo.waveSizes);
            BatchAnalysisPkg.BatchWaveInfoPanel.CreateHistogram(axesHandles(2), self.WaveInfo.waveSizes, sizeBins, pValues);
            title(axesHandles(2), 'Wave Size');
            xlabel(axesHandles(2), 'size (number of electrodes)');
            
            waveVelocityPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 0 1 .5]);
            axesHandles = NaN(1, 3);
            
            % Wave based AFCL distribution
            axesHandles(1) = subplot(1,3,1, 'parent', waveVelocityPanel);
            afclBins = 0:2:300;
            BatchAnalysisPkg.BatchWaveInfoPanel.CreateHistogram(axesHandles(1), self.WaveInfo.AFCL, afclBins, pValues);
            title(axesHandles(1), 'Cycle length');
            xlabel(axesHandles(1), 'interval length (ms)');
            
            % Wave based velocity distribution
            axesHandles(2) = subplot(1,3,2, 'parent', waveVelocityPanel);
            velocityBins = 0:2:150;
            BatchAnalysisPkg.BatchWaveInfoPanel.CreateHistogram(axesHandles(2), 100 * self.WaveInfo.waveVelocity, velocityBins, pValues);
            title(axesHandles(2), 'Wave velocity');
            xlabel(axesHandles(2), 'velocity (cm/s)');
            
            % Wave based tortuosity distribution
            axesHandles(3) = subplot(1,3,3, 'parent', waveVelocityPanel);
            tortuosityBins = -90:5:90;
            BatchAnalysisPkg.BatchWaveInfoPanel.CreateHistogram(axesHandles(3), self.WaveInfo.waveTortuosity, tortuosityBins, pValues);
            title(axesHandles(3), 'Wave tortuosity');
            xlabel(axesHandles(3), 'tortuosity (degrees)');
        end
    end
    
    methods (Static)
        function [binCounts, binPositions] = CreateHistogram(plotHandle, data, histogramBins, pValues)
            data = data(~isnan(data));
            
            [binCounts, binPositions] = hist(data , histogramBins);
            binCounts(end) = 0;
            bar(plotHandle, binPositions, binCounts,...
                'barLayout', 'grouped',...
                'barWidth', 1,...
                'edgeColor', [0 0 0],...
                'faceColor', [0 0 0]);
            
            if nargin > 3
                pIndexes = prctile(data, pValues);
                yLimits = get(plotHandle, 'yLim');
                
                for i = 1:numel(pIndexes)
                    if pIndexes(i) < binPositions(1) || pIndexes(i) > binPositions(end)
                        continue;
                    end
                    line('xData', [pIndexes(i) pIndexes(i)], 'yData', yLimits,...
                        'parent', plotHandle, 'color', [1 0 0 ]);
                    text('position', [pIndexes(i), yLimits(end)], ...
                        'string', ['p' num2str(pValues(i))],...
                        'parent', plotHandle,...
                        'color', [1 0 0]);
                end
            end
            set(plotHandle, 'xLim', histogramBins([1 end]));
            
            clipboardMenu = uicontextmenu;
            set(plotHandle, 'UIContextMenu', clipboardMenu);
            uimenu(clipboardMenu, 'Label', 'Copy data to clipboard', 'callback', @(self, source) UtilityPkg.num2clip(data(:)));
        end
    end
end