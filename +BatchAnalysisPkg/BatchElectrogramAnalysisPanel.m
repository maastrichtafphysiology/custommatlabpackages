classdef BatchElectrogramAnalysisPanel < BatchAnalysisPkg.BatchAnalysisPanel
    properties
        SettingsPanel
        SettingsText
        ReferenceFileText
        PaceRhythmCheckbox

        ResultsPanel
        ResultText
        
        Analyzer
    end
    
    methods
        function self = BatchElectrogramAnalysisPanel(position)
            self = self@BatchAnalysisPkg.BatchAnalysisPanel(position);
            
            self.Analyzer = BatchAnalysisPkg.BatchElectrogramAnalyzer();
        end
        
        function Create(self, parentHandle)
            Create@BatchAnalysisPkg.BatchAnalysisPanel(self, parentHandle);
            
            self.SettingsPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 0 .5 .8],...
                'title', 'Settings');
            self.ResultsPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.5 0 .5 .8],...
                'title', 'Results');
            
            self.CreateSettingsPanel();
            self.CreateResultPanel();
        end
    end
    
    methods (Access = protected)
        function AddFilesToFileListBox(self, varargin)
            [filenames, pathname] = uigetfile( ...
                {'*.DATA', 'IDEEQ Data (*.DATA)';...
                '*.E**', 'PACEMAP Data (*.E**)'}, ...
                'Select signal data files', ...
                'MultiSelect', 'on');
            if isnumeric(filenames), return; end
            
            if ischar( filenames)
                filenames = cellstr(filenames);
            end
            
            fileList = get(self.FileListBox, 'string');
            
            for i = 1:length(filenames)
                fullPath = [pathname filenames{i}];
                if all(~strcmp(fullPath, fileList))
                    fileList = [fileList; cellstr(fullPath)]; %#ok<*AGROW>
                end
            end
            
            set(self.FileListBox, 'string', fileList);
            set(self.FileListBox, 'value', numel(fileList));
        end
        
        function Compute(self, varargin)
            filenameList = get(self.FileListBox, 'string');
            
            if isempty(filenameList), return; end
            
            set(self.ResultText, 'string', 'Starting batch analysis');
            self.Analyzer.ResultTextHandle = self.ResultText;
            self.Analyzer.Compute(filenameList);
            
            resultText = get(self.ResultText, 'string');
            set(self.ResultText, 'string', vertcat(resultText, {'Done.'}));
        end
        
        function CreateSettingsPanel(self)
            uicontrol('parent', self.SettingsPanel,...
                'style', 'pushbutton',...
                'units', 'normalized',...
                'string', 'Select reference analysis',...
                'callback', @self.SelectReferenceAnalysis,...
                'position', [0 .9 .5 .1]);
            
            self.ReferenceFileText = uicontrol('parent', self.SettingsPanel,...
                'style', 'text',...
                'units', 'normalized',...
                'min', 1, 'max', 10,...
                'string', '',...
                'position', [.5, .95, .5, .05]);
            
            self.PaceRhythmCheckbox = uicontrol('parent', self.SettingsPanel,...
                'style', 'checkbox',...
                'units', 'normalized',...
                'string', 'Use pacing rhythm',...
                'value', self.Analyzer.BatchPaceBlanking.useAsDfPrior,...
                'callback', @self.SetPaceRhythmEnabled,...
                'position', [.5, .9, .5, .05],...
                'enable', 'off');
            
            self.SettingsText = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [0 0 1 .9],...
                'style', 'edit',...                
                'enable', 'inactive',...
                'min', 1, 'max', 10,...
                'string', '',...
                'backgroundColor', [1,1,1],...
                'horizontalAlignment', 'left');
        end
        
        function CreateResultPanel(self)          
            self.ResultText = uicontrol('parent', self.ResultsPanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 1],...
                'style', 'edit',...                
                'enable', 'inactive',...         
                'min', 1, 'max', 10,...
                'string', '',...
                'backgroundColor', [1,1,1],...
                'horizontalAlignment', 'left');
        end
    end
    
    methods (Access = private)
        function SelectReferenceAnalysis(self, varargin)
            [filename, pathname] = uigetfile( ...
                {'*.DATA', 'IDEEQ file (*.DATA)';...
                '*.E**', 'PACEMAP file (*.E**)'}, ...
                'Select reference analysis data file (_PP, _QRST, _TM available)');
            if isnumeric(filename), return; end
            
            [~ ,nameString, ~] = fileparts(filename);
            % find _PaceDetection file
            paceDetectionFilename = [nameString, '_PaceDetection.mat'];
            try
                paceDetectionData = load(fullfile(pathname, paceDetectionFilename));
                self.Analyzer.BatchPaceBlanking.enabled = true;
                self.Analyzer.BatchPaceBlanking.paceInterval = paceDetectionData.paceInterval;
                if isfield(paceDetectionData, 'minimalPaceAmplitude')
                    self.Analyzer.BatchPaceBlanking.minimalPaceAmplitude = paceDetectionData.minimalPaceAmplitude;
                else
                    self.Analyzer.BatchPaceBlanking.minimalPaceAmplitude = 0.75;
                end
                self.Analyzer.BatchPaceBlanking.blankingInterval = paceDetectionData.blankingInterval;
                self.Analyzer.BatchPaceBlanking.selectedPaceChannel =...
                    paceDetectionData.ecgData.ReferenceLabels{paceDetectionData.selectedPaceChannel};
            catch ME
                disp('No pace detection found, proceeding without pace blanking');
            end
            
            % find _PP.mat file
            preprocessingFilename = [nameString, '_PP.mat'];
            try
                ppData = load(fullfile(pathname, preprocessingFilename));
            catch ME
                disp(ME);
                disp(['Preprocessing file: ' preprocessingFilename, ' not found.']);
                return;
            end
            
            self.Analyzer.ElectrodeMapName = ppData.ecgData.ElectrodeMap.aliases{1};
            self.Analyzer.ElectrodePositions = ppData.ecgData.ElectrodePositions;
            self.Analyzer.BatchChannelSelection.channelsToAnalyze = ppData.ecgData.ElectrodeLabels;
            self.Analyzer.BatchChannelSelection.referenceChannel = ppData.ecgData.ReferenceLabels;
            
            self.Analyzer.BatchSignalPreProcessor = ppData.preProcessor;
            
            % find _QRST.mat file
            cancellationFilename = [nameString, '_QRST.mat'];
            try
                qrstData = load(fullfile(pathname, cancellationFilename));
            catch ME
                disp(ME);
                disp(['QRST cancellation file: ' cancellationFilename, ' not found.']);
                return;
            end
            
            self.Analyzer.BatchQRSTCancellator = qrstData.qrstCancellator;
            self.Analyzer.BatchQRSTCancellator.ShowDiagnostics = false;
            self.Analyzer.BatchChannelSelection.referenceChannel =...
                self.Analyzer.BatchChannelSelection.referenceChannel{qrstData.referenceChannel};
            
            % Template matching
            templateFilename = [nameString, '_TM.mat'];
            try
                templateData = load(fullfile(pathname, templateFilename));
            catch ME
                disp(ME);
                disp(['Template matching file: ' templateFilename, ' not found.']);
                return;
            end
            
            self.Analyzer.BatchDeflectionDetector = templateData.deflectionDetector;
            exampleTemplateMatchingResult = templateData.deflectionDetectionResults.AnalysisResults{1};
            self.Analyzer.DeflectionTemplates = exampleTemplateMatchingResult.templatePrototypes;
            
            set(self.ReferenceFileText, 'string', {'Selected reference analysis:'; filename});
            
            self.ShowSettings();
        end
        
        function ShowSettings(self)
            % Electrodemap name
            electrodemapNameText = {...
                ['- Electrode map name: ', self.Analyzer.ElectrodeMapName]};
            set(self.SettingsText, 'string',electrodemapNameText);
            
            % Pace blanking
            set(self.PaceRhythmCheckbox, 'value', self.Analyzer.BatchPaceBlanking.useAsDfPrior);
            if self.Analyzer.BatchPaceBlanking.enabled
                set(self.PaceRhythmCheckbox, 'enable', 'on');
                paceBlankingText = {'';...
                    '- Pace blanking settings:';...
                    ['  Channel: ' self.Analyzer.BatchPaceBlanking.selectedPaceChannel];...
                    ['  Pace interval: ', num2str(1000 * self.Analyzer.BatchPaceBlanking.paceInterval), 'ms'];...
                    ['  Blanking interval: ', num2str(1000 * self.Analyzer.BatchPaceBlanking.blankingInterval), 'ms']};
                settingText = get(self.SettingsText, 'string');
                set(self.SettingsText, 'string', vertcat(settingText, paceBlankingText));
            end
            
            % Pre-processing
            preprocessingText = {'';...
                '- Preprocessing settings:';...
                ['  Filter: ', num2str(self.Analyzer.BatchSignalPreProcessor.BandpassFrequencies, '%.0f-%.0f'), 'Hz']};
            settingText = get(self.SettingsText, 'string');
            set(self.SettingsText, 'string', vertcat(settingText, preprocessingText));
            
            % QRST cancellation
            qrstCancellationText = {'';...
                '- QRST cancellation settings:';...
                ['  Reference channel: ', self.Analyzer.BatchChannelSelection.referenceChannel];...
                ['  Method: ', self.Analyzer.BatchQRSTCancellator.CancellationMethod];...
                ['  QRST-range: ', num2str(100 * self.Analyzer.BatchQRSTCancellator.QRSTRange, '%.0f-%.0f'), '%'];...
                ['  Ventricular beat interval: ', num2str(self.Analyzer.BatchQRSTCancellator.VentricularRefractoryPeriod), ' seconds']};
            
            settingText = get(self.SettingsText, 'string');
            set(self.SettingsText, 'string', vertcat(settingText, qrstCancellationText));
            
            % Template matching
             templateMatchingText = {'';...
                '- Template matching settings:';...
                ['  Template name: ', [self.Analyzer.DeflectionTemplates.Name]];...
                ['  Template duration: ', num2str(self.Analyzer.BatchDeflectionDetector.TemplateDurations([1, end]), '%.0f-%.0f'), 'ms']};
                
            settingText = get(self.SettingsText, 'string');
            set(self.SettingsText, 'string', vertcat(settingText, templateMatchingText));
        end
        
        function SetPaceRhythmEnabled(self, source, varargin)
            if get(source, 'value')
                self.Analyzer.BatchPaceBlanking.useAsDfPrior = true;
            else
                self.Analyzer.BatchPaceBlanking.useAsDfPrior = false;
            end
        end
    end
    
end