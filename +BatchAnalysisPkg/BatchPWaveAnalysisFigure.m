classdef BatchPWaveAnalysisFigure < UserInterfacePkg.CustomFigure
    properties
        ParallelProcessing
        PWaveAnalysisView
    end
    
    methods
        function self = BatchPWaveAnalysisFigure(position)
            if nargin == 0
                screenSize = get(0,'Screensize');
                position = [screenSize(3) / 5, screenSize(4) / 5, 3 * screenSize(3) / 5, 3 * screenSize(4) / 5];
            end
            
            self = self@UserInterfacePkg.CustomFigure(position);
            self.Name = 'Batch P-Wave Analysis';
            self.ParallelProcessing = false;
            
            if nargin ==0
                self.Create();
            end
        end
        
        function Create(self)
            Create@UserInterfacePkg.CustomFigure(self);
            
            self.CreateSettingsMenu();
            
            self.PWaveAnalysisView = BatchAnalysisPkg.BatchPWaveAnalysisPanel([0 .025 1 .975]);
            self.PWaveAnalysisView.Create(self.ControlHandle);
            self.PWaveAnalysisView.Show();
            
            self.Show();
        end
    end
    
    methods (Access = private)
        function CreateSettingsMenu(self)
            settingsMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'Settings');
            uimenu(...
                'parent', settingsMenu,...
                'label', 'Parallel processing',...
                'callback', @self.SetParallelProcessing);
        end
        
        function SetParallelProcessing(self, source, varargin)
            if self.ParallelProcessing
                self.ParallelProcessing = false;
                set(source, 'checked', 'off');
                
                if verLessThan('matlab', '8.5.1')
                    if matlabpool('size') > 0
                        matlabpool('close');
                    end
                else
                    delete(gcp('noCreate'))
                end
            else
                self.ParallelProcessing = true;
                set(source, 'checked', 'on');
                if verLessThan('matlab', '8.5.1')
                    if matlabpool('size') == 0
                        matlabpool('open');
                    end
                else
                    gcp;
                end
            end
        end
    end
end