classdef BatchConductionMapPanel < UserInterfacePkg.CustomPanel
    properties
        PreferentialityMap
        PreferentialityUnidirectionalMap
        AnisotropyMap
    end
    
    methods
        function self = BatchConductionMapPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.PreferentialityMap = ExtendedUIPkg.ConductionMapControl([0 0 1/3 1]);
            self.PreferentialityMap.Create(self.ControlHandle);
            self.PreferentialityMap.Name = 'Preferentiality (bidirectional)';
            self.PreferentialityMap.Show();
            
            self.PreferentialityUnidirectionalMap = ExtendedUIPkg.UnidirectionalConductionMapControl([1/3 0 1/3 1]);
            self.PreferentialityUnidirectionalMap.Create(self.ControlHandle);
            self.PreferentialityUnidirectionalMap.Name = 'Preferentiality (unidirectional)';
            self.PreferentialityUnidirectionalMap.Show();
            
            self.AnisotropyMap = ExtendedUIPkg.ConductionMapControl([2/3 0 1/3 1]);
            self.AnisotropyMap.Create(self.ControlHandle);
            self.AnisotropyMap.Name = 'Anisotropy';
            self.AnisotropyMap.Show();
        end
        
        function SetData(self, conductionData, electrodePositions)
            self.PreferentialityMap.SetData(conductionData.Preferentiality, electrodePositions);
            self.AnisotropyMap.SetData(conductionData.Anisotropy, electrodePositions);
            
            if isfield(conductionData, 'UnidirectionalPreferentiality')
                self.PreferentialityUnidirectionalMap.SetData(conductionData.UnidirectionalPreferentiality, electrodePositions);
            end
        end
    end
end