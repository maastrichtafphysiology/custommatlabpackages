classdef BatchDissociationPanel < UserInterfacePkg.CustomPanel
    properties
        DissociationMap
        ElectrodePositions
        DissociationData
        
        NeighborDissociationPanel
        NeighborAxes
    end
    
    methods
        function self = BatchDissociationPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.DissociationMap = ExtendedUIPkg.PropertyMapControl([0 .5 .5 .5]);
            self.DissociationMap.Create(self.ControlHandle);
            addlistener(self.DissociationMap, 'MapClicked', @self.HandleMapClick);
            
            self.NeighborDissociationPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.5 .5 .5 .5]);
        end
        
        function SetData(self, dissociationData, electrodePositions)
            if ~isfield(dissociationData, 'ElectrodeDissociation'), return; end
                
            self.DissociationData = dissociationData;
            self.ElectrodePositions = electrodePositions;
            
            self.DissociationMap.SetData(dissociationData.MaxDissociation, electrodePositions);
            self.DissociationMap.Show();
            
            self.ShowTotalDissociation([5, 50, 95]);
        end
    end
    
    methods
        function ShowElectrodeDissociation(self, electrodeIndex)
            validHandles = ishandle(self.NeighborAxes);
            if any(validHandles(:))
                delete(self.NeighborAxes(validHandles));
            end
            
            electrodeDissociationData = self.DissociationData.ElectrodeDissociation(electrodeIndex, :);
            electrodeNeighbors = ~cellfun(@isempty, electrodeDissociationData);
            if ~any(electrodeNeighbors), return; end
            neigborPositions = self.ElectrodePositions(electrodeNeighbors, :);
            neighborIndices = find(electrodeNeighbors);
            numberOfNeighbors = numel(find(electrodeNeighbors));
            maxPosition = max(neigborPositions, [], 1);
            minPosition = min(neigborPositions, [], 1);
            
            distanceMatrix = pdist(self.ElectrodePositions);
            distanceMatrix = distanceMatrix(distanceMatrix ~= 0);
            minimumSpacing = min(distanceMatrix);
            
            xData = minPosition(1):minimumSpacing:maxPosition(1);
            yData = minPosition(2):minimumSpacing:maxPosition(2);
            axesSpacing = .025;
            axesWidth = 1 / numel(xData);
            axesHeight = 1 / numel(yData);
            
            tolerance = 1e3 * eps;
            [~, xIndices] = UtilityPkg.ismemberf(neigborPositions(:, 1), xData, 'tol', tolerance);
            [~, yIndices] = UtilityPkg.ismemberf(neigborPositions(:, 2), yData, 'tol', tolerance);
            
            self.NeighborAxes = NaN(numberOfNeighbors, 1);
            minValue = min(cellfun(@(x) min(x(:, 1)), electrodeDissociationData(electrodeNeighbors)));
            maxValue = max(cellfun(@(x) max(x(:, 1)), electrodeDissociationData(electrodeNeighbors)));
            bins = minValue:5:maxValue;
            for neighborIndex = 1:numberOfNeighbors
                self.NeighborAxes(neighborIndex) =...
                        axes('parent', self.NeighborDissociationPanel,...
                        'units', 'normalized',...
                        'position', [...
                        (xIndices(neighborIndex) - 1) * axesWidth,...
                        (numel(yData) - yIndices(neighborIndex)) * axesHeight + axesSpacing,...
                        axesWidth - axesSpacing, axesHeight - axesSpacing],...
                        'color', [0 0 0]); %#ok<LAXES>
                    
                 neigborData = electrodeDissociationData{neighborIndices(neighborIndex)};
                [binCounts, binPositions] = hist(self.NeighborAxes(neighborIndex), neigborData(:, 1), bins);
                bar(self.NeighborAxes(neighborIndex), binPositions, binCounts,...
                    'barLayout', 'grouped',...
                    'barWidth', 1,...
                    'edgeColor', [0 0 0],...
                    'faceColor', [0 0 0]);
 
                 set(self.NeighborAxes(neighborIndex), 'yTickLabel', {});
            end
        end
        
        function ShowTotalDissociation(self, pValues)
            totalDissociationPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 0 1 .5]);
            
            plotHandle = subplot(1,1,1, 'parent', totalDissociationPanel);
            
            uniqueDissociation = BatchAnalysisPkg.BatchDissociationPanel.ComputeUniqueDissociation(self.DissociationData.ElectrodeDissociation);
            
            histogramBins = min(uniqueDissociation):max(uniqueDissociation);
            [binCounts, binPositions] = hist(uniqueDissociation , histogramBins);
            binCounts(end) = 0;
            bar(plotHandle, binPositions, binCounts,...
                'barLayout', 'grouped',...
                'barWidth', 1,...
                'edgeColor', [0 0 0],...
                'faceColor', [0 0 0]);
            
            if nargin > 1
                pIndexes = prctile(uniqueDissociation, pValues);
                yLimits = get(plotHandle, 'yLim');
                textYPosition = 0.9 * (yLimits(end) - yLimits(1));
                
                for i = 1:numel(pIndexes)
                    if pIndexes(i) < binPositions(1) || pIndexes(i) > binPositions(end)
                        continue;
                    end
                    line('xData', [pIndexes(i) pIndexes(i)], 'yData', yLimits,...
                        'parent', plotHandle, 'color', [1 0 0 ]);
                    text('position', [pIndexes(i), textYPosition], ...
                        'string', [' p', num2str(pValues(i)), ' = ', num2str(pIndexes(i))],...
                        'parent', plotHandle,...
                        'color', [1 0 0]);
                end
            end
            set(plotHandle, 'xLim', histogramBins([1, end]));
            
            clipboardMenu = uicontextmenu;
            set(plotHandle, 'UIContextMenu', clipboardMenu);
            uimenu(clipboardMenu, 'Label', 'Copy data to clipboard', 'callback', @(self, source) UtilityPkg.num2clip(uniqueDissociation(:)));
        end
        
        function HandleMapClick(self, ~, eventData)
            selectedChannelIndex = eventData.Position;
            self.ShowElectrodeDissociation(selectedChannelIndex);
        end
    end
    
    methods (Static)
        function uniqueDissociation = ComputeUniqueDissociation(electrodeDissociation)
            uniqueDissociation = cell(size(electrodeDissociation, 1), 1);
            for electrodeIndex = 1:size(electrodeDissociation, 1)
                neighbors = find(~cellfun(@isempty, electrodeDissociation(electrodeIndex, :)));
                neighbors = neighbors(neighbors > electrodeIndex);
                numberOfNeighbors = numel(neighbors);
                uniqueElectrodeDissociation = cell(numberOfNeighbors, 1);
                for neigborIndex = 1:numberOfNeighbors
                    electrodeData = electrodeDissociation{electrodeIndex, neighbors(neigborIndex)};
                    neighborData = electrodeDissociation{neighbors(neigborIndex), electrodeIndex};
                    
                    combinedData = [[electrodeData, (1:size(electrodeData, 1))'];...
                        [-neighborData(:, 1), (1:size(neighborData, 1))', neighborData(:, 2)]];
                    combinedData = unique(combinedData, 'rows');
                    uniqueElectrodeDissociation{neigborIndex} = combinedData(:, 1);
                end
                uniqueDissociation{electrodeIndex} = vertcat(uniqueElectrodeDissociation{:});
            end
            uniqueDissociation = vertcat(uniqueDissociation{:});
        end
    end
end