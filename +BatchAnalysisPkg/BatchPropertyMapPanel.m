classdef BatchPropertyMapPanel < UserInterfacePkg.CustomPanel
    properties
        ControlPanel
        ElectrodePositions
        PropertyData
        PropertyRanges
        PropertyMap
        PropertyDropDown
        PropertyIndex
        RangeStartEdit
        RangeEndEdit
    end
    
    methods
        function self = BatchPropertyMapPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            
            self.PropertyRanges = [0 10];
            self.PropertyIndex = NaN;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            self.CreatePropertyMapPanel();
        end
        
        function SetMapData(self, mapData, electrodePositions)
            self.PropertyData = mapData;
            self.ElectrodePositions = electrodePositions;
            propertyNames = fieldnames(mapData);
            set(self.PropertyDropDown, 'string', propertyNames);
            if size(self.PropertyRanges, 1) ~= numel(propertyNames)
                self.PropertyRanges = [-Inf(numel(propertyNames), 1),...
                    Inf(numel(propertyNames), 1)];
                set(self.PropertyDropDown, 'value', 1);
            end
            
            self.SetProperty(self.PropertyDropDown);
            self.PropertyMap.Show();
        end
    end
    
    methods (Access = private)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .9 1 .1]);
            
            settingsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 .2 1 .8],...
                'title', 'Settings');
            
            self.PropertyDropDown = uicontrol('parent', settingsPanel,...
                'units', 'normalized',...
                'position', [0 0 .3 1],...
                'style', 'popupmenu',...
                'string', 'No maps',...
                'value', 1,...
                'callback', @self.SetProperty);
            
            propertyRangePanel = uipanel('parent', settingsPanel,...
                'units', 'normalized',...
                'position', [.3 0 .3 1],...
                'title', 'Property range');
            self.RangeStartEdit = uicontrol('parent', propertyRangePanel,...
                'units', 'normalized',...
                'position', [0 0 .5 1],...
                'style', 'edit',...
                'backgroundColor', [1 1 1],...
                'callback', @self.SetPropertyRangeStart);
            self.RangeEndEdit = uicontrol('parent', propertyRangePanel,...
                'units', 'normalized',...
                'position', [.5 0 .5 1],...
                'style', 'edit',...
                'backgroundColor', [1 1 1],...
                'callback', @self.SetPropertyRangeEnd);
        end
        
        function CreatePropertyMapPanel(self)
            self.PropertyMap = ExtendedUIPkg.PropertyMapControl([0 0 1 .9]);
            self.PropertyMap.Create(self.ControlHandle);
        end
        
        function SetProperty(self, source, varargin)
            propertyNames = get(source, 'string');
            self.PropertyIndex = get(source, 'value');
            propertyFieldName = propertyNames{self.PropertyIndex};
            propertyData = self.PropertyData.(propertyFieldName);
            self.PropertyMap.SetFramePosition(1);
            range = self.PropertyRanges(self.PropertyIndex, :);
            set(self.RangeStartEdit, 'string', num2str(range(1)));
            set(self.RangeEndEdit, 'string', num2str(range(2)));
            if any(isinf(abs(range)))
                range = [];
            end
            self.PropertyMap.SetDataRange(range);
            self.PropertyMap.SetData(propertyData, self.ElectrodePositions);
        end
        
        function SetPropertyRangeStart(self, source, varargin)
            if isnan(self.PropertyIndex), return; end
            self.PropertyRanges(self.PropertyIndex, 1) = str2double(get(source, 'string'));
            self.PropertyMap.SetDataRange(self.PropertyRanges(self.PropertyIndex, :));
        end
        
        function SetPropertyRangeEnd(self, source, varargin)
            if isnan(self.PropertyIndex), return; end
            self.PropertyRanges(self.PropertyIndex, 2) = str2double(get(source, 'string'));
            self.PropertyMap.SetDataRange(self.PropertyRanges(self.PropertyIndex, :));
        end
    end
end

