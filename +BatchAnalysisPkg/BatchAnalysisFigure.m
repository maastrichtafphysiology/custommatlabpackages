classdef BatchAnalysisFigure < UserInterfacePkg.CustomFigure
    properties
        AnalysisStatusBar
        ParallelProcessing
        
        TabGroup
        WavemapAnalysisTab
        WavemapAnalysisPanel
        ElectrogramAnalysisTab
        ElectrogramAnalysisPanel
    end
    
    methods
        function self = BatchAnalysisFigure(position)
            if nargin == 0
                screenSize = get(0,'Screensize');
                position = [screenSize(3) / 5, screenSize(4) / 5, 3 * screenSize(3) / 5, 3 * screenSize(4) / 5];
            end
            
            self = self@UserInterfacePkg.CustomFigure(position);
            self.Name = 'Batch AF Analysis';
            self.ParallelProcessing = false;
            
            if nargin ==0
                self.Create();
            end
        end
        
        function Create(self)
            Create@UserInterfacePkg.CustomFigure(self);
            
            self.CreateSettingsMenu();
            
            self.TabGroup = uitabgroup(...
                'parent', self.ControlHandle,...
                'position', [0 .025 1 .975]);
            
            self.WavemapAnalysisTab = uitab(self.TabGroup, 'title', 'Wavemap Analysis');
            self.WavemapAnalysisPanel = BatchAnalysisPkg.BatchWavemapAnalysisPanel([0 0 1 1]);
            self.WavemapAnalysisPanel.Create(self.WavemapAnalysisTab);
            self.WavemapAnalysisPanel.Show();
            
            self.ElectrogramAnalysisTab = uitab(self.TabGroup, 'title', 'Electrogram analysis');
            self.ElectrogramAnalysisPanel = BatchAnalysisPkg.BatchElectrogramAnalysisPanel([0 0 1 1]);
            self.ElectrogramAnalysisPanel.Create(self.ElectrogramAnalysisTab);
            self.ElectrogramAnalysisPanel.Show();
            
            self.Show();
        end
    end
    
    methods (Access = private)
        function CreateSettingsMenu(self)
            settingsMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'Settings');
            %             uimenu(...
            %                 'parent', settingsMenu,...
            %                 'label', 'Algorithm settings',...
            %                 'callback', @self.ShowApplicationSettings);
            uimenu(...
                'parent', settingsMenu,...
                'label', 'Parallel processing',...
                'callback', @self.SetParallelProcessing);
        end
        
        function SetParallelProcessing(self, source, varargin)
            if self.ParallelProcessing
                self.ParallelProcessing = false;
                set(source, 'checked', 'off');
                if matlabpool('size') > 0
                    matlabpool('close');
                end
            else
                self.ParallelProcessing = true;
                set(source, 'checked', 'on');
                if matlabpool('size') == 0
                    matlabpool('open');
                end
            end
        end
    end
end