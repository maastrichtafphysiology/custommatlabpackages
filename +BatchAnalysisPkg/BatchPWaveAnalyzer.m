classdef BatchPWaveAnalyzer < BatchAnalysisPkg.BatchAnalyzer
    properties
        PeakAmplitudeThreshold
        RecomputeOnsetOffSet
        UseRMSOnsetOffset
        RandomInterval
    end
    
    methods
        function self = BatchPWaveAnalyzer()
            self = self@BatchAnalysisPkg.BatchAnalyzer;
            self.PeakAmplitudeThreshold = 0.1;
            self.UseRMSOnsetOffset = false;
            self.RecomputeOnsetOffSet = false;
            self.RandomInterval=[];
        end
        
        function result = Compute(self, dataList)
            % dataList: a list of P-Wave filenames
            filenameList = dataList;
            numberOfFiles = numel(filenameList);
            
            self.UpdateTextProgress({' '});
            self.UpdateTextProgress({['%%%% ', datestr(datetime('now')), ' %%%%']});
            self.UpdateTextProgress({'Computing P-wave parameters...'});
            self.UpdateTextProgress({'File:...'});
            replacePreviousText = true;
            
            fileChannelLabels = cell(numberOfFiles, 1);
            
            result = cell(numberOfFiles, 1);
            filePWaveData = cell(numberOfFiles, 1);
            shortFilenames = cell(numberOfFiles, 1);
            for fileIndex = 1:numberOfFiles
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                self.UpdateTextProgress({['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']},...
                    replacePreviousText);
                shortFilenames{fileIndex} = shortFilename;
                
                fileData = load(filename);
                if ~isfield(fileData, 'XYZIndices')
                    fileData.XYZIndices = NaN(3, 2);
                end
                
                % check if the signal unit is (most likely) mV
                medianFileVoltage = median(mean(abs(fileData.baselineCorrectedData)));
                correctionFactor = NaN;
                if medianFileVoltage > 10
                    % probably stored in muV
                    correctionFactor = 1e-3;
                end
                if medianFileVoltage < 0.001
                    % probably stored in V
                    correctionFactor = 1e3;
                end
                
                if ~isnan(correctionFactor)
                    fileData.ecgData.Data = fileData.ecgData.Data * correctionFactor;
                    fileData.baselineCorrectedData = fileData.baselineCorrectedData * correctionFactor;
                end
                

                
                if strfind(filename, '_PWaveMatching')
                    % compute average p-waves
                    if ~isempty(self.RandomInterval)
                        % self.RandomInterval = interval duration in sec
                        numberOfSamplesInterval = self.RandomInterval * fileData.ecgData.SamplingFrequency;
                        randomInterval = randi([fileData.rWaveIndices(1) fileData.rWaveIndices(end)-numberOfSamplesInterval]);
                        % delete all rWaveIndices outside randomInterval
                        fileData.rWaveIndices(fileData.rWaveIndices<randomInterval | fileData.rWaveIndices>randomInterval+numberOfSamplesInterval)=[];

                        % PWAVE MATCHER TOEVOEGEN
                        pWaveMatcher = fileData.pWaveMatching;
                        [fileData.pWaveIntervals, fileData.baselineCorrectedData, fileData.baselineCorrectedReferenceData] =...
                            pWaveMatcher.MatchPWaves(fileData.ecgData, fileData.rWaveIndices, fileData.validChannels);
                    else 
                        pWaveMatcher = fileData.pWaveMatching;
                    end
                   
                    if self.RecomputeOnsetOffSet
                        onsetOffset = pWaveMatcher.DetectPStartEnd(fileData.ecgData, fileData.rWaveIndices, fileData.validChannels, fileData.pWaveIntervals);
%                         if all(fileData.onsetOffset(1:2) == onsetOffset)
%                             disp('Equal')
%                         else
%                             disp('Different')
%                         end
                        
                        fileData.onsetOffset(1:2) = onsetOffset;
                        
                        fileData.electrodeOnsetOffset = repmat(onsetOffset,size(fileData.electrodeOnsetOffset,1),1);
                        
%                         % recompute average p-wave energy on- and offset
%                         extendedIntervals = fileData.pWaveIntervals;
%                         pWaveEndIndex = round(pWaveMatcher.PWaveRange(2) * fileData.ecgData.SamplingFrequency);
%                         if pWaveMatcher.PWaveRange(2) < 0
%                             extendedIntervals(:, 2) = fileData.pWaveIntervals(:, 2) - pWaveEndIndex;
%                         end
%                         [intervalAverage, intervalTime] = pWaveMatcher.ComputeUnfilteredPWaveAverageEnergy(...
%                             fileData.ecgData, fileData.baselineCorrectedData, extendedIntervals);
%                         [pWaveStartIndex, pWaveEndIndex, qOnset] = pWaveMatcher.ComputeOnsetOffsetDuration(...
%                             fileData.ecgData, intervalAverage, intervalTime);
%                         onsetOffset = NaN(1, 3);
%                         if ~isnan(pWaveStartIndex)
%                             onsetOffset(1) = intervalTime(pWaveStartIndex);
%                         end
%                         if ~isnan(pWaveEndIndex)
%                             onsetOffset(2) = intervalTime(pWaveEndIndex);
%                         end
%                         if ~isnan(qOnset)
%                             onsetOffset(3) = intervalTime(qOnset);
%                         end
%                         fileData.onsetOffset = onsetOffset;
%                         
%                         % recompute electrode average p-wave on- and offset
%                         numberOfElectrodes = fileData.ecgData.GetNumberOfChannels();
%                         electrodeOnsetOffset = NaN(numberOfElectrodes, 3);
%                         for electrodeIndex = 1:numberOfElectrodes
%                             channelData = fileData.baselineCorrectedData(:, electrodeIndex);
%                             [intervalAverage, ~, intervalTime] =...
%                                 pWaveMatcher.ComputeUnfilteredPWaveAverage(fileData.ecgData, channelData, extendedIntervals);
%                             
%                             [pWaveStartIndex, pWaveEndIndex, qOnset] =...
%                                 pWaveMatcher.ComputeOnsetOffsetDuration(fileData.ecgData, intervalAverage, intervalTime);
%                             
%                             if ~isnan(pWaveStartIndex)
%                                 electrodeOnsetOffset(electrodeIndex, 1) = intervalTime(pWaveStartIndex);
%                             end
%                             if ~isnan(pWaveEndIndex)
%                                 electrodeOnsetOffset(electrodeIndex, 2) = intervalTime(pWaveEndIndex);
%                             end
%                             if ~isnan(qOnset)
%                                 electrodeOnsetOffset(electrodeIndex, 3) = intervalTime(qOnset);
%                             end
%                         end
%                         
%                         fileData.electrodeOnsetOffset = electrodeOnsetOffset;
                    end
                    
                    if self.UseRMSOnsetOffset
                        % set all electrode onsets and offsets to the RMS values
                        numberOfElectrodes = fileData.ecgData.GetNumberOfChannels();
                        fileData.electrodeOnsetOffset = repmat(fileData.onsetOffset, numberOfElectrodes, 1);
                    end
                    
                    pWaveData = pWaveMatcher.GetPWaveData(fileData.ecgData, fileData.baselineCorrectedData,...
                        fileData.pWaveIntervals, fileData.onsetOffset, fileData.electrodeOnsetOffset, fileData.validChannels,...
                        fileData.electrodeTerminalForce);
                    
                    currentLabels = deblank(fileData.ecgData.ElectrodeLabels);
                    fileChannelLabels{fileIndex} = currentLabels(:)';   % always a horizontal array
                else
                    if strfind(filename, '_PWave')
                        pWaveData = fileData.PWaveData;
                        currentLabels = deblank(pWaveData.ElectrodeLabels);
                        fileChannelLabels{fileIndex} = currentLabels(:)';
                    end
                end
                filePWaveData{fileIndex} = pWaveData;
                
                if isfield(pWaveData, 'ValidChannels')
                    validElectrodes = pWaveData.ValidChannels;
                    currentLabels = fileChannelLabels{fileIndex}(validElectrodes);
                    fileChannelLabels{fileIndex} = currentLabels(:)';
                else
                    numberOfChannels = numel(pWaveData.ElectrodeLabels);
                    validElectrodes = 1:numberOfChannels;
                end
                
                % number of leads
                numberOfLeads = numel(validElectrodes);
                
                % number of intervals
                numberOfIntervals = size(pWaveData.PWaveIntervals, 1);
                
                % duration
                pWaveDuration = pWaveData.OnsetOffset(2) - pWaveData.OnsetOffset(1);
                
                % dispersion & total P-wave duration
                electrodeOnsetOffset = pWaveData.ElectrodeOnsetOffset;
                invalidOnsetOffset = isnan(electrodeOnsetOffset(:, 1)) | isnan(electrodeOnsetOffset(:, 2));
                if any(~invalidOnsetOffset)
                    totalPWaveDuration = max(electrodeOnsetOffset(~invalidOnsetOffset, 2)) -...
                        min(electrodeOnsetOffset(~invalidOnsetOffset, 1));
                else
                    totalPWaveDuration = NaN;
                end
                electrodeOnsetOffset(isnan(electrodeOnsetOffset(:, 1)), 1) = pWaveData.OnsetOffset(1);
                electrodeOnsetOffset(isnan(electrodeOnsetOffset(:, 2)), 2) = pWaveData.OnsetOffset(2);
                pWaveDurations = electrodeOnsetOffset(validElectrodes, 2) - electrodeOnsetOffset(validElectrodes, 1);
                dispersion = max(pWaveDurations) - min(pWaveDurations);
                
                pWaveDurations(invalidOnsetOffset) = NaN;   % set unmarked leads to NaN duration
                
                % PQ time
                PQTime = pWaveData.OnsetOffset(3) - pWaveData.OnsetOffset(1);
                
                % area
                pWaveArea = pWaveData.PWaveArea(validElectrodes);
                
                % amplitude
                pWaveAmplitude = pWaveData.PWaveRange(validElectrodes, end) - pWaveData.PWaveRange(validElectrodes, 1);
                
                % terminal force
                if isfield(pWaveData, 'PWaveTerminalForce')
                    terminalForce = pWaveData.PWaveTerminalForce;
                else
                    terminalForce = NaN(numberOfLeads, 1);
                end
                
                % entropy
                pWaveEntropy = pWaveData.PWaveEntropy(validElectrodes);
                pWaveSampleEntropy = pWaveData.PWaveSampleEntropy(validElectrodes);
                
                % Concatenate parameters
                result{fileIndex} = [...
                    numberOfLeads;...
                    numberOfIntervals;...
                    pWaveDuration;...
                    totalPWaveDuration;...
                    dispersion;...
                    PQTime;...
                    pWaveArea;...
                    pWaveAmplitude;...
                    pWaveDurations;...
                    terminalForce;...
                    pWaveEntropy;...
                    pWaveSampleEntropy]';
                
                %                 % XYZ angle
                %                 XYZIndices = fileData.XYZIndices;
                %                 intervalTime = pWaveData.AveragingInterval;
                %                 validTime = intervalTime >= pWaveData.OnsetOffset(1) & intervalTime <= pWaveData.OnsetOffset(2);
                %                 % scale time to seconds
                %                 intervalTime = intervalTime / 1000;
                %
                %                 % scale signals to micro volt
                %                 if any(isnan(XYZIndices(:)))
                %                     result(fileIndex).XYZArea = NaN(1, 4);
                %                     result(fileIndex).XYZAbsArea = NaN(1, 3);
                %                     result(fileIndex).XYZSpherical = NaN(1, 3);
                %                 else
                %                     averagePWaves = pWaveData.AveragePWaves * 1000;
                %                     xPwave = averagePWaves(:, XYZIndices(1, 2)) - averagePWaves(:, XYZIndices(1, 1));
                %                     yPwave = averagePWaves(:, XYZIndices(2, 2)) - averagePWaves(:, XYZIndices(2, 1));
                %                     zPwave = averagePWaves(:, XYZIndices(3, 2)) - averagePWaves(:, XYZIndices(3, 1));
                %                     xyzWave = sqrt(xPwave.^2 + yPwave.^2 + zPwave.^2);
                %
                %                     xArea = trapz(intervalTime(validTime), xPwave(validTime));
                %                     yArea = trapz(intervalTime(validTime), yPwave(validTime));
                %                     zArea = trapz(intervalTime(validTime), zPwave(validTime));
                %
                %                     xAreaAbs = trapz(intervalTime(validTime), abs(xPwave(validTime)));
                %                     yAreaAbs = trapz(intervalTime(validTime), abs(yPwave(validTime)));
                %                     zAreaAbs = trapz(intervalTime(validTime), abs(zPwave(validTime)));
                %                     xyzArea = trapz(intervalTime(validTime), abs(xyzWave(validTime)));
                %
                %                     % azimuth (transversal XZ plane) & elevation (frontal Y-XZ plane)
                %                     [maximumDistance, maximumPosition] = max(sqrt(xPwave.^2 + yPwave.^2 + zPwave.^2));
                %                     [azimuth, elevation] = cart2sph(xPwave(maximumPosition), zPwave(maximumPosition), yPwave(maximumPosition));
                %
                %
                %                     result(fileIndex).XYZArea = [xArea, yArea, zArea, xyzArea];
                %                     result(fileIndex).XYZAbsArea = [xAreaAbs, yAreaAbs, zAreaAbs];
                %                     result(fileIndex).XYZSpherical = [azimuth, elevation, maximumDistance];
                %                 end
            end
            filePWaveData = vertcat(filePWaveData{:});
            
            % compute electrode label-based complexity
            channelLabels = unique(horzcat(fileChannelLabels{:}))';
            channelLabels = channelLabels(:)';
            numberOfUniqueChannels = numel(channelLabels);
            channelAmplitudes = NaN(numberOfFiles, numberOfUniqueChannels);
            for fileIndex = 1:numberOfFiles
                [currentValidChannels,  fileValidChannelIndices] = ismember(channelLabels, fileChannelLabels{fileIndex});
                if any(currentValidChannels)
                    pWaveData = filePWaveData(fileIndex);
                    if isfield(pWaveData, 'ValidChannels')
                        validElectrodes = pWaveData.ValidChannels;
                    else
                        numberOfChannels = numel(pWaveData.ElectrodeLabels);
                        validElectrodes = 1:numberOfChannels;
                    end
                    
                    pWaveAmplitude = pWaveData.PWaveRange(validElectrodes, end) - pWaveData.PWaveRange(validElectrodes, 1);
                    channelAmplitudes(fileIndex, currentValidChannels) =...
                        pWaveAmplitude(fileValidChannelIndices(currentValidChannels));
                end
            end
            channelComplexityThresholds = nanmean(channelAmplitudes, 1) * self.PeakAmplitudeThreshold;
            
            for fileIndex = 1:numberOfFiles
                pWaveData = filePWaveData(fileIndex);
                if isfield(pWaveData, 'ValidChannels')
                    validElectrodes = pWaveData.ValidChannels;
                else
                    numberOfChannels = numel(pWaveData.ElectrodeLabels);
                    validElectrodes = 1:numberOfChannels;
                end
                validElectrodeLabels = pWaveData.ElectrodeLabels(validElectrodes);
                averagePWaves = pWaveData.AveragePWaves(:, validElectrodes);
                
                electrodeOnsetOffset = pWaveData.ElectrodeOnsetOffset;
                electrodeOnsetOffset(isnan(electrodeOnsetOffset(:, 1)), 1) = pWaveData.OnsetOffset(1);
                electrodeOnsetOffset(isnan(electrodeOnsetOffset(:, 2)), 2) = pWaveData.OnsetOffset(2);
                electrodeOnsetOffset = electrodeOnsetOffset(validElectrodes, :);
                
                intervalTime = pWaveData.AveragingInterval;
                
                pWaveComplexity = NaN(numel(validElectrodeLabels), 1);
                for electrodeIndex = 1:numel(validElectrodeLabels)
                    pWaveAverage = averagePWaves(:, electrodeIndex);
                    electrodeLabel = validElectrodeLabels(electrodeIndex);
                    electrodeChannelIndex = strcmp(electrodeLabel, channelLabels);
                    
                    if any(electrodeChannelIndex(:))
                        currentPWaveComplexity = BatchAnalysisPkg.BatchPWaveAnalyzer.ComputePWaveComplexity(...
                            pWaveAverage, intervalTime, electrodeOnsetOffset(electrodeIndex, :),...
                            channelComplexityThresholds(electrodeChannelIndex));
                        pWaveComplexity(electrodeIndex) = numel(currentPWaveComplexity);
                    end
                end
                
                result{fileIndex} = [result{fileIndex}, pWaveComplexity(:)'];
            end
            
            % Structure result output
            summaryLabels = {'Number_of_Leads', 'Number_of_Intervals',...
                'PWave_Duration_RMS', 'PWave_Duration_MinMax', 'PWave_Dispersion', 'PQ_Time'};
            areaLabels = strcat('Area', '_', channelLabels);
            durationLabels = strcat('Duration', '_', channelLabels);
            amplitudeLabels = strcat('Amplitude', '_', channelLabels);
            terminalForceLabels = strcat('Terminal_Force', '_', channelLabels);
            entropyLabels = strcat('Entropy', '_', channelLabels);
            sampleEntropyLabels = strcat('SampleEntropy', '_', channelLabels);
            complexityLabels = strcat('Complexity', '_', channelLabels);
            parameterLabels = horzcat(summaryLabels, areaLabels, amplitudeLabels,...
                durationLabels, terminalForceLabels,...
                entropyLabels, sampleEntropyLabels, complexityLabels);
            
            collectedResults = NaN(numberOfFiles, numel(parameterLabels));
            for fileIndex = 1:numberOfFiles
                currentLabels = fileChannelLabels{fileIndex};
                currentLabels = currentLabels(:)';
                areaLabels = strcat('Area', '_', currentLabels);
                durationLabels = strcat('Duration', '_', currentLabels);
                amplitudeLabels = strcat('Amplitude', '_', currentLabels);
                terminalForceLabels = strcat('Terminal_Force', '_', currentLabels);
                entropyLabels = strcat('Entropy', '_', currentLabels);
                sampleEntropyLabels = strcat('SampleEntropy', '_', currentLabels);
                complexityLabels = strcat('Complexity', '_', currentLabels);
                currentParameterLabels = horzcat(summaryLabels, areaLabels, amplitudeLabels,...
                    durationLabels, terminalForceLabels,...
                    entropyLabels, sampleEntropyLabels, complexityLabels);
                
                [validParameters, parameterIndices] = ismember(currentParameterLabels, parameterLabels);
                currentResult = result{fileIndex};
                
                collectedResults(fileIndex, parameterIndices) = currentResult(validParameters);
            end
            
            parameterLabels = matlab.lang.makeValidName(parameterLabels);
            resultTable = array2table(collectedResults, 'VariableNames', parameterLabels,...
                'RowNames', shortFilenames);
            
            result = struct(...
                'ResultTable', resultTable);
            
            self.UpdateTextProgress({'Done'});
        end
        
        function result = ComputeComplexity(self, dataList, detectMap)
            if nargin < 3
                detectMap = false;
            end
            % compute map-based complexity
            if isempty(dataList), result = []; return; end
            % dataList: a list of PWave filenames
            filenameList = dataList;
            numberOfFiles = numel(filenameList);
            filePWaveData = cell(numberOfFiles, 1);
            fileMapData = cell(numberOfFiles, 1);
            
            self.UpdateTextProgress({' '});
            self.UpdateTextProgress({'Computing map-based P-wave complexity parameters...'});
            
            % assume all maps have equal dimensions (labels may differ)
            fileData = load(filenameList{1});
            if strfind(filenameList{1}, '_PWaveMatching')
                electrodeLabels = fileData.ecgData.ElectrodeLabels;
            else
                if strfind(filenameList{1}, '_PWave')
                    electrodeLabels = fileData.pWaveData.ElectrodeLabels;
                end
            end
            
            if detectMap
                referenceMap = PWaveMatcher.DetectECGMap(electrodeLabels);
            else
                referenceMap = fileData.Map;
            end
            referenceMapLabels = [referenceMap.Front, cell(size(referenceMap.Front, 1), 2), fliplr(referenceMap.Back)];
            mapThresholds = NaN([size(referenceMapLabels), numberOfFiles]);
            
            % determine complexity threshold for each electrode position
            self.UpdateTextProgress({'Determining average p-wave amplitude...'});
            for fileIndex = 1:numberOfFiles
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                self.UpdateTextProgress({['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']});
                
                fileData = load(filename);
                if strfind(filename, '_PWaveMatching')
                    % compute average p-waves
                    pWaveMatcher = fileData.pWaveMatching;
                    pWaveData = pWaveMatcher.GetPWaveData(fileData.ecgData, fileData.baselineCorrectedData,...
                        fileData.pWaveIntervals, fileData.onsetOffset, fileData.electrodeOnsetOffset, fileData.validChannels,...
                        fileData.electrodeTerminalForce);
                    map = PWaveMatcher.DetectECGMap(fileData.ecgData.ElectrodeLabels);
                else
                    if strfind(filename, '_PWave')
                        pWaveData = fileData.PWaveData;
                        if detectMap
                            map = PWaveMatcher.DetectECGMap(pWaveData.ElectrodeLabels);
                        else
                            map = fileData.Map;
                        end
                    end
                end
                filePWaveData{fileIndex} = pWaveData;
                fileMapData{fileIndex} = map;
                mapLabels = [map.Front, cell(size(map.Front, 1), 2), fliplr(map.Back)];
                
                if isfield(pWaveData, 'ValidChannels')
                    validElectrodes = pWaveData.ValidChannels;
                else
                    numberOfChannels = numel(pWaveData.ElectrodeLabels);
                    validElectrodes = 1:numberOfChannels;
                end
                validElectrodeLabels = pWaveData.ElectrodeLabels(validElectrodes);
                
                pWaveAmplitude = pWaveData.PWaveRange(validElectrodes, end) - pWaveData.PWaveRange(validElectrodes, 1);
                for i = 1:size(mapLabels, 1)
                    for j = 1:size(mapLabels, 2)
                        channelPosition = find(strcmpi(mapLabels{i, j}, validElectrodeLabels), 1, 'first');
                        if ~isempty(channelPosition)
                            mapThresholds(i, j, fileIndex) = pWaveAmplitude(channelPosition) * self.PeakAmplitudeThreshold;
                        end
                    end
                end
            end
            averageMapThreshold = nanmean(mapThresholds, 3);
            
            self.UpdateTextProgress({'Computing map-based p-wave complexity...'});
            fileComplexity = cell(numberOfFiles, 1);
            for fileIndex = 1:numberOfFiles
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                self.UpdateTextProgress({['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']});
                
                pWaveData = filePWaveData{fileIndex};
                map = fileMapData{fileIndex};
                mapLabels = [map.Front, cell(size(map.Front, 1), 2), fliplr(map.Back)];
                mapComplexity = NaN(size(referenceMapLabels));
                mapAmplitude = NaN(size(referenceMapLabels));
                mapArea = NaN(size(referenceMapLabels));
                mapDuration = NaN(size(referenceMapLabels));
                intervalTime = pWaveData.AveragingInterval;
                
                if isfield(pWaveData, 'ValidChannels')
                    validElectrodes = pWaveData.ValidChannels;
                else
                    numberOfChannels = numel(pWaveData.ElectrodeLabels);
                    validElectrodes = 1:numberOfChannels;
                end
                validElectrodeLabels = pWaveData.ElectrodeLabels(validElectrodes);
                
                averagePWaves = pWaveData.AveragePWaves(:, validElectrodes);
                pWaveRanges = pWaveData.PWaveRange(validElectrodes, :);
                pWaveAreas = pWaveData.PWaveArea(validElectrodes);
                
                electrodeOnsetOffset = pWaveData.ElectrodeOnsetOffset;
                electrodeOnsetOffset(isnan(electrodeOnsetOffset(:, 1)), 1) = pWaveData.OnsetOffset(1);
                electrodeOnsetOffset(isnan(electrodeOnsetOffset(:, 2)), 2) = pWaveData.OnsetOffset(2);
                electrodeOnsetOffset = electrodeOnsetOffset(validElectrodes, :);
                
                pWaveDurations = electrodeOnsetOffset(:, 2) - electrodeOnsetOffset(:, 1);
                
                pWaveComplexity = NaN(numel(validElectrodeLabels), 1);
                for electrodeIndex = 1:numel(validElectrodeLabels)
                    channelMapPosition = find(strcmpi(validElectrodeLabels(electrodeIndex), mapLabels(:)), 1, 'first');
                    if ~isempty(channelMapPosition)
                        voltageThreshold = averageMapThreshold(channelMapPosition);
                        pWaveAverage = averagePWaves(:, electrodeIndex);
                        
                        % complexity
                        currentPWaveComplexity = BatchAnalysisPkg.BatchPWaveAnalyzer.ComputePWaveComplexity(...
                            pWaveAverage, intervalTime, electrodeOnsetOffset(electrodeIndex, :), voltageThreshold);
                        pWaveComplexity(electrodeIndex) = numel(currentPWaveComplexity);
                        mapComplexity(channelMapPosition) = numel(currentPWaveComplexity);
                        
                        % amplitude
                        mapAmplitude(channelMapPosition) = pWaveRanges(electrodeIndex, end) - pWaveData.PWaveRange(electrodeIndex, 1);
                        
                        % area
                        mapArea(channelMapPosition) = pWaveAreas(electrodeIndex);
                        
                        % duration
                        mapDuration(channelMapPosition) = pWaveDurations(electrodeIndex);
                    end
                end
                interpolatedMap = BatchAnalysisPkg.BatchPWaveAnalyzer.ComputeInterpolatedMap(mapComplexity);
                fileComplexity{fileIndex} = struct(...
                    'mapComplexity', mapComplexity,...
                    'interpolated', interpolatedMap,...
                    'mapAmplitude', mapAmplitude,...
                    'mapArea', mapArea,...
                    'mapDuration', mapDuration);
            end
            
            result = struct(...
                'averageMapThreshold', averageMapThreshold,...
                'fileComplexity', vertcat(fileComplexity{:}));
            
            self.UpdateTextProgress({'Done'});
        end
    end
    
    methods (Static)
        function peakComplexityIndices = ComputePWaveComplexity(pWaveAverage, intervalTime, onsetOffsetTime, voltageThreshold)
            if isnan(onsetOffsetTime(1))
                startIndex = 1;
            else
                startIndex = find(intervalTime >= onsetOffsetTime(1), 1, 'first');
            end
            if isnan(onsetOffsetTime(2))
                endIndex = numel(intervalTime);
            else
                endIndex = find(intervalTime <= onsetOffsetTime(2), 1, 'last');
            end
            
            [pWavePositivePeakValues, pWavePositivePeakIndices] = findpeaks(pWaveAverage);
            [pWaveNegativePeakValues, pWaveNegativePeakIndices] = findpeaks(-pWaveAverage);
            
            peakValues = [pWavePositivePeakValues; -pWaveNegativePeakValues];
            peakIndices = [pWavePositivePeakIndices; pWaveNegativePeakIndices];
            [sortedPeakValues, sortedPeakIndices] = sort(peakIndices, 'ascend');
            peakIndices = sortedPeakValues;
            peakValues = peakValues(sortedPeakIndices);
            
            validPeakIndices = peakIndices > startIndex & peakIndices < endIndex;
            peakIndices = peakIndices(validPeakIndices);
            peakValues = peakValues(validPeakIndices);
            
            peakIndices = [startIndex; peakIndices(:); endIndex];
            peakValues = [pWaveAverage(startIndex); peakValues; pWaveAverage(endIndex);];
            
            numberOfThresholds = numel(voltageThreshold);
            peakComplexityIndices = cell(numberOfThresholds, 1);
            for thresholdIndex = 1:numberOfThresholds
                currentPeakIndices = peakIndices;
                currentPeakValues = peakValues;
                changed = true;
                while changed
                    voltageDifferences = diff(currentPeakValues);
                    [minDifference, minIndex] = min(abs(voltageDifferences));
                    if minDifference < voltageThreshold(thresholdIndex)
                        currentPeakIndices([minIndex, minIndex + 1]) = [];
                        currentPeakValues([minIndex, minIndex + 1]) = [];
                        
                        if numel(currentPeakIndices) > 0 && currentPeakIndices(1) > startIndex
                            currentPeakIndices = [startIndex; currentPeakIndices];
                            currentPeakValues = [pWaveAverage(startIndex); currentPeakValues];
                        end
                        if numel(currentPeakIndices) > 0 && currentPeakIndices(end) < endIndex
                            currentPeakIndices = [currentPeakIndices; endIndex];
                            currentPeakValues = [currentPeakValues; pWaveAverage(endIndex)];
                        end
                    else
                        changed = false;
                    end
                end
                
                if numel(currentPeakIndices) > 0 && currentPeakIndices(1) == startIndex
                    currentPeakIndices = currentPeakIndices(2:end);
                end
                if numel(currentPeakIndices) > 0 && currentPeakIndices(end) == endIndex
                    currentPeakIndices = currentPeakIndices(1:(end-1));
                end
                
                peakComplexityIndices{thresholdIndex} = currentPeakIndices;
            end
            
            if numberOfThresholds == 1
                peakComplexityIndices = peakComplexityIndices{1};
            end
        end
        
        function interpolatedMap = ComputeInterpolatedMap(mapData)
            [Y, X] = find(~isnan(mapData));
            [Xint, Yint] = meshgrid(1:size(mapData, 2), 1:size(mapData, 1));
            interpolant = scatteredInterpolant([X, Y], mapData(~isnan(mapData)));
            interpolant.Method = 'linear';
            interpolant.ExtrapolationMethod = 'none';
            interpolatedMap = interpolant(Xint, Yint);
        end
        
        function resultTable = CreateResultTable(result, complexityResults)
            filenames = {result.shortFilename}';
            
            numberOfLeads = vertcat(result.numberOfLeads);
            numberOfIntervals = vertcat(result.numberOfIntervals);
            areaData = vertcat(result.area);
            amplitudeData = vertcat(result.amplitude);
            duration = vertcat(result.duration);
            dispersion = vertcat(result.dispersion);
            PQtime = vertcat(result.PQTime);
            
            complexity = complexityResults.fileComplexity;
            
            XYZArea = vertcat(result.XYZArea);
            XYZAbsArea = vertcat(result.XYZAbsArea);
            XYZSpherical = vertcat(result.XYZSpherical);
            
            resultTable = table(numberOfLeads, numberOfIntervals, duration, dispersion, PQtime,...
                areaData(:, 1), areaData(:, 2),areaData(:, 3), areaData(:, 4), areaData(:, 5),...
                amplitudeData(:, 1), amplitudeData(:, 2), amplitudeData(:, 3), amplitudeData(:, 4), amplitudeData(:, 5),...
                cellfun(@(x) mean(x(~isnan(x))), {complexity.mapComplexity}'), cellfun(@(x) std(x(~isnan(x))), {complexity.mapComplexity}'),...
                cellfun(@(x) prctile(x(~isnan(x)), 5), {complexity.mapComplexity}'), cellfun(@(x) prctile(x(~isnan(x)), 50), {complexity.mapComplexity}'), cellfun(@(x) prctile(x(~isnan(x)), 95), {complexity.mapComplexity}'),...
                XYZArea(:, 1), XYZArea(:, 2), XYZArea(:, 3), XYZArea(:, 4),...
                XYZAbsArea(:, 1), XYZAbsArea(:, 2), XYZAbsArea(:, 3),...
                180 * XYZSpherical(:, 1) / pi, 180 * XYZSpherical(:, 2) / pi, XYZSpherical(:, 3),...
                'VariableNames', {'NumberOfLeads', 'NumberOfIntervals', 'Duration', 'Dispersion', 'PQtime',...
                'AreaMean', 'AreaSD', 'AreaP5', 'AreaP50', 'AreaP95',...
                'AmplitudeMean', 'AmplitudeSD', 'AmplitudeP5', 'AmplitudeP50', 'AmplitudeP95',...
                'PeaksMean', 'PeaksSD', 'PeaksP5', 'PeaksP50', 'PeaksP95',...
                'X_Area', 'Y_Area', 'Z_Area', 'XYZ_Area',...
                'X_AbsArea', 'Y_AbsArea', 'Z_AbsArea',...
                'XYZ_Azimuth', 'XYZ_Elevation', 'XYZ_Radius'},...
                'RowNames', filenames);
        end
        
        function pWaveData = LoadPWaveMatchingData(filename)
            data = load(filename);
            pWaveMatcher = data.pWaveMatching;
            pWaveData = pWaveMatcher.GetPWaveData(data.ecgData, data.baselineCorrectedData,...
                data.pWaveIntervals, data.onsetOffset, data.electrodeOnsetOffset, data.validChannels,...
                data.electrodeTerminalForce);
        end
    end
end