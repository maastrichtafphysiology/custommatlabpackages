classdef BatchPWaveAnalysisPanel < BatchAnalysisPkg.BatchAnalysisPanel
    properties
        Results
        ComplexityResults
        
        TabGroup
        SettingsPanel
        LogPanel
        ResultPanel
        ResultText
        ResultTable
        
        Analyzer
    end
    
    methods
        function self = BatchPWaveAnalysisPanel(position)
            self = self@BatchAnalysisPkg.BatchAnalysisPanel(position);
            self.Analyzer = BatchAnalysisPkg.BatchPWaveAnalyzer();
        end
        
        function Create(self, parentHandle)
            Create@BatchAnalysisPkg.BatchAnalysisPanel(self, parentHandle);
            
            uicontrol('parent', self.FileListPanel,...
                'style', 'pushbutton',...
                'units', 'normalized',...
                'string', 'Load result',...
                'callback', @self.LoadResult,...
                'position', [.05 .1 .1 .2]);
            
            self.CreateTabGroup();
        end
    end
    
    methods (Access = protected)
        function AddFilesToFileListBox(self, varargin)
            [filenames, pathname] = uigetfile( ...
                {'*_PWave.mat','Average P-Wave Information (*_PWave.mat)';...
                '*_PWaveMatching.mat', 'P-Wave Matching Data (*_PWaveMatching.mat)'}, ...
                'Select P-Wave Info or Data File', ...
                'MultiSelect', 'on');
            if isnumeric(filenames), return; end
            
            if ischar( filenames)
                filenames = cellstr(filenames);
            end
            
            fileList = get(self.FileListBox, 'string');
            
            for i = 1:length(filenames)
                fullPath = [pathname filenames{i}];
                if all(~strcmp(fullPath, fileList))
                    fileList = [fileList; cellstr(fullPath)]; %#ok<*AGROW>
                end
            end
            
            set(self.FileListBox, 'string', fileList);
            set(self.FileListBox, 'value', numel(fileList));
        end
        
        function RemoveFilesFromFileListBox(self, varargin)
            selectedFile = get(self.FileListBox, 'value');
            fileList = get(self.FileListBox, 'string');
            
            if isempty(fileList), return; end
            
            fileList(selectedFile) = [];
            
            set(self.FileListBox, 'value', 1);
            set(self.FileListBox, 'string', fileList);
        end
        
        function Compute(self, varargin)
            filenameList = get(self.FileListBox, 'string');
            self.Results = cell(size(filenameList, 1), 1);
            
            if isempty(filenameList), return; end
            
            self.Results = self.Analyzer.Compute(filenameList);
%             detectMap = true;
%             self.ComplexityResults = self.Analyzer.ComputeComplexity(filenameList, detectMap);
            
            self.InitializeResultPanel();
        end
        
        function CreateTabGroup(self)
            self.TabGroup = uitabgroup(...
                'parent', self.ControlHandle,...
                'position', [0 0 1 .8]);
            
            self.SettingsPanel = uitab(self.TabGroup, 'title', 'Parameter settings');
            self.CreateSettingsPanel();
            
            self.LogPanel = uitab(self.TabGroup, 'title', 'Log');
            self.ResultPanel = uitab(self.TabGroup, 'title', 'Results');
            
            self.ResultText = uicontrol('parent', self.LogPanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 1],...
                'style', 'edit',...
                'enable', 'inactive',...
                'min', 1, 'max', 10,...
                'string', '',...
                'backgroundColor', [1,1,1],...
                'horizontalAlignment', 'left');
            self.Analyzer.ResultTextHandle = self.ResultText;
            
            uicontrol('parent', self.ResultPanel,...
                'style', 'pushbutton',...
                'units', 'normalized',...
                'string', 'Save result as .mat',...
                'callback', @self.SaveResult,...
                'position', [.1 .925 .2 .05]);
            
            uicontrol('parent', self.ResultPanel,...
                'style', 'pushbutton',...
                'units', 'normalized',...
                'string', 'Export result to .csv',...
                'callback', @self.ExportResult,...
                'position', [.4 .925 .2 .05]);
            
            self.ResultTable = uitable(...
                'parent', self.ResultPanel,...
                'units', 'normalized',...
                'position', [0 0 1 .9]);
        end
        
        function CreateResultPanel(self)
            return;
        end
        
        function CreateSettingsPanel(self)
            
            textHeight = 0.45;
            textSpacing = 0.45;
            labelWidth = 0.6;
            editWidth = 0.2;
            
            onsetOffsetPanel = uipanel(...
                'parent', self.SettingsPanel,...
                'title', 'Onset and offset',...
                'units', 'normalized',...
                'position', [0 .7 1/3 .2]);
            
            uicontrol('parent', onsetOffsetPanel,...
                'style', 'checkbox',...
                'units', 'normalized',...
                'string', 'Only use RMS onset and offset',...
                'horizontalAlignment', 'left',...
                'position', [.1, 1-textSpacing, labelWidth, textHeight],...
                'value', self.Analyzer.UseRMSOnsetOffset,...
                'callback', @self.SetUseRMSOnsetOffset);
              
            uicontrol('parent', onsetOffsetPanel,...
                'style', 'checkbox',...
                'units', 'normalized',...
                'string', 'Recompute onset and offsets',...
                'horizontalAlignment', 'left',...
                'position', [.1, 1-2*textSpacing, labelWidth, textHeight],...
                'value', self.Analyzer.RecomputeOnsetOffSet,...
                'callback', @self.SetRecomputeOnsetOffSet);
        end
        
        function InitializeResultPanel(self)
            resultTable = self.Results.ResultTable;
            
            if ishandle(self.ResultTable)
                delete(self.ResultTable)
            end
            
            self.ResultTable = uitable(...
                'parent', self.ResultPanel,...
                'units', 'normalized',...
                'position', [0 0 1 .9],...
                'data',  resultTable{:, :},...
                'columnName', resultTable.Properties.VariableNames,...
                'rowName', resultTable.Properties.RowNames);
        end
        
        function SaveResult(self, varargin)
            if isempty(self.Results), return; end
            
            [filename, pathname] = HelperFunctions.customUiputfile('*.mat', 'Save result as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            result = self.Results; %#ok<NASGU>
            complexityResult = self.ComplexityResults; %#ok<NASGU>
            save(filename, 'result', 'complexityResult');
        end
        
        function LoadResult(self, varargin)
            [filename, pathname] = HelperFunctions.customUigetfile('*.mat', 'Select batch result file');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            resultData = load(filename);
            if isfield(resultData, 'result')
                self.Results = resultData.result;
                self.ComplexityResults = resultData.complexityResult;
                
                self.InitializeResultPanel();
            else
                disp('Invalid batch result file: does not contain result field.');
            end
        end
        
        function ExportResult(self, varargin)
            if isempty(self.Results), return; end
            
            [filename, pathname] = HelperFunctions.customUiputfile('*.csv', 'Export result as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            outputFilename = fullfile(pathname, filename);
            
%             resultTable = BatchAnalysisPkg.BatchPWaveAnalyzer.CreateResultTable(self.Results, self.ComplexityResults);
            resultTable = self.Results.ResultTable;
            
            writetable(resultTable, outputFilename, 'WriteRowNames', true);
        end
        
        function SetUseRMSOnsetOffset(self, source, varargin)
            self.Analyzer.UseRMSOnsetOffset = get(source, 'value');
        end
        
        function SetRecomputeOnsetOffSet(self, source, varargin)
            self.Analyzer.RecomputeOnsetOffSet = get(source, 'value');
        end
    end
end