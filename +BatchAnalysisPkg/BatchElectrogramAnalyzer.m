classdef BatchElectrogramAnalyzer < BatchAnalysisPkg.BatchAnalyzer
    properties
        ElectrodeMapName
        ElectrodePositions
        BatchChannelSelection
        BatchPaceBlanking
        BatchSignalPreProcessor
        BatchQRSTCancellator
        BatchDeflectionDetector
        DeflectionTemplates
    end
    
    methods
        function self = BatchElectrogramAnalyzer()
            self = self@BatchAnalysisPkg.BatchAnalyzer;
            
            self.ElectrodeMapName = '';
            self.BatchChannelSelection = struct(...
                'channelsToAnalyze', '',...
                'referenceChannel', '');
            
            self.BatchPaceBlanking = struct(...
                'enabled', false,...
                'useAsDfPrior', true,...
                'selectedPaceChannel', NaN,...
                'paceInterval', NaN,...
                'minimalPaceAmplitude', NaN,...
                'blankingInterval', NaN);
        end
        
        function result = Compute(self, dataList)
            % dataList: a list of raw signal filenames
            for fileIndex = 1:numel(dataList)
                % 1. Load file, select channels & reference and create DCMData
                filename = dataList{fileIndex};
                [pathname, filenameString, extensionString] = fileparts(filename);
                
                self.UpdateTextProgress({''; ['Reading file: ', filename]});
                
                if strcmpi(extensionString, '.DATA'),
                    ecgData = InputOutputPkg.ECGReader.ReadIDEEQ(filename, self.ElectrodeMapName);
                elseif strcmpi(extensionString(1:2), '.E') && length(extensionString) == 4,
                    ecgData = InputOutputPkg.ECGReader.ReadPACEMAPBinary(filename, self.ElectrodeMapName);
                else
                    self.UpdateTextProgress({'Unknown format...skipped'});
                    continue;
                end
                
                referenceChannel = self.BatchChannelSelection.referenceChannel;
                referenceChannelPosition = strcmp(referenceChannel, ecgData.ElectrodeLabels);
                if any(referenceChannelPosition)
                    ecgData.AddReferenceChannel(referenceChannelPosition);
                else
                    self.UpdateTextProgress({'Reference channel ', referenceChannel, ' not found'});
                    continue;
                end
                
                if self.BatchPaceBlanking.enabled
                    paceReferenceChannel = self.BatchPaceBlanking.selectedPaceChannel;
                    paceReferenceChannelPosition = strcmp(paceReferenceChannel, ecgData.ElectrodeLabels);
                    if any(paceReferenceChannelPosition)
                        ecgData.AddReferenceChannel(paceReferenceChannelPosition);
                    else
                        self.UpdateTextProgress({'Reference channel ', paceReferenceChannel, ' not found'});
                        continue;
                    end
                end
                
                validChannels = ismember(ecgData.ElectrodeLabels, self.BatchChannelSelection.channelsToAnalyze);
                newEcgData = ecgData.Copy(validChannels);
                clear('ecgData');
                ecgData = newEcgData;
                
                % 1. Optional: pace blanking
                dfPrior = NaN;
                if self.BatchPaceBlanking.enabled
                    self.UpdateTextProgress({'0. Pace blanking'});
                    
                    samplingFrequency = ecgData.SamplingFrequency;
                    data = ecgData.ReferenceData(:, 2);
                    signal = abs(data - mean(data));
                    
                    minimumPacePeakDistance = floor(self.BatchPaceBlanking.paceInterval * samplingFrequency);
                    [peakAmplitudes, paceIndices] = findpeaks(signal, 'MINPEAKDISTANCE', minimumPacePeakDistance);
                    
                    validPeaks = peakAmplitudes > (max(peakAmplitudes) * self.BatchPaceBlanking.minimalPaceAmplitude);
                    paceIndices = paceIndices(validPeaks);
                    self.UpdateTextProgress({['Detected pacing interval: ' num2str(1000 * mean(diff(paceIndices) / samplingFrequency), '%.0f'), 'ms']});
                    
                    if self.BatchPaceBlanking.useAsDfPrior
                        meanPaceInterval = mean(diff(paceIndices) / samplingFrequency);
                        dfPrior = 1 / meanPaceInterval;
                    end
                    
                    blankingSampleInterval = round(self.BatchPaceBlanking.blankingInterval * samplingFrequency);
                    blankingRange = -blankingSampleInterval:1:blankingSampleInterval;
                    blankingIndices = bsxfun(@plus, paceIndices, blankingRange);
                    numberOfSamples = ecgData.GetNumberOfSamples();
                    blankingIndices = blankingIndices(blankingIndices > 0 & blankingIndices < numberOfSamples);
                    sampleRange = 1:numberOfSamples;
                    sampleRange(blankingIndices) = [];
                    for channelIndex = 1:ecgData.GetNumberOfChannels()
                        signal = ecgData.Data(:, channelIndex);
                        interpolant = spline(sampleRange, signal(sampleRange));
                        ecgData.Data(:, channelIndex) = ppval(interpolant, 1:numberOfSamples);
                    end
                end
                
                % 2. Filter signals
                self.UpdateTextProgress({'1. Filtering signals'});
                preProcessedData = self.BatchSignalPreProcessor.Filter(ecgData);
                clear('ecgData');
                
                % 3. Cancel QRS
                self.UpdateTextProgress({'2. QRST cancellation'});
                rWaveIndices = self.BatchQRSTCancellator.DetectRWaves(preProcessedData, 1);
                self.UpdateTextProgress({['Number of R-waves detected: ', num2str(numel(rWaveIndices))]});
                self.BatchQRSTCancellator.ReferenceChannelIndex = 1;
                cancelledEcgData = self.BatchQRSTCancellator.CancelQRST(preProcessedData, rWaveIndices);
                clear('preProcessedData');
                
                % 4. Deflection detection (template matching)
                self.UpdateTextProgress({'3. Template matching'});
                deflectionDetector = AlgorithmPkg.DeflectionDetector();
                deflectionData = deflectionDetector.Apply(cancelledEcgData, self.DeflectionTemplates);
                
                % 5. Deflection assignment (save _DD file)
                self.UpdateTextProgress({'4. Deflection assignment'});
                cycleLengthFit = AlgorithmPkg.DeflectionAssignmentCalculator.ComputeSignalCycleLengthDistribution(cancelledEcgData, dfPrior);
                self.UpdateTextProgress({['Estimated AFCL distribution (mean - SD): ', num2str(1000 * cycleLengthFit.Beta(2:3), '%.0f - %.0f'), 'ms']});
                deflectionCalculator = AlgorithmPkg.DeflectionAssignmentCalculator(cycleLengthFit);
                deflectionCalculator.Time = cancelledEcgData.GetTimeRange();
                templateMatchingAnalysisResult = deflectionData.AnalysisResults;
                intrinsicDeflectionResult = deflectionCalculator.Apply(templateMatchingAnalysisResult); %#ok<NASGU>
                
                ecgData = cancelledEcgData; %#ok<NASGU>
                ddFilename = fullfile(pathname, [filenameString, '_DD.mat']);
                self.UpdateTextProgress({['Saving file: ', [filenameString, '_DD.mat']]});
                save(ddFilename, 'ecgData', 'deflectionData', 'intrinsicDeflectionResult');
                
                clear('ecgData', 'deflectionData', 'intrinsicDeflectionResult');
            end
            
            result  = [];
        end
    end
end