classdef BatchWavemapAnalyzer < BatchAnalysisPkg.BatchAnalyzer
    properties
        FractionationThresholds
        WavemapSettings
        WaveAnalysisThresholds
    end
    
    methods
        function self = BatchWavemapAnalyzer()
            self = self@BatchAnalysisPkg.BatchAnalyzer;
            self.FractionationThresholds = struct(...
                'amplitude', 10,...
                'slope', 10,...
                'close', 0);
            
            self.WavemapSettings = struct(...
                'minimalConductionVelocity', 0.2,...
                'mergeOverlapThreshold', 0.9);
            
            self.WaveAnalysisThresholds = struct(...
                'minimumWaveSize', 3,...
                'velocityRange', [0 Inf]);
        end
        
        function result = Compute(self, dataList)
            filenameList = dataList;
            numberOfFiles = numel(filenameList);
            % dataList: a list DD data filenames
            result(numberOfFiles, 1) = struct(...
                'shortFilename', [],...
                'filename', [],...
                'waves', [],...
                'maps', []);
            
            self.UpdateTextProgress({' '});
            self.UpdateTextProgress({['%%%% ', datestr(datetime('now')), ' %%%%']});
            self.UpdateTextProgress({'Computing wavemap and complexity parameters...'});
            
            for fileIndex = 1:numberOfFiles
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                result(fileIndex).shortFilename = shortFilename;
                result(fileIndex).filename = filename;
                
                self.UpdateTextProgress({['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']});
                
                try
                    ddData = load(filename);
                catch
                    self.UpdateTextProgress({'Could not load file - check DD file'});
                    continue;
                end
                
                self.UpdateTextProgress({'- deflection assignment...'});
                intrinsicDeflectionData = self.AssignIntrinsicDeflections(ddData);
                [intrinsicDeflectionResult, fractionationIndices] = self.AssignFarFieldDeflections(intrinsicDeflectionData);
                
                afcl = AlgorithmPkg.AFComplexityCalculator.ComputeAFCL(intrinsicDeflectionResult);
                afcl = cellfun(@median, afcl);
                
                self.UpdateTextProgress({'- wavefront reconstruction...'});
                detectedWaves = self.ComputeWavemap(ddData, intrinsicDeflectionResult);
                
                wavemapData = ddData.ecgData;
                wavemapData.Deflections = intrinsicDeflectionResult;
                wavemapData.Waves = detectedWaves;
                
                self.UpdateTextProgress({'- wavemap complexity...'});
                waveAnalysisData = self.WaveAnalysisData(wavemapData);
                mapData = self.WaveDataMaps(wavemapData);
                conductionData = self.ConductionAnalysisData(waveAnalysisData.electrodeVelocity);
                mapData.FractionationIndices = fractionationIndices;
                mapData.AFCL = afcl;
                mapData.MeanDissociation = waveAnalysisData.MeanDissociation;
                mapData.MaxDissociation = waveAnalysisData.MaxDissociation;
                velocities = arrayfun(@(x) sqrt(x.xVelocity.^2 + x.yVelocity.^2),...
                    waveAnalysisData.electrodeVelocity, 'uniformOutput', false);
                mapData.MeanVelocity = cellfun(@(x) mean(x(~(isnan(x) | isinf(x)))), velocities);
                mapData.MedianVelocity = cellfun(@(x) median(x(~(isnan(x) | isinf(x)))), velocities);
                
                result(fileIndex).waves = waveAnalysisData;
                result(fileIndex).maps = mapData;
                result(fileIndex).conduction = conductionData;
                result(fileIndex).electrodePositions = wavemapData.ElectrodePositions;
                result(fileIndex).electrodeLabels = wavemapData.ElectrodeLabels;
                
                timeRange = ddData.ecgData.GetTimeRange();
                result(fileIndex).duration = timeRange(end) - timeRange(1);
            end
            
            self.UpdateTextProgress({'Done'});
        end
        
%         function result = Compute(self, dataList)
%             % dataList: a list DD data
%             numberOfFiles = numel(dataList);
%             result(numberOfFiles, 1) = struct(...
%                 'waves', [],...
%                 'maps', []);
%             
%             self.UpdateTextProgress({' '});
%             self.UpdateTextProgress({['%%%% ', datestr(datetime('now')), ' %%%%']});
%             self.UpdateTextProgress({'Computing wavemap and complexity parameters...'});
%             
%             for fileIndex = 1:numberOfFiles
%                 ddData = dataList(fileIndex);
%                 intrinsicDeflectionData = self.AssignIntrinsicDeflections(ddData);
%                 [intrinsicDeflectionResult, fractionationIndices] = self.AssignFarFieldDeflections(intrinsicDeflectionData);
%                 
%                 afcl = AlgorithmPkg.AFComplexityCalculator.ComputeAFCL(intrinsicDeflectionResult);
%                 afcl = cellfun(@median, afcl);
%                 
%                 detectedWaves = self.ComputeWavemap(ddData, intrinsicDeflectionResult);
%                 
%                 wavemapData = ddData.ecgData;
%                 wavemapData.Deflections = intrinsicDeflectionResult;
%                 wavemapData.Waves = detectedWaves;
%                 
%                 waveAnalysisData = self.WaveAnalysisData(wavemapData);
%                 mapData = self.WaveDataMaps(wavemapData);
%                 conductionData = self.ConductionAnalysisData(waveAnalysisData.electrodeVelocity);
%                 mapData.FractionationIndices = fractionationIndices;
%                 mapData.AFCL = afcl;
%                 mapData.MeanDissociation = waveAnalysisData.MeanDissociation;
%                 mapData.MaxDissociation = waveAnalysisData.MaxDissociation;
%                 velocities = arrayfun(@(x) sqrt(x.xVelocity.^2 + x.yVelocity.^2),...
%                     waveAnalysisData.electrodeVelocity, 'uniformOutput', false);
%                 mapData.MeanVelocity = cellfun(@(x) mean(x(~(isnan(x) | isinf(x)))), velocities);
%                 mapData.MedianVelocity = cellfun(@(x) median(x(~(isnan(x) | isinf(x)))), velocities);
%                 
%                 result(fileIndex).waves = waveAnalysisData;
%                 result(fileIndex).maps = mapData;
%                 result(fileIndex).conduction = conductionData;
%                 result(fileIndex).electrodePositions = wavemapData.ElectrodePositions;
%                 result(fileIndex).electrodeLabels = wavemapData.ElectrodeLabels;
%                 
%                 timeRange = ddData.ecgData.GetTimeRange();
%                 result(fileIndex).duration = timeRange(end) - timeRange(1);
%             end
%             
%             self.UpdateTextProgress({'Done'});
%         end
        
        % Computation of intrinsic deflections and wavemap
        function intrinsicDeflectionData = AssignIntrinsicDeflections(self, ddData)
            if isfield(ddData, 'intrinsicDeflectionResult') && ~isempty(ddData.intrinsicDeflectionResult)
                intrinsicDeflectionData = ddData.intrinsicDeflectionResult;
                return;
            end
            
            cycleLengthFit = ddData.deflectionData.Fit;
            deflectionCalculator = AlgorithmPkg.DeflectionAssignmentCalculator(cycleLengthFit);
            deflectionCalculator.AssignmentAlgorithm = AlgorithmPkg.DeflectionAssignmentCalculator.ASSIGNMENT_ALGORITHMS{4};
            
            templateMatchingAnalysisResult = ddData.deflectionData.AnalysisResults;
            intrinsicDeflectionData = deflectionCalculator.Apply(templateMatchingAnalysisResult);
        end
        
        function [intrinsicDeflectionResult, fractionationIndices] = AssignFarFieldDeflections(self, intrinsicDeflectionData)
            filteredResults = cell(size(intrinsicDeflectionData));
            filterSettings = FilterSettings();
            settings = ApplicationSettings.Instance();
            settings.TemplateMatching.OverlapFilterEnabled = true;
            
            amplitudePercentage = self.FractionationThresholds.amplitude / 100;
            slopePercentage = self.FractionationThresholds.slope / 100;
            closeThreshold = self.FractionationThresholds.close;
            
            fractionationIndices = NaN(numel(intrinsicDeflectionData), 1);
            parfor channelIndex = 1:numel(intrinsicDeflectionData)
                intrinsicDeflections = intrinsicDeflectionData{channelIndex}.PrimaryDeflections;
                
                intrinsicAmplitudes = intrinsicDeflectionData{channelIndex}.templatePeakAmplitudes(intrinsicDeflections);
                amplitudeMedian = median(intrinsicAmplitudes);
                
                intrinsicSlopes = intrinsicDeflectionData{channelIndex}.deflectionPeakSlopes(intrinsicDeflections);
                slopeMedian = median(intrinsicSlopes);
                
                currentFilterSettings = filterSettings;
                currentFilterSettings.amplitudeThreshold = amplitudeMedian * amplitudePercentage;
                currentFilterSettings.slopeThreshold = slopeMedian * slopePercentage;
                deflectionFilter = AlgorithmPkg.DeflectionFilter(currentFilterSettings);
                filteredResult = deflectionFilter.Apply(intrinsicDeflectionData{channelIndex});
                
                filteredResults{channelIndex} = filteredResult;
                numberOfIntrinsicDeflections = numel(find(intrinsicDeflections));
                farFieldDeflections = ~filteredResult.PrimaryDeflections;
                    
                intrinsicDeflectionTimes = intrinsicDeflectionData{channelIndex}.templatePeakIndices(intrinsicDeflections) / intrinsicDeflectionData{channelIndex}.samplingFrequency;
                farfieldDeflectionTimes = filteredResult.templatePeakIndices(farFieldDeflections) / intrinsicDeflectionData{channelIndex}.samplingFrequency;
                validFarFieldDeflections = true(size(farfieldDeflectionTimes));
                for deflectionIndex = 1:numberOfIntrinsicDeflections
                    validFarFieldDeflections(...
                        farfieldDeflectionTimes >= intrinsicDeflectionTimes(deflectionIndex) - closeThreshold &...
                        farfieldDeflectionTimes <= intrinsicDeflectionTimes(deflectionIndex) + closeThreshold) = false;
                end
                numberOfFarFieldDeflections = numel(find(validFarFieldDeflections));
                fractionationIndices(channelIndex) = numberOfFarFieldDeflections / numberOfIntrinsicDeflections;
            end
            intrinsicDeflectionResult = filteredResults;
        end
        
        function detectedWaves = ComputeWavemap(self, ddData, intrinsicDeflectionResult)
            wavemapCalculator = AlgorithmPkg.WavemapCalculator();
            
            minimumSpacing = ddData.ecgData.ComputeMinimumSpacing();
            wavemapCalculator.NeighborRadius = sqrt(2 * (minimumSpacing + 1e3 * eps)^2);
            wavemapCalculator.MergeSearchRadius = 2 * wavemapCalculator.NeighborRadius;
            wavemapCalculator.ConductionThreshold = self.WavemapSettings.minimalConductionVelocity;
            wavemapCalculator.MergeOverlapThreshold = self.WavemapSettings.mergeOverlapThreshold;
            
            settings = ApplicationSettings.Instance;
            settings.VelocityComputation.radius = 2 * sqrt(2 * (minimumSpacing + 1e3 * eps)^2);
            settings.VelocityComputation.hierarchical = false;
            
            detectedWaves = wavemapCalculator.DetectWaves(ddData.ecgData, intrinsicDeflectionResult);
        end
        
        % Complexity parameter computation (based on waves)
        function waveData = WaveAnalysisData(self, wavemapData)
            minimumSpacing = wavemapData.ComputeMinimumSpacing();
            radius = sqrt(2 * minimumSpacing^2) + 1e3 * eps;
            
            settings = ApplicationSettings.Instance;
            settings.VelocityComputation.radius = 2 * sqrt(2 * (minimumSpacing + 1e3 * eps)^2);
            settings.VelocityComputation.hierarchical = false;
            
            waveData = AlgorithmPkg.WavemapAnalyzer.ComputeWaveCharacteristics(wavemapData.Waves,...
                wavemapData.ElectrodePositions,...
                self.WaveAnalysisThresholds.minimumWaveSize, radius);
            
            [dissociationValues, ~, maxDissociation, electrodeDissociation] =...
                AlgorithmPkg.AFComplexityCalculator.ComputeDissociationData(wavemapData, wavemapData.Deflections, radius);
            waveData.MeanDissociation = dissociationValues;
            waveData.MaxDissociation = maxDissociation;
            waveData.ElectrodeDissociation = electrodeDissociation;
        end
        
        function mapData = WaveDataMaps(self, wavemapData)
            numberOfStartingPointsPW = zeros(size(wavemapData.ElectrodeLabels));
            numberOfStartingPointsBT = zeros(size(wavemapData.ElectrodeLabels));
            for waveIndex = 1:numel(wavemapData.Waves)
                wave = wavemapData.Waves(waveIndex);
                if wave.Size < self.WaveAnalysisThresholds.minimumWaveSize, continue; end
                startingPoints = wave.ComputeAllStartingPoints();
                if wave.Peripheral
                    numberOfStartingPointsPW(startingPoints(:, 1)) = numberOfStartingPointsPW(startingPoints(:, 1)) + 1;
                else
                    numberOfStartingPointsBT(startingPoints(:, 1)) = numberOfStartingPointsBT(startingPoints(:, 1)) + 1;
                end
            end
            
            mapData = struct(...
                'NumberOfStartingPointsPW', numberOfStartingPointsPW,...
                'NumberOfStartingPointsBT', numberOfStartingPointsBT);
        end
        
        function conductionData = ConductionAnalysisData(self, electrodeVelocities)
            analyzer = AlgorithmPkg.VelocityAnalyzer();
            conductionData.Preferentiality = analyzer.ComputePreferentialVelocities(electrodeVelocities);
            conductionData.UnidirectionalPreferentiality = analyzer.ComputePreferentialVelocities(electrodeVelocities, false);
            conductionData.Anisotropy = analyzer.ComputeElectrodesAnisotropy(electrodeVelocities);
        end
    end
end