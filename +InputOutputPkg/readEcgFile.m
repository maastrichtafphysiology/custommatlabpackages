function ecgData = readEcgFile(filename)
filedata = importdata(filename);
beginTimePos = strfind(filedata.textdata{1}, 'BeginTime:');
endTimePos = strfind(filedata.textdata{1}, 'EndTime:');
msPos = strfind(filedata.textdata{1}, 'ms');

if isempty(beginTimePos) || isempty(endTimePos) || size(msPos,1)~=1 || size(msPos,2)~=2
    ME = MException('readEcgFile', ...
       'File format is not valid');
    throw(ME);
end

beginTime = str2double(filedata.textdata{1}(size('BeginTime:',2)+beginTimePos:msPos(1)-1));
endTime = str2double(filedata.textdata{1}(size('EndTime:',2)+endTimePos:msPos(2)-1));
filedata.data(1,:)=[];
sampleFrequency = size(filedata.data,1) / ((endTime-beginTime) / 1000);
ecgData = ECGData(filename, filedata.data, sampleFrequency);