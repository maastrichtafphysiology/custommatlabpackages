classdef ECGReader < handle
    properties (Constant)
        NUMBER_OF_PACEMAP_ELECTRODES = 256;
    end
    methods (Static)
        function ecgData = ReadBDF(filename, samplingFrequency, secondsToLoad)
            if nargin < 3
                bdfInfo = InputOutputPkg.ECGReader.ReadBDFInfo(filename);
                recordingDuration = bdfInfo.NumberOfRecords * bdfInfo.RecordDuration;
                prompt = {'Start (seconds)','End (seconds'};
                dialogTitle = 'Select interval to load';
                editFieldDimensions = [1;1];
                defaultInput = {'0', num2str(recordingDuration)};
                answer = inputdlg(prompt, dialogTitle, editFieldDimensions, defaultInput);
                secondsToLoad = [str2double(answer{1}), str2double(answer{2})];
                
                if nargin < 2 || isnan(samplingFrequency)
                    samplingFrequency = bdfInfo.SamplingFrequency;
                    
                    if samplingFrequency == 2048
                        fsOptions = [256; 512; 1024; 2048];
                        numberOfFs = numel(fsOptions);
                        optionString = cell(numberOfFs, 1);
                        for fsIndex = 1:numberOfFs
                            optionString{fsIndex} = [num2str(fsOptions(fsIndex), '%.0f'), 'Hz'];
                        end
                        choice = menu('Choose sampling frequency', optionString);
                        samplingFrequency = fsOptions(choice);
                    end
                end
            end
            %             ecgData = InputOutputPkg.readBDF(filename, 'resampleFrequency', samplingFrequency,...
            %                 'secondsToLoad', [120, 180]);
            ecgData = InputOutputPkg.readBDF(filename, 'resampleFrequency', samplingFrequency,...
                'secondsToLoad', secondsToLoad);
        end
        
        function bdfInfo = ReadBDFInfo(filename)
            fid = fopen(filename);
            
            % user text
            fseek(fid, 130, 'bof');
            subjectInfo = cellstr(char(fread(fid, 54, '10*char'))');
            
            % number of data records
            fseek(fid, 236, 'bof');
            numberOfRecords = str2double(deblank(char(fread(fid, 7, '1*char'))));
            
            % duration of a data record in seconds
            fseek(fid, 244, 'bof');
            recordDuration = str2double(deblank(char(fread(fid, 3, '1*char'))));
            
            % number of EEG channels
            fseek(fid, 252, 'bof');
            numberOfChannels = str2double(deblank(char(fread(fid, 3, '1*char'))));
            
            % EEG electrodeLabels
            fseek(fid, 256, 'bof');
            electrodeLabels = cellstr(deblank(char(fread(fid, [7, numberOfChannels], '7*char', 9)')));
            
            % filters applied - need to check from this point
            fseek(fid, 256 + numberOfChannels * 136, 'bof');
            prefiltering = cellstr(char(fread(fid, 25, '10*char'))');
            
            % physical dimension of the channels (uV,  for instance)
            fseek(fid, 256 + numberOfChannels * 96, 'bof');
            channelDimensions = cellstr(deblank(char(fread(fid, 5, '5*char'))'));
            
            % number of samples in each record
            fseek(fid, 256 + numberOfChannels * 216, 'bof');
            numberOfSamples = str2double(deblank(char(fread(fid, 4, '1*char'))));
            
            % sampling frequency
            samplingFrequency = numberOfSamples / recordDuration;
            
            bdfInfo = struct(...
                'SamplingFrequency', samplingFrequency,...
                'ElectrodeLabels', {electrodeLabels},...
                'SubjectInfo', subjectInfo,...
                'Prefiltering', prefiltering,...
                'ChannelDimensions', channelDimensions,...
                'NumberOfRecords', numberOfRecords,...
                'RecordDuration', recordDuration);
        end
        
        function ecgData = ReadDCMBDF(filename)
            dcmInfo = InputOutputPkg.ECGReader.ReadDCMBDFInfo(filename);
            
            intervalToLoad = [0, dcmInfo.NumberOfSamples / dcmInfo.SamplingFrequency];
            if ~isempty(dcmInfo.StatusIndex)
                numberOfSegments = size(dcmInfo.SignalSegments, 1);
                optionString = cell(numberOfSegments + 1, 1);
                timeIntervals = dcmInfo.SignalSegments / dcmInfo.SamplingFrequency;
                timeIntervals = [timeIntervals; intervalToLoad];
                for segmentIndex = 1:numberOfSegments
                    optionString{segmentIndex} = [num2str(timeIntervals(segmentIndex, 1), '%.1f'), 's to ',...
                        num2str(timeIntervals(segmentIndex, 2), '%.1f'), 's'];
                end
                optionString{end} = ['0s to', num2str(dcmInfo.NumberOfSamples / dcmInfo.SamplingFrequency, '%.1f'), 's (All)'];
                choice = menu('Choose interval', optionString);
                intervalToLoad = timeIntervals(choice, :);
            end
            
            ecgData = InputOutputPkg.readDCMBDF(filename,...
                'resampleFrequency', dcmInfo.SamplingFrequency,...
                'secondsToLoad', intervalToLoad);
        end
        
        function dcmInfo = ReadDCMBDFInfo(filename)
            fid = fopen(filename);
            
            % user text
            fseek(fid, 130, 'bof');
            subjectInfo = cellstr(char(fread(fid, 54, '10*char'))');
            
            % number of data records
            fseek(fid, 236, 'bof');
            numberOfRecords = str2double(deblank(char(fread(fid, 7, '1*char'))));
            
            % duration of a data record in seconds
            fseek(fid, 244, 'bof');
            recordDuration = str2double(deblank(char(fread(fid, 3, '1*char'))));
            
            % number of EEG channels
            fseek(fid, 252, 'bof');
            numberOfChannels = str2double(deblank(char(fread(fid, 3, '1*char'))));
            
            % EEG electrodeLabels
            fseek(fid, 256, 'bof');
            electrodeLabels = cellstr(deblank(char(fread(fid, [7, numberOfChannels], '7*char', 9)')));
            dcmInfo.ElectrodeLabels = electrodeLabels;
            
            % Find and read status channel
            statusIndex = find(strcmpi('Status', electrodeLabels));
            dcmInfo.StatusIndex = statusIndex;
            
            % filters applied - need to check from this point
            fseek(fid, 256 + numberOfChannels * 136, 'bof');
            prefiltering = cellstr(char(fread(fid, 25, '10*char'))');
            dcmInfo.Prefiltering = prefiltering;
            
            % physical dimension of the channels (uV,  for instance)
            fseek(fid, 256 + numberOfChannels * 96, 'bof');
            channelDimensions = cellstr(deblank(char(fread(fid, 5, '5*char'))'));
            
            % number of samples in each record
            fseek(fid, 256 + numberOfChannels * 216, 'bof');
            numberOfSamples = str2double(deblank(char(fread(fid, 4, '1*char'))));
            
            % sampling frequency
            samplingFrequency = numberOfSamples / recordDuration;
            
            dcmInfo.SamplingFrequency = samplingFrequency;
            
            startRecord = 1;
            recordsToLoad = numberOfRecords;
            dcmInfo.NumberOfSamples = recordDuration  * numberOfSamples  *  recordsToLoad;
            
            if ~isempty(statusIndex)
                % positions the pointer to the beginning of data
                % fseek(fid, 256 * (numberOfChannels + 1), 'bof');
                startPosition = (startRecord - 1) * recordDuration * numberOfSamples * numberOfChannels * 3;
                fseek(fid, 256 * (numberOfChannels + 1) + startPosition, 'bof');
                fseek(fid, recordDuration * numberOfSamples * (statusIndex - 1) * 3, 'cof');
                
                statusData = NaN(recordDuration  * numberOfSamples  *  recordsToLoad, 1);
                
                UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                    UserInterfacePkg.StatusChangeEventData('Reading BDF data segments...', 0, []));
                for recordIndex = 1:recordsToLoad
                    aux = fread(fid, [recordDuration * numberOfSamples, 1], 'bit24');
                    dataPosition = (recordDuration  *  numberOfSamples)  *  (recordIndex - 1)  +  1;
                    statusData(dataPosition:(dataPosition  +  recordDuration  *  numberOfSamples - 1)) = aux;
                    
                    fseek(fid, recordDuration * numberOfSamples * (numberOfChannels - 1) * 3, 'cof');
                end
                UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                    UserInterfacePkg.StatusChangeEventData('Done', 1, []));
                
                statusSegmentStartIndices = find(diff(statusData) < -eps);
                statusSegmentEndIndices = find(diff(statusData) > eps);
                
                signalSegments = [statusSegmentStartIndices(:), NaN(numel(statusSegmentStartIndices), 1)];
                for segmentStartIndex = 1:numel(statusSegmentStartIndices)
                    nextEndIndex = find(statusSegmentEndIndices > statusSegmentStartIndices(segmentStartIndex), 1, 'first');
                    if ~isempty(nextEndIndex)
                        signalSegments(segmentStartIndex, 2) = statusSegmentEndIndices(nextEndIndex);
                    else
                        signalSegments(segmentStartIndex, 2) = numel(statusData);
                    end
                end
                
                dcmInfo.SignalSegments = signalSegments;
            end
        end
        
        function ecgData = ReadPACEMAPEcg(filename)
            filedata = importdata(filename);
            beginTimePos = strfind(filedata.textdata{1}, 'BeginTime:');
            endTimePos = strfind(filedata.textdata{1}, 'EndTime:');
            msPos = strfind(filedata.textdata{1}, 'ms');
            
            if isempty(beginTimePos) || isempty(endTimePos) || size(msPos,1) ~= 1 || size(msPos,2) ~= 2
                ME = MException('ReadPACEMAPEcg', 'File format is not valid');
                throw(ME);
            end
            
            beginTime = str2double(filedata.textdata{1}(size('BeginTime:', 2) + beginTimePos:msPos(1) - 1));
            endTime = str2double(filedata.textdata{1}(size('EndTime:', 2) + endTimePos:msPos(2) - 1));
            filedata.data(1, :) = [];
            samplingFrequency = size(filedata.data, 1) / ((endTime - beginTime) / 1000);
            electrodeLabels = cellstr(num2str((1:size(filedata.data, 2))'));
            ecgData = DCMData(filename, filedata.data, samplingFrequency, electrodeLabels);
            ecgData.SetElectrodeMap('');
        end
        
        function ecgData = ReadPACEMAPBinary(filename, electrodeMap, resample)
            if nargin < 3
                resample = true;
                if nargin < 2
                    electrodeMap = '';
                end
            end
            fileHandle = fopen(filename, 'r');
            
%             textData = fread(fileHandle, 2048, '*char');
            fseek(fileHandle, 2048, 'bof'); % skip header
            data = fread(fileHandle, 'int16=>double');
            
            numberOfElectrodes = InputOutputPkg.ECGReader.NUMBER_OF_PACEMAP_ELECTRODES;
            numberOfSamples = numel(data) / numberOfElectrodes;
            data = reshape(data, numberOfElectrodes, numberOfSamples);
            data = data(:, 5:end);
            numberOfSamples = size(data, 2);
            
            samplingFrequency = 1e3;
            if resample
                blockSize = 16;
                resampledData = data;
                for electrodeIndex = 1:numberOfElectrodes
                    blockIndex = mod((electrodeIndex + blockSize - 1), blockSize);
                    if blockIndex > 0
                        electrodeSamples = (1:numberOfSamples) + blockIndex / blockSize;
                        resampledData(electrodeIndex, :) = interp1(electrodeSamples, data(electrodeIndex, :), 1:numberOfSamples,...
                            'linear', 'extrap');
                    end
                end
                data = resampledData;
            end
            
            electrodeLabels = cellstr(num2str((1:numberOfElectrodes)'));
            ecgData = DCMData(filename, data', samplingFrequency, electrodeLabels);
            ecgData.SetElectrodeMap(electrodeMap);
        end
        
        function ecgData = ReadPACEMAPBinarySeries(filename, electrodeMap)
            if nargin < 2
                electrodeMap = '';
            end
            [pathname, shortFilename] = fileparts(filename);
            listing = dir(fullfile(pathname, [shortFilename, '.E*']));
            filenames = {listing.name};
            
            mergedData = [];
            for fileIndex = 1:numel(filenames)
                currentFilename = fullfile(pathname, filenames{fileIndex});
                fileHandle = fopen(currentFilename, 'r');
                
                textData = fread(fileHandle, 2048, '*char');
                data = fread(fileHandle, 'int16');
                
                numberOfElectrodes = InputOutputPkg.ECGReader.NUMBER_OF_PACEMAP_ELECTRODES;
                numberOfSamples = numel(data) / numberOfElectrodes;
                mergedData = [mergedData, reshape(data, numberOfElectrodes, numberOfSamples)]; %#ok<AGROW>
            end
            
            samplingFrequency = 1e3;
            electrodeLabels = cellstr(num2str((1:numberOfElectrodes)'));
            
            ecgData = DCMData(filename, mergedData', samplingFrequency, electrodeLabels);
            ecgData.SetElectrodeMap(electrodeMap);
        end
        
        function ecgData = ReadTVIEeg(filename)
            filedata = importdata(filename);
            
            time = filedata.data(:, 1);
            validTimePositions = ~isnan(time);
            time = time(validTimePositions);
            samplingFrequency = round(1 / mean(diff(time)));
            %             tracePositions = strncmp('Trace', filedata.textdata(3, :), 5);
            tracePositions = 2:size(filedata.textdata, 2);
            electrodeLabels = filedata.textdata(3, tracePositions);
            data = filedata.data(validTimePositions, tracePositions);
            
            upsampledFrequency = ceil(1e3 / samplingFrequency) * samplingFrequency;
            data = resample(data, ceil(1e3 / samplingFrequency), 1);
            
            ecgData = ECGData(filename, data, upsampledFrequency, electrodeLabels);
        end
        
        function ecgData = ReadPhysionetCSV(filename)
            data = dlmread(filename, '\t');
            time = data(:, 1);
            samplingFrequency = 1 / mean(diff(time));
            upsampledFrequency = ceil(1e3 / samplingFrequency) * samplingFrequency;
            data = data(:, 2:end);
            data = resample(data, ceil(1e3 / samplingFrequency), 1);
            numberOfElectrodes = size(data, 2);
            electrodeLabels = cellstr(num2str((1:numberOfElectrodes)'));
            ecgData = ECGData(filename, data, upsampledFrequency, electrodeLabels);
        end
        
        function ecgData = ReadMuensterXML(filename)
            xmlData = xmlread(filename);
            
            stripData = xmlData.getElementsByTagName('StripData');
            sequences = stripData.item(0).getElementsByTagName('WaveformData');
            numberOfSequences = sequences.getLength;
            signals = cell(numberOfSequences, 1);
            signalLabels = cell(numberOfSequences, 1);
            for sequenceIndex = 0:(numberOfSequences - 1)
                sequenceNode = sequences.item(sequenceIndex);
                
                attributes = sequenceNode.getAttributes;
                signalLabels{sequenceIndex + 1} = char(attributes.item(0).getValue);
                data = textscan(char(sequenceNode.getFirstChild.getData), '%f', 'delimiter', ',');
                signals{sequenceIndex + 1} = data{:}';
            end
            signals = vertcat(signals{:})';
            samplingFrequency = 500;
            ecgData = ECGData(filename, signals, samplingFrequency, signalLabels);
        end
        
        function ecgData = ReadOxfordXML(filename)
            xmlData = xmlread(filename);
            
            %             stripData = xmlData.getElementsByTagName('StripData');
            stripData = xmlData.getElementsByTagName('ArrhythmiaData');
            sequences = stripData.item(0).getElementsByTagName('WaveformData');
            
            numberOfSequences = sequences.getLength;
            signals = cell(numberOfSequences, 1);
            signalLabels = cell(numberOfSequences, 1);
            for sequenceIndex = 0:(numberOfSequences - 1)
                sequenceNode = sequences.item(sequenceIndex);
                
                attributes = sequenceNode.getAttributes;
                signalLabels{sequenceIndex + 1} = char(attributes.item(0).getValue);
                data = textscan(char(sequenceNode.getFirstChild.getData), '%f', 'delimiter', ',');
                signals{sequenceIndex + 1} = data{:}';
            end
            signals = vertcat(signals{:})';
            samplingFrequency = 500;
            ecgData = ECGData(filename, signals, samplingFrequency, signalLabels);
        end
        
        function ecgData = ReadBirminghamXML(filename)
            xmlData = xmlread(filename);
            
            % signals
            sequences = xmlData.getElementsByTagName('sequence');
            numberOfSequences = sequences.getLength - 1;
            signals = cell(numberOfSequences, 1);
            signalLabels = cell(numberOfSequences, 1);
            for sequenceIndex = 1:(numberOfSequences)
                sequenceNode = sequences.item(sequenceIndex);
                
                codes = sequenceNode.getElementsByTagName('code');
                attributes = codes.item(0).getAttributes;
                signalLabels{sequenceIndex} = char(attributes.item(0).getValue);
                
                digits = sequenceNode.getElementsByTagName('digits');
                if digits.getLength > 0
                    signals{sequenceIndex} = str2num(char(digits.item(0).getFirstChild.getData)); %#ok<ST2NM>
                end
            end
            signals = vertcat(signals{:})';
            
            % sampling frequency
            samplingFrequency = 250;
            controlVariables = xmlData.getElementsByTagName('controlVariable');
            for variableIndex = 1:controlVariables.getLength
                variableNode = controlVariables.item(variableIndex);
                
                codes = variableNode.getElementsByTagName('code');
                attributes = codes.item(0).getAttributes;
                variableName = char(attributes.item(0).getValue);
                if ~strcmp('MDC_ECG_CTL_VBL_SAMPLE_RATE', variableName), continue; end
                
                values = variableNode.getElementsByTagName('value');
                attributes = values.item(0).getAttributes;
                samplingFrequency = str2double(char(attributes.item(1).getFirstChild.getData));
                break;
            end
            
            ecgData = ECGData(filename, signals, samplingFrequency, signalLabels);
        end
        
        function ecgData = ReadMUMCBase64XML(filename)
            %check and correct xml encoding
            fileHandle = fopen(filename, 'r');
            lineText = fgetl(fileHandle);
            lineNumber = 1;
            while(ischar(lineText))
                fileData{lineNumber} = lineText;
                lineText = fgetl(fileHandle);
                lineNumber = lineNumber + 1;
            end
            fclose(fileHandle);
            
            encodingText = fileData{1};
            correctedEncoding = false;
            position = strfind(encodingText, 'ISO8859-1');
            if ~isempty(position);
                correctedEncodingLine = [encodingText(1:(position-1)),...
                    'ISO-8859-1',...
                    encodingText((position + 9):end)];
                fileData{1} = correctedEncodingLine;
                correctedEncoding = true;
            end
            
            doctypeText = fileData{2};
            position = strfind(doctypeText, 'DOCTYPE');
            removedDocType = false;
            if ~isempty(position);
                fileData{2} = [];
                removedDocType = true;
            end
            
            if correctedEncoding || removedDocType
                fileHandle = fopen(filename, 'w');
                fprintf(fileHandle, '%s\n', fileData{:});
                fclose(fileHandle);
            end
            
            xmlData = xmlread(filename);
            base64 = org.apache.commons.codec.binary.Base64;
            
            stripData = xmlData.getElementsByTagName('Waveform');
            
            try
                sampleFrequencyNodes = stripData.item(1).getElementsByTagName('SampleBase');
            catch
                sampleFrequencyNodes = stripData.item(0).getElementsByTagName('SampleBase');
            end
            sampleFrequencyNode = sampleFrequencyNodes.item(0);
            samplingFrequency = str2double(char(sampleFrequencyNode.getFirstChild.getData));
            
            try
                sequences = stripData.item(1).getElementsByTagName('LeadData');
            catch
                sequences = stripData.item(0).getElementsByTagName('LeadData');
            end
            numberOfSequences = sequences.getLength;
            signals = cell(numberOfSequences, 1);
            signalLabels = cell(numberOfSequences, 1);
            
            for sequenceIndex = 0:(numberOfSequences - 1)
                sequenceNode = sequences.item(sequenceIndex);
                
                labelNodes = sequenceNode.getElementsByTagName('LeadID');
                labelNode = labelNodes.item(0);
                signalLabels{sequenceIndex + 1} = char(labelNode.getFirstChild.getData);
                
                gainNodes = sequenceNode.getElementsByTagName('LeadAmplitudeUnitsPerBit');
                gainNode = gainNodes.item(0);
                gain = str2double(char(gainNode.getFirstChild.getData));
                
                dataNodes = sequenceNode.getElementsByTagName('WaveFormData');
                dataNode = dataNodes.item(0);
                base64Data = dataNode.getFirstChild.getData;
                data = typecast(base64.decodeBase64(base64Data.getBytes()), 'int16');
                
                % convert signals to mV
                signals{sequenceIndex + 1} = gain * double(data(:)') / 1e3;
            end
            signals = vertcat(signals{:})';
            
            leadI = strcmp('I', signalLabels);
            leadII = strcmp('II', signalLabels);
            
            numberfOfSamples = size(signals, 1);
            time = (0:(numberfOfSamples - 1)) / samplingFrequency;
            exportData = array2table([time', signals], 'variableNames', [{'Time'}; signalLabels]);
            writetable(exportData, [filename(1:(end-3)), 'txt']);
            if any(leadI) && any(leadII)
                leadIII = signals(:, leadII) - signals(:, leadI);
                leadAVR = -(signals(:, leadI) + signals(:, leadII)) / 2;
                leadAVL = signals(:, leadI) - signals(:, leadII) / 2;
                leadAVF = signals(:, leadII) - signals(:, leadI) / 2;
                
                leadIIPosition = find(leadII);
                signals = [signals(:, 1:leadIIPosition),...
                    leadIII, leadAVR, leadAVL, leadAVF,...
                    signals(:, (leadIIPosition + 1):end)];
                signalLabels = [signalLabels(1:leadIIPosition);...
                    'III'; 'aVR'; 'aVL'; 'aVF';...
                    signalLabels((leadIIPosition + 1):end)];
            end
            ecgData = ECGData(filename, signals, samplingFrequency, signalLabels);
        end
        
        function ecgData = ReadCardioSoftFullDisclosure(filename)
            %check and correct xml encoding
            fileHandle = fopen(filename, 'r');
            lineText = fgetl(fileHandle);
            lineNumber = 1;
            while(ischar(lineText))
                fileData{lineNumber} = lineText;
                lineText = fgetl(fileHandle);
                lineNumber = lineNumber + 1;
            end
            fclose(fileHandle);
            
            encodingText = fileData{1};
            correctedEncoding = false;
            position = strfind(encodingText, 'ISO8859-1');
            if ~isempty(position);
                correctedEncodingLine = [encodingText(1:(position-1)),...
                    'ISO-8859-1',...
                    encodingText((position + 9):end)];
                fileData{1} = correctedEncodingLine;
                correctedEncoding = true;
            end
            
            doctypeText = fileData{2};
            position = strfind(doctypeText, 'DOCTYPE');
            removedDocType = false;
            if ~isempty(position);
                fileData{2} = [];
                removedDocType = true;
            end
            
            if correctedEncoding || removedDocType
                fileHandle = fopen(filename, 'w');
                fprintf(fileHandle, '%s\n', fileData{:});
                fclose(fileHandle);
            end
            
            xmlData = xmlread(filename);
            
            fullDisclosureData = xmlData.getElementsByTagName('FullDisclosure');
            
            sampleFrequencyNodes = fullDisclosureData.item(0).getElementsByTagName('SampleRate');
            sampleFrequencyNode = sampleFrequencyNodes.item(0);
            samplingFrequency = str2double(char(sampleFrequencyNode.getFirstChild.getData));
            
            numberOfChannelsNode = fullDisclosureData.item(0).getElementsByTagName('NumberOfChannels');
            numberOfChannelsNode = numberOfChannelsNode.item(0);
            numberOfChannels = str2double(char(numberOfChannelsNode.getFirstChild.getData));
            
            resolutionNode = fullDisclosureData.item(0).getElementsByTagName('Resolution');
            resolutionNode = resolutionNode.item(0);
            resolution = str2double(char(resolutionNode.getFirstChild.getData));
            
            leadOrderNode = fullDisclosureData.item(0).getElementsByTagName('LeadOrder');
            leadOrderNode = leadOrderNode.item(0);
            leadOrder = char(leadOrderNode.getFirstChild.getData);
            signalLabels = strsplit(leadOrder, ',');
            
            dataNode = fullDisclosureData.item(0).getElementsByTagName('FullDisclosureData');
            dataNode = dataNode.item(0);
            bitData = textscan(char(dataNode.getFirstChild.getData), '%f', 'delimiter', ',');
            bitData = bitData{:};
            bitData = bitData(~isnan(bitData));
            
            % samples stored in blocks per seconds (for all channels)
            numberOfSamples = round(numel(bitData) / numberOfChannels);
            numberOfSeconds = floor(numberOfSamples / samplingFrequency);
            
            blockData = reshape(bitData, numberOfChannels * samplingFrequency, numberOfSeconds);
            
            signals = NaN(numberOfSamples, numberOfChannels);
            for channelIndex = 1:numberOfChannels
                channelIndexStart = (channelIndex - 1) * samplingFrequency + 1;
                channelIndexEnd = channelIndex * samplingFrequency;
                channelData = blockData(channelIndexStart:channelIndexEnd, :);
                signals(:, channelIndex) = channelData(:);
            end
            
            signals = signals * resolution;
            
            ecgData = ECGData(filename, signals, samplingFrequency, signalLabels);
        end
        
        function ecgData = ReadSchillerXML(filename)
            xmlData = xmlread(filename);
            
            waveData = xmlData.getElementsByTagName('wavedata');
            numberOfWaveDataTags = waveData.getLength;
            ecgRhythmIndex = 1;
            ecgRhythmFound = false;
            while ~ecgRhythmFound && ecgRhythmIndex <= numberOfWaveDataTags,
                waveDataNode = waveData.item(ecgRhythmIndex - 1);
                type = waveDataNode.getElementsByTagName('type');
                typeName = type.item(0).getFirstChild.getData;
                if strcmp(typeName, 'ECG_RHYTHMS')
                    ecgRhythmFound = true;
                    break;
                else
                    ecgRhythmIndex = ecgRhythmIndex + 1;
                end
            end
            
            if ~ecgRhythmFound
                ecgData = [];
                return;
            end
            
            % channel labels and data
            channelNodeData = waveDataNode.getElementsByTagName('channel');
            numberOfChannels = channelNodeData.getLength;
            channelData = cell(numberOfChannels, 1);
            channelLabels = cell(numberOfChannels, 1);
            for channelIndex = 1:numberOfChannels
                channelNode = channelNodeData.item(channelIndex - 1);
                nameNode = channelNode.getElementsByTagName('name');
                channelLabels{channelIndex} = char(nameNode.item(0).getFirstChild.getData);
                dataNode = channelNode.getElementsByTagName('data');
                channelData{channelIndex} = str2num(char(dataNode.item(0).getFirstChild.getData)); %#ok<ST2NM>
                channelData{channelIndex} = channelData{channelIndex} / 1000;
            end
            channelData = vertcat(channelData{:})';
            
            % sampling frequency
            sampleRateNode = waveDataNode.getElementsByTagName('samplerate');
            valueNode = sampleRateNode.item(0).getElementsByTagName('value');
            samplingFrequency = str2double(char(valueNode.item(0).getFirstChild.getData));
            
            ecgData = ECGData(filename, channelData, samplingFrequency, channelLabels);
        end
        
        function ecgData = ReadYRXML(filename)
            xmlData = xmlread(filename);
            samplingFrequency = 250;
            
            numberOfChannels = 19;
            signalLabels = {'AUX1', 'AUX2', 'AUX3', 'AUX4', 'AUX5',...
                'TE1', 'TE2', 'TE3', 'TE4',...
                'V1', 'V2', 'V3', 'V4', 'V5', 'V6',...
                'LA', 'RA', 'LL',...
                'Channel19'};
            signals = cell(numberOfChannels, 1);
            validChannels = true(numberOfChannels, 1);
            for channelIndex = 1:numberOfChannels
                channelName = ['Channel', num2str(channelIndex - 1)];
                %                 try
                disp(channelName);
                channelData = xmlData.getElementsByTagName(channelName);
                stringData = char(channelData.item(0).getFirstChild.getData);
                %                     xyData = strsplit(stringData, {'{', '}, {'});
                %                     xyData = xyData(2:(end-1));
                %                     numberOfSamples = numel(xyData);
                %                     yData = NaN(numberOfSamples, 1);
                %                     for sampleIndex = 1:1:numel(xyData)
                %                         xyString = strsplit(xyData{sampleIndex}, ',');
                %                         yString = xyString{2};
                %                         yData(sampleIndex) = str2double(yString(4:end));
                %                     end
                
                yData = textscan(stringData, '%f', 'delimiter', ',');
                
                signals{channelIndex} = yData{:};
                
                %                 catch ME
                %                     validChannels(channelIndex) = false;
                %                     disp(ME);
                %                     disp([channelName, ' not found']);
                %                 end
            end
            
            signals = signals(validChannels);
            signals = horzcat(signals{:});
            signalLabels = signalLabels(validChannels);
            ecgData = BDFData(filename, signals, samplingFrequency, signalLabels(:));
        end
        
        function ecgData = ReadYRTXT(filename)
            fileID = fopen(filename);
            
            % Header information
            timeFound = false;
            lineIndex = 1;
            while ~(timeFound)
                textLine = fgetl(fileID);
                timeFound = ~isempty(strfind(textLine, 'Time (s)'));
                lineIndex = lineIndex + 1;
            end
            frewind(fileID);
            
            if ~timeFound, ecgData = []; return; end
            
            labelLine = textLine;
            labels = strsplit(labelLine, '\t');
            
            numberOfChannels = numel(labels) - 1;
            stringFormat = repmat('%s ', 1, numberOfChannels + 1);
            textData = textscan(fileID, stringFormat, 'headerLines', lineIndex - 1, 'delimiter', '\t');
            fclose(fileID);
            
            data = cell(1, numberOfChannels);
            parfor channelIndex = 1:numberOfChannels
                data{channelIndex} = str2double(strrep(textData{channelIndex + 1}, ',', '.'));
            end
            data = horzcat(data{:});
            samplingFrequency = 2e03;
            channelLabels = labels(2:end);
            
            ecgData = ECGData(filename, data, samplingFrequency, channelLabels);
        end
        
        function ecgData = ReadBordeauxBSPM(filename, interval)
            if nargin < 2
                interval = [];
            end
            fileHandle = fopen(filename, 'r');
            
            numberOfElectrodes = 256;
            samplingFrequency = 1e3;
            if isempty(interval)
                data = fread(fileHandle, [numberOfElectrodes, Inf], 'float');
            else
                offset = ceil(numberOfElectrodes * (samplingFrequency * interval(1))) / 4;
                numberOfSamples = ceil((interval(2) - interval(1)) * samplingFrequency);
                fseek(fileHandle, offset, 'bof');
                data = fread(fileHandle, [numberOfElectrodes, numberOfSamples], 'float');
            end
            
            electrodeLabels = cellstr(num2str((1:numberOfElectrodes)'));
            ecgData = BDFData(filename, data', samplingFrequency, electrodeLabels);
        end
        
        function ecgData = ReadBARDECG(filename)
            fileID = fopen(filename);
            
            % Header information
            dataFound = false;
            lineIndex = 1;
            while ~(dataFound)
                textLine = fgetl(fileID);
                dataFound = ~isempty(strfind(textLine, '[Data]'));
                lineIndex = lineIndex + 1;
            end
            frewind(fileID);
            
            dataIndex = lineIndex;
            textData = cell(dataIndex - 2, 1);
            for lineIndex = 1:(dataIndex - 2)
                textData{lineIndex} = fgetl(fileID);
            end
            
            channelInfoIndices = find(~cellfun(@isempty, strfind(textData, 'Channel #')));
            numberOfChannels = numel(channelInfoIndices);
            channelLabels = cell(numberOfChannels, 1);
            for channelInfoIndex = 1:numel(channelInfoIndices)
                channelLabelString = textData{channelInfoIndices(channelInfoIndex) + 1};
                stringParts = textscan(channelLabelString, '%s');
                channelLabels{channelInfoIndex} = horzcat(stringParts{1}{2:end});
            end
            
            sampleRateIndices = find(~cellfun(@isempty, strfind(textData, 'Sample Rate')));
            sampleRateString = textData{sampleRateIndices(1)};
            stringParts = textscan(sampleRateString, '%s');
            sampleRateString = stringParts{1}{end};
            samplingFrequency = str2double(sampleRateString(1:(end-2)));
            
            rangeIndices = find(~cellfun(@isempty, strfind(textData, 'Range')));
            rangeString = textData{rangeIndices(1)};
            stringParts = textscan(rangeString, '%s');
            rangeString = stringParts{1}{end};
            voltageRange = str2double(rangeString(1:(end-2)));
            dataScaleFactor = voltageRange / 2^15;
            
            % Data
            %             fgetl(fileID);
            %             fgetl(fileID);
            %
            %             dataline = fgetl(fileID);
            %             data = [];
            %             while ischar(dataline)
            %                 lineData = textscan(dataline, '%d', numberOfChannels, 'delimiter', ',');
            %                 lineData = lineData{:}';
            %                 data = [data; lineData]; %#ok<AGROW>
            %                 dataline = fgetl(fileID);
            %             end
            
            numberOfheaderLines = dataIndex;
            formatSpecifier = repmat('%d ', [1, numberOfChannels]);
            frewind(fileID);
            data = textscan(fileID, formatSpecifier, 'delimiter', ',',...
                'headerLines', numberOfheaderLines, 'collectOutput', 1);
            data = data{1};
            
            fclose(fileID);
            
            data = double(data) * dataScaleFactor;
            
            ecgData = ECGData(filename, data, samplingFrequency, channelLabels);
        end
        
        function [ecgData, activations] = ReadPacemapWavemap(filename, eFilename)
            if nargin < 2
                eFilename = [];
            end
            
            [result, waveMemberships, waveActivations, electrodeMapName] =...
                InputOutputPkg.ECGReader.ReadPacemapWaveData(filename); %#ok<ASGLU>
            
            waveActivations = waveActivations(:, 3:end);
            waveMembership = waveMemberships(:, 3:end);
            startDC = (waveMembership > 3000);
            startBT = (waveMembership > 2000) & (waveMembership < 3000);
            startPW = (waveMembership > 1000) & (waveMembership < 2000);
            
            waveMembership = waveMembership - startDC * 3000;
            waveMembership = waveMembership - startBT * 2000;
            waveMembership = waveMembership - startPW * 1000;
            maxActivation = max(waveActivations(:));
            
            if isempty(eFilename)
                electrogramData = NaN(maxActivation, size(waveActivations, 1));
            else
                dcmData = InputOutputPkg.ECGReader.ReadPACEMAPBinary(eFilename, electrodeMapName);
                electrogramData = dcmData.Data;
            end
            
            electrodeLabels = cellstr(num2str((1:size(waveActivations, 1))'));
            ecgData = DCMData(filename, electrogramData, 1e3, electrodeLabels);
            ecgData.SetElectrodeMap(electrodeMapName);
            
            % delete empty electrodes
            mappedElectrodeLabels = ecgData.ElectrodeLabels;
            validChannels = ~all(isnan(waveActivations), 2);
            validLabels = ismember(electrodeLabels, mappedElectrodeLabels);
            validMappedChannels = validChannels(validLabels);
            dcmDataCopy = ecgData.Copy(validMappedChannels);
            delete(ecgData);
            ecgData = dcmDataCopy;
            
            waveActivations = waveActivations(validChannels & validLabels, :);
            waveMembership = waveMembership(validChannels & validLabels, :);
            
            activations = cell(ecgData.GetNumberOfChannels(), 1);
            for electrodeIndex = 1:ecgData.GetNumberOfChannels()
                validActivations = ~isnan(waveActivations(electrodeIndex, :));
                activations{electrodeIndex} = waveActivations(electrodeIndex, validActivations);
            end
            
            distanceMatrix = squareform(pdist(ecgData.ElectrodePositions));
            
            waveIDs = unique(waveMembership(~isnan(waveMembership)));
            waves = AlgorithmPkg.Wave.empty(numel(waveIDs), 0);
            for waveIndex = 1:numel(waveIDs)
                waveMembers = (waveMembership == waveIDs(waveIndex));
                [rowPositions, ~] = find(waveMembers);
                waveMembers = find(waveMembers);
                waves(waveIndex) = AlgorithmPkg.Wave(waveIndex, ecgData.ElectrodePositions);
                for waveMemberIndex = 1:numel(waveMembers)
                    waves(waveIndex).Add(rowPositions(waveMemberIndex),...
                        NaN,...
                        waveActivations(waveMembers(waveMemberIndex)));
                end
                % create connectivity matrix
                conductionThreshold = 0.2;
                conductionMatrix = waves(waveIndex).LocalConduction();
                waveElectrodeIndices = waves(waveIndex).Members(:, 1);
                waveDistanceMatrix = distanceMatrix(waveElectrodeIndices, waveElectrodeIndices);
                electrodeDistance = ecgData.ComputeMinimumSpacing();
                radius = sqrt(electrodeDistance^2 + electrodeDistance^2) + 1e3 * eps;
                connectivityGraph = conductionMatrix >= conductionThreshold &...
                    waveDistanceMatrix <= radius;
                waves(waveIndex).ConnectivityMatrix = sparse(connectivityGraph);
                waves(waveIndex).GetAllStartingPoints();
            end
            ecgData.Waves =  waves;
            ecgData.SetWaveType();
        end
        
        function [result, waveMemberships, waveActivations, electrodeMapName] = ReadPacemapWaveData(filename)
            infile = fopen(filename, 'rt');
            
            sampleOffset = 2;
            linesToSkip = 7;
            
            line = 1;
            electrode = 1;
            wave = [];
            activations = [];
            electrodeMapName = '';
            while ~feof(infile)
                lineText = fgetl(infile);
                
                if line == 2
                    try
                        if ispc
                            [pathString, filename] = fileparts(lineText);
                            electrodeMapName = filename;
                        else
                            fileParts = textscan(lineText, '%s', 'Delimiter', '\\');
                            electrodeMapName = fileParts{1}{end};
                            extensionPosition = strfind(lower(electrodeMapName), '.mat');
                            electrodeMapName = electrodeMapName(1:(extensionPosition - 1));
                        end
                    catch ME
                        errordlg(ME.message, 'No electrodemap information found in file');
                    end
                end
                
                if line > linesToSkip
                    % parse first electrode line
                    rem = lineText;
                    [token, rem] = strtok(rem);
                    sample = 0;
                    
                    % sanity check
                    if 0 == strcmp(token,'Ele:')
                        %             result = -1;
                        %             waveMemberships = [];
                        %             waveActivations = [];
                        %             return;
                        break;
                    end
                    
                    % set electrode number
                    [token, rem] = strtok(rem);
                    wave(electrode, 1) = str2num(token);
                    activations(electrode, 1) = str2num(token);
                    
                    % get samples for the current electrode
                    [token, rem] = strtok(rem);
                    while ~isempty(rem)
                        sample = sample + 1;
                        activations(electrode, sample + sampleOffset) = str2num(token);
                        [token, rem] = strtok(rem);
                    end
                    if ~isempty(token)
                        if ~isempty(str2num(token))
                            sample = sample + 1;
                            activations(electrode, sample + sampleOffset) = str2num(token);
                        end
                    end
                    
                    % parse second electrode line
                    sample = 0;
                    lineText = fgetl(infile);
                    if -1 ~= lineText
                        rem = lineText;
                        [token, rem] = strtok(rem);
                        while 0 == isempty(rem)
                            sample = sample + 1;
                            wave(electrode, sample + sampleOffset) = str2num(token);
                            [token, rem] = strtok(rem);
                        end
                        if ~isempty(token)
                            if ~isempty(str2num(token))
                                sample = sample + 1;
                                wave(electrode, sample + sampleOffset) = str2num(token);
                            end
                        end
                    end
                    
                    % parse third electrode line
                    lineText = fgetl(infile);
                    
                    % write number of samples for current electrode
                    wave(electrode, sampleOffset) = sample;
                    activations(electrode, sampleOffset) = sample;
                    electrode = electrode + 1;
                end
                line = line + 1;
            end
            
            % replace zeros with NaN in wave and activations matrix
            wave(0 == wave) = NaN;
            activations(0 == activations) = NaN;
            
            waveMemberships = wave;
            result = fclose(infile);
            waveActivations = activations;
        end
        
        function [ecgData, dataLoss] = ReadIDEEQ(filename, electrodeMap, setElectrodeMap)
            if nargin < 3
                setElectrodeMap = true;
                if nargin < 2
                    electrodeMap = '';
                end
            end
            fileID = fopen(filename);
            
            % number of channels
            fseek(fileID, 195, 'bof');
            numberOfChannels = str2double(deblank(fread(fileID, 3, '*char')));
            
            % start of data blocks
            fseek(fileID, 199, 'bof');
            headerSize = str2double(deblank(fread(fileID, 6, '*char')));
            
            % number of data blocks
            fseek(fileID, 206, 'bof');
            numberOfDataBlocks = str2double(deblank(fread(fileID, 6, '*char')));
            
            % read channel headers
            channelLabels = cell(numberOfChannels, 1);
            channelSamples = zeros(numberOfChannels, 1);
            channelBlockSamples = zeros(numberOfChannels, 1);
            channelSampleRate = NaN(numberOfChannels, 1);
            for channelIndex = 1:numberOfChannels
                fseek(fileID, channelIndex * 256 + 1, 'bof');
                channelLabels{channelIndex} = strtrim(fread(fileID, 20, '*char')');
                fseek(fileID, 1, 'cof');
                channelSampleRate(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
                fseek(fileID, 1, 'cof');
                channelBlockSamples(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
                fseek(fileID, 1, 'cof');
                channelSamples(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
            end
            
            % determine global samples per channel and sample rate
            validChannels = channelSamples > 0;
            numberOfValidChannels = numel(find(validChannels));
            samplesPerBlock = mean(channelBlockSamples(validChannels));
            samplingFrequency = mean(channelSampleRate(validChannels));
            
            % positions the pointer to the beginning of data
            fseek(fileID, headerSize, 'bof');
            
            data = NaN(samplesPerBlock * numberOfDataBlocks, numberOfValidChannels);
            for blockIndex = 1:numberOfDataBlocks
                dataPosition = (blockIndex - 1) * samplesPerBlock + 1;
                currentData = fread(fileID, [samplesPerBlock, numberOfValidChannels], 'single');
                if isempty(currentData)
                    fseek(fileID, - samplesPerBlock * numberOfValidChannels * 4, 'cof');
                    break;
                end
                
                data(dataPosition:(dataPosition + samplesPerBlock - 1), 1:size(currentData, 2)) = currentData;
            end
            
            % remove trailing zeros at the end of the recording
            allZeroPositions = all(data == 0, 2) | all(isnan(data), 2);
            lastNonZeroPosition = find(~allZeroPositions, 1, 'last');
            data = data(1:lastNonZeroPosition, :);
            
            % retrieve data loss information
            if nargout > 1
                dataLoss = [];
                dataLossIndex = 1;
                
                fseek(fileID, 6, 'cof');
                while feof(fileID) == 0
                    eventType = fread(fileID, 1, 'uint16');
                    eventTime = fread(fileID, 1, 'double');
                    eventDuration = fread(fileID, 1, 'double');
                    eventComment = fread(fileID, 100, '*char')';
                    eventComment = deblank(eventComment);
                    
                    packagesLostString = 'Packages lost :';
                    stringPosition = strfind(eventComment, packagesLostString);
                    if isempty(stringPosition), continue; end
                    
                    packagesLost = str2double(eventComment((stringPosition + length(packagesLostString)):end));
                    dataLoss(dataLossIndex).time = eventTime;
                    dataLoss(dataLossIndex).numberOfSamples = packagesLost;
                    
                    dataLossIndex = dataLossIndex + 1;
                    fseek(fileID, 10, 'cof');
                end
                
                disp(dataLoss);
            end
            
            fclose(fileID);
            
            % create ecgData
            fullData = NaN(size(data, 1), numberOfChannels);
            fullData(:, validChannels) = data;
            ecgData = DCMData(filename, fullData, samplingFrequency, channelLabels(validChannels));
            
            if setElectrodeMap
                ecgData.SetElectrodeMap(electrodeMap);
            end
        end
        
        function [ecgData, YRSVersion] = ReadYourRhythmicsData(filename)

            % !!!!!! all YRS100 versions < 1.0.43 should be corrected with factor 2
            % !!!!!! Scaling issues !!!!!!
            % YRS100 versions < 1.0.43 should be multiplied with factor 2.
            % YRS100 versions [1.0.43, 1.0.47) should be multiplied with factor 0.862069.
            
            fileID = fopen(filename);

            % YRS version
            YRSVersion = strtrim(fread(fileID, 20, '*char'));
            
            % YRS version
            YRSVersion = strtrim(deblank(fread(fileID, 20, '*char')));
            
            % number of channels
            fseek(fileID, 195, 'bof');
            numberOfListedChannels = str2double(deblank(fread(fileID, 3, '*char')));
            numberOfChannels = numberOfListedChannels;
            
            % start of data blocks
            fseek(fileID, 199, 'bof');
            headerSize = str2double(deblank(fread(fileID, 6, '*char')));
            
            % number of data blocks
            fseek(fileID, 206, 'bof');
            numberOfDataBlocks = str2double(deblank(fread(fileID, 6, '*char')));
            
            % number of data blocks
            fseek(fileID, 213, 'bof');
            dataBlockSize = str2double(deblank(fread(fileID, 6, '*char')));
            
            % read channel headers (next 256 bytes)
            channelLabels = cell(numberOfChannels, 1);
            channelSamples = zeros(numberOfChannels, 1);
            channelBlockSamples = zeros(numberOfChannels, 1);
            channelSampleRate = NaN(numberOfChannels, 1);
            channelMinMaxMean = NaN(numberOfChannels, 3);
            for channelIndex = 1:numberOfChannels
                fseek(fileID, channelIndex * 256, 'bof');
                channelLabels{channelIndex} = strtrim(fread(fileID, 20, '*char')');
                channelLabels{channelIndex} = regexprep(channelLabels{channelIndex},char(20),'');
                fseek(fileID, 2, 'cof');
                channelSampleRate(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
                fseek(fileID, 1, 'cof');
                channelBlockSamples(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
                fseek(fileID, 1, 'cof');
                channelSamples(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
                fseek(fileID, 2, 'cof');
                channelMinMaxMean(channelIndex, :) = fread(fileID, 3, 'real*8');
            end
            
            % determine global samples per channel and sample rate
            samplesPerBlock = mean(channelBlockSamples);
            samplingFrequency = mean(channelSampleRate);
            
            % determine the channels that contain samples
            recordedChannels = channelSamples > 0;
            channelLabels = channelLabels(recordedChannels);
            numberOfChannels = numel(channelLabels);
            
            for channelIndex = 1:numberOfListedChannels
                fseek(fileID, numberOfListedChannels * 256 + channelIndex * 256, 'bof');
                channelOn = fread(fileID, 1, 'uint8');
                channelLabel = strtrim(fread(fileID, 20, '*char')');
                channelLabel = regexprep(channelLabel, char(20), '');
                fseek(fileID, 3, 'cof');
                channelSampleRate = fread(fileID, 1, 'uint32');
                fseek(fileID, 1, 'cof');
                channelScriptIndex = fread(fileID, 1, 'uint8');
                channelUnit = strtrim(fread(fileID, 8, '*char')');
                fseek(fileID, 2, 'cof');
                channelAmplifier = fread(fileID, 1, 'real*8');
                channelGain = fread(fileID, 1, 'real*8');
            end
            
            % positions the pointer to the beginning of the data
            fseek(fileID, headerSize, 'bof');
            
            data = NaN(samplesPerBlock * numberOfDataBlocks, numberOfChannels);
            for blockIndex = 1:numberOfDataBlocks
                dataPosition = (blockIndex - 1) * samplesPerBlock + 1;
                currentData = fread(fileID, [samplesPerBlock, numberOfChannels], 'single');
                if isempty(currentData)
                    fseek(fileID, - samplesPerBlock * numberOfChannels * 4, 'cof');
                    break;
                end
                
                data(dataPosition:(dataPosition + samplesPerBlock - 1), 1:size(currentData, 2)) = currentData;
            end
            
            % remove trailing zeros at the end of the recording
            allZeroPositions = all(data == 0, 2) | all(isnan(data), 2);
            lastNonZeroPosition = find(~allZeroPositions, 1, 'last');
            data = data(1:lastNonZeroPosition, :);
            
            % data in muV in stead of mV
%             if nanmean(max(data))<50 
%                 data=data*1e3;
%             end
            
            fclose(fileID);
            
            % create ecgData
            ecgData = ECGData(filename, data, samplingFrequency, channelLabels);
        end
        
        function ecgData = ReadYourRhythmicsCSV(filename)
            fileData = detectImportOptions(filename, 'Delimiter', ';');
            variableNames = fileData.VariableNames;
            channelLabels = variableNames(2:end);
            channelLabels = channelLabels(:);

            channelData = readmatrix(filename, 'Delimiter', ';', 'DecimalSeparator', ',');

            % timestamp in first column
            timeStamp = channelData(:, 1);
            samplingFrequency = 1 / mean(diff(timeStamp));

            channelData = channelData(:, 2:end);

            ecgData = ECGData(filename, channelData, samplingFrequency, channelLabels);
        end
       
        function ecgData = ReadBasketExportData(filename, electrodeMap)
            if nargin < 2
                electrodeMap = 'Constellation Mapping Catheter';
            end
            delimiterIn = ',';
            headerlinesIn = 1;
            filedata = importdata(filename,delimiterIn,headerlinesIn);
            
            electrodeLabels = filedata.colheaders(:);
            data = filedata.data;
            samplingFrequency = 2048;
            ecgData = DCMData(filename, data, samplingFrequency, electrodeLabels);
            ecgData.SetElectrodeMap(electrodeMap);
        end
        
        function ecgData = ReadEDFECG(filename)
            [~, signalHeader, signalCell] = InputOutputPkg.blockEdfLoad(filename);
            
            channelData = horzcat(signalCell{1:12}) / 1000;
            samplingFrequency = signalHeader(1).samples_in_record;
            channelLabels = {signalHeader.signal_labels};
            channelLabels = channelLabels(1:12);
            
            ecgData = ECGData(filename, channelData, samplingFrequency, channelLabels);
        end%
        
        function ecgData = ReadPrucka(filename)
            text = fileread(filename);
            if ~isempty(strfind(text, ','))
                data = strrep(text, ',', '.');
                FID = fopen(filename, 'w');
                fwrite(FID, data, 'char');
                fclose(FID);
            end
            channelData = dlmread(filename, ' ');
            channelData = channelData(:, 1:12);
            samplingFrequency = 1000;
            channelLabels = {'I', 'II', 'III', 'avR', 'aVL', 'aVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6'};
            
            ecgData = ECGData(filename, channelData, samplingFrequency, channelLabels);
        end
        
        function ecgData = ReadBrugadaCSV(filename)
            fileData = importdata(filename);
            
            channelData = fileData.data;
            channelLabels = fileData.colheaders;
            samplingFrequency = 1e3;
            
            ecgData = ECGData(filename, channelData, samplingFrequency, channelLabels);
        end
        
        function ecgData = Read8LeadCSV(filename)
            fileData = importdata(filename);
            
            channelData = fileData;
            channelLabels = {'I', 'II', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6'};
            samplingFrequency = 1e3;
            
            ecgData = ECGData(filename, channelData, samplingFrequency, channelLabels);
        end
        
        function ecgData = ReadAmedtecData(filename)
            AmedtecLeadIDs = {...
                'I', 1; 'II', 2; 'III', 61; 'aVR', 62; 'aVL', 63; 'aVF', 64; '-aVR', 65;...
                'V1', 3; 'V2', 4; 'V3', 5; 'V4', 6; 'V5', 7; 'V6', 8;...
                'V7', 9; 'V8', 66; 'V9', 67; 'V10', 287; 'V11', 288; 'V12', 289;...
                'V1R', 286; 'V2R', 10; 'V3R', 11; 'V4R', 12; 'V5R', 13; 'V6R', 14;...
                'VR', 103; 'VL', 104; 'VF', 105; 'D', 70; 'A', 71; 'J', 72;...
                'VX', 16; 'VY', 17; 'VZ', 18; '-VX', 114; '-VY', 115; '-VZ', 116;...
                'I mod', 260; 'II mod', 261; 'III mod', 262;...
                'aVR mod', 263; 'aVL mod', 264; 'aVF mod', 265;...
                'V1 mod', 266; 'V2 mod', 267; 'V3 mod', 268; 'V4 mod', 269;...
                'V5 mod', 270; 'V6 mod', 271; 'XAC1', 139; 'XAC2', 140; 'XAC3', 141;...
                'K1', 228; 'K2', 229; 'K3', 230; 'K4', 231; 'K5', 232; 'K6', 233;...
                'K7', 280; 'K8', 281; 'K9', 282; 'K10', 283; 'K11', 284; 'K12', 285;...
                'CC5', 19; 'CM5', 20; 'CR5', 125; 'Sample-Status', 236};
            
            fid = fopen(filename);
            
            % header (1024 bytes)
            fseek(fid, 834, 'bof');
            samplingFrequency = str2double(deblank(char(fread(fid, 4, '1*char'))));
            
            fseek(fid, 839, 'bof');
            numberOfBitsPerMV = str2double(deblank(char(fread(fid, 4, '1*char'))));
            
            fseek(fid, 987, 'bof');
            dataVersion = fread(fid, 1);
            
            fseek(fid, 988, 'bof');
            numberOfSamples = fread(fid, 1, 'uint32');
            
            fseek(fid, 992, 'bof');
            numberOfLeads = fread(fid, 1, 'int16');
            
            fseek(fid, 994, 'bof');
            leadIDs= fread(fid, numberOfLeads, 'int16');
            
            [validLeadIDs, leadIDPositions] = ismember(leadIDs, vertcat(AmedtecLeadIDs{:, 2}));
            channelLabels = cell(numberOfLeads, 1);
            channelLabels(validLeadIDs) = AmedtecLeadIDs(leadIDPositions(validLeadIDs), 1);
            
            % data
            fseek(fid, 1024, 'bof');
            
            if dataVersion == 1
                channelData = fread(fid, [numberOfLeads, numberOfSamples], 'int16');
            elseif dataVersion == 2
                channelData = fread(fid, [numberOfLeads, numberOfSamples], 'int32');
            else
                errordlg('Unknown data version for Amedtec ECG recording', 'File formate error');
            end
            
            fclose(fid);
            
            ecgData = ECGData(filename, channelData' / numberOfBitsPerMV, samplingFrequency, channelLabels);
        end
        
        function ecgData = ReadKORAS4(filename)
            [pathname, filename, ~] = fileparts(filename);
            filenameParts = strsplit(filename, '_');
            baseFilename = filenameParts{1};
            ecgFilename = fullfile(pathname, baseFilename);
            
            fileListing = dir(fullfile(pathname, [filenameParts{1}, '*.asc']));
            numberOfFiles = numel(fileListing);
            
            fileChannelData = cell(numberOfFiles, 1);
            channelLabels = cell(numberOfFiles, 1);
            
            numberOfHeaderLines = 2;
            
            for fileIndex = 1:numberOfFiles
                currentFilename = fullfile(pathname, fileListing(fileIndex).name);
                filenameParts = strsplit(fileListing(fileIndex).name, {'_', '.'});
                channelLabels{fileIndex} = filenameParts{2};
                
                fileData = importdata(currentFilename, ' ', numberOfHeaderLines);
                data = fileData.data';
                fileChannelData{fileIndex} = data(:);
            end
            fileChannelData = horzcat(fileChannelData{:});
            
            samplingFrequency = 500;
            ecgData = ECGData(ecgFilename, fileChannelData, samplingFrequency, channelLabels);
        end
        
        function ecgData = ReadKORAS4RHY(filename)
            samplingFrequency = 500;
            defaultChannelLabels = {'I', 'II', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6'};
            
            fid = fopen(filename);
                        
            % header (71 bytes)
            fseek(fid, 39, 'bof');
            numberOfLeads = fread(fid, 1, 'int16');
            
            fseek(fid, 41, 'bof');
            leadIDs = fread(fid, 13, 'int16');
            channelLabels = defaultChannelLabels(leadIDs(1:numberOfLeads));
                                   
            % data
            fseek(fid, 71, 'bof');
            samples = fread(fid, Inf, 'int16', 0, 'l');
            numberOfSamples = round(numel(samples) / numberOfLeads);
            channelData = reshape(samples, [numberOfLeads, numberOfSamples])';
                        
            ecgData = ECGData(filename, channelData, samplingFrequency, channelLabels);
        end
        
        function ecgData = ReadMyDiagnostic(filename)
            fileData = readtable(filename);
            channelData = fileData.ECGValue;
            channelLabels = {'I'};
            samplingFrequency = 200;
            
            ecgData = ECGData(filename, channelData, samplingFrequency, channelLabels);
        end
        
        function ecgData = ReadSimulationECGData(filename)
            fileData = load(filename);
            samplingFrequency = 1e3;
            
            numberOfProbeElectrodes = size(fileData.probe, 1);
            probeElectrodeLabels = cellstr(num2str((1:numberOfProbeElectrodes)'));
            probeElectrodeLabels =strcat('Probe', probeElectrodeLabels);
            
            numberOfBSM65Electrodes = size(fileData.bsm65, 1);
            bsm65ElectrodeLabels = cellstr(num2str((1:numberOfBSM65Electrodes)'));
            bsm65ElectrodeLabels =strcat('BSM65', bsm65ElectrodeLabels);
            
            numberOfMBSMElectrodes = size(fileData.mbsm, 1);
            mbsmElectrodeLabels = cellstr(num2str((1:numberOfMBSMElectrodes)'));
            mbsmElectrodeLabels =strcat('MBSM', mbsmElectrodeLabels);
            
            ecgData = ECGData('', [(fileData.probe)', (fileData.bsm65)', (fileData.mbsm)'],...
                samplingFrequency, vertcat(probeElectrodeLabels, bsm65ElectrodeLabels, mbsmElectrodeLabels));
        end
        
        function ecgData = ReadEPSolutionsECG240(filename)
            % .240 format:
            %   0..15 STD:
            %   0 I
            %   1 II
            %   2 III
            %   3 AVR
            %   4 AVL
            %   5  AVF
            %   16..31 A1..A16
            %   32..47 B1..B16
            %   48..63 C1..C16
            %   64..80 D1..D16
            %   ...
            
            % Input data:
            %   filename - path to .240 file
            % Output data:
            %   ecg - an ECG matrix with dimension 256xNt, where Nt is the number of
            %   time samples
            
            % Author: Danila Potyagaylo
            % EP Solutions SA
            % Last revision: 30 January 2018
            
            % ------------------------------------------------------------
            scale = 1e-3; % scale to millivolts
            
            fid = fopen(filename);
            header = fread(fid, 256, 'char');
            fseek(fid, 256, 'bof'); % skip header
            
            channelData = fread(fid, Inf, 'int16');
            channelData = reshape(channelData, 256, []);
            channelData = channelData' * scale;
            
            fclose(fid);
            samplingFrequency = 1e3;
            channelLabels = horzcat({'I', 'II', 'III', 'aVR', 'aVL', 'aVF',...
                'V1', 'V2', 'V3', 'V4', 'V5', 'V6',...
                'STD_ECG1', 'STD_ECG2', 'STD_ECG3', 'STD_ECG4'}, strcat('BSPM', cellstr(num2str((1:240)'))'));
            ecgData = ECGData(filename, channelData, samplingFrequency, channelLabels);
        end 

        function ecgData = ReadLabChart(filename)
            % MATLAB format
            labChartData = load(filename);

            dataStart = labChartData.datastart;
            dataEnd = labChartData.dataend;
            leadI = labChartData.data(dataStart(1):dataEnd(1));
            leadII = labChartData.data(dataStart(2):dataEnd(2));
            leadIII = leadII - leadI;

            leadaVR = -leadI + leadIII / 2;
            leadaVL = -leadIII + leadII / 2;
            leadaVF = -leadII - leadI / 2;

            channelData = [leadI(:), leadII(:), leadIII(:),...
                leadaVR(:), leadaVL(:), leadaVF(:)];

            channelData = channelData * 1e3;    % V to mV

            samplingFrequency = labChartData.samplerate(1);
            channelLabels = {'I', 'II', 'III', 'aVR', 'aVL', 'aVF'};

            ecgData = ECGData(filename, channelData, samplingFrequency, channelLabels);
        end

        function ecgData = ReadDICOM(filename)
            % ECG data is stored in the info structure
            dataInfo = dicominfo(filename);

            waveformSequenceData = dataInfo.WaveformSequence.Item_1;
            samplingFrequency = waveformSequenceData.SamplingFrequency;
            numberOfChannels = waveformSequenceData.NumberOfWaveformChannels;
            numberOfSamples = waveformSequenceData.NumberOfWaveformSamples;

            channelInfo = waveformSequenceData.ChannelDefinitionSequence;
            channelLabels = cell(numberOfChannels, 1);
            for channelIndex = 1:numberOfChannels
                currentChannelInfo = channelInfo.(['Item_', num2str(channelIndex)]);
                channelLabels{channelIndex} = currentChannelInfo.ChannelLabel;
            end

            % assuming int16 (signed 16 bit) values
            channelData = typecast(waveformSequenceData.WaveformData, 'int16');
            % demultiplex
            channelData = reshape(channelData, numberOfChannels, numberOfSamples);
            % transpose and convert to double
            channelData = double(channelData');

            ecgData = ECGData(filename, channelData, samplingFrequency, channelLabels);
        end
    end
end