classdef ECGReader < handle
    properties (Constant)
        NUMBER_OF_PACEMAP_ELECTRODES = 256;
    end
    methods (Static)
        function ecgData = ReadBDF(filename, samplingFrequency)
            if nargin < 2
                bdfInfo = InputOutputPkg.ECGReader.ReadBDFInfo(filename);
                samplingFrequency = bdfInfo.SamplingFrequency;
            end
            %             ecgData = InputOutputPkg.readBDF(filename, 'resampleFrequency', samplingFrequency,...
            %                 'secondsToLoad', [120, 180]);
            ecgData = InputOutputPkg.readBDF(filename, 'resampleFrequency', samplingFrequency);
        end
        
        function bdfInfo = ReadBDFInfo(filename)
            fid = fopen(filename);
            
            % user text
            fseek(fid, 130, 'bof');
            subjectInfo = cellstr(char(fread(fid, 54, '10*char'))');
            
            % number of data records
            fseek(fid, 236, 'bof');
            numberOfRecords = str2double(deblank(char(fread(fid, 7, '1*char'))));
            
            % duration of a data record in seconds
            fseek(fid, 244, 'bof');
            recordDuration = str2double(deblank(char(fread(fid, 3, '1*char'))));
            
            % number of EEG channels
            fseek(fid, 252, 'bof');
            numberOfChannels = str2double(deblank(char(fread(fid, 3, '1*char'))));
            
            % EEG electrodeLabels
            fseek(fid, 256, 'bof');
            electrodeLabels = cellstr(deblank(char(fread(fid, [7, numberOfChannels], '7*char', 9)')));
            
            % filters applied - need to check from this point
            fseek(fid, 256 + numberOfChannels * 136, 'bof');
            prefiltering = cellstr(char(fread(fid, 25, '10*char'))');
            
            % physical dimension of the channels (uV,  for instance)
            fseek(fid, 256 + numberOfChannels * 96, 'bof');
            channelDimensions = cellstr(deblank(char(fread(fid, 5, '5*char'))'));
            
            % number of samples in each record
            fseek(fid, 256 + numberOfChannels * 216, 'bof');
            numberOfSamples = str2double(deblank(char(fread(fid, 4, '1*char'))));
            
            % sampling frequency
            samplingFrequency = numberOfSamples / recordDuration;
            
            bdfInfo = struct(...
                'SamplingFrequency', samplingFrequency,...
                'ElectrodeLabels', {electrodeLabels},...
                'SubjectInfo', subjectInfo,...
                'Prefiltering', prefiltering,...
                'ChannelDimensions', channelDimensions);
        end
        
        function ecgData = ReadDCMBDF(filename)
            dcmInfo = InputOutputPkg.ECGReader.ReadDCMBDFInfo(filename);
            
            intervalToLoad = [0, dcmInfo.NumberOfSamples / dcmInfo.SamplingFrequency];
            if ~isempty(dcmInfo.StatusIndex)
                numberOfSegments = size(dcmInfo.SignalSegments, 1);
                optionString = cell(numberOfSegments + 1, 1);
                timeIntervals = dcmInfo.SignalSegments / dcmInfo.SamplingFrequency;
                timeIntervals = [timeIntervals; intervalToLoad];
                for segmentIndex = 1:numberOfSegments
                   optionString{segmentIndex} = [num2str(timeIntervals(segmentIndex, 1), '%.1f'), 's to ',...
                       num2str(timeIntervals(segmentIndex, 2), '%.1f'), 's'];
                end
                optionString{end} = ['0s to', num2str(dcmInfo.NumberOfSamples / dcmInfo.SamplingFrequency, '%.1f'), 's (All)']; 
                choice = menu('Choose interval', optionString);
                intervalToLoad = timeIntervals(choice, :);
            end
            
            ecgData = InputOutputPkg.readDCMBDF(filename,...
                'resampleFrequency', dcmInfo.SamplingFrequency,...
                'secondsToLoad', intervalToLoad);
        end
        
        function dcmInfo = ReadDCMBDFInfo(filename)
            fid = fopen(filename);
            
            % user text
            fseek(fid, 130, 'bof');
            subjectInfo = cellstr(char(fread(fid, 54, '10*char'))');
            
            % number of data records
            fseek(fid, 236, 'bof');
            numberOfRecords = str2double(deblank(char(fread(fid, 7, '1*char'))));
            
            % duration of a data record in seconds
            fseek(fid, 244, 'bof');
            recordDuration = str2double(deblank(char(fread(fid, 3, '1*char'))));
            
            % number of EEG channels
            fseek(fid, 252, 'bof');
            numberOfChannels = str2double(deblank(char(fread(fid, 3, '1*char'))));
            
            % EEG electrodeLabels
            fseek(fid, 256, 'bof');
            electrodeLabels = cellstr(deblank(char(fread(fid, [7, numberOfChannels], '7*char', 9)')));
            
            % Find and read status channel
            statusIndex = find(strcmpi('Status', electrodeLabels));
            dcmInfo.StatusIndex = statusIndex;
            
            % filters applied - need to check from this point
            fseek(fid, 256 + numberOfChannels * 136, 'bof');
            prefiltering = cellstr(char(fread(fid, 25, '10*char'))');
            dcmInfo.Prefiltering = prefiltering;
            
            % physical dimension of the channels (uV,  for instance)
            fseek(fid, 256 + numberOfChannels * 96, 'bof');
            channelDimensions = cellstr(deblank(char(fread(fid, 5, '5*char'))'));
            
            % number of samples in each record
            fseek(fid, 256 + numberOfChannels * 216, 'bof');
            numberOfSamples = str2double(deblank(char(fread(fid, 4, '1*char'))));
            
            % sampling frequency
            samplingFrequency = numberOfSamples / recordDuration;
            
            dcmInfo.SamplingFrequency = samplingFrequency;
            
            startRecord = 1;
            recordsToLoad = numberOfRecords;
            dcmInfo.NumberOfSamples = recordDuration  * numberOfSamples  *  recordsToLoad;
    
            if ~isempty(statusIndex)
                % positions the pointer to the beginning of data
                % fseek(fid, 256 * (numberOfChannels + 1), 'bof');    
                startPosition = (startRecord - 1) * recordDuration * numberOfSamples * numberOfChannels * 3;
                fseek(fid, 256 * (numberOfChannels + 1) + startPosition, 'bof');
                fseek(fid, recordDuration * numberOfSamples * (statusIndex - 1) * 3, 'cof');
                
                statusData = NaN(recordDuration  * numberOfSamples  *  recordsToLoad, 1);
                
                UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                    UserInterfacePkg.StatusChangeEventData('Reading BDF data segments...', 0, []));
                for recordIndex = 1:recordsToLoad
                    aux = fread(fid, [recordDuration * numberOfSamples, 1], 'bit24');
                    dataPosition = (recordDuration  *  numberOfSamples)  *  (recordIndex - 1)  +  1;
                    statusData(dataPosition:(dataPosition  +  recordDuration  *  numberOfSamples - 1)) = aux;
                    
                    fseek(fid, recordDuration * numberOfSamples * (numberOfChannels - 1) * 3, 'cof');
                end
                UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                    UserInterfacePkg.StatusChangeEventData('Done', 1, []));
                
                statusSegmentStartIndices = find(diff(statusData) < -eps);
                statusSegmentEndIndices = find(diff(statusData) > eps);
                
                signalSegments = [statusSegmentStartIndices(:), NaN(numel(statusSegmentStartIndices), 1)];
                for segmentStartIndex = 1:numel(statusSegmentStartIndices)
                    nextEndIndex = find(statusSegmentEndIndices > statusSegmentStartIndices(segmentStartIndex), 1, 'first');
                    if ~isempty(nextEndIndex)
                        signalSegments(segmentStartIndex, 2) = statusSegmentEndIndices(nextEndIndex);
                    else
                        signalSegments(segmentStartIndex, 2) = numel(statusData);
                    end
                end
                
                dcmInfo.SignalSegments = signalSegments;
            end
        end
        
        function ecgData = ReadPACEMAPEcg(filename)
            filedata = importdata(filename);
            beginTimePos = strfind(filedata.textdata{1}, 'BeginTime:');
            endTimePos = strfind(filedata.textdata{1}, 'EndTime:');
            msPos = strfind(filedata.textdata{1}, 'ms');
            
            if isempty(beginTimePos) || isempty(endTimePos) || size(msPos,1) ~= 1 || size(msPos,2) ~= 2
                ME = MException('ReadPACEMAPEcg', 'File format is not valid');
                throw(ME);
            end
            
            beginTime = str2double(filedata.textdata{1}(size('BeginTime:', 2) + beginTimePos:msPos(1) - 1));
            endTime = str2double(filedata.textdata{1}(size('EndTime:', 2) + endTimePos:msPos(2) - 1));
            filedata.data(1, :) = [];
            samplingFrequency = size(filedata.data, 1) / ((endTime - beginTime) / 1000);
            electrodeLabels = cellstr(num2str((1:size(filedata.data, 2))'));
            ecgData = DCMData(filename, filedata.data, samplingFrequency, electrodeLabels);
            ecgData.SetElectrodeMap('');
        end
        
        function ecgData = ReadPACEMAPBinary(filename, electrodeMap, resample)
            if nargin < 3
                resample = true;
                if nargin < 2
                    electrodeMap = '';
                end
            end
            fileHandle = fopen(filename, 'r');
            
            textData = fread(fileHandle, 2048, '*char');
            data = fread(fileHandle, 'int16=>double');
            
            numberOfElectrodes = InputOutputPkg.ECGReader.NUMBER_OF_PACEMAP_ELECTRODES;
            numberOfSamples = numel(data) / numberOfElectrodes;
            data = reshape(data, numberOfElectrodes, numberOfSamples);
            data = data(:, 5:end);
            numberOfSamples = size(data, 2);
            
            samplingFrequency = 1e3;
            if resample
                blockSize = 16;
                resampledData = data;
                for electrodeIndex = 1:numberOfElectrodes
                    blockIndex = mod((electrodeIndex + blockSize - 1), blockSize);
                    if blockIndex > 0
                        electrodeSamples = (1:numberOfSamples) + blockIndex / blockSize;
                        resampledData(electrodeIndex, :) = interp1(electrodeSamples, data(electrodeIndex, :), 1:numberOfSamples,...
                            'linear', 'extrap');
                    end
                end
                data = resampledData;
            end
            
            electrodeLabels = cellstr(num2str((1:numberOfElectrodes)'));
            ecgData = DCMData(filename, data', samplingFrequency, electrodeLabels);
            ecgData.SetElectrodeMap(electrodeMap);
        end
        
        function ecgData = ReadPACEMAPBinarySeries(filename, electrodeMap)
            if nargin < 2
                electrodeMap = '';
            end
            [pathname, shortFilename] = fileparts(filename);
            listing = dir(fullfile(pathname, [shortFilename, '.E*']));
            filenames = {listing.name};
            
            mergedData = [];
            for fileIndex = 1:numel(filenames)
                currentFilename = fullfile(pathname, filenames{fileIndex});
                fileHandle = fopen(currentFilename, 'r');
                
                textData = fread(fileHandle, 2048, '*char');
                data = fread(fileHandle, 'int16');
                
                numberOfElectrodes = InputOutputPkg.ECGReader.NUMBER_OF_PACEMAP_ELECTRODES;
                numberOfSamples = numel(data) / numberOfElectrodes;
                mergedData = [mergedData, reshape(data, numberOfElectrodes, numberOfSamples)]; %#ok<AGROW>
            end
            
            samplingFrequency = 1e3;
            electrodeLabels = cellstr(num2str((1:numberOfElectrodes)'));
            
            ecgData = DCMData(filename, mergedData', samplingFrequency, electrodeLabels);
            ecgData.SetElectrodeMap(electrodeMap);
        end
        
        function ecgData = ReadTVIEeg(filename)
            filedata = importdata(filename);
            
            time = filedata.data(:, 1);
            validTimePositions = ~isnan(time);
            time = time(validTimePositions);
            samplingFrequency = round(1 / mean(diff(time)));
            %             tracePositions = strncmp('Trace', filedata.textdata(3, :), 5);
            tracePositions = 2:size(filedata.textdata, 2);
            electrodeLabels = filedata.textdata(3, tracePositions);
            data = filedata.data(validTimePositions, tracePositions);
            
            upsampledFrequency = ceil(1e3 / samplingFrequency) * samplingFrequency;
            data = resample(data, ceil(1e3 / samplingFrequency), 1);
            
            ecgData = ECGData(filename, data, upsampledFrequency, electrodeLabels);
        end
        
        function ecgData = ReadPhysionetCSV(filename)
            data = dlmread(filename, '\t');
            time = data(:, 1);
            samplingFrequency = 1 / mean(diff(time));
            upsampledFrequency = ceil(1e3 / samplingFrequency) * samplingFrequency;
            data = data(:, 2:end);
            data = resample(data, ceil(1e3 / samplingFrequency), 1);
            numberOfElectrodes = size(data, 2);
            electrodeLabels = cellstr(num2str((1:numberOfElectrodes)'));
            ecgData = ECGData(filename, data, upsampledFrequency, electrodeLabels);
        end
        
        function ecgData = ReadMuensterXML(filename)
            xmlData = xmlread(filename);
            
            stripData = xmlData.getElementsByTagName('StripData');
            sequences = stripData.item(0).getElementsByTagName('WaveformData');
            numberOfSequences = sequences.getLength;
            signals = cell(numberOfSequences, 1);
            signalLabels = cell(numberOfSequences, 1);
            for sequenceIndex = 0:(numberOfSequences - 1)
                sequenceNode = sequences.item(sequenceIndex);
                
                attributes = sequenceNode.getAttributes;
                signalLabels{sequenceIndex + 1} = char(attributes.item(0).getValue);
                data = textscan(char(sequenceNode.getFirstChild.getData), '%f', 'delimiter', ',');
                signals{sequenceIndex + 1} = data{:}';
            end
            signals = vertcat(signals{:})';
            samplingFrequency = 500;
            ecgData = ECGData(filename, signals, samplingFrequency, signalLabels);
        end
        
        function ecgData = ReadOxfordXML(filename)
            xmlData = xmlread(filename);
            
            %             stripData = xmlData.getElementsByTagName('StripData');
            stripData = xmlData.getElementsByTagName('ArrhythmiaData');
            sequences = stripData.item(0).getElementsByTagName('WaveformData');
            
            numberOfSequences = sequences.getLength;
            signals = cell(numberOfSequences, 1);
            signalLabels = cell(numberOfSequences, 1);
            for sequenceIndex = 0:(numberOfSequences - 1)
                sequenceNode = sequences.item(sequenceIndex);
                
                attributes = sequenceNode.getAttributes;
                signalLabels{sequenceIndex + 1} = char(attributes.item(0).getValue);
                data = textscan(char(sequenceNode.getFirstChild.getData), '%f', 'delimiter', ',');
                signals{sequenceIndex + 1} = data{:}';
            end
            signals = vertcat(signals{:})';
            samplingFrequency = 500;
            ecgData = ECGData(filename, signals, samplingFrequency, signalLabels);
        end
        
        function ecgData = ReadBirminghamXML(filename)
            xmlData = xmlread(filename);
            
            % signals
            sequences = xmlData.getElementsByTagName('sequence');
            numberOfSequences = sequences.getLength - 1;
            signals = cell(numberOfSequences, 1);
            signalLabels = cell(numberOfSequences, 1);
            for sequenceIndex = 1:(numberOfSequences)
                sequenceNode = sequences.item(sequenceIndex);
                
                codes = sequenceNode.getElementsByTagName('code');
                attributes = codes.item(0).getAttributes;
                signalLabels{sequenceIndex} = char(attributes.item(0).getValue);
                
                digits = sequenceNode.getElementsByTagName('digits');
                if digits.getLength > 0
                    signals{sequenceIndex} = str2num(char(digits.item(0).getFirstChild.getData)); %#ok<ST2NM>
                end
            end
            signals = vertcat(signals{:})';
            
            % sampling frequency
            samplingFrequency = 250;
            controlVariables = xmlData.getElementsByTagName('controlVariable');
            for variableIndex = 1:controlVariables.getLength
                variableNode = controlVariables.item(variableIndex);
                
                codes = variableNode.getElementsByTagName('code');
                attributes = codes.item(0).getAttributes;
                variableName = char(attributes.item(0).getValue);
                if ~strcmp('MDC_ECG_CTL_VBL_SAMPLE_RATE', variableName), continue; end
                
                values = variableNode.getElementsByTagName('value');
                attributes = values.item(0).getAttributes;
                samplingFrequency = str2double(char(attributes.item(1).getFirstChild.getData));
                break;
            end
            
            ecgData = ECGData(filename, signals, samplingFrequency, signalLabels);
        end
        
        function ecgData = ReadMUMCBase64XML(filename)
            %check and correct xml encoding
            fileHandle = fopen(filename, 'r');
            lineText = fgetl(fileHandle);
            lineNumber = 1;
            while(ischar(lineText))
                fileData{lineNumber} = lineText;
                lineText = fgetl(fileHandle);
                lineNumber = lineNumber + 1;
            end
            fclose(fileHandle);
            
            encodingText = fileData{1};
            correctedEncoding = false;
            position = strfind(encodingText, 'ISO8859-1');
            if ~isempty(position);
                correctedEncodingLine = [encodingText(1:(position-1)),...
                    'ISO-8859-1',...
                    encodingText((position + 9):end)];
                fileData{1} = correctedEncodingLine;
                correctedEncoding = true;
            end
            
            doctypeText = fileData{2};
            position = strfind(doctypeText, 'DOCTYPE');
            removedDocType = false;
            if ~isempty(position);
                fileData{2} = [];
                removedDocType = true;
            end
            
            if correctedEncoding || removedDocType
                fileHandle = fopen(filename, 'w');
                fprintf(fileHandle, '%s\n', fileData{:});
                fclose(fileHandle);
            end
            
            xmlData = xmlread(filename);
            base64 = org.apache.commons.codec.binary.Base64;
            
            stripData = xmlData.getElementsByTagName('Waveform');
            
            sampleFrequencyNodes = stripData.item(1).getElementsByTagName('SampleBase');
            sampleFrequencyNode = sampleFrequencyNodes.item(0);
            samplingFrequency = str2double(char(sampleFrequencyNode.getFirstChild.getData));
            
            sequences = stripData.item(1).getElementsByTagName('LeadData');
            numberOfSequences = sequences.getLength;
            signals = cell(numberOfSequences, 1);
            signalLabels = cell(numberOfSequences, 1);
            
            for sequenceIndex = 0:(numberOfSequences - 1)
                sequenceNode = sequences.item(sequenceIndex);
                
                labelNodes = sequenceNode.getElementsByTagName('LeadID');
                labelNode = labelNodes.item(0);
                signalLabels{sequenceIndex + 1} = char(labelNode.getFirstChild.getData);
                
                gainNodes = sequenceNode.getElementsByTagName('LeadAmplitudeUnitsPerBit');
                gainNode = gainNodes.item(0);
                gain = str2double(char(gainNode.getFirstChild.getData));
                
                dataNodes = sequenceNode.getElementsByTagName('WaveFormData');
                dataNode = dataNodes.item(0);
                base64Data = dataNode.getFirstChild.getData;
                data = typecast(base64.decodeBase64(base64Data.getBytes()), 'int16');
                signals{sequenceIndex + 1} = gain * double(data(:)');
            end
            signals = vertcat(signals{:})';
            
            leadI = strcmp('I', signalLabels);
            leadII = strcmp('II', signalLabels);
            
            %             dlmwrite([filename(1:(end-3)), 'txt'], signals, 'delimiter','\t','precision', '%.2f');
            if any(leadI) && any(leadII)
                leadIII = signals(:, leadII) - signals(:, leadI);
                leadAVR = -(signals(:, leadI) + signals(:, leadII)) / 2;
                leadAVL = signals(:, leadI) - signals(:, leadII) / 2;
                leadAVF = signals(:, leadII) - signals(:, leadI) / 2;
                
                leadIIPosition = find(leadII);
                signals = [signals(:, 1:leadIIPosition),...
                    leadIII, leadAVR, leadAVL, leadAVF,...
                    signals(:, (leadIIPosition + 1):end)];
                signalLabels = [signalLabels(1:leadIIPosition);...
                    'III'; 'aVR'; 'aVL'; 'aVF';...
                    signalLabels((leadIIPosition + 1):end)];
            end
            ecgData = ECGData(filename, signals, samplingFrequency, signalLabels);
        end
        
        function ecgData = ReadYRXML(filename)
            xmlData = xmlread(filename);
            samplingFrequency = 250;
            
            numberOfChannels = 19;
            signalLabels = {'AUX1', 'AUX2', 'AUX3', 'AUX4', 'AUX5',...
                'TE1', 'TE2', 'TE3', 'TE4',...
                'V1', 'V2', 'V3', 'V4', 'V5', 'V6',...
                'LA', 'RA', 'LL',...
                'Channel19'};
            signals = cell(numberOfChannels, 1);
            validChannels = true(numberOfChannels, 1);
            for channelIndex = 1:numberOfChannels
                channelName = ['Channel', num2str(channelIndex - 1)];
                %                 try
                disp(channelName);
                channelData = xmlData.getElementsByTagName(channelName);
                stringData = char(channelData.item(0).getFirstChild.getData);
                %                     xyData = strsplit(stringData, {'{', '}, {'});
                %                     xyData = xyData(2:(end-1));
                %                     numberOfSamples = numel(xyData);
                %                     yData = NaN(numberOfSamples, 1);
                %                     for sampleIndex = 1:1:numel(xyData)
                %                         xyString = strsplit(xyData{sampleIndex}, ',');
                %                         yString = xyString{2};
                %                         yData(sampleIndex) = str2double(yString(4:end));
                %                     end
                
                yData = textscan(stringData, '%f', 'delimiter', ',');
                
                signals{channelIndex} = yData{:};
                
                %                 catch ME
                %                     validChannels(channelIndex) = false;
                %                     disp(ME);
                %                     disp([channelName, ' not found']);
                %                 end
            end
            
            signals = signals(validChannels);
            signals = horzcat(signals{:});
            signalLabels = signalLabels(validChannels);
            ecgData = BDFData(filename, signals, samplingFrequency, signalLabels(:));
        end
        
        function ecgData = ReadBordeauxBSPM(filename, interval)
            if nargin < 2
                interval = [];
            end
            fileHandle = fopen(filename, 'r');
            
            numberOfElectrodes = 256;
            samplingFrequency = 1e3;
            if isempty(interval)
                data = fread(fileHandle, [numberOfElectrodes, Inf], 'float');
            else
                offset = ceil(numberOfElectrodes * (samplingFrequency * interval(1))) / 4;
                numberOfSamples = ceil((interval(2) - interval(1)) * samplingFrequency);
                fseek(fileHandle, offset, 'bof');
                data = fread(fileHandle, [numberOfElectrodes, numberOfSamples], 'float');
            end
            
            electrodeLabels = cellstr(num2str((1:numberOfElectrodes)'));
            ecgData = BDFData(filename, data', samplingFrequency, electrodeLabels);
        end
        
        function ecgData = ReadBordeauxECG(filename)
            fileID = fopen(filename);
            
            % Header information
            dataFound = false;
            lineIndex = 1;
            while ~(dataFound)
                textLine = fgetl(fileID);
                dataFound = ~isempty(strfind(textLine, '[Data]'));
                lineIndex = lineIndex + 1;
            end
            frewind(fileID);
            
            dataIndex = lineIndex;
            textData = cell(dataIndex - 2, 1);
            for lineIndex = 1:(dataIndex - 2)
                textData{lineIndex} = fgetl(fileID);
            end
            
            channelInfoIndices = find(~cellfun(@isempty, strfind(textData, 'Channel #')));
            numberOfChannels = numel(channelInfoIndices);
            channelLabels = cell(numberOfChannels, 1);
            for channelInfoIndex = 1:numel(channelInfoIndices)
                channelLabelString = textData{channelInfoIndices(channelInfoIndex) + 1};
                stringParts = textscan(channelLabelString, '%s');
                channelLabels{channelInfoIndex} = horzcat(stringParts{1}{2:end});
            end
            
            sampleRateIndices = find(~cellfun(@isempty, strfind(textData, 'Sample Rate')));
            sampleRateString = textData{sampleRateIndices(1)};
            stringParts = textscan(sampleRateString, '%s');
            sampleRateString = stringParts{1}{end};
            samplingFrequency = str2double(sampleRateString(1:(end-2)));
            
            % Data
            fgetl(fileID);
            fgetl(fileID);
            
            dataline = fgetl(fileID);
            data = [];
            while ischar(dataline)
                lineData = textscan(dataline, '%d', numberOfChannels, 'delimiter', ',');
                lineData = lineData{:}';
                data = [data; lineData]; %#ok<AGROW>
                dataline = fgetl(fileID);
            end
            
            fclose(fileID);
            
            ecgData = ECGData(filename, double(data), samplingFrequency, channelLabels);
        end
        
        function [ecgData, activations] = ReadPacemapWavemap(filename, eFilename)
            if nargin < 2
                eFilename = [];
            end
            
            [result, waveMemberships, waveActivations, electrodeMapName] =...
                InputOutputPkg.ECGReader.ReadPacemapWaveData(filename); %#ok<ASGLU>
            
            waveActivations = waveActivations(:, 3:end);
            waveMembership = waveMemberships(:, 3:end);
            startDC = (waveMembership > 3000);
            startBT = (waveMembership > 2000) & (waveMembership < 3000);
            startPW = (waveMembership > 1000) & (waveMembership < 2000);
            
            waveMembership = waveMembership - startDC * 3000;
            waveMembership = waveMembership - startBT * 2000;
            waveMembership = waveMembership - startPW * 1000;
            maxActivation = max(waveActivations(:));
            
            if isempty(eFilename)
                electrogramData = NaN(maxActivation, size(waveActivations, 1));
            else
                dcmData = InputOutputPkg.ECGReader.ReadPACEMAPBinary(eFilename, electrodeMapName);
                electrogramData = dcmData.Data;
            end
            
            electrodeLabels = cellstr(num2str((1:size(waveActivations, 1))'));
            ecgData = DCMData(filename, electrogramData, 1e3, electrodeLabels);
            ecgData.SetElectrodeMap(electrodeMapName);
            
            % delete empty electrodes
            mappedElectrodeLabels = ecgData.ElectrodeLabels;
            validChannels = ~all(isnan(waveActivations), 2);
            validLabels = ismember(electrodeLabels, mappedElectrodeLabels);
            validMappedChannels = validChannels(validLabels);
            dcmDataCopy = ecgData.Copy(validMappedChannels);
            delete(ecgData);
            ecgData = dcmDataCopy;
            
            waveActivations = waveActivations(validChannels & validLabels, :);
            waveMembership = waveMembership(validChannels & validLabels, :);
            
            activations = cell(ecgData.GetNumberOfChannels(), 1);
            for electrodeIndex = 1:ecgData.GetNumberOfChannels()
                validActivations = ~isnan(waveActivations(electrodeIndex, :));
                activations{electrodeIndex} = waveActivations(electrodeIndex, validActivations);
            end
            
            distanceMatrix = squareform(pdist(ecgData.ElectrodePositions));
            
            waveIDs = unique(waveMembership(~isnan(waveMembership)));
            waves = AlgorithmPkg.Wave.empty(numel(waveIDs), 0);
            for waveIndex = 1:numel(waveIDs)
                waveMembers = (waveMembership == waveIDs(waveIndex));
                [rowPositions, ~] = find(waveMembers);
                waveMembers = find(waveMembers);
                waves(waveIndex) = AlgorithmPkg.Wave(waveIndex, ecgData.ElectrodePositions);
                for waveMemberIndex = 1:numel(waveMembers)
                    waves(waveIndex).Add(rowPositions(waveMemberIndex),...
                        NaN,...
                        waveActivations(waveMembers(waveMemberIndex)));
                end
                % create connectivity matrix
                conductionThreshold = 0.2;
                conductionMatrix = waves(waveIndex).LocalConduction();
                waveElectrodeIndices = waves(waveIndex).Members(:, 1);
                waveDistanceMatrix = distanceMatrix(waveElectrodeIndices, waveElectrodeIndices);
                electrodeDistance = ecgData.ComputeMinimumSpacing();
                radius = sqrt(electrodeDistance^2 + electrodeDistance^2) + 1e3 * eps;
                connectivityGraph = conductionMatrix >= conductionThreshold &...
                    waveDistanceMatrix <= radius;
                waves(waveIndex).ConnectivityMatrix = sparse(connectivityGraph);
                waves(waveIndex).GetAllStartingPoints();
            end
            ecgData.Waves =  waves;
            ecgData.SetWaveType();
        end
        
        function [result, waveMemberships, waveActivations, electrodeMapName] = ReadPacemapWaveData(filename)
            infile = fopen(filename, 'rt');
            
            sampleOffset = 2;
            linesToSkip = 7;
            
            line = 1;
            electrode = 1;
            wave = [];
            activations = [];
            electrodeMapName = '';
            while ~feof(infile)
                lineText = fgetl(infile);
                
                if line == 2
                    try
                        if ispc
                            [pathString, filename] = fileparts(lineText);
                            electrodeMapName = filename;
                        else
                            fileParts = textscan(lineText, '%s', 'Delimiter', '\\');
                            electrodeMapName = fileParts{1}{end};
                            extensionPosition = strfind(lower(electrodeMapName), '.mat');
                            electrodeMapName = electrodeMapName(1:(extensionPosition - 1));
                        end
                    catch ME
                        errordlg(ME.message, 'No electrodemap information found in file');
                    end
                end
                
                if line > linesToSkip
                    % parse first electrode line
                    rem = lineText;
                    [token, rem] = strtok(rem);
                    sample = 0;
                    
                    % sanity check
                    if 0 == strcmp(token,'Ele:')
                        %             result = -1;
                        %             waveMemberships = [];
                        %             waveActivations = [];
                        %             return;
                        break;
                    end
                    
                    % set electrode number
                    [token, rem] = strtok(rem);
                    wave(electrode, 1) = str2num(token);
                    activations(electrode, 1) = str2num(token);
                    
                    % get samples for the current electrode
                    [token, rem] = strtok(rem);
                    while ~isempty(rem)
                        sample = sample + 1;
                        activations(electrode, sample + sampleOffset) = str2num(token);
                        [token, rem] = strtok(rem);
                    end
                    if ~isempty(token)
                        if ~isempty(str2num(token))
                            sample = sample + 1;
                            activations(electrode, sample + sampleOffset) = str2num(token);
                        end
                    end
                    
                    % parse second electrode line
                    sample = 0;
                    lineText = fgetl(infile);
                    if -1 ~= lineText
                        rem = lineText;
                        [token, rem] = strtok(rem);
                        while 0 == isempty(rem)
                            sample = sample + 1;
                            wave(electrode, sample + sampleOffset) = str2num(token);
                            [token, rem] = strtok(rem);
                        end
                        if ~isempty(token)
                            if ~isempty(str2num(token))
                                sample = sample + 1;
                                wave(electrode, sample + sampleOffset) = str2num(token);
                            end
                        end
                    end
                    
                    % parse third electrode line
                    lineText = fgetl(infile);
                    
                    % write number of samples for current electrode
                    wave(electrode, sampleOffset) = sample;
                    activations(electrode, sampleOffset) = sample;
                    electrode = electrode + 1;
                end
                line = line + 1;
            end
            
            % replace zeros with NaN in wave and activations matrix
            wave(0 == wave) = NaN;
            activations(0 == activations) = NaN;
            
            waveMemberships = wave;
            result = fclose(infile);
            waveActivations = activations;
        end
        
        function ecgData = ReadIDEEQ(filename, electrodeMap)
            if nargin < 2
                electrodeMap = '';
            end
            fileID = fopen(filename);
            
            % number of channels
            fseek(fileID, 195, 'bof');
            numberOfChannels = str2double(deblank(fread(fileID, 3, '*char')));
            
            % start of data blocks
            fseek(fileID, 199, 'bof');
            headerSize = str2double(deblank(fread(fileID, 6, '*char')));
            
            % number of data blocks
            fseek(fileID, 206, 'bof');
            numberOfDataBlocks = str2double(deblank(fread(fileID, 6, '*char')));
            
            % read channel headers
            channelLabels = cell(numberOfChannels, 1);
            channelSamples = zeros(numberOfChannels, 1);
            channelBlockSamples = zeros(numberOfChannels, 1);
            channelSampleRate = NaN(numberOfChannels, 1);
            for channelIndex = 1:numberOfChannels
                fseek(fileID, channelIndex * 256, 'bof');
                channelLabels{channelIndex} = strtrim(fread(fileID, 20, '*char')');
                fseek(fileID, 2, 'cof');
                channelSampleRate(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
                fseek(fileID, 2, 'cof');
                channelBlockSamples(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
                fseek(fileID, 1, 'cof');
                channelSamples(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
            end
            
            % determine global samples per channel and sample rate
            samplesPerChannel = mean(channelSamples);
            samplesPerBlock = mean(channelBlockSamples);
            samplingFrequency = mean(channelSampleRate);
            
            % positions the pointer to the beginning of data
            fseek(fileID, headerSize, 'bof');
            
            data = NaN(samplesPerBlock * numberOfDataBlocks, numberOfChannels);
            for blockIndex = 1:numberOfDataBlocks
                dataPosition = (blockIndex - 1) * samplesPerBlock + 1;
                data(dataPosition:(dataPosition + samplesPerBlock - 1), :) =...
                    fread(fileID, [samplesPerBlock, numberOfChannels], 'single');
            end
            
            % remove trailing zeros at the end of the recording
            allZeroPositions = all(data == 0, 2);
            lastNonZeroPosition = find(~allZeroPositions, 1, 'last');
            data = data(1:lastNonZeroPosition, :);
            
            % create ecgData
            ecgData = DCMData(filename, data, samplingFrequency, channelLabels);
            ecgData.SetElectrodeMap(electrodeMap);
        end
        
        function ecgData = ReadBasketExportData(filename, electrodeMap)
            if nargin < 2
                electrodeMap = 'Constellation Mapping Catheter';
            end
            delimiterIn = ',';
            headerlinesIn = 1;
            filedata = importdata(filename,delimiterIn,headerlinesIn);
            
            electrodeLabels = filedata.colheaders(:);
            data = filedata.data;
            samplingFrequency = 2048;
            ecgData = DCMData(filename, data, samplingFrequency, electrodeLabels);
            ecgData.SetElectrodeMap(electrodeMap);
        end
    end
end