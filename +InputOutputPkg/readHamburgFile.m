function ecgData = readHamburgFile(filename)
fileData = importData(filename);

numberOfChannelsPosition = strmatch(fileData.textdata, 'Channels exported');

channelDescriptionPositions = strmatch(fileData.textdata, 'Channel #:');

end