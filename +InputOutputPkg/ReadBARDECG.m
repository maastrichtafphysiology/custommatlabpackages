function [data, channelLabels, samplingFrequency] = ReadBARDECG(filename)
fileID = fopen(filename);

% Search for [Data] field in file
dataFound = false;
lineIndex = 1;
while ~(dataFound)
    textLine = fgetl(fileID);
    dataFound = ~isempty(strfind(textLine, '[Data]'));
    lineIndex = lineIndex + 1;
end
frewind(fileID);

% Collect text data
dataIndex = lineIndex;
textData = cell(dataIndex - 2, 1);
for lineIndex = 1:(dataIndex - 2)
    textData{lineIndex} = fgetl(fileID);
end

% Determine channel labels
channelInfoIndices = find(~cellfun(@isempty, strfind(textData, 'Channel #')));
numberOfChannels = numel(channelInfoIndices);
channelLabels = cell(numberOfChannels, 1);
for channelInfoIndex = 1:numel(channelInfoIndices)
    channelLabelString = textData{channelInfoIndices(channelInfoIndex) + 1};
    stringParts = textscan(channelLabelString, '%s');
    channelLabels{channelInfoIndex} = horzcat(stringParts{1}{2:end});
end

% Determine sample rate (assume equal sample rate for all channels)
sampleRateIndices = find(~cellfun(@isempty, strfind(textData, 'Sample Rate')));
sampleRateString = textData{sampleRateIndices(1)};
stringParts = textscan(sampleRateString, '%s');
sampleRateString = stringParts{1}{end};
samplingFrequency = str2double(sampleRateString(1:(end-2)));

rangeIndices = find(~cellfun(@isempty, strfind(textData, 'Range')));
rangeString = textData{rangeIndices(1)};
stringParts = textscan(rangeString, '%s');
rangeString = stringParts{1}{end};
voltageRange = str2double(rangeString(1:(end-2)));
dataScaleFactor = voltageRange / 2^15;
            
numberOfheaderLines = dataIndex;
formatSpecifier = repmat('%d ', [1, numberOfChannels]);
frewind(fileID);
data = textscan(fileID, formatSpecifier, 'delimiter', ',',...
    'headerLines', numberOfheaderLines, 'collectOutput', 1);
data = data{1};

fclose(fileID);

data = double(data) * dataScaleFactor;

end