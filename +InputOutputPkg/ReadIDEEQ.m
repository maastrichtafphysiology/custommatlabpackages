function [data, channelLabels, samplingFrequency] = ReadIDEEQ(filename)
fileID = fopen(filename);

% number of channels
fseek(fileID, 195, 'bof');
numberOfChannels = str2double(deblank(fread(fileID, 3, '*char')));

% start of data blocks
fseek(fileID, 199, 'bof');
headerSize = str2double(deblank(fread(fileID, 6, '*char')));

% number of data blocks
fseek(fileID, 206, 'bof');
numberOfDataBlocks = str2double(deblank(fread(fileID, 6, '*char')));

% read channel headers
channelLabels = cell(numberOfChannels, 1);
channelSamples = zeros(numberOfChannels, 1);
channelBlockSamples = zeros(numberOfChannels, 1);
channelSampleRate = NaN(numberOfChannels, 1);
for channelIndex = 1:numberOfChannels
    fseek(fileID, channelIndex * 256, 'bof');
    channelLabels{channelIndex} = strtrim(fread(fileID, 20, '*char')');
    fseek(fileID, 2, 'cof');
    channelSampleRate(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
    fseek(fileID, 2, 'cof');
    channelBlockSamples(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
    fseek(fileID, 1, 'cof');
    channelSamples(channelIndex) = str2double(deblank(fread(fileID, 10, '*char')));
end

% determine global samples per channel and sample rate
samplesPerChannel = mean(channelSamples);
samplesPerBlock = mean(channelBlockSamples);
samplingFrequency = mean(channelSampleRate);

% positions the pointer to the beginning of data
fseek(fileID, headerSize, 'bof');

data = NaN(samplesPerBlock * numberOfDataBlocks, numberOfChannels);
for blockIndex = 1:numberOfDataBlocks
    dataPosition = (blockIndex - 1) * samplesPerBlock + 1;
    data(dataPosition:(dataPosition + samplesPerBlock - 1), :) =...
        fread(fileID, [samplesPerBlock, numberOfChannels], 'single');
end
end