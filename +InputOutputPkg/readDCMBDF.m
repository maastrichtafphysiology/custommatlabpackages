function [ecgData, subjectInfo, gain, prefiltering, channelDimensions] =...
    readDCMBDF(filename, varargin)

% this function loads at least "secondsToLoad" seconds of EEG data from the
% specified bdf-file "filename". If all available data is needed, specify
% 'all' instead of "secondsToLoad" like this:
% [...] = eeg_read_bdf('test.bdf','all');
% EEg data is read from the first sample.
% "reshape" ('y' or 'n') specifies whether data must be reshaped (ActiveTwo
% by BioSemi 65 electrodes)... according to author's humble understanding.
%
% The outputs are:
% "data" - EEG data of size numberOfChannels by secondsToLoad * samplingFrequency
% "numberOfChannels" - number of EEG channel (including the status channel)
% "electrodeLabels" - EEG electrode electrodeLabels
% "subjectInfo" - a string containg information about the subject entered wile
% recorging EEG
% "samplingFrequency" - sampling frequency, Hz
% "gain" - gain factor, times
% "channelDimensions" - Physical dimension of the EEG channels (for instance, uV)
% "prefiltering" - information about filters used during EEG recording
%
% Note: for some reason, bdf format specifies separate sets of "samplingFrequency",
% "gain", "channelDimensions", and "prefiltering" for every EEG channel. This
% function returns these values for the first channel (electrode) only!
%
% Copyleft by Gleb Tcheslavski, gleb@vt.edu
% Edited by Stef Zeemering, s.zeemering@maastrichtuniversity.nl

parser = inputParser;
parser.addRequired('filename', @(x)(ischar(x) && exist(x, 'file')));
parser.addOptional('secondsToLoad', 'all', @(x)(strcmpi(x, 'all') || isnumeric(x)));
parser.addOptional('reshape', 'n', @(x)(any(strcmpi(x, {'n', 'y'}))));
parser.addOptional('resampleFrequency', 1, @(x)(isnumeric(x)));
parser.parse(filename, varargin{:});
secondsToLoad = parser.Results.secondsToLoad;
reshape = parser.Results.reshape;
resampleFrequency = parser.Results.resampleFrequency;

idx = [1 34 5 40 13 50 21 58 27 64 7 42 15 52 23 60 38 48 31 47 32 18 55 10 45 16 53 8 43 33 37 30 26 63 3 36 2 35 25 62 4 39 12 49 20 57 6 41 9 44 14 51 17 54 22 59 19 56 11 46 24 61 28 29 65];

fid = fopen(filename);

% user text
fseek(fid, 130, 'bof');
subjectInfo = cellstr(char(fread(fid, 54, '10*char'))');

% number of data records
fseek(fid, 236, 'bof');
numberOfRecords = str2double(deblank(char(fread(fid, 7, '1*char'))));

% duration of a data record in seconds
fseek(fid, 244, 'bof');
recordDuration = str2double(deblank(char(fread(fid, 3, '1*char'))));

% number of EEG channels
fseek(fid, 252, 'bof');
numberOfChannels = str2double(deblank(char(fread(fid, 3, '1*char'))));

% EEG electrodeLabels
fseek(fid, 256, 'bof');
electrodeLabels = cellstr(deblank(char(fread(fid, [7, numberOfChannels], '7*char', 9)')));

% Find status channel
statusIndex = find(strcmpi('Status', electrodeLabels));
if isempty(statusIndex)
    
end

% filters applied - need to check from this point
fseek(fid, 256 + numberOfChannels * 136, 'bof');
prefiltering = cellstr(char(fread(fid, 25, '10*char'))');

% physical dimension of the channels (uV,  for instance)
fseek(fid, 256 + numberOfChannels * 96, 'bof');
channelDimensions = cellstr(deblank(char(fread(fid, 5, '5*char'))'));

% number of samples in each record
fseek(fid, 256 + numberOfChannels * 216, 'bof');
numberOfSamples = str2double(deblank(char(fread(fid, 4, '1*char'))));

% sampling frequency
samplingFrequency = numberOfSamples / recordDuration;
[p, q] = rat(resampleFrequency / samplingFrequency, 0.0001);
if p == 1 && q == 1
    resampleData = false;
else
    resampleData = true;
end
samplingFrequency = (p / q) * samplingFrequency;
numberOfStoredSamples = ceil((p / q) * numberOfSamples);

% physical min
fseek(fid, 256 + numberOfChannels * 104, 'bof');
physicalMin = str2double(deblank(char(fread(fid, 8, '1*char'))));

% physical max
fseek(fid, 256 + numberOfChannels * 112, 'bof');
physicalMax = str2double(deblank(char(fread(fid, 8, '1*char'))));

% digital min
fseek(fid, 256 + numberOfChannels * 120, 'bof');
digitalMin = str2double(deblank(char(fread(fid, 8, '1*char'))));

% digital max
fseek(fid, 256 + numberOfChannels * 128, 'bof');
digitalMax = str2double(deblank(char(fread(fid, 8, '1*char'))));

% gain factor
gain = (physicalMax - physicalMin) / (digitalMax - digitalMin);

if strcmpi(secondsToLoad, 'all')
    recordsToLoad = numberOfRecords;
    startRecord = 1;
else
    startRecord = floor(secondsToLoad(1) / recordDuration) + 1;
    if startRecord < 1
        startRecord = 1;
    end
    endRecord = ceil(secondsToLoad(end) / recordDuration);
    if endRecord > numberOfRecords
        endRecord = numberOfRecords;
    end
    recordsToLoad = endRecord - startRecord + 1;
end

% define optional decimate filter
rip = .05;
nfilt = 8;
r = q;
[chebB, chebA] = cheby1(nfilt, rip, .8 / r);

UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
    UserInterfacePkg.StatusChangeEventData('Reading BDF data...', 0, []));

% positions the pointer to the beginning of data
% fseek(fid, 256 * (numberOfChannels + 1), 'bof');
startPosition = (startRecord - 1) * recordDuration * numberOfSamples * numberOfChannels * 3;
fseek(fid, 256 * (numberOfChannels + 1) + startPosition, 'bof');
data = NaN(recordDuration  * numberOfStoredSamples  *  recordsToLoad, numberOfChannels);
for recordIndex = 1:recordsToLoad
    aux = fread(fid, [recordDuration * numberOfSamples, numberOfChannels], 'bit24');
    if resampleData
        if p == 1
            downsampledData = NaN(numberOfStoredSamples, numberOfChannels);
            for channelIndex = 1:numberOfChannels
                downsampledData(:, channelIndex) = customDecimate(aux(:, channelIndex), q, chebA, chebB);
            end
            aux = downsampledData;
        elseif q == 1
            aux = upsample(aux, p);
        else
            aux = resample(aux, p, q);
        end
    end
    if reshape == 'y'
        aux = aux(idx, :);
    end;
    dataPosition = (recordDuration  *  numberOfStoredSamples)  *  (recordIndex - 1)  +  1;
    data(dataPosition:(dataPosition  +  recordDuration  *  numberOfStoredSamples - 1), :) = aux;
    
    UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
        UserInterfacePkg.StatusChangeEventData(['Reading BDF data... (' num2str(recordIndex)...
        ' of ' num2str(recordsToLoad) ' records)'], recordIndex / recordsToLoad, []));
end;

UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
    UserInterfacePkg.StatusChangeEventData('Done', 1, []));

% chop off excess data at beginning and end (if any)
startDifference = secondsToLoad(1) - ((startRecord - 1) * recordDuration);
if startDifference >= (1 / samplingFrequency)
    data = data(floor(startDifference * samplingFrequency):end, :);
end
endDifference = (endRecord * recordDuration)- secondsToLoad(end);
if endDifference >= (1 / samplingFrequency)
    data = data(1:(end - floor(endDifference * samplingFrequency)), :);
end

data = data * gain / 1000;
ecgData = DCMData(filename, data, samplingFrequency, electrodeLabels);

fclose(fid);
end

function odata = customDecimate(idata, r, chebA, chebB)
% adapted from decimate

if fix(r) == 1
    odata = idata;
    return
end

nd = length(idata);
nout = ceil(nd/r);

% be sure to filter in both directions to make sure the filtered data has zero phase
% make a data vector properly pre- and ap- pended to filter forwards and back
% so end effects can be obliterated.
odata = filtfilt(chebB, chebA, idata);
nbeg = r - (r * nout - nd);
odata = odata(nbeg:r:nd);
end