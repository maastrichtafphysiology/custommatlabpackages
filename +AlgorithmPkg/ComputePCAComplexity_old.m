function [segmentSpatialComplexities segmentTemporalComplexity] =...
    ComputePCAComplexity(ecgData, samplingFrequency, rIndices, numberOfSegments, v1Index, cnvThreshold, qShift)
%#codegen

numberOfSamples = size(ecgData, 1);

% extract TQ intervals from data
qShift = round((qShift / 1000) * samplingFrequency);
minRR = min(diff(rIndices, 1, 1), [], 1);
qtStart = -qShift;
qtEnd = minRR - qShift;
QTRange = qtStart:1:qtEnd;
QTIndices = bsxfun(@plus, rIndices, QTRange)';
TQIndices = ~(ismember(1:numberOfSamples, QTIndices(:)));

segmentSize = floor(numberOfSamples / numberOfSegments);

%1. spatial complexity
segmentSpatialComplexities = NaN(numberOfSegments, 1);
for segmentIndex = 1:numberOfSegments
    firstIndex = (segmentIndex - 1) * segmentSize + 1;
    lastIndex = segmentIndex * segmentSize;
    
    data = ecgData(firstIndex:lastIndex, :);
    data = data(TQIndices(firstIndex:lastIndex), :);
    data = bsxfun(@minus, data, mean(data));
    [U, S, V] = svd(data', 0);
    
    singularValues = diag(S, 0);
    cumulativeNormalizedVariance = cumsum(singularValues(:).^2) ./ sum(singularValues(:).^2);
    cNVAboveThreshold = find(cumulativeNormalizedVariance >= cnvThreshold);
    if ~isempty(cNVAboveThreshold)
        segmentSpatialComplexities(segmentIndex) = cNVAboveThreshold(1);
    end
end

%2. temporal complexity
data = ecgData(1:segmentSize, :);
data = data(TQIndices(1:segmentSize), :);
data = bsxfun(@minus, data, mean(data));
[U, S, V] = svd(data', 0);
M1 = U * S ./ sqrt(double(segmentSize));
M1 = M1(:, 1:segmentSpatialComplexities(1));

segmentTemporalComplexity = NaN(numberOfSegments - 1, 1);
for segmentIndex = 2:numberOfSegments
    firstIndex = (segmentIndex - 1) * segmentSize + 1;
    lastIndex = segmentIndex * segmentSize;
    
    data = ecgData(firstIndex:lastIndex, :);
    data = data(TQIndices(firstIndex:lastIndex), :);
    data = bsxfun(@minus, data, mean(data));
    
    dataProjection = (M1 * pinv(M1) * (data'))';
    segmentTemporalComplexity(segmentIndex - 1) =...
        sum((data(:, v1Index) - dataProjection(:, v1Index)).^2) / sum(data(:, v1Index).^2);
end