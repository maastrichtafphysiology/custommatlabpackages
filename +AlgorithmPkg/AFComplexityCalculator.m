classdef AFComplexityCalculator < handle
    properties
        AmplitudePercentage
        LongIntervalPercentage
        LongIntervalThreshold
        
        QRInterval
        CNVThreshold
        SegmentLength
        FWaveDistanceThreshold
        
        DFParameters
        SampleEntropyParameters
    end
    
    methods
        function self = AFComplexityCalculator()
            self.AmplitudePercentage = 25;
            self.LongIntervalPercentage = 0;
            self.LongIntervalThreshold = 250;
            
            self.QRInterval = [40, 40];
            self.CNVThreshold = 0.95;
            self.SegmentLength = 10;
            
            self.FWaveDistanceThreshold = 100;
            
            self.DFParameters = struct(...
                'NumberOfFFTPoints', 2^10,...
                'WindowLength', 2^10,...
                'FFTOverlap', 0.5,...
                'DFRange', [3, 12],...
                'NumberOfPeaks', 5,...
                'PeakRange', 1,...
                'FundamentalFrequency', false,...
                'FundamentalFrequencyThreshold', 0.75);
            
            self.SampleEntropyParameters = struct(...
                'samples', 2,...
                'tolerance', 0.35);
        end
        
        function complexity = ComputeAutomaticAmplitudeAFComplexity(self, deflectionData)
            cycleLengthFit = deflectionData.Fit;
            deflectionCalculator = AlgorithmPkg.DeflectionAssignmentCalculator(cycleLengthFit);
            templateMatchingAnalysisResult = deflectionData.AnalysisResults;
            
            deflectionCalculator.AssignmentAlgorithm = 'Smallest amplitude';
            intrinsicDeflectionData = deflectionCalculator.Apply(templateMatchingAnalysisResult);
            
            amplitudePercentage = self.AmplitudePercentage;
            complexity = NaN(size(amplitudePercentage));
            
            for amplitudeIndex = 1:numel(amplitudePercentage)
                channelComplexities = NaN(size(intrinsicDeflectionData));
                currentAmplitudePercentage = amplitudePercentage(amplitudeIndex);
                parfor channelIndex = 1:numel(intrinsicDeflectionData)
                    intrinsicDeflections = intrinsicDeflectionData{channelIndex}.PrimaryDeflections;
                    numberOfIntrinsicDeflections = numel(find(intrinsicDeflections));
                    intrinsicAmplitudes = intrinsicDeflectionData{channelIndex}.templatePeakAmplitudes(intrinsicDeflections);
                    amplitudeMean = median(intrinsicAmplitudes);
                    
                    filterSettings = FilterSettings();
                    filterSettings.OverlapFilterEnabled = true;
                    filterSettings.amplitudeThreshold = amplitudeMean * (currentAmplitudePercentage / 100);
                    deflectionFilter = AlgorithmPkg.DeflectionFilter(filterSettings);
                    filteredResult = deflectionFilter.Apply(intrinsicDeflectionData{channelIndex});
                    
                    farFieldDeflections = ~filteredResult.PrimaryDeflections;
                    numberOfFarFieldDeflections = numel(find(farFieldDeflections));
                    channelComplexities(channelIndex) = numberOfFarFieldDeflections / numberOfIntrinsicDeflections;
                end
                complexity(amplitudeIndex) = mean(channelComplexities);
            end
        end
        
        function complexity = ComputeAutomaticLongIntervalAFComplexity(self, ecgData, deflectionData)
            complexity = NaN(size(self.LongIntervalPercentage));
            
            time = ecgData.GetTimeRange();
            duration = time(end) - time(1);
            numberOfChannels = ecgData.GetNumberOfChannels();
            cycleLengthFit = deflectionData.Fit;
            expectedNumberOfIntrinsicDeflections =...
                round(numberOfChannels * duration / cycleLengthFit.Beta(2));
            
            templateMatchingAnalysisResult = deflectionData.AnalysisResults;
            
            longIntervalPercentage = self.LongIntervalPercentage;
            parfor intervalIndex = 1:numel(longIntervalPercentage)
                parameters = struct(...
                    'FilterSettingName', 'amplitudeThreshold',...
                    'AnalysisResultName', 'templateAmplitudes',...
                    'Min', [], 'Max', [], 'StepSize', [],...
                    'Direction', 'increase',...
                    'Friendly', 'Amplitude');
                optimizationProtocol = struct(...
                    'Parameter', 'amplitudeThreshold',...
                    'Action', 'Optimize',...
                    'Value', 0,...
                    'ParameterIndex', 1,...
                    'Factor', 1,...
                    'Percentage', longIntervalPercentage(intervalIndex));
                
                deflectionOptimizationFilter = AlgorithmPkg.TemplateMatchingOptimizationFilter(...
                    parameters, optimizationProtocol);
                
                filteredAnalysisResults = deflectionOptimizationFilter.Apply(templateMatchingAnalysisResult);
                
                numberOfCandidateDeflections = 0;
                for channelIndex = 1:numel(filteredAnalysisResults)
                    numberOfCandidateDeflections = numberOfCandidateDeflections +...
                        numel(filteredAnalysisResults{channelIndex}.templatePeakIndices);
                end
                
                complexity(intervalIndex) = (numberOfCandidateDeflections / expectedNumberOfIntrinsicDeflections) - 1;
            end
        end
        
        function [segmentSpatialComplexities, segmentTemporalComplexity] = ComputePCAComplexity(self, ecgData, rIndices, v1Index)
            if nargin < 4
                v1Index = 1:ecgData.GetNumberOfChannels;
                if nargin < 3
                    rIndices = [];
                end
            end
            numberOfSamples = ecgData.GetNumberOfSamples();
            
            % extract TQ intervals from data
            if isempty(rIndices)
                TQIndices = true(ecgData.GetNumberOfSamples, 1);
            else
                qShift = round((self.QRInterval / 1000) * ecgData.SamplingFrequency);
                minRR = min(diff(rIndices));
                QTRange = (-qShift(1)):(minRR - qShift(end));
                QTIndices = bsxfun(@plus, rIndices, QTRange);
                TQIndices = ~(ismember(1:numberOfSamples, QTIndices(:)));
                % exclude beginning and end
                TQIndices(1:QTIndices(1)) = false;
                TQIndices(QTIndices(end):end) = false;
            end
            
            segmentSize = floor(ecgData.SamplingFrequency * self.SegmentLength);
            numberOfSegments = floor(numberOfSamples / segmentSize);
            if numberOfSegments == 0
                segmentSize = ecgData.GetNumberOfSamples;
                numberOfSegments = 1;
            end
            
            %1. spatial complexity
            segmentSpatialComplexities = NaN(numberOfSegments, 1);
            for segmentIndex = 1:numberOfSegments
                firstIndex = (segmentIndex - 1) * segmentSize + 1;
                lastIndex = segmentIndex * segmentSize;
                
                data = ecgData.Data(firstIndex:lastIndex, :);
                data = data(TQIndices(firstIndex:lastIndex), :);
                data = bsxfun(@minus, data, mean(data));
                [V, S, U] = svd(data, 0);
                
                singularValues = diag(S, 0);
                cumulativeNormalizedVariance = cumsum(singularValues(:).^2) ./ sum(singularValues(:).^2);
                cNVAboveThreshold = find(cumulativeNormalizedVariance >= self.CNVThreshold);
                if ~isempty(cNVAboveThreshold)
                    segmentSpatialComplexities(segmentIndex) = cNVAboveThreshold(1);
                end
            end
            
            %2. temporal complexity
            data = ecgData.Data(1:segmentSize, :);
            data = data(TQIndices(1:segmentSize), :);
            data = bsxfun(@minus, data, mean(data));
            [V, S, U] = svd(data, 0);
            M1 = U * S ./ sqrt(double(segmentSize));
            M1 = M1(:, 1:segmentSpatialComplexities(1));
            
            segmentTemporalComplexity = NaN(numberOfSegments, numel(v1Index));
            for segmentIndex = 1:numberOfSegments
                for referenceIndex = 1:numel(v1Index)
                    firstIndex = (segmentIndex - 1) * segmentSize + 1;
                    lastIndex = segmentIndex * segmentSize;
                    
                    data = ecgData.Data(firstIndex:lastIndex, :);
                    data = data(TQIndices(firstIndex:lastIndex), :);
                    data = bsxfun(@minus, data, mean(data));
                    
                    dataProjection = (M1 * pinv(M1) * (data'))';
                    segmentTemporalComplexity(segmentIndex, referenceIndex) =...
                        sum((data(:, v1Index(referenceIndex)) - dataProjection(:, v1Index(referenceIndex))).^2) / sum(data(:, v1Index(referenceIndex)).^2);
                end
            end
        end
        
        function [fWaveAmplitudes, fWaveIndices, fWavePeakValleyIndices] = ComputeFWaveAmplitudes(self, ecgData)
            %             [B,A] = cheby2(3, 20, [0.5 / (ecgData.SamplingFrequency / 2 ) 60 / (ecgData.SamplingFrequency / 2)]);
             % Highpass filter of 3Hz
             nyquistFrequency = ecgData.SamplingFrequency / 2;
             [b, a] = cheby2(3, 20,...
                 3 / nyquistFrequency, 'high');
                
            peakDistanceThreshold = floor(self.FWaveDistanceThreshold * ecgData.SamplingFrequency / 1000);
            
            numberOfChannels = ecgData.GetNumberOfChannels;
            fWaveIndices = cell(numberOfChannels, 1);
            fWaveAmplitudes = cell(numberOfChannels, 1);
            fWavePeakValleyIndices = cell(numberOfChannels, 1);
            
            for channelIndex = 1:numberOfChannels
                signal = ecgData.Data(:, channelIndex);
%                 signal = filtfilt(b, a, signal);
                [peakAmplitudes, peakIndices] = findpeaks(signal, 'MINPEAKDISTANCE', peakDistanceThreshold);
                [valleyAmplitudes, valleyIndices] = findpeaks(-signal);
                valleyAmplitudes = -valleyAmplitudes;
                
                % filter for zero- crossings
                peakIndices = peakIndices(peakAmplitudes > 0);
                valleyIndices = valleyIndices(valleyAmplitudes < 0);
                
                % take the highest peak in concurrent peaks
                currentPeakIndex = 1;
                validPeaks = false(size(peakIndices));
                while currentPeakIndex <= numel(peakIndices);
                    nextValleyIndex = find(valleyIndices > peakIndices(currentPeakIndex), 1, 'first');
                    
                    if isempty(nextValleyIndex)
                        lastPeakBeforeValleyIndex = numel(peakIndices);
                    else
                        lastPeakBeforeValleyIndex = find(peakIndices < valleyIndices(nextValleyIndex), 1, 'last');
                    end
                    concurrentPeakIndices = peakIndices(currentPeakIndex:lastPeakBeforeValleyIndex);
                    
                    [maxPeakAmplitude, maxPeakIndex] = max(signal(concurrentPeakIndices)); %#ok<ASGLU>
                    validPeaks(currentPeakIndex + maxPeakIndex - 1) = true;
                    
                    currentPeakIndex = lastPeakBeforeValleyIndex + 1;
                end
                peakIndices = peakIndices(validPeaks);
                
                % take the deepest valley in concurrent valleys
                currentValleyIndex = 1;
                validValleys = false(size(valleyIndices));
                while currentValleyIndex <= numel(valleyIndices);
                    nextPeakIndex = find(peakIndices > valleyIndices(currentValleyIndex), 1, 'first');
                    
                    if isempty(nextPeakIndex)
                        lastValleyBeforePeakIndex = numel(valleyIndices);
                    else
                        lastValleyBeforePeakIndex = find(valleyIndices < peakIndices(nextPeakIndex), 1, 'last');
                    end
                    concurrentValleyIndices = valleyIndices(currentValleyIndex:lastValleyBeforePeakIndex);
                    
                    [minValleyAmplitude, minValleyIndex] = min(signal(concurrentValleyIndices)); %#ok<ASGLU>
                    validValleys(currentValleyIndex + minValleyIndex - 1) = true;
                    
                    currentValleyIndex = lastValleyBeforePeakIndex + 1;
                end
                valleyIndices = valleyIndices(validValleys);
                
                peakAmplitudes = NaN(size(peakIndices));
                peakValleyIndices = NaN(size(peakIndices));
                for currentPeakIndex = 1:numel(peakIndices)
                    nextValleyIndex = find(valleyIndices > peakIndices(currentPeakIndex), 1, 'first');
                    if isempty(nextValleyIndex), break; end;
                    
                    peakAmplitudes(currentPeakIndex) = signal(peakIndices(currentPeakIndex)) - signal(valleyIndices(nextValleyIndex));
                    peakValleyIndices(currentPeakIndex) = valleyIndices(nextValleyIndex);
                end
                
                validAmplitudes = ~isnan(peakAmplitudes);
                fWaveAmplitudes{channelIndex} = peakAmplitudes(validAmplitudes);
                fWaveIndices{channelIndex} = peakIndices(validAmplitudes);
                fWavePeakValleyIndices{channelIndex} = peakValleyIndices(validAmplitudes);
            end
        end
        
        function [dominantFrequencyIndex, frequencyPower, frequencies] = ComputeDominantFrequency(self, ecgData, validChannelIndices)
            if nargin < 3
                validChannelIndices = 1:ecgData.GetNumberOfChannels;
            end
            
%             time = ecgData.GetTimeRange();
%             validTime = time >= 1 & time <= (time(end) - 1);
            
            [spectra, ~, dominantFrequencyIndex] = self.ComputePWelchECGSpectrum(ecgData.Data(:, validChannelIndices), ecgData.SamplingFrequency);
            
            frequencies = horzcat(spectra.Frequencies);
            frequencyPower = horzcat(spectra.Data);
            
            
%             segmentLength = self.DFParameters.NumberOfFFTPoints;
%             overlap = floor(self.DFParameters.FFTOverlap * segmentLength);
%             
%             frequencyPower = zeros(self.DFParameters.NumberOfFFTPoints / 2 + 1, numel(validChannelIndices));
%             frequencies = (0:(self.DFParameters.NumberOfFFTPoints / 2)) * (ecgData.SamplingFrequency / self.DFParameters.NumberOfFFTPoints);
%             dominantFrequencyIndex = NaN(numel(validChannelIndices), 1);
%             
%             validFrequencies = frequencies >= self.DFParameters.DFRange(1) &...
%                 frequencies <= self.DFParameters.DFRange(2);
% 
%             for channelIndex = 1:numel(validChannelIndices)
%                 channelData = ecgData.Data(:, validChannelIndices(channelIndex));
%                 if self.DFParameters.NumberOfFFTPoints > numel(channelData)
%                     channelData = [channelData; zeros(self.DFParameters.NumberOfFFTPoints - numel(channelData), 1)]; %#ok<AGROW>
%                 end
% %                 [frequencyPower(:, channelIndex), frequencies] =...
% %                     pwelch(channelData,...
% %                     segmentLength, overlap, self.DFParameters.NumberOfFFTPoints, ecgData.SamplingFrequency);
%                 
%                 [frequencyPower(:, channelIndex), frequencies] =...
%                     pwelch(channelData,...
%                     [], [], [], ecgData.SamplingFrequency);
%                 
%                 [peakValues, peakLocations] = findpeaks(frequencyPower(:, channelIndex));
%                 peakValues = peakValues(validFrequencies(peakLocations));
%                 peakLocations = peakLocations(validFrequencies(peakLocations));
%                 if isempty(peakLocations), continue; end
%                 
%                 [~, maxPosition] = max(peakValues);
%                 dominantFrequencyIndex(channelIndex) = peakLocations(maxPosition);
%             end
        end
        
        function [dominantFrequencyIndex, frequencyPower, frequencies, dominantFrequencyValues, dominantFrequencyPower, spectra] =...
                ComputeDominantSignalFrequency(self, signal, samplingFrequency, validChannelIndices)
            if nargin < 4
                validChannelIndices = 1:size(signal, 2);
            end
            
            [spectra, dominantFrequencyValues, dominantFrequencyIndex, dominantFrequencyPower] = self.ComputePWelchECGSpectrum(signal(:, validChannelIndices), samplingFrequency);
            
            frequencies = horzcat(spectra.Frequencies);
            frequencyPower = horzcat(spectra.Data);
        end
        
        function windowedDF = ComputeWindowedDominantFrequency(self, ecgData, windowLength, windowShift)
            windowSampleLength = round(windowLength * ecgData.SamplingFrequency);
            windowSampleShift = round(windowShift * ecgData.SamplingFrequency);
            
            windowStart = 1:windowSampleShift:ecgData.GetNumberOfSamples;
            windowEnd = (windowSampleLength):windowSampleShift:ecgData.GetNumberOfSamples;
            numberOfWindows = numel(windowEnd);
            windowStart = windowStart(1:numberOfWindows);
            
            numberOfChannels = ecgData.GetNumberOfChannels();
            windowedDF = NaN(numberOfChannels, numberOfWindows);
            
            for windowIndex = 1:numberOfWindows
                windowedData = ecgData.Data(windowStart(windowIndex):windowEnd(windowIndex), :);
                psdEstimates = self.ComputeDCMSpectrum(windowedData, ecgData.SamplingFrequency);
                for channelIndex = 1:numberOfChannels
                    psdEstimate = psdEstimates(channelIndex);
                    [peakValues peakIndices] = findpeaks(psdEstimate.Data);
                    
                    validDFFrequencies = psdEstimate.Frequencies > self.DFParameters.DFRange(1) &...
                        psdEstimate.Frequencies < self.DFParameters.DFRange(end);
                    
                    peakDFValues = peakValues(validDFFrequencies(peakIndices));
                    peakDFIndices = peakIndices(validDFFrequencies(peakIndices));
                    if isempty(peakDFIndices), continue; end
                    [~, maxPosition] = max(peakDFValues);
                    windowedDF(channelIndex, windowIndex) = psdEstimate.Frequencies(peakDFIndices(maxPosition));
                end
            end
        end
        
        function [sampleEntropy, dfSampleEntropy] = ComputeSampleEntropy(self, ecgData, validChannelIndices, dominantFrequency, resampleFrequency)
            if nargin < 5
                resampleFrequency = ecgData.SamplingFrequency;
                if nargin < 4
                    dominantFrequency = NaN(ecgData.GetNumberOfChannels, 1);
                    if nargin < 3
                        validChannelIndices = 1:ecgData.GetNumberOfChannels;
                    end
                end
            end
            
            [p, q] = rat(resampleFrequency / ecgData.SamplingFrequency, 0.0001);
            if p == 1 && q == 1
                resampleData = false;
            else
                resampleData = true;
            end
            
            sampleEntropy = NaN(numel(validChannelIndices), 1);
            dfSampleEntropy = NaN(numel(validChannelIndices), 1);
            for channelIndex = 1:numel(validChannelIndices)
                data = ecgData.Data(:, validChannelIndices(channelIndex));
                if resampleData
                    if p == 1
                        data = decimate(data, q);
                    elseif q == 1
                        data = interp(data, p);
                    else
                        data = resample(data, p, q);
                    end
                end
                
                channelEntropy =...
                    AlgorithmPkg.AFComplexityCalculator.SampleEntropy(data,...
                    self.SampleEntropyParameters.samples,...
                    self.SampleEntropyParameters.tolerance);
                sampleEntropy(channelIndex) = channelEntropy(end);
                
                df = dominantFrequency(validChannelIndices(channelIndex));
                if isnan(df), continue; end
                
                nyquistFrequency = resampleFrequency / 2;
                [b, a] = cheby2(3, 20,...
                    [(df - 1.5) / nyquistFrequency, (df + 1.5) / nyquistFrequency]);
                signal = filtfilt(b, a, ecgData.Data(:, validChannelIndices(channelIndex)));
                dfChannelEntropy =...
                    AlgorithmPkg.AFComplexityCalculator.SampleEntropy(signal,...
                    self.SampleEntropyParameters.samples,...
                    self.SampleEntropyParameters.tolerance);
                dfSampleEntropy(channelIndex) = dfChannelEntropy(end);
            end
        end
        
        function spectralParameters = ComputeSpectralParameters(self, ecgData, isECG)
            if nargin < 3
                isECG = true;
            end
            
            if isECG
%                 spectra = self.ComputeECGSpectrum(ecgData.Data, ecgData.SamplingFrequency);
                spectra = self.ComputePWelchECGSpectrum(ecgData.Data, ecgData.SamplingFrequency);
            else
                spectra = self.ComputeDCMSpectrum(ecgData.Data, ecgData.SamplingFrequency);
            end
            
            [DF, OI] = self.ComputeOrganizationIndex(spectra);
            RI = self.ComputeRegularityIndex(spectra, DF, self.DFParameters.PeakRange);
            SE = self.ComputeSpectralEntropy(spectra);
            
            spectralParameters = struct(...
                'DominantFrequency', DF,...
                'OrganizationIndex', OI,...
                'RegularityIndex', RI,...
                'SpectralEntropy', SE,...
                'psd', spectra);
        end
        
        function spectralEntropy = ComputeSpectralEntropy(self, psd)
            spectralEntropy = NaN(size(psd));
            for spectrumIndex = 1:numel(psd)
                normalizedPower = psd(spectrumIndex).Data ./ sum(psd(spectrumIndex).Data);
                spectralEntropy(spectrumIndex) = -normalizedPower(:)' * log2(normalizedPower(:));
            end
        end
        
        function waveletEntropy = ComputeWaveletEntropy(self, ecgData, maxLevel, waveletName, validChannelIndices)
            if nargin < 5
                validChannelIndices = 1:ecgData.GetNumberOfChannels;
                if nargin < 4
                    waveletName = 'haar';
                    if nargin < 3
                        maxLevel = 7;
                    end
                end
            end
            waveletEntropy = NaN(numel(validChannelIndices), 1);
            
            for channelIndex = 1:numel(validChannelIndices)
                % resample to 1024Hz to match wavelet scales to AF frequency range
                data = resample(ecgData.Data(:, validChannelIndices(channelIndex)), 1024, ecgData.SamplingFrequency);
                [coefficients, details] = wavedec(data, maxLevel, waveletName);
                levelEnergy = NaN(maxLevel, 1);
                for level = 1:maxLevel
                    levelCoefficients = appcoef(coefficients, details, waveletName, level);
                    levelEnergy(level) = sum(levelCoefficients.^2);
                end
                relativeEnergy = levelEnergy ./ sum(levelEnergy);
                waveletEntropy(channelIndex) = -relativeEnergy' * log(relativeEnergy);
            end
        end
        
        function [df, organizationIndex] = ComputeOrganizationIndex(self, psd)
            df = NaN(size(psd));
            organizationIndex = NaN(size(psd));
            for i = 1:numel(psd)
                lowerThreshold = self.DFParameters.DFRange(1);
                upperThreshold = self.DFParameters.DFRange(2);
                numberOfDominantPeaks = self.DFParameters.NumberOfPeaks;
                binWidth = mean(diff(psd(i).Frequencies));
                validDFFrequencies = psd(i).Frequencies > lowerThreshold &...
                    psd(i).Frequencies < upperThreshold;
                
                [peakValues, peakIndices] = findpeaks(psd(i).Data);
                numberOfDominantPeaks = min([numberOfDominantPeaks, numel(peakValues)]);
                
                peakDFValues = peakValues(validDFFrequencies(peakIndices));
                peakDFIndices = peakIndices(validDFFrequencies(peakIndices));
                if isempty(peakDFIndices), continue; end
                [~, maxPosition] = max(peakDFValues);
                df(i) = psd(i).Frequencies(peakDFIndices(maxPosition));
                
                [sortedPeakValues, sortedPeakIndices] = sort(peakValues, 'descend'); %#ok<ASGLU>
                peakIndices = peakIndices(sortedPeakIndices);
                peakIndices = peakIndices(1:numberOfDominantPeaks);
                peakFrequencyRanges = NaN(numberOfDominantPeaks, numel(psd(i).Frequencies));
                for peakIndex = 1:numberOfDominantPeaks
                    peakFrequencyRanges(peakIndex, :) = psd(i).Frequencies >= psd(i).Frequencies(peakIndices(peakIndex)) - self.DFParameters.PeakRange &...
                        psd(i).Frequencies <= psd(i).Frequencies(peakIndices(peakIndex)) + self.DFParameters.PeakRange;
                end
                areaUnderPeaks = sum(binWidth * psd(i).Data(any(peakFrequencyRanges, 1)));
                
                totalArea = sum(binWidth * psd(i).Data);
                organizationIndex(i) = areaUnderPeaks / totalArea;
            end
        end
        
        function regularityIndex = ComputeRegularityIndex(self, psd, dominantFrequency, bandwidth)
            if nargin < 4
                bandwidth = 1;
            end
            
            regularityIndex = NaN(size(psd));
            for spectrumIndex = 1:numel(psd)
                if isnan(dominantFrequency(spectrumIndex)), continue; end
                binWidth = mean(diff(psd(spectrumIndex).Frequencies));
                frequencies = psd(spectrumIndex).Frequencies;
                validDfFrequencies = mod(frequencies + bandwidth / 2, dominantFrequency(spectrumIndex)) <= bandwidth & frequencies >= bandwidth;
                areaUnderPeaks = sum(binWidth * psd(spectrumIndex).Data(validDfFrequencies));
                totalArea = sum(binWidth * psd(spectrumIndex).Data);
                regularityIndex(spectrumIndex) = areaUnderPeaks / totalArea;
            end
        end
        
        function psdEstimates = ComputeDCMSpectrum(self, data, samplingFrequency)
            numberOfChannels = size(data, 2);
            psdEstimates(numberOfChannels) = dspdata.psd;
            nyquistFrequency = samplingFrequency / 2;
            [bHigh, aHigh] = cheby2(3, 20, [40 250] / nyquistFrequency);
            [bLow, aLow] = cheby2(3, 20, [1 20] / nyquistFrequency);
            psdEstimator = spectrum.welch('hann', self.DFParameters.NumberOfFFTPoints, self.DFParameters.FFTOverlap);
            for channelIndex = 1:numberOfChannels
                channelData = data(:, channelIndex);
                channelData = filtfilt(bHigh, aHigh, channelData);
                channelData = abs(channelData);
                channelData = filtfilt(bLow, aLow, channelData);
                if self.DFParameters.NumberOfFFTPoints > numel(channelData)
                    channelData = [channelData; zeros(self.DFParameters.NumberOfFFTPoints - numel(channelData), 1)]; %#ok<AGROW>
                end
                psdEstimate = psd(psdEstimator, channelData, 'Fs', samplingFrequency);
                psdEstimates(channelIndex) = psdEstimate;
            end
        end
        
        function channelDominantFrequencies = ComputeDCMDominantFrequency(self, ecgData, preFilter)
            if nargin < 3
                preFilter = false;
            end
            
            samplingFrequency = ecgData.SamplingFrequency; 
            nyquistFrequency = samplingFrequency / 2;
            numberOfChannels = ecgData.GetNumberOfChannels();
            
            if preFilter
                [bHigh, aHigh] = butter(2, [40, 250] / nyquistFrequency);
                [bLow, aLow] = butter(2, 20 / nyquistFrequency, 'low');
                
                filteredSignals = filtfilt(bHigh, aHigh, ecgData.Data);
                filteredSignals = abs(filteredSignals);
                filteredSignals = [zeros(100, numberOfChannels); filteredSignals; zeros(100, numberOfChannels)];
                filteredSignals = filtfilt(bLow, aLow, filteredSignals);
                filteredSignals = filteredSignals(101:(end-100), :);
            else
                filteredSignals = ecgData.Data;
            end
            
            channelDominantFrequencies = NaN(numberOfChannels, 1);
            
            minimalAFFrequency = self.DFParameters.DFRange(1);
            maximalAFFrequency = self.DFParameters.DFRange(end);
            
            nFFT = self.DFParameters.NumberOfFFTPoints;
                
            parfor channelIndex = 1:numberOfChannels
                channelData = filteredSignals(:, channelIndex);
                % compute DF
                [frequencyPower, frequencies] = pwelch(channelData, [], [], nFFT, samplingFrequency);
                [peakValues, peakIndices] = findpeaks(frequencyPower);
                
                validDFFrequencies = frequencies >= minimalAFFrequency &...
                    frequencies < maximalAFFrequency;
                peakDFValues = peakValues(validDFFrequencies(peakIndices));
                peakDFIndices = peakIndices(validDFFrequencies(peakIndices));
                [~, maxPosition] = max(peakDFValues);
                
                channelDominantFrequencies(channelIndex) = frequencies(peakDFIndices(maxPosition));
            end
        end
        
        function [psdEstimate, dominantFrequency, dominantFrequencyIndex] = ComputePWelchDCMSpectrum(self, data, samplingFrequency)
            numberOfChannels = size(data, 2);
            numberOfSamples = size(data, 1);
            psdEstimate(numberOfChannels, 1) = struct(...
                'Frequencies', [],...
                'Data', []);
            
            windowLength = self.DFParameters.WindowLength;
            windowOverlap = round(windowLength / 2);
            
            if  numberOfSamples < windowLength
                data = [data; zeros(windowLength - numberOfSamples, numberOfChannels)];
            end
            
            dominantFrequency = NaN(numberOfChannels, 1);
            dominantFrequencyIndex = NaN(numberOfChannels, 1);
            nyquistFrequency = samplingFrequency / 2;
            [bHigh, aHigh] = cheby2(3, 20, [40 250] / nyquistFrequency);
            [bLow, aLow] = cheby2(3, 20, [1 20] / nyquistFrequency);
            for channelIndex = 1:numberOfChannels
                channelData = data(:, channelIndex);
                channelData = filtfilt(bHigh, aHigh, channelData);
                channelData = abs(channelData);
                channelData = filtfilt(bLow, aLow, channelData);
                
%                 [frequencyPower, frequencies] = pwelch(channelData, [], [],...
%                     [], samplingFrequency);
                
                [frequencyPower, frequencies] = pwelch(channelData, windowLength, windowOverlap,...
                    self.DFParameters.NumberOfFFTPoints, samplingFrequency);
                
                psdEstimate(channelIndex).Frequencies = frequencies;
                psdEstimate(channelIndex).Data = frequencyPower;
                
                % dominant frequency identification
                minimalAFFrequency = self.DFParameters.DFRange(1);
                maximalAFFrequency = self.DFParameters.DFRange(2);
                validDFFrequencies = frequencies >= minimalAFFrequency &...
                    frequencies < maximalAFFrequency;
                [peakValues, peakIndices] = findpeaks(frequencyPower);
                peakDFValues = peakValues(validDFFrequencies(peakIndices));
                peakDFIndices = peakIndices(validDFFrequencies(peakIndices));
                if isempty(peakDFIndices),
                    dominantFrequency(channelIndex) = NaN;
                else
                    [~, maxPosition] = max(peakDFValues);
                    dominantFrequencyIndex(channelIndex) = peakDFIndices(maxPosition);
                    dominantFrequency(channelIndex) = frequencies(peakDFIndices(maxPosition));
                end
            end
        end
        
        function [compressedPsdEstimate, dominantFrequency] = ComputeCompressedECGSpectrum(self, data, samplingFrequency, numberOfHarmonics)
            if nargin < 4 || isempty(numberOfHarmonics)
                numberOfHarmonics = 3;
            end
            
            % spectrum computation
            [frequencyPower, frequencies] = pwelch(data, [], [], self.DFParameters.NumberOfFFTPoints, samplingFrequency);
            
            % compressed spectrum computation
            minimalAFFrequency = self.DFParameters.DFRange(1);
            maximalAFFrequency = self.DFParameters.DFRange(2);
            compressedSpectrum = zeros(size(frequencies));
            for k = 1:numel(frequencies)
                for j = 1:numberOfHarmonics
                    if j * k <= numel(frequencies)
                        if frequencies(k) >= minimalAFFrequency && frequencies(k) <= maximalAFFrequency
                            compressedSpectrum(k) = compressedSpectrum(k) + abs(frequencyPower(j * k - (j - 1))); 
                        end
                    end
                end
            end
            
            compressedPsdEstimate = struct(...
                'Frequencies', frequencies,...
                'Data', compressedSpectrum);
            
            % dominant frequency identification
            validDFFrequencies = frequencies >= minimalAFFrequency &...
                    frequencies < maximalAFFrequency;    
            [peakValues, peakIndices] = findpeaks(compressedSpectrum);    
            peakDFValues = peakValues(validDFFrequencies(peakIndices));
            peakDFIndices = peakIndices(validDFFrequencies(peakIndices));
            if isempty(peakDFIndices),
                dominantFrequency = NaN;
            else
                [~, maxPosition] = max(peakDFValues);
                dominantFrequency = frequencies(peakDFIndices(maxPosition));
            end
        end
        
        function [psdEstimate, dominantFrequency, dominantFrequencyIndex, dominantFrequencyPower] = ComputePWelchECGSpectrum(self, data, samplingFrequency)
            numberOfChannels = size(data, 2);
            numberOfSamples = size(data, 1);
            
            psdEstimate(numberOfChannels, 1) = struct(...
                    'Frequencies', [],...
                    'Data', []);
            dominantFrequency = NaN(numberOfChannels, 1);
            dominantFrequencyIndex = NaN(numberOfChannels, 1);
            dominantFrequencyPower = NaN(numberOfChannels, 1);
            windowLength = self.DFParameters.WindowLength;
            windowOverlap = round(windowLength / 2);
            numberOfFFTPoints = self.DFParameters.NumberOfFFTPoints;
            dfRange = self.DFParameters.DFRange;
            fundamentalFrequency = self.DFParameters.FundamentalFrequency;
            
            if  numberOfSamples < windowLength
                data = [data; zeros(windowLength - numberOfSamples, numberOfChannels)];
            end
            
            parfor channelIndex = 1:numberOfChannels
                channelData = data(:, channelIndex);
                % spectrum computation
                
%                 % 1. Legacy spectrum.pwelch
%                 welchEstimator = spectrum.welch('hann', self.DFParameters.NumberOfFFTPoints, 0.5);
%                 welchEstimate = psd(welchEstimator, channelData, 'Fs', samplingFrequency);
%                 frequencyPower = welchEstimate.Data;
%                 frequencies = welchEstimate.Frequencies;

%                 % 2. PWELCH with default settings (close to 8 segments, 50% overlap)
%                 [frequencyPower, frequencies] = pwelch(channelData, [], [],...
%                     [], samplingFrequency);
                
                % 3. PWELCH with specified window length and overlap
                [frequencyPower, frequencies] = pwelch(channelData, windowLength, windowOverlap,...
                    numberOfFFTPoints, samplingFrequency);
                
                psdEstimate(channelIndex).Frequencies = frequencies;
                psdEstimate(channelIndex).Data = frequencyPower;
                
                % dominant frequency identification
                minimalAFFrequency = dfRange(1);
                maximalAFFrequency = dfRange(2);
                validDFFrequencies = frequencies >= minimalAFFrequency &...
                    frequencies < maximalAFFrequency;
                [peakValues, peakIndices] = findpeaks(frequencyPower);
                peakDFValues = peakValues(validDFFrequencies(peakIndices));
                peakDFIndices = peakIndices(validDFFrequencies(peakIndices));
                if isempty(peakDFIndices)
                    dominantFrequency(channelIndex) = NaN;
                    dominantFrequencyPower(channelIndex) = NaN;
                else
                    [~, maxPosition] = max(peakDFValues);
                    
                    if fundamentalFrequency
                        % search for fundamental frequency in allowed frequencies
                        frequencyResolution = mean(diff(frequencies));
                        maximumPeakFrequency = frequencies(peakDFIndices(maxPosition));
                        peakDFFrequencies = frequencies(peakDFIndices);
                        harmonicIndex = 2;
                        newHarmonicFrequency = maximumPeakFrequency / harmonicIndex;
                        while newHarmonicFrequency >= minimalAFFrequency
                            [newPeakFrequencyDifference, newPeakFrequencyIndex] = min(abs(peakDFFrequencies - newHarmonicFrequency));
                            if newPeakFrequencyDifference < frequencyResolution
                                newPeakFrequencyPower = peakDFValues(newPeakFrequencyIndex);
                                if newPeakFrequencyPower >= frequencyPower(peakDFIndices(maxPosition)) * self.DFParameters.FundamentalFrequencyThreshold
                                    maxPosition = newPeakFrequencyIndex;
                                end
                            end
                            harmonicIndex = harmonicIndex + 1;
                            newHarmonicFrequency = maximumPeakFrequency / harmonicIndex;
                        end
                    end
                    
                    dominantFrequencyIndex(channelIndex) = peakDFIndices(maxPosition);
                    dominantFrequency(channelIndex) = frequencies(peakDFIndices(maxPosition));
                    dominantFrequencyPower(channelIndex) = frequencyPower(peakDFIndices(maxPosition));
                end
            end
        end
        
        function psdEstimates = ComputeECGSpectrum(self, data, samplingFrequency)
            numberOfChannels = size(data, 2);
            psdEstimates(numberOfChannels, 1) = dspdata.psd;
            nyquistFrequency = samplingFrequency / 2;
            [bLow, aLow] = cheby2(3, 20, [1 100] / nyquistFrequency);
            psdEstimator = spectrum.welch('hann', self.DFParameters.NumberOfFFTPoints, self.DFParameters.FFTOverlap);
            for channelIndex = 1:numberOfChannels
                channelData = data(:, channelIndex);
                channelData = filtfilt(bLow, aLow, channelData);
                if self.DFParameters.NumberOfFFTPoints > numel(channelData)
                    channelData = [channelData; zeros(self.DFParameters.NumberOfFFTPoints - numel(channelData), 1)]; %#ok<AGROW>
                end
                psdEstimate = psd(psdEstimator, channelData, 'Fs', samplingFrequency);
                psdEstimates(channelIndex) = psdEstimate;
            end
        end
        
        function spectralParameters = ComputeMultivariateSpectralParameters(self, ecgData, channelCombinations)
            if nargin < 3
                channelCombinations = 1:ecgData.GetNumberOfChannels();
            end
            
            periodogramSmoothingWindow = [1/9 2/9 3/9 2/9 1/9];
            samplingFrequency = ecgData.SamplingFrequency;
            windowLength = round(samplingFrequency * 4); % 4 second window
            windowOverlap = round(windowLength / 2);
            
            numberOfCombinations = size(channelCombinations, 1);
            spectra = cell(numberOfCombinations, 1);
            for combinationIndex = 1:numberOfCombinations
                %                 spectra{combinationIndex} = AlgorithmPkg.AFComplexityCalculator.ComputeSpectralEnvelope(ecgData.Data(:, channelCombinations(combinationIndex, :))',...
                %                     ecgData.SamplingFrequency, weights);
                
                if iscell(channelCombinations)
                    currentCombination = channelCombinations{combinationIndex};
                else
                    currentCombination = channelCombinations(combinationIndex, :);
                end
                [frequencies, envelopeValues] = AlgorithmPkg.AFComplexityCalculator.smooth_spectral_envelope(ecgData.Data(:, currentCombination),...
                    windowLength, windowOverlap, samplingFrequency, periodogramSmoothingWindow);
                spectrum.Data = envelopeValues;
                spectrum.Frequencies = frequencies;
                spectra{combinationIndex} = spectrum;
            end
            spectra = vertcat(spectra{:});
            
            [DF, OI] = self.ComputeOrganizationIndex(spectra);
            SE = self.ComputeSpectralEntropy(spectra);
            
            spectralParameters = struct(...
                'DominantFrequency', DF,...
                'OrganizationIndex', OI,...
                'SpectralEntropy', SE,...
                'psd', spectra);
        end
        
        function spectralConcentration = ComputeSignalSpectralConcentration(self, psd)
            spectralConcentration = NaN(size(psd));
            for spectrumIndex = 1:numel(psd)
                frequencies = psd(spectrumIndex).Frequencies;
                validFrequencyRange = self.DFParameters.DFRange;
                validFrequencies = frequencies>= validFrequencyRange(1) & frequencies <= validFrequencyRange(end);
                binWidth = mean(diff(frequencies));
                areaUnderRange = sum(binWidth * psd(spectrumIndex).Data(validFrequencies));
                totalArea = sum(binWidth * psd(spectrumIndex).Data);
                spectralConcentration(spectrumIndex) = areaUnderRange / totalArea;
            end
        end
    end
    
    methods (Static)
        function AFCL = ComputeAFCL(deflections)
            AFCL = cell(numel(deflections), 1);
            for electrodeIndex = 1:numel(deflections)
                intrinsicDeflections = deflections{electrodeIndex}.PrimaryDeflections;
                intervals = diff(deflections{electrodeIndex}.templatePeakIndices(intrinsicDeflections)) /...
                    deflections{electrodeIndex}.samplingFrequency;
                AFCL{electrodeIndex} = intervals(:);
            end
        end
        
        function spectrum = ComputeSpectralEnvelope(signals, samplingFrequency, weights)
            YY = fft(signals');
            frequencies = (0:length(signals) - 1) * samplingFrequency / length(signals);
            YY = [zeros((length(weights) - 1) / 2, size(signals, 1));...
                YY; zeros((length(weights) - 1) / 2, size(signals, 1))];
            Vy = signals * signals' / length(signals);
            
            for omega = (length(weights) - 1) / 2 + 1:size(YY, 1) - (length(weights) - 1) / 2
                fy = zeros(size(Vy));
                for m = 1:length(weights)
                    ftemp = YY(omega - (length(weights) - 1) / 2 + m - 1, :)' * YY(omega - (length(weights) - 1) / 2 + m - 1, :);
                    fy = fy + weights(m) * ftemp;
                end
                [V, D] = eig(real(fy), Vy);
                [~, ii] = max(max(D));
                lambda(omega) = D(ii, ii);
                beta(:, omega) = V(:, ii);
            end
            
            lambda(1:(length(weights) - 1) / 2) = [];
            beta(:, 1:(length(weights) - 1) / 2) = [];
            
            % create mock psd object
            spectrum.Data = lambda;
            spectrum.Frequencies = frequencies;
        end
        
        function [f_vec,lambda,beta] = spectral_envelope(x,h,Fe,nFFT)
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % SPECTRAL ENVELOPE
            
            % This function refers to the following article:
            
            % Stoffer et al. 2000 Stat Sci
            % Jiang et al. 2007 J. Proc. Control
            
            % This function computes the spectral envelope of a multivariate signal. It
            % applies the rigorous version of spectral envelope presented in Jiang et
            % al. 2007.
            
            % Inputs:
            % - x: NxM matrix with M channels, each channel having N samples.
            % - h: smoothing weights for periodogram smoothing. Should sum to 1.
            % - Fe: sampling rate
            % - nFFT: length of the FFT signal.
            
            % Outputs:
            % - f_vec: frequency vector of the spectral envelope
            % - lambda: spectral envelope of the signal
            % - beta: best scaling corresponding to lambda
            
            % Author: Laurent Uldry
            % 09.02.2010
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % 0. initialization
            % -----------------
            
            [N,M] = size(x);
            
            if nargin < 4,
                nFFT = N;
            end
            
            L = ceil((nFFT + 1)/2);  % Length of the main frequency vector
            h = h(:);
            h = h/sum(h);
            H = (length(h) - 1)/2;
            
            df = Fe/nFFT;
            f_vec = 0:df:Fe;
            f_vec = f_vec(1:L);
            
            
            % 1. normalization
            % ----------------
            
            R = std(x,0,1);
            x = bsxfun(@minus,x,mean(x,1));
            x = bsxfun(@rdivide,x,R);
            
            
            % 2. Covariance Matrix Vx
            % -----------------------
            
            Vx=cov(x);
            if(sum(sum(isnan(Vx)))>0)
                %     disp('No result can be returned, NaN matrix during Covariance computation')
                lambda = [];
                beta = [];
                return;
            else
                S = Vx^(-1/2);
            end
            
            % 3. Discrete Fourier transform of the multivariate set
            % -----------------------------------------------------
            
            d=fft(x,nFFT)./sqrt(nFFT);
            d = d(1:L,:);
            
            % 4. Periodogram
            % --------------
            
            P=zeros(M,M,L);
            for l=1:L
                P(:,:,l)=d(l,:)'*d(l,:);
            end
            
            % 5. Smoothing of the periodogram
            % -------------------------------
            
            for l = 1:L,
                idl = l + (-H:H);
                idl(idl<1) = abs(idl(idl<1) - 1) + 1;
                idl(idl>L) = 2*L - idl(idl>L) + mod(nFFT,2);
                Pn(:,:,l) = sum(bsxfun(@times,P(:,:,idl),permute(h,[3 2 1])),3);
            end
            P=Pn;
            clear Pn
            
            % 6. Spectral Envelope and corresponding Best Scaling
            % ---------------------------------------------------
            
            % Index 1: Complete version of spectral envelope
            % Index 2: Simplified version of spectral envelope
            
            V1 = zeros(M,M,L);
            d1 = zeros(M,L);
            l1 = zeros(L,1);
            b1 = zeros(M,L);
            
            V2 = zeros(M,M,L);
            d2 = zeros(M,L);
            l2 = zeros(L,1);
            b2 = zeros(M,L);
            for l = 1:L,
                [V1(:,:,l),D1] = eig(S*P(:,:,l)*S);
                d1(:,l) = diag(D1);
                [l1(l),idd1] = max(abs(d1(:,l)));
                b1(:,l) = V1(:,idd1,l);
                
                [V2(:,:,l),D2] = eig(P(:,:,l));
                d2(:,l) = diag(D2);
                [l2(l),idd2] = max(abs(d2(:,l)));
                b2(:,l) = V2(:,idd2,l);
            end
            lambda = l1;
            beta = b1;
            
            % 7. Statistical tests for oscillation detection
            % ----------------------------------------------
            vn = sum(h.^2);
            t1 = zeros(M,L);
            t2 = zeros(M,L);
            for l = 1:L,
                [ll1,idd1] = sort(abs(d1(:,l)),'descend');
                bb1 = V1(:,idd1,l);
                Sb1 = zeros(M,M);
                for m = 2:M,
                    Sb1 = Sb1 + ll1(m)*(ll1(1) - ll1(m))^(-2)*bb1(:,m)*bb1(:,m)';
                end
                Sb1 = vn*ll1(1)*Sb1;
                t1(:,l) = 2*abs(bb1(:,1)).^2./real(diag(Sb1));
                [ll2,idd2] = sort(abs(d2(:,l)),'descend');
                bb2 = V2(:,idd2,l);
                Sb2 = zeros(M,M);
                for m = 2:M,
                    Sb2 = Sb2 + ll2(m)*(ll2(1) - ll2(m))^(-2)*bb2(:,m)*bb2(:,m)';
                end
                Sb2 = vn*ll2(1)*Sb2;
                t2(:,l) = 2*abs(bb2(:,1)).^2./real(diag(Sb2));
            end
            
            
        end
        
        function [f_vec,lambda] = smooth_spectral_envelope(x,winlen,overlap,Fe,h)
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % SMOOTH SPECTRAL ENVELOPE
            
            % This function computes the smooth spectral envelope of a multivariate
            % signal. It applies the rigorous version of spectral envelope presented in
            % Jiang et al. 2007.
            
            % Inputs:
            % - x: NxM matrix with M channels, each channel having N samples.
            % - winlen: window length for the averaging procedure during smooth spectral
            % envelope computation
            % - overlap: overlap for the averaging procedure during smooth spectral
            % envelope computation
            % - Fe: sampling rate
            % - h: smoothing weights for periodogram smoothing. Should sum to 1.
            
            % Outputs:
            % - f_vec: frequency vector of the spectral envelope
            % - lambda: spectral envelope of the signal
            
            % Author: Laurent Uldry
            % 09.02.2010
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % 0. initialization
            % -----------------
            
            N = size(x,1);
            nb_wins = length(0:overlap:N-winlen+1);
            lambda = zeros(ceil((winlen+1)/2),nb_wins);
            if nargin < 5,
                h = [1/9 2/9 3/9 2/9 1/9];
            end
            
            % 1. spectral envelope
            % --------------------
            
            for ii=1:nb_wins
                ind = 1+overlap*(ii-1);
                [f_vec,lambda(:,ii)] = AlgorithmPkg.AFComplexityCalculator.spectral_envelope(x(ind:ind+winlen-1,:),h,Fe,winlen);
            end
            lambda = mean(lambda,2);
        end
        
        function fractionationIndices = ComputeFractionation(intrinsicDeflections)
            fractionationIndices = NaN(size(intrinsicDeflections));
            for channelIndex = 1:numel(fractionationIndices)
                numberOfIntrinsicDeflections = numel(find(intrinsicDeflections{channelIndex}.PrimaryDeflections));
                numberOfFarFieldDeflections = numel(find(~intrinsicDeflections{channelIndex}.PrimaryDeflections));
                fractionationIndices(channelIndex) = numberOfFarFieldDeflections / numberOfIntrinsicDeflections;
            end
        end
        
        function fractionationIndices = ComputeDeflectionFractionation(intrinsicDeflections)
            numberOfChannels = numel(intrinsicDeflections);
            fractionationIndices = cell(numberOfChannels, 1);
            for channelIndex = 1:numberOfChannels
                primaryDeflections = intrinsicDeflections{channelIndex}.PrimaryDeflections;
                intrinsicDeflectionTimes = intrinsicDeflections{channelIndex}.templatePeakIndices(primaryDeflections);
                farFieldDeflectionTimes = intrinsicDeflections{channelIndex}.templatePeakIndices(~primaryDeflections);
                deflectionFractionationIndices = NaN(size(intrinsicDeflectionTimes));
                for deflectionIndex = 2:(numel(intrinsicDeflectionTimes) - 1)
                    complexStart = intrinsicDeflectionTimes(deflectionIndex - 1) +...
                        (intrinsicDeflectionTimes(deflectionIndex) - intrinsicDeflectionTimes(deflectionIndex - 1)) / 2;
                    complexEnd = intrinsicDeflectionTimes(deflectionIndex) +...
                        (intrinsicDeflectionTimes(deflectionIndex + 1) - intrinsicDeflectionTimes(deflectionIndex)) / 2;
                    farFieldDeflectionsInComplex = farFieldDeflectionTimes >= complexStart &...
                        farFieldDeflectionTimes < complexEnd;
                    deflectionFractionationIndices(deflectionIndex) = numel(find(farFieldDeflectionsInComplex));
                end
                fractionationIndices{channelIndex} = deflectionFractionationIndices;
            end
        end
        
        function [allDifferences, parameterEstimates, differencesFit] = ComputeMicroAndMacroFractionation(analysisResult, time, fitReplicates)
            deflectionTimeDifferences = cell(size(analysisResult));
            for channelIndex = 1:numel(analysisResult)
                intrinsicDeflections = analysisResult{channelIndex}.PrimaryDeflections;
                intrinsicDeflectionIndices = analysisResult{channelIndex}.templatePeakIndices(intrinsicDeflections);
                farFieldDeflectionIndices = analysisResult{channelIndex}.templatePeakIndices(~intrinsicDeflections);
                
                intrinsicDeflectionRangeIndices = analysisResult{channelIndex}.templatePeakRanges(intrinsicDeflections, :);
                farFieldDeflectionRangeIndices = analysisResult{channelIndex}.templatePeakRanges(~intrinsicDeflections, :);
                
                intrinsicDeflectionTimes = time(intrinsicDeflectionIndices);
                farFieldDeflectionTimes = time(farFieldDeflectionIndices);
                intrinsicDeflectionRanges = [time(intrinsicDeflectionRangeIndices(:, 1))', time(intrinsicDeflectionRangeIndices(:, 2))'];
                farFieldDeflectionRanges = [time(farFieldDeflectionRangeIndices(:, 1))', time(farFieldDeflectionRangeIndices(:, 2))'];
                
                deflectionTimeDifferences{channelIndex} = [];
                cycleLength = mean(diff(intrinsicDeflectionTimes));
                complexWindow = cycleLength / 2;
                for deflectionIndex = 1:numel(intrinsicDeflectionTimes)
                    validCandidateDeflections =...
                        farFieldDeflectionTimes > (intrinsicDeflectionTimes(deflectionIndex) - complexWindow) &...
                        farFieldDeflectionTimes < intrinsicDeflectionTimes(deflectionIndex);
                    differencesBefore = farFieldDeflectionRanges(validCandidateDeflections, 2) - intrinsicDeflectionRanges(deflectionIndex, 1);
                    
                    validCandidateDeflections =...
                        farFieldDeflectionTimes > intrinsicDeflectionTimes(deflectionIndex) &...
                        farFieldDeflectionTimes < (intrinsicDeflectionTimes(deflectionIndex) + complexWindow);
                    differencesAfter = farFieldDeflectionRanges(validCandidateDeflections, 1) - intrinsicDeflectionRanges(deflectionIndex, 2);
                    deflectionTimeDifferences{channelIndex} = [deflectionTimeDifferences{channelIndex};...
                        differencesBefore(:); differencesAfter(:)];
                end
            end
            allDifferences = 1e3 * vertcat(deflectionTimeDifferences{:});
            
            [differencesFit, parameterEstimates] = AlgorithmPkg.AFComplexityCalculator.ComputeUniformGaussianMixtureFit(...
                allDifferences, fitReplicates);
        end
        
        function [directionFit, parameterEstimate] = ComputeUniformGaussianMixtureFit(data, replicates)
            data = data(~isnan(data));
            
            % ignore edges
            edges = quantile(data, [.01, .99]);
            data = data(data >= edges(1) & data <= edges(2));
            
            uniformBounds = [min(data) max(data)];
            pdfNormalUniformMixture = @(x, p, sigma )...
                p * normpdf(x, 0, sigma) + (1 - p) * unifpdf(x, uniformBounds(1),...
                uniformBounds(2));
            pStart = .5;
            sigmaStart = sqrt(var(data));
            start = [pStart sigmaStart];
            
            lb = [0 -Inf];
            ub = [1 Inf];
            options = statset('maxIter', 1e5, 'maxFunEvals', 1e5);
            parameterEstimate = start;
            currentConfidence = Inf;
            for i = 1:replicates
                pStart = rand;
                sigmaStart = rand * sqrt(var(data)) / 2;
                start = [pStart sigmaStart];
                try
                    currentEstimate = mle(data, 'pdf', pdfNormalUniformMixture,...
                        'start', start, 'lower', lb, 'upper', ub,...
                        'options', options, 'optimfun', 'fminsearch');
                    if currentEstimate(2) < currentConfidence
                        currentConfidence = currentEstimate(2);
                        parameterEstimate = currentEstimate;
                    end
                catch  %#ok<CTCH>
                end
            end
            directionFit = @(x)...
                parameterEstimate(1) * normpdf(x, 0, parameterEstimate(2)) +...
                (1 - parameterEstimate(1)) * unifpdf(x, uniformBounds(1), uniformBounds(2));
        end
        
        function [dissociation, differences, maxDissociation, electrodeDissociation, maxElectrodeDissociation] = ComputeDissociationData(ecgData, intrinsicDeflections, radius)
            numberOfChannels = ecgData.GetNumberOfChannels();
            
            % convert deflection indices to milliseconds
            activationTimes = cell(numberOfChannels, 1);
            for channelIndex = 1:numberOfChannels
                activationTimes{channelIndex} =...
                    intrinsicDeflections{channelIndex}.templatePeakIndices(intrinsicDeflections{channelIndex}.PrimaryDeflections) *...
                    (1e3 / ecgData.SamplingFrequency);
            end
            
            electrodePositions = ecgData.ElectrodePositions;
            dissociation = NaN(numberOfChannels, 1);
            maxDissociation = NaN(numberOfChannels, 1);
            differences = cell(numberOfChannels, 1);
            electrodeDissociation = cell(numberOfChannels, numberOfChannels);
            maxElectrodeDissociation = cell(numberOfChannels, 1);
            for channelIndex = 1:numberOfChannels
                neighbors = AlgorithmPkg.AFComplexityCalculator.GetElectrodeNeighbors(channelIndex, electrodePositions, radius);
                selfPosition = neighbors.positions == channelIndex;
                neighbors.positions(selfPosition) = [];
                neighbors.distances(selfPosition) = [];
                neighborActivations = activationTimes(neighbors.positions);
                numberOfNeighbors = numel(neighbors.positions);
                if numberOfNeighbors == 0, continue; end
                
                activations = activationTimes{channelIndex};
                afcl = median(diff(activations));
                numberOfActivations = numel(activations);
                minimalDifferences = NaN(numberOfActivations, numberOfNeighbors);
                minimalPositions = NaN(numberOfActivations, numberOfNeighbors);
                for activationIndex = 1:numberOfActivations
                    [minimalDifferences(activationIndex, :),  minimalPositions(activationIndex, :)] =...
                        cellfun(@(x) AlgorithmPkg.AFComplexityCalculator.GetMinimalAbsoluteDifference(x, activations(activationIndex)), neighborActivations);
                end
                minimalDifferences(abs(minimalDifferences) > (afcl / 2)) = NaN;
                
                differences{channelIndex} = [min(minimalDifferences, [], 2),...
                    max(minimalDifferences, [], 2)];
                
                for neighborIndex = 1:numberOfNeighbors
                    electrodeDissociation{channelIndex, neighbors.positions(neighborIndex)} =...
                        [minimalDifferences(:, neighborIndex), minimalPositions(:, neighborIndex)];
                end
                
                %                 % max absolute difference
                %                 minimalDifferences(minimalDifferences > (afcl / 2)) = NaN;
                %                 dissociation(channelIndex) = mean(max(abs(minimalDifferences), [], 2));
                
                
                %                 % activation interval length
                %                 minimalDifferences = max(minimalDifferences, [], 2) - min(minimalDifferences, [], 2);
                %                 minimalDifferences = minimalDifferences(minimalDifferences <= afcl);
                
                % mean absolute difference
                meanActivationDifferences = NaN(numberOfActivations, 1);
                for activationIndex = 1:numberOfActivations
                    activationDifferences = minimalDifferences(activationIndex, :);
                    meanActivationDifferences(activationIndex) = mean(abs(activationDifferences(~isnan(activationDifferences))));
                end
                differences{channelIndex} = [zeros(numberOfActivations, 1), meanActivationDifferences];
                
                dissociation(channelIndex) = mean(meanActivationDifferences(~isnan(meanActivationDifferences)));
                maxDifferences = max(abs(minimalDifferences), [], 2);
                maxDissociation(channelIndex) = mean(maxDifferences(~isnan(maxDifferences)));
                maxElectrodeDissociation{channelIndex} = maxDifferences;
            end
        end
        
        function windowedAFCL = ComputeWindowedAFCL(ecgData, intrinsicDeflections, windowLength, windowShift)
            windowSampleLength = round(windowLength * ecgData.SamplingFrequency);
            windowSampleShift = round(windowShift * ecgData.SamplingFrequency);
            
            windowStart = 1:windowSampleShift:ecgData.GetNumberOfSamples;
            windowEnd = (windowSampleLength):windowSampleShift:ecgData.GetNumberOfSamples;
            numberOfWindows = numel(windowEnd);
            windowStart = windowStart(1:numberOfWindows);
            
            numberOfChannels = ecgData.GetNumberOfChannels();
            windowedAFCL = NaN(numberOfChannels, numberOfWindows);
            
            for channelIndex = 1:numberOfChannels
                deflectionSampleIndices = intrinsicDeflections{channelIndex}.templatePeakIndices(intrinsicDeflections{channelIndex}.PrimaryDeflections);
                for windowIndex = 1:numberOfWindows
                    validDeflections = deflectionSampleIndices >= windowStart(windowIndex) &...
                        deflectionSampleIndices <= windowEnd(windowIndex);
                    if any(validDeflections)
                        windowedAFCL(channelIndex, windowIndex) = median(deflectionSampleIndices(validDeflections));
                    end
                end
            end
            
            windowedAFCL = 1000 * windowedAFCL ./ ecgData.SamplingFrequency;
        end
        
        function stability = ComputePropertyStability(propertyData)
            if ~iscell(propertyData)
                propertyData = {propertyData};
            end
            
            numberOfInstances = numel(propertyData);
            stability = NaN(numberOfInstances, 1);
            for instanceIndex = 1:numberOfInstances
                correlationMatrix = corrcoef(propertyData{instanceIndex});
                values = triu(correlationMatrix, 1);
                values = values(values ~= 0);
                stability(instanceIndex) = max(abs(values));
            end
        end
        
        function sampleEntropy = SampleEntropyResampled(data, samples, tolerance, samplingFrequency, resamplingFrequency)
            
            [p, q] = rat(resamplingFrequency / samplingFrequency, 0.0001);
            if p == 1 && q == 1
                resampleData = false;
            else
                resampleData = true;
            end
                        
            
            if resampleData
                if p == 1
                    data = decimate(data, q);
                elseif q == 1
                    data = interp(data, p);
                else
                    data = resample(data, p, q);
                end
            end
            
            sampleEntropy = AlgorithmPkg.AFComplexityCalculator.SampleEntropy(...
                data, samples, tolerance);
        end
        
        function [e, C, M, R] = SampleEntropy(y, m, r)
            %e=sampen(y,m,r)
            %
            %Input Parameters
            %
            %y  input signal vector
            %m  maximum number of matches (default m=5)
            %r  matching threshold (default r=.2)
            %
            %Output Parameters
            %
            %e  sample entropy calculations (m-1 values)
            %
            %Full usage:
            %
            %[e,C,M,R]=sampen(y,m,r,sflag,cflag)
            %
            %
            %Output Parameters
            %
            %C        average number of m-matches per sample (m values)
            %M        number of matches
            %R        number of runs
            y = y(:);
            n = length(y);
            
            % normalization
            y = y - mean(y);
            s = sqrt(mean(y.^2));
            y = y / s;
            
            [M, R] = AlgorithmPkg.AFComplexityCalculator.Matches(y, r);
            
            k = length(M);
            if k < m
                M((k + 1):m) = 0;
            end
            
            C = M(1:m) ./ (n - (1:m)');
            e = zeros(m - 1,1);
            for i= 1:(m - 1)
                if C(i + 1) > 0
                    e(i)= -log(C(i + 1) / C(i));
                else
                    e(i)=Inf;
                end
            end
        end
        
        function [M, R] = Matches(y, r)
            %[M,R]=matches(y,r)
            %
            %Input Parameters
            %
            %y  input signal vector
            %r  matching threshold (default r=.2)
            %
            %Output Parameters
            %
            %M  number of matches
            %R  number of runs
            
            n = length(y);
            R = zeros(n - 1, 1);
            for j = 1:(n - 1)
                i = 1:(n - j);
                d = abs(y(i + j) - y(i));
                a = d < r;
                a1 = [0;a];
                a2 = [a;0];
                aa = find(a1 ~= a2);
                kk = length(aa) / 2;
                rr = diff(reshape(aa, 2, kk));
                for i = 1:kk
                    k = rr(i);
                    R(k)= R(k) + 1;
                end
            end
            k = find(R > 0, 1, 'last' );
            R = R(1:k);
            M = zeros(k, 1);
            for i = 1:k
                M(i) = R(i);
                for j = (i + 1):k
                    M(i) = M(i) + (j + 1 - i) * R(j);
                end
            end
        end
        
        function neighbors = GetElectrodeNeighbors(electrodeIndex, electrodePositions, radius)
            electrodePosition = electrodePositions(electrodeIndex, : );
            positionDifferences = bsxfun(@minus, electrodePositions, electrodePosition);
            electrodeDistances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
            validNeigbors = electrodeDistances <= radius;
            
            neighbors = struct(...
                'positions', find(validNeigbors),...
                'distances', electrodeDistances(validNeigbors));
        end
        
        function [difference, minPosition] = GetMinimalAbsoluteDifference(values, reference)
            differences = (values - reference);
            [minVal, minPosition] = min(abs(differences));
            difference = differences(minPosition);
        end
    end
end