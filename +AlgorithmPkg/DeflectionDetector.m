classdef DeflectionDetector <  UserInterfacePkg.IStatusChange
    properties (Access = public)
        AnalysisResults
        TemplateDurations
        FastModeEnabled
        SlopeCalculationStepsize
        
        ApplyBandpassFilter = false
        ApplyRectifiedFilter = false
        BandpassFrequencies = [1, 500]
        BandpassOrder = 3
    end
    
    properties (Access = private)
        SamplingFrequency
        ResultAxes
        Templates
    end
    
    methods
        function self = DeflectionDetector()
            self.Templates = [];
            self.TemplateDurations = 5:1:50;
            self.FastModeEnabled = true;
            self.SlopeCalculationStepsize = 1;
            
            self.ApplyBandpassFilter = false;
            self.BandpassFrequencies = [1, 500];
            self.BandpassOrder = 3;
            self.ApplyRectifiedFilter = false;
            
            self.AbortNow = 0;
        end
        
        function [analysisResult, data] = Apply(self, ecgData, templates)
            self.Templates = templates;
            self.SamplingFrequency = ecgData.SamplingFrequency;
            
            numberOfChannels = ecgData.GetNumberOfChannels();
            
            analysisResults = cell(1, numberOfChannels);
            
            data = self.FilterSignals(ecgData);
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Starting deflection detection...', 0, self));
            
            parfor channelIndex = 1:numberOfChannels
                analysisResults{channelIndex} = self.ComputeTemplateCorrelogram(...
                    data(:, channelIndex), self.Templates);
                
                if mod(channelIndex, 10) == 0
                    UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                        UserInterfacePkg.StatusChangeEventData(['Detecting deflections (' num2str(channelIndex)...
                        ' of ' num2str(numberOfChannels) ' ECG signals completed)'], channelIndex / numberOfChannels, self));
                end
            end
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Done', 1, self));
            
            analysisResult = DeflectionDetectionResults(analysisResults);
        end
    end
    
    methods ( Access = private )
        function analysisResult = ComputeTemplateCorrelogram(self, signal, templates)
            durations = self.TemplateDurations;
            signalSampleRate = self.SamplingFrequency;
            
            analysisResult = struct(...
                'samplingFrequency', signalSampleRate,...
                'durations', durations,...
                'templates', {cell(numel(durations), 1)},...
                'templatePrototypes', {templates},...
                'templateZeroDeviations', [],...
                'templatePeakIndices', [],...
                'templatePeakCorrelations', [],...
                'templatePeakCoefficients', [],...
                'templatePeakTemplates', [],...
                'templatePeakRanges', [],...
                'templateCorrelations', []);
            
            % compute templates
            templateZeroDeviation = zeros(numel(durations), numel(templates), 2);
            templateDescendentPart = zeros(numel(durations), numel(templates), 2);
            for durationIndex = 1:numel(durations)
                numberOfSamples = round(signalSampleRate * durations(durationIndex) / 1000);
                for shapeIndex = 1:numel(templates)
                    templateDuration = templates(shapeIndex).x(end) - templates(shapeIndex).x(1);
                    newX = templates(shapeIndex).x(1):(templateDuration / numberOfSamples):...
                        templates(shapeIndex).x(end);
                    interpolatedTemplate = interp1(templates(shapeIndex).x, templates(shapeIndex).y, newX);
                    analysisResult.templates{durationIndex, shapeIndex} = interpolatedTemplate;
                    
                    %                     templateDescendentPart(durationIndex, shapeIndex, 1) = ceil(1 / (templates(shapeIndex).x(end)) * (numel(newX) - 1) / 2);
                    descendentY = diff(interpolatedTemplate) < 0;
                    templateDescendentPart(durationIndex, shapeIndex, 1) = numel(find(newX(descendentY) < 0));
                    templateDescendentPart(durationIndex, shapeIndex, 2) = numel(find(newX(descendentY) >= 0));
                    
                    templateZeroDeviation(durationIndex, shapeIndex, 1) = numel(find(newX < 0));
                    templateZeroDeviation(durationIndex, shapeIndex, 2) = numel(find(newX >= 0));
                end
            end
            analysisResult.templateZeroDeviations = templateZeroDeviation;
            
            % compute maximum correlation correlogram
            %             [correlationCoefficients2 regressionCoefficients2 templateIndices2 templateRanges2 templateDescendentPartRanges2] = ...
            %                 self.MatchDeflectionTemplates(analysisResult.templates, templateZeroDeviation, templateDescendentPart, signal);
            
            [correlationCoefficients, regressionCoefficients, templateIndices, templateRanges, templateDescendentPartRanges] = ...
                self.MatchDeflectionTemplatesRefactored(analysisResult.templates, templateZeroDeviation, templateDescendentPart, signal);
            
            [peakCorrelations, peakIndices] = findpeaks(correlationCoefficients);
            peakIndices = peakIndices(peakCorrelations >= 0);
            peakCorrelations = peakCorrelations(peakCorrelations >= 0);
            
            analysisResult.templatePeakIndices = peakIndices;
            analysisResult.templatePeakCorrelations = peakCorrelations;
            analysisResult.templatePeakCoefficients = regressionCoefficients(peakIndices, :);
            analysisResult.templatePeakAmplitudes = 2 * regressionCoefficients(peakIndices, 1);
            analysisResult.templatePeakTemplates = templateIndices(peakIndices, :);
            
            validTemplatePeakTemplates = analysisResult.templatePeakTemplates(:,1) > 0;
            analysisResult.templatePeakDurations = zeros(1,length(analysisResult.templatePeakTemplates(:,1)));
            analysisResult.templatePeakDurations(validTemplatePeakTemplates) = ...
                uint32(analysisResult.durations(analysisResult.templatePeakTemplates(validTemplatePeakTemplates,1)));
            
            analysisResult.templateDescendentPartPeakRanges = templateDescendentPartRanges(peakIndices, :);
            analysisResult.templatePeakRanges = templateRanges(peakIndices, :);
            
            %             analysisResult.templateCorrelations = correlationCoefficients;
            %             analysisResult.templateCoefficients = regressionCoefficients;
            %             analysisResult.templateAmplitudes = 2 *  regressionCoefficients(:,1);
            %             analysisResult.correlationTemplates = uint8(templateIndices);
            
            %             analysisResult.templateDurations = NaN(1, length(templateIndices));
            %             validIndices = templateIndices(:,1) > 0;
            %             analysisResult.templateDurations(validIndices) = analysisResult.durations(analysisResult.correlationTemplates(validIndices, 1));
            
            %             analysisResult.amplitudeDividedByDuration = analysisResult.templateAmplitudes' ./ analysisResult.templateDurations;
            analysisResult.peakAmplitudeDividedByDuration = analysisResult.templatePeakAmplitudes' ./ double(analysisResult.templatePeakDurations);
            
            %             analysisResult.deflectionSlopes = self.Diff(signal, self.SamplingFrequency);
            deflectionSlopes = self.Diff(signal, self.SamplingFrequency);
            analysisResult.deflectionPeakSlopes = deflectionSlopes(peakIndices);
        end
        
        function [correlationCoefficients, regressionCoefficients, templateIndices, templateRanges, templateDescendentPartRanges] = ...
                MatchDeflectionTemplates(self, templates, deviations, descendentPart, signal)
            
            % Find area's with negative slope
            [b, a] = butter(4, 250 / self.SamplingFrequency, 'low');
            fsignal = filtfilt(b, a, signal);
            negativeSlope = zeros(size(diff(fsignal)));
            negativeSlope(diff(fsignal) < 0) = 1;
            
            startDeviations = max(deviations(:, :, 1));
            maxStartDeviation = max(startDeviations(:));
            endDeviations = max(deviations(:, :, 2));
            numberOfTemplates = numel(templates(:, 1));
            maxEndDeviation = max(endDeviations(:));
            numberOfShapes = numel(templates(1, :));
            templateLengths = cellfun(@numel, templates);
            signalLength = numel(signal);
            correlationCoefficients = NaN(signalLength, 1);
            regressionCoefficients = NaN(signalLength, 2);
            templateIndices = NaN(signalLength, 2);
            templateRanges = NaN(signalLength, 2);
            templateDescendentPartRanges = NaN(signalLength, 2);
            
            currentCorrelations = NaN(numberOfShapes, numberOfTemplates, 1);
            currentCoefficients = NaN(numberOfShapes, numberOfTemplates, 2);
            for templateShift = maxStartDeviation:1:(signalLength - maxEndDeviation)
                if (negativeSlope(templateShift) || ~self.FastModeEnabled)
                    for shapeIndex = 1:numberOfShapes
                        startDeviations = deviations(:, shapeIndex, 1);
                        endDeviations = deviations(:, shapeIndex, 2);
                        
                        templateCorrelations = NaN(1, numberOfTemplates);
                        templateCoefficients = NaN(1, numberOfTemplates);
                        
                        for templateIndex = 1:numberOfTemplates
                            currentSignal = signal((templateShift - startDeviations(templateIndex) + 1):...
                                (templateShift + endDeviations(templateIndex)));
                            
                            currentCovariance = HelperFunctions.fastcov(transpose(templates{templateIndex, shapeIndex}),currentSignal(1:templateLengths(templateIndex)));
                            
                            templateCorrelations(templateIndex) = currentCovariance(2) / sqrt(currentCovariance(1) * currentCovariance(4));
                            templateCoefficients(templateIndex) = currentCovariance(2) / currentCovariance(1);
                        end
                        
                        currentCorrelations(shapeIndex, :) = templateCorrelations;
                        currentCoefficients(shapeIndex, :, 1) = templateCoefficients;
                    end
                    
                    [maxTemplateCorrelations, maxTemplateRowIndex] = max(currentCorrelations, [], 1);
                    [maxCorrelation, maxTemplateColumnIndex] = max(maxTemplateCorrelations);
                    bestShapeIndex = maxTemplateRowIndex(maxTemplateColumnIndex);
                    bestTemplateTemplateIndex = maxTemplateColumnIndex;
                    
                    correlationCoefficients(templateShift) = maxCorrelation;
                    regressionCoefficients(templateShift, :) = currentCoefficients(bestShapeIndex, bestTemplateTemplateIndex, :);
                    
                    templateRanges(templateShift, 1) = templateShift - deviations(bestTemplateTemplateIndex, bestShapeIndex, 1) + 1;
                    templateRanges(templateShift, 2) = templateShift + deviations(bestTemplateTemplateIndex, bestShapeIndex, 2);
                    
                    templateDescendentPartRanges(templateShift, 1) = templateShift - descendentPart(bestTemplateTemplateIndex, bestShapeIndex, 1);
                    templateDescendentPartRanges(templateShift, 2) = templateShift + descendentPart(bestTemplateTemplateIndex, bestShapeIndex, 2);
                    
                    templateIndices(templateShift, 1) = bestTemplateTemplateIndex;
                    templateIndices(templateShift, 2) = bestShapeIndex;
                end
            end
        end
        
        function [correlationCoefficients, regressionCoefficients, templateIndices, templateRanges, templateDescendentPartRanges] = ...
                MatchDeflectionTemplatesRefactored(self, templates, deviations, descendentPart, signal)
            
            startDeviations = max(deviations(:, :, 1));
            maxStartDeviation = max(startDeviations(:));
            endDeviations = max(deviations(:, :, 2));
            numberOfTemplates = numel(templates(:, 1));
            maxEndDeviation = max(endDeviations(:));
            numberOfShapes = numel(templates(1, :));
            signalLength = numel(signal);
            
            templateRanges = NaN(signalLength, 2);
            templateDescendentPartRanges = NaN(signalLength, 2);
            
            maxTemplateCorrelations = NaN(signalLength, numberOfShapes);
            maxTemplateCoefficients = NaN(signalLength, numberOfShapes);
            maxTemplateShapeTemplateIndex = NaN(signalLength, numberOfShapes);
            signalIndices = (1:signalLength)';
            
            signalSum = cumsum(signal);
            signalSquaredSum = cumsum(signal.^2);
            
            for shapeIndex = 1:numberOfShapes
                startDeviations = deviations(:, shapeIndex, 1);
                endDeviations = deviations(:, shapeIndex, 2);
                
                templateCorrelations = NaN(signalLength, numberOfTemplates);
                templateCoefficients = NaN(signalLength, numberOfTemplates);
                
                for templateIndex = 1:numberOfTemplates
                    templateStartDeviation = startDeviations(templateIndex);
                    templateEndDeviation = endDeviations(templateIndex);
                    
                    [templateCorrelations(:, templateIndex), templateCoefficients(:, templateIndex)] =...
                        AlgorithmPkg.templateCorrelation(signal, transpose(templates{templateIndex, shapeIndex}),...
                        signalSum, signalSquaredSum, int32(templateStartDeviation), int32(templateEndDeviation));
                end
                [maxTemplateCorrelations(:, shapeIndex), maxTemplateShapeTemplateIndex(:, shapeIndex)] = max(templateCorrelations, [], 2);
                coefficientIndices = sub2ind([signalLength, numberOfTemplates], signalIndices, maxTemplateShapeTemplateIndex(:, shapeIndex));
                maxTemplateCoefficients(:, shapeIndex) = templateCoefficients(coefficientIndices);
            end
            [correlationCoefficients, maxShapeIndices] = max(maxTemplateCorrelations, [], 2);
            coefficientIndices = sub2ind([signalLength, numberOfShapes], signalIndices, maxShapeIndices);
            regressionCoefficients = maxTemplateCoefficients(coefficientIndices);
            templateIndices = [maxTemplateShapeTemplateIndex(coefficientIndices), maxShapeIndices];
            
            for signalIndex = 1:signalLength
                if isnan(templateIndices(signalIndex, 1)), continue; end
                templateRanges(signalIndex, 1) = signalIndex - deviations(templateIndices(signalIndex, 1), templateIndices(signalIndex, 2), 1) + 1;
                templateRanges(signalIndex, 2) = signalIndex + deviations(templateIndices(signalIndex, 1), templateIndices(signalIndex, 2), 2);
                
                templateDescendentPartRanges(signalIndex, 1) = signalIndex - descendentPart(templateIndices(signalIndex, 1), templateIndices(signalIndex, 2), 1);
                templateDescendentPartRanges(signalIndex, 2) = signalIndex + descendentPart(templateIndices(signalIndex, 1), templateIndices(signalIndex, 2), 2);
            end
        end
        
        function result = Diff(self, vector, sampleRate)
            dsamples = self.SlopeCalculationStepsize;
            
            xVector = 1:1:numel(vector);
            downsampledVector = downsample(vector, dsamples);
            downsampledxVector = downsample(xVector, dsamples);
            diffDownsampledVector = diff(downsampledVector) * dsamples * sampleRate;
            xDiffDownsampledVector = downsampledxVector(1:end-1) + dsamples/2;
            result = interp1(xDiffDownsampledVector, diffDownsampledVector, xVector, 'pchip');
        end
        
        function filteredSignals = FilterSignals(self, ecgData)
            samplingFrequency = ecgData.SamplingFrequency; 
            nyquistFrequency = samplingFrequency / 2;
            numberOfChannels = ecgData.GetNumberOfChannels();
            if self.ApplyBandpassFilter
                if self.BandpassFrequencies(1) > 0 && self.BandpassFrequencies(2) < nyquistFrequency
                    [b, a] = cheby2(self.BandpassOrder, 20, self.BandpassFrequencies / nyquistFrequency);
                else
                    if self.BandpassFrequencies(1) == 0 && self.BandpassFrequencies(2) < nyquistFrequency
                        [b, a] = cheby2(self.BandpassOrder, 20, self.BandpassFrequencies(2) / nyquistFrequency, 'low');
                    elseif self.BandpassFrequencies(1) > 0 && self.BandpassFrequencies(2) >= nyquistFrequency
                        [b, a] = cheby2(self.BandpassOrder, 20, self.BandpassFrequencies(1) / nyquistFrequency, 'high');
                    end
                end
                
                filteredSignals = ecgData.Data;
                parfor channelIndex = 1:numberOfChannels
                    filteredSignals(:, channelIndex) = filtfilt(b, a, filteredSignals(:, channelIndex));
                end
                
            elseif self.ApplyRectifiedFilter
                orginalDataRange = [min(ecgData.Data); max(ecgData.Data)];
                [bHigh, aHigh] = butter(2, [40 250] / nyquistFrequency);
                [bLow, aLow] = butter(2, 20 / nyquistFrequency, 'low');
                
                filteredSignals = filtfilt(bHigh, aHigh, ecgData.Data);
                filteredSignals = abs(filteredSignals);
                filteredSignals = [zeros(100, numberOfChannels); filteredSignals; zeros(100, numberOfChannels)];
                filteredSignals = filtfilt(bLow, aLow, filteredSignals);
                filteredSignals = filteredSignals(101:(end-100), :);
                
                channelDF = NaN(numberOfChannels, 1);
                parfor channelIndex = 1:numberOfChannels
                    channelData = filteredSignals(:, channelIndex);
                    % compute DF
                    [frequencyPower, frequencies] = pwelch(channelData, [], [],...
                        [], samplingFrequency);
                    minimalAFFrequency = 3;
                    maximalAFFrequency = 12;
                    validDFFrequencies = frequencies >= minimalAFFrequency &...
                        frequencies < maximalAFFrequency;
                    [peakValues, peakIndices] = findpeaks(frequencyPower);
                    peakDFValues = peakValues(validDFFrequencies(peakIndices));
                    peakDFIndices = peakIndices(validDFFrequencies(peakIndices));
                    [~, maxPosition] = max(peakDFValues);
                    channelDF(channelIndex) = frequencies(peakDFIndices(maxPosition));
                end
                
                windowLength = round((1 / mean(channelDF)) * nyquistFrequency);
                if mod(windowLength, 2) == 0
                    windowLength = windowLength + 1;
                end
                filteredSignals = filtfilt(ones(1, windowLength) / windowLength, 1, filteredSignals);
                
                parfor channelIndex = 1:numberOfChannels
                    channelData = gradient(filteredSignals(:, channelIndex));
                    channelData = (channelData - min(channelData)) / (max(channelData) - min(channelData));
                    channelData = channelData * (orginalDataRange(2, channelIndex) - orginalDataRange(1, channelIndex)) + orginalDataRange(1, channelIndex);
                    filteredSignals(:, channelIndex) = channelData;
                end
            else
                filteredSignals = ecgData.Data;
            end
        end
    end
end