classdef Wave < handle
    properties
        ID
        Members
        Peripheral
        ConnectivityMatrix
        StartingPoints
        StartingPointIndices
    end
    
    properties (SetAccess = protected)
        ElectrodePositions
    end
    
    properties (Dependent = true, SetAccess = private)
        Size
    end
    
    methods
        function self = Wave(id, electrodePositions)
          self.ID = id;
          self.ElectrodePositions = electrodePositions;
          self.Peripheral = true;
        end
        
        function Add(self, electrodeIndex, deflectionIndex, activationTime)
            self.Members = [self.Members;...
                [electrodeIndex, deflectionIndex, activationTime]];
        end
        
        function value = get.Size(self)
            value = size(self.Members, 1);
        end
        
        function [startingPoints, pointIndices] = GetAllStartingPoints(self)
            if isempty(self.StartingPoints)
                [startingPoints, pointIndices] = self.ComputeAllStartingPoints();
            else
                startingPoints = self.StartingPoints;
                pointIndices = self.StartingPointIndices;
            end
        end
        
        function [startingPoints, pointIndices] = ComputeAllStartingPoints(self)
            validStartingPoints = false(self.Size, 1);
            if ~isempty(self.ConnectivityMatrix)
                connectedMembers = any(self.ConnectivityMatrix);
                validStartingPoints(~connectedMembers) = true;
                
                % connected components
%                 [numberOfComponents, componentIndices] = graphconncomp(self.ConnectivityMatrix);
                % Change for > R2022a
                [componentIndices, componentSize] = conncomp(digraph(self.ConnectivityMatrix));
                numberOfComponents = numel(componentSize);
                %
                for componentIndex = 1:numberOfComponents
                    componentMembers = componentIndices == componentIndex;
                    if numel(find(componentMembers)) < 2, continue; end
                    
                    componentConnectivity = self.ConnectivityMatrix(:, componentMembers);
                    componentConnectivity(componentMembers, :) = false;
                    if ~any(componentConnectivity)
                        validStartingPoints(componentMembers) = true;
                    end
                end
                
%                 % recursive search
%                 connectedMembersIndices = find(connectedMembers & ~validStartingPoints');
%                 for memberIndex = connectedMembersIndices
%                     validStartingPoints(memberIndex) = self.IsStartingPoint(memberIndex, self.ConnectivityMatrix);
%                 end
            else
                startingPointTime = min(self.Members(:, 3));
                validStartingPoints = self.Members(:, 3) == startingPointTime;
            end
            startingPoints = self.Members(validStartingPoints, :);
            pointIndices = find(validStartingPoints);
            self.StartingPoints = startingPoints;
            self.StartingPointIndices = pointIndices;
        end
        
        function peripheral = IsPeripheral(self, edgeElectrodes)
            startingPoints = self.GetAllStartingPoints();
            if any(edgeElectrodes(startingPoints(:, 1)))
                peripheral = true;
            else
                peripheral = false;
            end
        end
        
        function overlap = Overlap(self, wave, activationThreshold)
            selfPositions = self.ElectrodePositions(self.Members(:, 1), :);
            wavePositions = wave.ElectrodePositions(wave.Members(:, 1), :);
            overlap = 0;
            for memberIndex = 1:self.Size()
                overlappingPosition = selfPositions(memberIndex, 1) == wavePositions(:, 1) &...
                    selfPositions(memberIndex, 2) == wavePositions(:, 2) &...
                    selfPositions(memberIndex, 3) == wavePositions(:, 3);
                if any(overlappingPosition)
                    if any(abs(wave.Members(overlappingPosition, 3) - self.Members(memberIndex, 3))<= activationThreshold)
                        overlap = overlap + 1;
                    end
                end
            end
        end
        
        function [conductionMatrix, distanceMatrix] = LocalConduction(self, useConnectivityMatrix)
            if nargin < 2
                useConnectivityMatrix = true;
            end
            
            if isempty(self.ConnectivityMatrix) || ~useConnectivityMatrix
                distanceMatrix = squareform(pdist(self.ElectrodePositions(self.Members(:, 1), :)));
                activationDifferenceMatrix = bsxfun(@minus, self.Members(:, 3)', self.Members(:, 3));
                conductionMatrix = distanceMatrix ./ activationDifferenceMatrix;
            else
                numberOfMembers = self.Size();
                if nargout > 1
                    distanceMatrixValues = cell(numberOfMembers, 1);
                end
                
                conductionMatrixRowIndices = cell(numberOfMembers, 1);
                conductionMatrixColumnIndices = cell(numberOfMembers, 1);
                conductionMatrixValues = cell(numberOfMembers, 1);
                for memberIndex = 1:numberOfMembers
                    memberPosition = self.ElectrodePositions(self.Members(memberIndex, 1), :);
                    memberActivation = self.Members(memberIndex, 3);
                    
                    connectedMemberIndices = find(self.ConnectivityMatrix(:, memberIndex));
                    connectedMemberPositions = self.ElectrodePositions(self.Members(self.ConnectivityMatrix(:, memberIndex), 1), :);
                    connectedMemberActivations = self.Members(self.ConnectivityMatrix(:, memberIndex), 3);
                    
                    positionDifferences = bsxfun(@minus, memberPosition, connectedMemberPositions);
                    distances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
                    activationDifferences = bsxfun(@minus, memberActivation, connectedMemberActivations);
                    
                    conductionMatrixRowIndices{memberIndex} = connectedMemberIndices(:);
                    conductionMatrixColumnIndices{memberIndex} = memberIndex * ones(numel(connectedMemberIndices), 1);
                    conductionMatrixValues{memberIndex} = distances ./ activationDifferences;
                    
                    if nargout > 1
                        distanceMatrixValues{memberIndex} = distances;
                    end
                end
                
                conductionMatrix = sparse(vertcat(conductionMatrixRowIndices{:}), vertcat(conductionMatrixColumnIndices{:}),...
                    vertcat(conductionMatrixValues{:}), numberOfMembers, numberOfMembers);
                
                if nargout > 1
                    distanceMatrix = sparse(vertcat(conductionMatrixRowIndices{:}), vertcat(conductionMatrixColumnIndices{:}),...
                        vertcat(distanceMatrixValues{:}), numberOfMembers, numberOfMembers);
                end
            end
        end
        
        function SetConnectivityMatrix(self, conductionThreshold, radius)
            if nargin < 3
                radius = Inf;
            end
            [localConduction, distanceMatrix] = self.LocalConduction(false);
            self.ConnectivityMatrix = sparse(localConduction >= conductionThreshold &...
                distanceMatrix <= radius);
        end
    end
    
    methods (Access = private)
        function tf = IsStartingPoint(self, memberIndex, connectivityMatrix)
            connectedMembers = connectivityMatrix(:, memberIndex);
            if any(connectedMembers)
                if all(self.Members(connectedMembers, 3) == self.Members(memberIndex, 3))
                    tf = true;
                    connectivityMatrix(connectedMembers, connectedMembers) = false;
                    connectivityMatrix(memberIndex, connectedMembers) = false;
                    connectivityMatrix(connectedMembers, memberIndex) = false;
                    for connectedIndex = find(connectedMembers)'
                        tf = self.IsStartingPoint(connectedIndex, connectivityMatrix);
                        return 
                    end
                else
                    tf = false;
                end
            else
                tf = true;
            end
        end
    end
end