classdef DeflectionFilter < UserInterfacePkg.IStatusChange & AlgorithmPkg.DeflectionDetectionResultsFilter
    properties (Access = private)
        AnalysisResult
        FilterSettings
    end
    
    methods
        function self = DeflectionFilter(settings)
            self.FilterSettings = settings;
        end
        
        function [filteredResult, filterSettings] = Apply(self, analysisResult)
            if ~iscell(analysisResult)
                [filteredResult, filterSettings] = ...
                    self.FilterTemplatePeaks(self.FilterSettings, analysisResult);
            elseif iscell(analysisResult) && numel(analysisResult)==1
                [filteredResult, filterSettings] = ...
                    self.FilterTemplatePeaks(self.FilterSettings, analysisResult{1});
            elseif iscell(analysisResult) && iscell(self.FilterSettings)
                filteredResult = cell(1, numel(analysisResult));
                filterSettings = cell(1, numel(analysisResult));
                parfor i = 1 : numel(analysisResult)
                    if (~isempty(analysisResult(i)))
                        [filteredResult{i}, filterSettings{i}] =...
                            self.FilterTemplatePeaks(self.FilterSettings{i}, analysisResult{i});
                    end
                end
            else
                filteredResult = cell(1, numel(analysisResult));
                filterSettings = cell(1, numel(analysisResult));
                parfor i = 1 : numel(analysisResult)
                    if (~isempty(analysisResult(i)))
                        [filteredResult{i}, filterSettings{i}] =...
                            FilterTemplatePeaks(self.FilterSettings, analysisResult{i});
                    end
                end
            end
        end
    end
    
    methods ( Access = private )
        function [filteredResult, filterSettings] = FilterTemplatePeaks(self, filterSettings, analysisResult)
            correlationThreshold = filterSettings.correlationThreshold;
            amplitudeThreshold = filterSettings.amplitudeThreshold;
            durationThreshold = filterSettings.durationThreshold;
            slopeThreshold = filterSettings.slopeThreshold;
            amplitudeDividedByDurationThreshold = filterSettings.amplitudeDividedByDurationThreshold;
            
            validPeaks = analysisResult.templatePeakCorrelations(:) >= correlationThreshold &...
                analysisResult.templatePeakAmplitudes(:) >= amplitudeThreshold & ...
                analysisResult.deflectionPeakSlopes(:) <= slopeThreshold & ...
                analysisResult.templatePeakDurations(:) <= durationThreshold & ...
                analysisResult.peakAmplitudeDividedByDuration(:) > amplitudeDividedByDurationThreshold;
            
            if isfield(analysisResult, 'PrimaryDeflections')
                validPeaks(analysisResult.PrimaryDeflections) = true;
            end
            
            if (ApplicationSettings.Instance.TemplateMatching.OverlapFilterEnabled)
                validPeaks = AlgorithmPkg.DeflectionFilter.FilterOverlap(analysisResult, validPeaks);
            end
            
            filteredResult = analysisResult;
            filteredResult.templatePeakIndices = analysisResult.templatePeakIndices(validPeaks);
            filteredResult.templatePeakCorrelations = analysisResult.templatePeakCorrelations(validPeaks);
            filteredResult.templatePeakCoefficients = analysisResult.templatePeakCoefficients(validPeaks);
            filteredResult.templatePeakDurations = analysisResult.templatePeakDurations(validPeaks);
            filteredResult.templatePeakAmplitudes = analysisResult.templatePeakAmplitudes(validPeaks);
            filteredResult.templatePeakTemplates = analysisResult.templatePeakTemplates(validPeaks, :);
            filteredResult.templatePeakRanges = analysisResult.templatePeakRanges(validPeaks, :);
            filteredResult.templateDescendentPartPeakRanges = analysisResult.templateDescendentPartPeakRanges(validPeaks, :);
            filteredResult.peakAmplitudeDividedByDuration = analysisResult.peakAmplitudeDividedByDuration(validPeaks);
            filteredResult.deflectionPeakSlopes = analysisResult.deflectionPeakSlopes(validPeaks);
            filteredResult.mostLikeliIndices = [];
            
            if isfield(analysisResult, 'PrimaryDeflections')
                filteredResult.PrimaryDeflections = analysisResult.PrimaryDeflections(validPeaks);
            end
        end
    end
    
    methods (Static)
        function validPeaks = FilterOverlap(analysisResult, validPeaks)
            if nargin < 2
                validPeaks = true(size(analysisResult.templatePeakIndices));
            end
            
            % Backward compatibility
            try
                validPeakRanges = analysisResult.templateDescendentPartPeakRanges(validPeaks, :);
            catch
                analysisResult.templateDescendentPartPeakRanges = analysisResult.templateDescendentPartRanges;
                validPeakRanges = analysisResult.templateDescendentPartPeakRanges(validPeaks, :);
            end
            
            validPeakIndices = find(validPeaks);
            overlapGraph = sparse(numel(validPeakIndices), numel(validPeakIndices));
            for peakIndex = 1:numel(validPeakIndices)
                matchRange = validPeakRanges(peakIndex, :);
                endMinusStart = uint32(matchRange(2) - validPeakRanges(:, 1));
                endMinusEnd = uint32(matchRange(2) - validPeakRanges(:, 2));
                startMinusStart = uint32(matchRange(1) - validPeakRanges(:, 1));
                candidatePeaks = (endMinusStart - endMinusEnd - startMinusStart > 0);
                candidatePeaks = candidatePeaks & validPeaks(validPeakIndices);
                candidatePeaks(peakIndex) = false;
                if any(candidatePeaks)
                    overlapGraph(peakIndex, candidatePeaks) = 1;
                end
            end
            
            if isfield(analysisResult, 'PrimaryDeflections')
                primaryPeaks = analysisResult.PrimaryDeflections(validPeaks);
                primaryIndices = find(primaryPeaks);
                invalidPeaks = false(size(validPeakIndices));
                invalidPeaks(primaryPeaks) = true;
                for deflectionIndex = 1:numel(primaryIndices)
                    overlappingFarFieldDeflections = (overlapGraph(primaryIndices(deflectionIndex), :))' > 0 &...
                        (~primaryPeaks(:));
                    invalidPeaks(overlappingFarFieldDeflections) = true;
                    validPeaks(validPeakIndices(overlappingFarFieldDeflections)) = false;
                end
                overlapGraph(invalidPeaks, :) = [];
                overlapGraph(:, invalidPeaks) = [];
                validPeakIndices = validPeakIndices(~invalidPeaks);
            end
            
            validPeakCorrelations = analysisResult.templatePeakCorrelations(validPeakIndices);
%             [numberOfComponents, componentIndices] = graphconncomp(overlapGraph);
            % Change for > R2022a
            [componentIndices, componentSize] = conncomp(digraph(overlapGraph));
            numberOfComponents = numel(componentSize);
            %
            for componentIndex = 1:numberOfComponents
                componentPeaks = find(componentIndices == componentIndex);
                if numel(componentPeaks) > 1
                    componentCorrelations = validPeakCorrelations(componentPeaks);
                    componentCorrelations = componentCorrelations(:);
                    componentGraph = overlapGraph(componentPeaks, componentPeaks);
                    componentGraph = abs(componentGraph - 1) - eye(numel(componentPeaks));
                    independentSets = AlgorithmPkg.DeflectionFilter.maximalCliques(componentGraph);
                    setValues = (independentSets') * componentCorrelations;
                    [maxSetValue, maxSetIndex] = max(setValues' ./ sum(independentSets, 1)); %#ok<ASGLU>
                    validNodes = independentSets(:, maxSetIndex(1));
                    validPeaks(validPeakIndices(componentPeaks)) = validNodes;
                end
            end
        end
        
        function MC = maximalCliques(A)
            %MAXIMALCLIQUES Find maximal cliques using the Bron-Kerbosch algorithm
            %   Given a graph's boolean adjacency matrix, A, find all maximal cliques
            %   on A using the Bron-Kerbosch algorithm in a recursive manner.  The
            %   graph is required to be undirected and must contain no self-edges.
            %
            %   This function can be used to compute the maximal independent sets
            %   (maximal matchings) of a graph by providing the adjacency matrix of the
            %   corresponding conflict graph (complement of the conflict graph).
            %
            %   V_STR is an optional input string with the version of the Bron-Kerbosch
            %   algorithm to be used (either 'v1' or 'v2').  Version 2 is faster (and
            %   default), and version 1 is included for posterity.
            %
            %   MC is the output matrix that contains the maximal cliques in its
            %   columns.
            %
            %   Ref: Bron, Coen and Kerbosch, Joep, "Algorithm 457: finding all cliques
            %   of an undirected graph", Communications of the ACM, vol. 16, no. 9,
            %   pp: 575???577, September 1973.
            %
            %   Ref: Cazals, F. and Karande, C., "A note on the problem of reporting
            %   maximal cliques", Theoretical Computer Science (Elsevier), vol. 407,
            %   no. 1-3, pp: 564-568, November 2008.
            %
            %   Jeffrey Wildman (c) 2011
            %   jeffrey.wildman@gmail.com
            
 
            % second, set up some variables
            
            n = size(A,2);      % number of vertices
            MC = [];            % storage for maximal cliques
            R = [];             % currently growing clique
            P = 1:n;            % prospective nodes connected to all nodes in R
            X = [];             % nodes already processed
            
            % third, run the algorithm!
            BKv2(R,P,X);
            
            % version 2 of the Bron-Kerbosch algo
            function [] = BKv2 ( R, P, X )
                
                if (isempty(P) && isempty(X))
                    % report R as a maximal clique
                    newMC = zeros(1,n);
                    newMC(R) = 1;                   % newMC contains ones at indices equal to the values in R
                    MC = [MC newMC.'];
                else
                    if ~isempty(X)
                        ppivots = HelperFunctions.myUnion(P,X);           % potential pivots
                    else
                        ppivots = P;
                    end
                    
                    
                    binP = zeros(1,n);
                    binP(P) = 1;                    % binP contains ones at indices equal to the values in P
                    % rows of A(ppivots,:) contain ones at the neighbors of ppivots
                    pcounts = A(ppivots,:)*binP.';  % cardinalities of the sets of neighbors of each ppivots intersected with P
                    [~,ind] = max(pcounts);
                    u_p = ppivots(ind);             % select one of the ppivots with the largest count
                    
                    if any(~A(u_p,:))
                        for u = HelperFunctions.myIntersect(find(~A(u_p,:)), P)   % all prospective nodes who are not neighbors of the pivot
                            P = HelperFunctions.mySetxor(P,u);
                            Rnew = [R u];
                            Nu = find(A(u,:));
                            if isempty(Nu)
                                Pnew = [];
                                Xnew = [];
                            else
                                if isempty(P)
                                    Pnew = [];
                                else
                                    Pnew = HelperFunctions.myIntersect(P, Nu);
                                end

                                if isempty(X)
                                    Xnew = [];
                                else
                                    Xnew = HelperFunctions.myIntersect(X, Nu);
                                end
                            end
                            BKv2(Rnew, Pnew, Xnew);
                            X = [X u];
                        end
                    end
                    
%                     for u = intersect(find(~A(u_p,:)), P)   % all prospective nodes who are not neighbors of the pivot
%                         P = setxor(P,u);
%                         Rnew = [R u];
%                         Nu = find(A(u,:));
%                         Pnew = intersect(P, Nu);
%                         Xnew = intersect(X, Nu);
%                         
%                         BKv2(Rnew, Pnew, Xnew);
%                         X = [X u];
%                     end
                end
                
            end % BKv2
            
            
        end % maximalCliques
    end
end