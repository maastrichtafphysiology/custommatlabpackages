classdef DeflectionDetectionResultsFilter < UserInterfacePkg.IStatusChange
    methods	(Abstract)		
        [filteredResult, filterSettings] = Apply(self, analysisResult)
    end
end