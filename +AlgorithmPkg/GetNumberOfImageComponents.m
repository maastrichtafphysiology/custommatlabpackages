function [numberOfComponents labelMatrix] = GetNumberOfImageComponents(image, threshold)
%#codegen

bw = image > threshold;
labelMatrix = zeros(size(image));

numberOfRows = size(image, 1);
numberOfElements = numel(image);
offsets = [...
    -numberOfRows - 1,...
    -numberOfRows,...
    -numberOfRows + 1,...
    -1,...
    1,...
    numberOfRows - 1,...
    numberOfRows,...
    numberOfRows + 1];
label = 1;
currentPosition = find(bw, 1, 'first');
labelMatrix(currentPosition) = label;

while ~isempty(currentPosition)
    bw(currentPosition) = false;
    neighbors = bsxfun(@plus, currentPosition, offsets);
    neighbors = unique(neighbors(:)')';
    neighbors = neighbors(neighbors > 0 & neighbors <= numberOfElements);
    
    neigborValues = bw(neighbors);
    if any(neigborValues)
        currentPosition = neighbors(neigborValues);
        bw(currentPosition) = 0;
        labelMatrix(currentPosition) = label;
    else
        label = label + 1;
        currentPosition = find(bw, 1, 'first');
    end
end

numberOfComponents = label - 1;
end