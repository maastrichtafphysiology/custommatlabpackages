function [correlation, coefficient] = templateCorrelation(signal, template, signalSum, signalSquaredSum, templateStartDeviation, templateEndDeviation)
%#codegen
signalLength = size(signal, 1);
templateLength = size(template, 1);
template = template - sum(template) / templateLength;
SSyy = sum(template.^2);
SSxy = NaN(signalLength, 1);

signalSums = zeros(signalLength, 1);
signalSums(templateStartDeviation + 1:(signalLength - templateEndDeviation)) =...
    signalSum((templateStartDeviation + 1 + templateEndDeviation):(signalLength))...
    - signalSum(1:(signalLength - templateEndDeviation - templateStartDeviation));

signalMeans = signalSums / templateLength;

SSxx = NaN(signalLength, 1);
SSxx(templateStartDeviation + 1:(signalLength - templateEndDeviation)) =...
    signalSquaredSum((templateStartDeviation + 1 + templateEndDeviation):(signalLength)) - ...
    signalSquaredSum(1:(signalLength - templateEndDeviation - templateStartDeviation));
SSxx = SSxx - (signalSums.^2) / templateLength;
SSxx(SSxx < 0) = 0;

for templateShift = (templateStartDeviation + 1):1:(signalLength - templateEndDeviation)
    currentSignal = signal((templateShift - templateStartDeviation + 1):...
        (templateShift + templateEndDeviation));
    SSxy(templateShift) = sum((currentSignal - signalMeans(templateShift)) .* template);
end

correlation = SSxy ./ sqrt(SSxx * SSyy);
coefficient = SSxy ./ SSyy;
end