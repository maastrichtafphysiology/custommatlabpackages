classdef ConductionBlockAnalyzer < handle
    properties
        ConductionBlockVelocityThreshold
    end
    
    methods
        function self = ConductionBlockAnalyzer()
            self.ConductionBlockVelocityThreshold = 0.1;
        end
        
    end
    
    methods (Static)
        function [electrodePairs, electrodePairConduction] = ComputeElectrodeConduction(ecgData)
            numberOfElectrodes = ecgData.GetNumberOfChannels();
            activations = ecgData.Activations;
            minimumSpacing = ecgData.ComputeMinimumSpacing();
            time = ecgData.GetTimeRange() * 1000;
            
            % default 3x3 grid to look for conduction block
            electrodeRadius = sqrt(2 * minimumSpacing^2 + 1e3 * eps);
            
            [neighbors, electrodeDistances] = ecgData.GetElectrodeNeighbors(1:numberOfElectrodes, electrodeRadius);
            [electrode1Index, electrode2Index] = find(triu(neighbors, 1));
            numberOfElectrodePairs = numel(electrode1Index);
            
            electrodePairs = [electrode1Index(:), electrode2Index(:), NaN(numberOfElectrodePairs, 1)];
            electrodePairConduction = NaN(ecgData.GetNumberOfSamples, numberOfElectrodePairs);
            for pairIndex = 1:numberOfElectrodePairs
                pairDistance =  electrodeDistances(electrodePairs(pairIndex, 1), electrodePairs(pairIndex, 2));
                electrodePairs(pairIndex, 3) = pairDistance;
                activationsElectrode1 = activations{electrodePairs(pairIndex, 1)};
                activationsElectrode2 = activations{electrodePairs(pairIndex, 2)};
                
                activationDifferences = bsxfun(@minus, activationsElectrode1, activationsElectrode2');
                localConductionMatrix = pairDistance ./ activationDifferences;
                
                maximumConductionElectrode1 = max(abs(localConductionMatrix));
                maximumConductionElectrode2 = max(abs(localConductionMatrix), [], 2);
                
                currentElectrodePairConduction = NaN(ecgData.GetNumberOfSamples, 2);
                for activationIndex = 1:numel(activationsElectrode1)
                    currentElectrodePairConduction(time >= activationsElectrode1(activationIndex), 1) =...
                        maximumConductionElectrode1(activationIndex);
                end
                
                for activationIndex = 1:numel(activationsElectrode2)
                    currentElectrodePairConduction(time >= activationsElectrode2(activationIndex), 2) =...
                        maximumConductionElectrode2(activationIndex);
                end
                
                electrodePairConduction(:, pairIndex) = max(abs(currentElectrodePairConduction), [], 2);
            end
        end
        
        function [electrodePairs, electrodePairConduction, electrodePairComponents, components] =...
                ComputeComponentConductionBlock(ecgData, conductionBlockThreshold, minimumComponentSize, excludeIntraComponentBlock)
            minimumSpacing = ecgData.ComputeMinimumSpacing();
            electrodeRadius = sqrt(2 * minimumSpacing^2 + 1e3 * eps);
            numberOfElectrodes = ecgData.GetNumberOfChannels();
            time = ecgData.GetTimeRange() * 1000;
            
            % determine activation component membership
            wavemapCalculator = AlgorithmPkg.WavemapCalculator();
            wavemapCalculator.NeighborRadius = electrodeRadius;
            wavemapCalculator.ConductionThreshold = conductionBlockThreshold;
            
            [components, activationData, activationComponentIndex] = wavemapCalculator.GetConnectedActivations(ecgData);
            activationComponentData = cell(numberOfElectrodes, 1);
            for electrodeIndex = 1:numberOfElectrodes
                electrodeActivationIndices = activationData(:, 1) == electrodeIndex;
                activationComponentData{electrodeIndex} = [...
                    activationData(electrodeActivationIndices, 3)';...
                    activationComponentIndex(electrodeActivationIndices)];
            end
            
            % determine small components
            smallComponentIndices = find(vertcat(components.Size) < minimumComponentSize);
            
            % determine local conduction block
            [neighbors, electrodeDistances] = ecgData.GetElectrodeNeighbors(1:numberOfElectrodes, electrodeRadius);
            [electrode1Index, electrode2Index] = find(triu(neighbors, 1));
            numberOfElectrodePairs = numel(electrode1Index);
            
            electrodePairs = [electrode1Index(:), electrode2Index(:), NaN(numberOfElectrodePairs, 1)];
            electrodePairConduction = NaN(ecgData.GetNumberOfSamples, numberOfElectrodePairs);
            electrodePairComponents = NaN(ecgData.GetNumberOfSamples, 2, numberOfElectrodePairs);
            for pairIndex = 1:numberOfElectrodePairs
                pairDistance =  electrodeDistances(electrodePairs(pairIndex, 1), electrodePairs(pairIndex, 2));
                electrodePairs(pairIndex, 3) = pairDistance;
                
                activationComponentsElectrode1 = activationComponentData{electrodePairs(pairIndex, 1)};
                activationComponentsElectrode2 = activationComponentData{electrodePairs(pairIndex, 2)};
                
                activationsElectrode1 = activationComponentsElectrode1(1, :);
                componentsElectrode1 = activationComponentsElectrode1(2, :);
                
                activationsElectrode2 = activationComponentsElectrode2(1, :);
                componentsElectrode2 = activationComponentsElectrode2(2, :);
                
                activationDifferences = bsxfun(@minus, activationsElectrode1, activationsElectrode2');
                localConductionMatrix = pairDistance ./ activationDifferences;
                
                [maximumConductionElectrode1, electrode2Index] = max(abs(localConductionMatrix));
                [maximumConductionElectrode2, electrode1Index] = max(abs(localConductionMatrix), [], 2);
                
                % no block between activations from the same component
                if excludeIntraComponentBlock
                    maximumConductionElectrode1(componentsElectrode1 == componentsElectrode2(electrode2Index)) = Inf;
                    maximumConductionElectrode2(componentsElectrode2 == componentsElectrode1(electrode1Index)) = Inf;
                end
                
                % no defined block to activations in small components
                maximumConductionElectrode1(ismember(componentsElectrode2(electrode2Index), smallComponentIndices)) = Inf;
                maximumConductionElectrode2(ismember(componentsElectrode1(electrode1Index), smallComponentIndices)) = Inf;
                
                combinedActivations = [activationsElectrode1(:); activationsElectrode2(:)];
                combinedElectrodeID = [ones(numel(activationsElectrode1), 1); 2 * ones(numel(activationsElectrode2), 1)];
                combinedActivationIndex = [(1:numel(activationsElectrode1))'; (1:numel(activationsElectrode2))'];
                [sortedActivations, sortedIndex] = sort(combinedActivations, 'ascend');
                combinedElectrodeID = combinedElectrodeID(sortedIndex);
                combinedActivationIndex = combinedActivationIndex(sortedIndex);
                
                currentElectrodePairConduction = NaN(ecgData.GetNumberOfSamples, 2);
                currentElectrodePairComponents = NaN(ecgData.GetNumberOfSamples, 2);
                electrode1ConductionComponents = componentsElectrode2(electrode2Index);
                electrode2ConductionComponents = componentsElectrode1(electrode1Index);
                for activationIndex = 1:numel(sortedActivations)
                    validTime = time >= sortedActivations(activationIndex);
                    
                    if combinedElectrodeID(activationIndex) == 1
                        currentElectrodePairConduction(validTime, 1) =...
                            maximumConductionElectrode1(combinedActivationIndex(activationIndex));
                        
                        currentElectrodePairComponents(validTime, 1) =...
                            componentsElectrode1(combinedActivationIndex(activationIndex));
                        currentElectrodePairComponents(validTime, 2) =...
                            electrode1ConductionComponents(combinedActivationIndex(activationIndex));
                    else
                        currentElectrodePairConduction(validTime, 1) =...
                            maximumConductionElectrode2(combinedActivationIndex(activationIndex));
                        
                        currentElectrodePairComponents(validTime, 1) =...
                            componentsElectrode2(combinedActivationIndex(activationIndex));
                        currentElectrodePairComponents(validTime, 2) =...
                            electrode2ConductionComponents(combinedActivationIndex(activationIndex));
                    end
                end

                electrodePairConduction(:, pairIndex) = max(abs(currentElectrodePairConduction), [], 2);
                electrodePairComponents(:, :, pairIndex) = currentElectrodePairComponents;
            end
        end
        
        function [phaseSingularityBlockDistance, phaseSingularityComponents, phaseSingularityBlock] =...
                ComputePhaseSingularityBlockDistance(phaseSingularityResult, electrodeBlockResult, phaseSingularityPositionCorrection)
            if nargin < 3
                phaseSingularityPositionCorrection = [0, 0, 0];
            end
            electrodePairs = electrodeBlockResult.electrodePairs;
            pairPositions = cat(3, electrodeBlockResult.electrodePositions(electrodePairs(:, 1), :), electrodeBlockResult.electrodePositions(electrodePairs(:, 2), :));
            pairPositions = mean(pairPositions, 3);
            
            numberOfPhaseSingularities = numel(phaseSingularityResult.phaseSingularityElectrodes);
            phaseSingularityBlockDistance = cell(numberOfPhaseSingularities, 1);
            phaseSingularityComponents = cell(numberOfPhaseSingularities, 1);
            phaseSingularityBlock = false(size(electrodeBlockResult.electrodePairConduction));
            for phaseSingularityIndex = 1:numberOfPhaseSingularities
                electrodes = phaseSingularityResult.phaseSingularityElectrodes{phaseSingularityIndex};
                interval = phaseSingularityResult.phaseSingularityIntervals(phaseSingularityIndex, :);
                sampleIndices = interval(1):interval(end);
                
                distance = NaN(size(sampleIndices));
                components = NaN(numel(sampleIndices), 2);
                for sampleIndex = 1:numel(sampleIndices)
                    electrodeBlock = electrodeBlockResult.electrodePairConduction(sampleIndices(sampleIndex), :);
                    if ~any(electrodeBlock), continue; end
                    blockedPairIndices = find(electrodeBlock);
                    electrodeComponents = squeeze(electrodeBlockResult.electrodePairComponents(sampleIndices(sampleIndex), :, :))';
                    
                    blockedPositions = pairPositions(electrodeBlock, :);
                    blockedComponents = electrodeComponents(electrodeBlock, :);
                    phaseSingularityPosition = electrodeBlockResult.electrodePositions(electrodes(sampleIndex), :); 
                    phaseSingularityPosition = phaseSingularityPosition + phaseSingularityPositionCorrection;
                    
                    positionDifferences = bsxfun(@minus, blockedPositions, phaseSingularityPosition);
                    distances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
                    
                    [distance(sampleIndex), minDistanceIndex] = min(distances);
                    components(sampleIndex, :) = blockedComponents(minDistanceIndex, :);
                    phaseSingularityBlock(sampleIndices(sampleIndex), blockedPairIndices(minDistanceIndex)) = true;
                end
                phaseSingularityBlockDistance{phaseSingularityIndex} = distance;
                phaseSingularityComponents{phaseSingularityIndex} = components;
            end
        end
        
        function blockDistance = ComputeSurrogateBlockDistance(ecgData, electrodeBlockResult)
            % determine electrodes with sufficient amount of neighbors to
            % compute phase singularities
            numberOfSamples = ecgData.GetNumberOfSamples();            
            minimumSpacing = ecgData.ComputeMinimumSpacing();
            maxPosition = max(ecgData.ElectrodePositions);
            minPosition = min(ecgData.ElectrodePositions);
            xData = (minPosition(1) + minimumSpacing / 2):minimumSpacing:(maxPosition(1) - minimumSpacing / 2);
            yData = (minPosition(2) + minimumSpacing / 2):minimumSpacing:(maxPosition(2) - minimumSpacing / 2);
            [ringCentersX, ringCentersY] = meshgrid(xData, yData);
            ringCenters = [ringCentersX(:), ringCentersY(:), ones(numel(ringCentersX), 1)];
            
            electrodeRadius = sqrt((1.5 * minimumSpacing)^2  + (minimumSpacing / 2)^2 + 1e3 * eps);   % 4 + 8 electrode ring
            minimumNumberOfNeighbors = 4 + 8;
            
            neighbors = ConductionBlockAnalyzer.GetElectrodesWithinRadius(ringCenters, ecgData.ElectrodePositions, electrodeRadius);
            validRingCenters = sum(neighbors) >= minimumNumberOfNeighbors;
            candidateCenters = ringCenters(validRingCenters, :);
            numberOfCandidateCenters = size(candidateCenters, 1);
            
            electrodePairs = electrodeBlockResult.electrodePairs;
            pairPositions = cat(3, electrodeBlockResult.electrodePositions(electrodePairs(:, 1), :), electrodeBlockResult.electrodePositions(electrodePairs(:, 2), :));
            pairPositions = mean(pairPositions, 3);
            electrodePairConduction = electrodeBlockResult.electrodePairConduction;
            
            blockDistance = NaN(numberOfSamples, numberOfCandidateCenters);
            
            parfor sampleIndex = 1:numberOfSamples
                electrodeBlock = electrodePairConduction(sampleIndex, :);
                    if ~any(electrodeBlock), continue; end
                    
                    blockedPositions = pairPositions(electrodeBlock, :);
                    positionDifferences = pdist2(blockedPositions, candidateCenters, 'euclidean', 'Smallest', 1);
                    
                    blockDistance(sampleIndex, :) = positionDifferences;
            end
        end
        
        function phaseSingularityBlockDistances = ComputeShuffledBlockPhaseSingularityDistance(...
                phaseSingularityResult, electrodeBlockResult, phaseSingularityPositionCorrection,...
                shuffleInSpace, shuffleInTime)
            % Shuffle block incidence (in time and space)
            numberOfTrials = 100;
            
            electrodePairConduction = electrodeBlockResult.electrodePairConduction;
            [numberOfSamples, numberOfElectrodePairs] = size(electrodePairConduction);
            
            numberOfPhaseSingularities = numel(phaseSingularityResult.phaseSingularityElectrodes);
            phaseSingularityBlockDistances = cell(numberOfPhaseSingularities, numberOfTrials);
            
            parfor trialIndex = 1:numberOfTrials
                % create random block distribution (sampled from empirical block distribution
                % random permutation of sample rows and electrode pair columns
                newElectrodeBlockResult = electrodeBlockResult;
                spaceShuffle = 1:numberOfElectrodePairs;
                if shuffleInSpace
                    spaceShuffle = randperm(numberOfElectrodePairs);
                end
                
                timeShuffle = 1:numberOfSamples;
                if shuffleInTime
                    timeShuffle = randperm(numberOfSamples);
                end
                
                newElectrodeBlockResult.electrodePairConduction = electrodePairConduction(timeShuffle, spaceShuffle);
                
                phaseSingularityBlockDistances(:, trialIndex) = ConductionBlockAnalyzer.ComputePhaseSingularityBlockDistance(...
                    phaseSingularityResult, newElectrodeBlockResult, phaseSingularityPositionCorrection); 
            end
        end
        
        function phaseSingularityBlockDistance = ComputeShuffledPhaseSingularityBlockDistance(ecgData,...
                 phaseSingularityResult, electrodeBlockResult, phaseSingularityPositionCorrection,...
                shuffleInSpace, shuffleInTime)
            
            % determine electrodes with sufficient amount of neighbors to compute phase singularities
            centerElectrodes = ConductionBlockAnalyzer.GetCandidatePhaseSingularityCenterElectrodes(ecgData);
            centerElectrodeIndices = find(centerElectrodes);
            numberOfCandidateCenters = numel(centerElectrodeIndices);
            
            numberOfTrials = 1000;
            electrodePositions = electrodeBlockResult.electrodePositions;
            
            electrodePairs = electrodeBlockResult.electrodePairs;
            pairPositions = cat(3, electrodeBlockResult.electrodePositions(electrodePairs(:, 1), :), electrodeBlockResult.electrodePositions(electrodePairs(:, 2), :));
            pairPositions = mean(pairPositions, 3);
            electrodePairConduction = electrodeBlockResult.electrodePairConduction;
            numberOfSamples = size(electrodePairConduction, 1);
            
            numberOfPhaseSingularities = numel(phaseSingularityResult.phaseSingularityElectrodes);
            phaseSingularityBlockDistance = cell(numberOfPhaseSingularities, 1);
            for phaseSingularityIndex = 1:numberOfPhaseSingularities
                electrodes = phaseSingularityResult.phaseSingularityElectrodes{phaseSingularityIndex};
                
                interval = phaseSingularityResult.phaseSingularityIntervals(phaseSingularityIndex, :);
                trueSampleIndices = interval(1):interval(end);
                numberOfSampleIndices = numel(trueSampleIndices);
                
                distance = NaN(numberOfSampleIndices, numberOfTrials);
                parfor trialIndex= 1:numberOfTrials
                    currentElectrodes = electrodes;
                    if shuffleInSpace
                        newElectrodeIndex = randi([1, numberOfCandidateCenters]);
                        currentElectrodes = ones(size(currentElectrodes)) * centerElectrodeIndices(newElectrodeIndex);
                    end
                    
                    sampleIndices = trueSampleIndices;
                    if shuffleInTime
                        newSampleStartIndex = randi([1, numberOfSamples - numberOfSampleIndices]);
                        sampleIndices = newSampleStartIndex:(newSampleStartIndex + numberOfSampleIndices - 1);
                    end
                    
                    for sampleIndex = 1:numberOfSampleIndices
                        electrodeBlock = electrodePairConduction(sampleIndices(sampleIndex), :);
                        if ~any(electrodeBlock), continue; end
                        
                        blockedPositions = pairPositions(electrodeBlock, :);
                        phaseSingularityPosition = electrodePositions(currentElectrodes(sampleIndex), :);
                        phaseSingularityPosition = phaseSingularityPosition + phaseSingularityPositionCorrection;
                        
                        positionDifferences = bsxfun(@minus, blockedPositions, phaseSingularityPosition);
                        distances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
                        
                        distance(sampleIndex, trialIndex) = min(distances);
                    end
                end
                phaseSingularityBlockDistance{phaseSingularityIndex} = distance;
            end
        end
        
        function [phaseSingularityIntervalBlockDistance, nonPhaseSingularityIntervalBlockDistance, nonPhaseSingularityBlockDistance] =...
                ComputeNonPhaseSingularityBlockDistance(ecgData, phaseSingularityResult, electrodeBlockResult, phaseSingularityPositionCorrection)
            
            % determine electrodes with sufficient amount of neighbors to compute phase singularities
            candidatePhaseSingularityElectrodes = ConductionBlockAnalyzer.GetCandidatePhaseSingularityCenterElectrodes(ecgData);
            
            electrodePositions = electrodeBlockResult.electrodePositions;
            numberOfElectrodes = size(electrodePositions, 1);
            
            electrodePairs = electrodeBlockResult.electrodePairs;
            pairPositions = cat(3, electrodeBlockResult.electrodePositions(electrodePairs(:, 1), :), electrodeBlockResult.electrodePositions(electrodePairs(:, 2), :));
            pairPositions = mean(pairPositions, 3);
            electrodePairConduction = electrodeBlockResult.electrodePairConduction;
            numberOfSamples = size(electrodePairConduction, 1);
            
            % determine intervals with phase singularities
            time = ecgData.GetTimeRange();
            phaseSingularityPresent = false(size(time));
            phaseSingularityElectrodesInTime = false(numberOfSamples, numberOfElectrodes);
            numberOfPhaseSingularities = numel(phaseSingularityResult.phaseSingularityElectrodes);
            for phaseSingularityIndex = 1:numberOfPhaseSingularities
                interval = phaseSingularityResult.phaseSingularityIntervals(phaseSingularityIndex, :);
                interval = interval(1):interval(end);
                phaseSingularityPresent(round(interval(1):interval(end))) = true;
                
                electrodes = phaseSingularityResult.phaseSingularityElectrodes{phaseSingularityIndex};
                for sampleIndex = 1:numel(interval)
                    phaseSingularityElectrodesInTime(interval(sampleIndex), electrodes(sampleIndex)) = true;
                end
            end
            
            % during a single phase singularity
            nonPhaseSingularityBlockDistance = cell(numberOfPhaseSingularities, 1);
            for phaseSingularityIndex = 1:numberOfPhaseSingularities
                electrodes = phaseSingularityResult.phaseSingularityElectrodes{phaseSingularityIndex};
                interval = phaseSingularityResult.phaseSingularityIntervals(phaseSingularityIndex, :);
                trueSampleIndices = interval(1):interval(end);
                numberOfSampleIndices = numel(trueSampleIndices);
                
                % always exactly 1 phase singularity electrode
                numberOfNonPhaseSingularityElectrodeIndices = numel(find(candidatePhaseSingularityElectrodes)) - 1;
                currentNonPhaseSingularityBlockDistance = NaN(numberOfNonPhaseSingularityElectrodeIndices, numberOfSampleIndices);
                for sampleIndex = 1:numberOfSampleIndices
                    electrodeBlock = electrodePairConduction(trueSampleIndices(sampleIndex), :);
                    blockedPositions = pairPositions(electrodeBlock, :);
                    
                    if ~any(electrodeBlock), continue; end
                
                    currentPhaseSingularityElectrodeIndex = electrodes(sampleIndex);
                    
                    nonPhaseSingularityElectrodeIndices = candidatePhaseSingularityElectrodes;
                    nonPhaseSingularityElectrodeIndices(currentPhaseSingularityElectrodeIndex) = false;
                    nonPhaseSingularityElectrodeIndices = find(nonPhaseSingularityElectrodeIndices);
                    
                    for nonPhaseSingularityElectrodeIndex = 1:numberOfNonPhaseSingularityElectrodeIndices
                        phaseSingularityPosition = electrodePositions(nonPhaseSingularityElectrodeIndices(nonPhaseSingularityElectrodeIndex), :);
                        phaseSingularityPosition = phaseSingularityPosition + phaseSingularityPositionCorrection;
                        
                        positionDifferences = bsxfun(@minus, blockedPositions, phaseSingularityPosition);
                        distances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
                        
                        currentNonPhaseSingularityBlockDistance(nonPhaseSingularityElectrodeIndex, sampleIndex) = min(distances);
                    end
                end
                nonPhaseSingularityBlockDistance{phaseSingularityIndex} = currentNonPhaseSingularityBlockDistance;
            end
            
            % during time instances with a phase singularity present
            sampleIndicesWithPhaseSingularities = find(phaseSingularityPresent);
            phaseSingularityIntervalBlockDistance = cell(numel(sampleIndicesWithPhaseSingularities), 1);
            nonPhaseSingularityIntervalBlockDistance = cell(numel(sampleIndicesWithPhaseSingularities), 1);
            for sampleIndex = 1:numel(sampleIndicesWithPhaseSingularities)
                phaseSingularityElectrodeIndices = find(phaseSingularityElectrodesInTime(sampleIndicesWithPhaseSingularities(sampleIndex), :)');
                numberOfPhaseSingularityElectrodeIndices = numel(phaseSingularityElectrodeIndices);
                
                nonPhaseSingularityElectrodeIndices = find(candidatePhaseSingularityElectrodes &...
                    ~phaseSingularityElectrodesInTime(sampleIndicesWithPhaseSingularities(sampleIndex), :)');
                numberOfNonPhaseSingularityElectrodeIndices = numel(nonPhaseSingularityElectrodeIndices);
                
                nonPhaseSingularityIntervalBlockDistance{sampleIndex} = NaN(numberOfNonPhaseSingularityElectrodeIndices, 1);
                phaseSingularityIntervalBlockDistance{sampleIndex} = NaN(numberOfPhaseSingularityElectrodeIndices, 1);
                
                electrodeBlock = electrodePairConduction(sampleIndicesWithPhaseSingularities(sampleIndex), :);
                blockedPositions = pairPositions(electrodeBlock, :);
                
                if ~any(electrodeBlock), continue; end
                
                currentPhaseSingularityBlockDistance = NaN(numberOfPhaseSingularityElectrodeIndices, 1);
                for electrodeIndex = 1:numberOfPhaseSingularityElectrodeIndices
                    phaseSingularityPosition = electrodePositions(phaseSingularityElectrodeIndices(electrodeIndex), :);
                    phaseSingularityPosition = phaseSingularityPosition + phaseSingularityPositionCorrection;
                    
                    positionDifferences = bsxfun(@minus, blockedPositions, phaseSingularityPosition);
                    distances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
                    
                    currentPhaseSingularityBlockDistance(electrodeIndex) = min(distances);
                end
                phaseSingularityIntervalBlockDistance{sampleIndex} = currentPhaseSingularityBlockDistance;
                
                currentNonPhaseSingularityBlockDistance = NaN(numberOfNonPhaseSingularityElectrodeIndices, 1);
                for electrodeIndex = 1:numberOfNonPhaseSingularityElectrodeIndices
                    phaseSingularityPosition = electrodePositions(nonPhaseSingularityElectrodeIndices(electrodeIndex), :);
                    phaseSingularityPosition = phaseSingularityPosition + phaseSingularityPositionCorrection;
                    
                    positionDifferences = bsxfun(@minus, blockedPositions, phaseSingularityPosition);
                    distances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
                    
                    currentNonPhaseSingularityBlockDistance(electrodeIndex) = min(distances);
                end
                nonPhaseSingularityIntervalBlockDistance{sampleIndex} = currentNonPhaseSingularityBlockDistance;
            end
        end
        
        function [phaseSingularityPresent, blockPresent] = ComputePhaseSingularityBlockCoIncidence(ecgData, phaseSingularityResult, electrodeBlockResult)
            electrodePositions = electrodeBlockResult.electrodePositions;
            numberOfElectrodes = size(electrodePositions, 1);
            
            electrodePairConduction = electrodeBlockResult.electrodePairConduction;
            numberOfSamples = size(electrodePairConduction, 1);
            
            % determine intervals with phase singularities
            time = ecgData.GetTimeRange();
            phaseSingularityPresent = false(size(time));
            phaseSingularityElectrodesInTime = false(numberOfSamples, numberOfElectrodes);
            numberOfPhaseSingularities = numel(phaseSingularityResult.phaseSingularityElectrodes);
            for phaseSingularityIndex = 1:numberOfPhaseSingularities
                interval = phaseSingularityResult.phaseSingularityIntervals(phaseSingularityIndex, :);
                interval = interval(1):interval(end);
                phaseSingularityPresent(round(interval(1):interval(end))) = true;
            end
            
            phaseSingularityPresent = any(phaseSingularityElectrodesInTime, 2);
            blockPresent = any(electrodePairConduction, 2);
        end
        
        
        function phaseSingularityBlockDistance = ComputeSurrogatePhaseSingularityInterval(phaseSingularityResult, electrodeBlockResult, phaseSingularityPositionCorrection)
            numberOfTrials = 100;
            electrodePositions = electrodeBlockResult.electrodePositions;
            
            electrodePairs = electrodeBlockResult.electrodePairs;
            pairPositions = cat(3, electrodeBlockResult.electrodePositions(electrodePairs(:, 1), :), electrodeBlockResult.electrodePositions(electrodePairs(:, 2), :));
            pairPositions = mean(pairPositions, 3);
            electrodePairConduction = electrodeBlockResult.electrodePairConduction;
            numberOfSamples = size(electrodePairConduction, 1);
            
            numberOfPhaseSingularities = numel(phaseSingularityResult.phaseSingularityElectrodes);
            phaseSingularityBlockDistance = cell(numberOfPhaseSingularities, 1);
            for phaseSingularityIndex = 1:numberOfPhaseSingularities
                electrodes = phaseSingularityResult.phaseSingularityElectrodes{phaseSingularityIndex};
                interval = phaseSingularityResult.phaseSingularityIntervals(phaseSingularityIndex, :);
                trueSampleIndices = interval(1):interval(end);
                numberOfSampleIndices = numel(trueSampleIndices);
                
                distance = NaN(numberOfSampleIndices, numberOfTrials);
                parfor trialIndex= 1:numberOfTrials
                    newSampleStartIndex = randi([1, numberOfSamples - numberOfSampleIndices]);
                    sampleIndices = newSampleStartIndex:(newSampleStartIndex + numberOfSampleIndices - 1);
                    
                    for sampleIndex = 1:numberOfSampleIndices
                        electrodeBlock = electrodePairConduction(sampleIndices(sampleIndex), :);
                        if ~any(electrodeBlock), continue; end
                        
                        blockedPositions = pairPositions(electrodeBlock, :);
                        phaseSingularityPosition = electrodePositions(electrodes(sampleIndex), :);
                        phaseSingularityPosition = phaseSingularityPosition + phaseSingularityPositionCorrection;
                        
                        positionDifferences = bsxfun(@minus, blockedPositions, phaseSingularityPosition);
                        distances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
                        
                        distance(sampleIndex, trialIndex) = min(distances);
                    end
                end
                phaseSingularityBlockDistance{phaseSingularityIndex} = distance;
            end
        end
        
        function neighbors = GetElectrodesWithinRadius(position, electrodePositions, radius)
            positionDistances = pdist2(electrodePositions, position);
            neighbors = positionDistances <= radius;
        end
    end
    
    methods (Static, Access = private)
        function centerElectrodes = GetCandidatePhaseSingularityCenterElectrodes(ecgData)
            
            % determine electrodes with sufficient amount of neighbors to compute phase singularities
            minimumSpacing = ecgData.ComputeMinimumSpacing();
            maxPosition = max(ecgData.ElectrodePositions);
            minPosition = min(ecgData.ElectrodePositions);
            centerCorrection = [minimumSpacing / 2, minimumSpacing / 2, 0];
            xData = (minPosition(1) + minimumSpacing / 2):minimumSpacing:(maxPosition(1) - minimumSpacing / 2);
            yData = (minPosition(2) + minimumSpacing / 2):minimumSpacing:(maxPosition(2) - minimumSpacing / 2);
            [ringCentersX, ringCentersY] = meshgrid(xData, yData);
            ringCenters = [ringCentersX(:), ringCentersY(:), ones(numel(ringCentersX), 1)];
            
            electrodeRadius = sqrt((1.5 * minimumSpacing)^2  + (minimumSpacing / 2)^2 + 1e3 * eps);   % 4 + 8 electrode ring
            minimumNumberOfNeighbors = 4 + 8;
            
            neighbors = ConductionBlockAnalyzer.GetElectrodesWithinRadius(ringCenters, ecgData.ElectrodePositions, electrodeRadius);
            validRingCenters = sum(neighbors) >= minimumNumberOfNeighbors;
            candidateCenters = ringCenters(validRingCenters, :);
            
            centerElectrodes = ismember(ecgData.ElectrodePositions, bsxfun(@minus, candidateCenters, centerCorrection), 'rows');
        end
    end
end