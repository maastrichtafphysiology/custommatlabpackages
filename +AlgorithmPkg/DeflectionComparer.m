classdef DeflectionComparer < handle
    properties
        ActivationShift
        BlankingPeriod
        RangeExtension
    end
    
    methods
        function self = DeflectionComparer()
            self.ActivationShift = 0;
            self.BlankingPeriod = 0;
            self.RangeExtension = 0;
        end
        
        function [electrodeSpecificity, electrodeSensitivity] = Compare(self, deflectionData, activationData, time)
            startTime = time(1) + self.BlankingPeriod;
            endTime = time(end) - self.BlankingPeriod;
            electrodeSpecificity = cell(numel(deflectionData), 1);
            electrodeSensitivity = cell(numel(deflectionData), 1);
            
            for electrodeIndex = 1:numel(deflectionData)
                numberOfDeflections = numel(find(deflectionData{electrodeIndex}.PrimaryDeflections));
                deflectionRanges = deflectionData{electrodeIndex}.templatePeakRanges(deflectionData{electrodeIndex}.PrimaryDeflections, :);
                deflectionRanges = time(deflectionRanges);
                deflectionRanges = [deflectionRanges(:, 1) - self.RangeExtension...
                    deflectionRanges(:, end) + self.RangeExtension];
                
                activations = activationData(electrodeIndex, :);
                activations = activations(~isnan(activations));
                activations = activations + self.ActivationShift;
                numberOfActivations = numel(activations);
                
                currentSpecificity = false(numberOfDeflections, numberOfActivations);
                for deflectionIndex = 1:numberOfDeflections
                    currentSpecificity(deflectionIndex, :) = AlgorithmPkg.DeflectionComparer.GetActivationWithinRange(...
                        deflectionRanges(deflectionIndex, :), activations);
                end
                validDeflections = deflectionRanges(:, 1) >= startTime & deflectionRanges(:, 2) <= endTime;
                currentSpecificity = currentSpecificity(validDeflections, :);
                
                currentSensitivity = false(numberOfActivations, numberOfDeflections);
                for activationIndex = 1:numberOfActivations
                    currentSensitivity(activationIndex, :) = AlgorithmPkg.DeflectionComparer.GetDeflectionCloseToActivation(...
                        activations(activationIndex), deflectionRanges);
                end
                validActivations = activations >= startTime & activations <= endTime;
                currentSensitivity = currentSensitivity(validActivations, :);
                
                electrodeSpecificity{electrodeIndex} = any(currentSpecificity, 2);
                electrodeSensitivity{electrodeIndex} = any(currentSensitivity, 2);
            end
        end
        
        function [electrodeSpecificity, electrodeSensitivity] = CompareTheoretical(self, deflectionData, activationData, time)
            startTime = time(1) + self.BlankingPeriod;
            endTime = time(end) - self.BlankingPeriod;
            electrodeSpecificity = cell(numel(deflectionData), 1);
            electrodeSensitivity = cell(numel(deflectionData), 1);
            
            for electrodeIndex = 1:numel(deflectionData)
                numberOfDeflections = numel(deflectionData{electrodeIndex}.templatePeakIndices);
                deflectionRanges = deflectionData{electrodeIndex}.templatePeakRanges;
                deflectionRanges = time(deflectionRanges);
                
                activations = activationData(electrodeIndex, :);
                activations = activations(~isnan(activations));
                activations = activations + self.ActivationShift;
                numberOfActivations = numel(activations);
                
                currentSpecificity = false(numberOfDeflections, numberOfActivations);
                for deflectionIndex = 1:numberOfDeflections
                    currentSpecificity(deflectionIndex, :) = AlgorithmPkg.DeflectionComparer.GetActivationWithinRange(...
                        deflectionRanges(deflectionIndex, :), activations);
                end
                validDeflections = deflectionRanges(:, 1) >= startTime & deflectionRanges(:, 2) <= endTime;
                currentSpecificity = currentSpecificity(validDeflections, :);
                
                currentSensitivity = false(numberOfActivations, numberOfDeflections);
                for activationIndex = 1:numberOfActivations
                    currentSensitivity(activationIndex, :) = AlgorithmPkg.DeflectionComparer.GetDeflectionCloseToActivation(...
                        activations(activationIndex), deflectionRanges);
                end
                validActivations = activations >= startTime & activations <= endTime;
                currentSensitivity = currentSensitivity(validActivations, :);
                
                electrodeSpecificity{electrodeIndex} = any(currentSpecificity, 2);
                electrodeSensitivity{electrodeIndex} = any(currentSensitivity, 2);
            end
        end
    end
    
    methods (Static)
        function validActivations = GetActivationWithinRange(range, activations)
            validActivations = activations >= range(1) & activations <= range(end);
        end
        
        function validDeflections = GetDeflectionCloseToActivation(activation, deflectionRanges)
            validDeflections = deflectionRanges(:, 1) <= activation & deflectionRanges(:, end) >= activation;
        end
    end
end