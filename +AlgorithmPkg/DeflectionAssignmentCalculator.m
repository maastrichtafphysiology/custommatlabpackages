classdef DeflectionAssignmentCalculator < UserInterfacePkg.IStatusChange & AlgorithmPkg.DeflectionDetectionResultsFilter
    properties (Access = public)
        AssignmentAlgorithm
        InitialDeflectionAssignment
        Fit
        Time
    end
    
    properties (Constant)
        ASSIGNMENT_ALGORITHMS = {'Local search',...
            'Interval', 'Interval sorted by amplitude',...
            'Interval sorted by slope * amplitude', 'Shortest weighthed interval path'}
        InitialDeflectionAssignments = {'All deflections', 'Random selected deflections', 'Most likely deflections'}
    end
    
    methods
        function self = DeflectionAssignmentCalculator(fit)
            self.AssignmentAlgorithm = AlgorithmPkg.DeflectionAssignmentCalculator.ASSIGNMENT_ALGORITHMS{4};
            self.InitialDeflectionAssignment = AlgorithmPkg.DeflectionAssignmentCalculator.InitialDeflectionAssignments{1};
            self.Fit = fit;
        end
        
        function [filteredResult, filterSettings] = Apply(self, analysisResult)
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Assigning deflections...', 0, self));
            numberOfChannels = numel(analysisResult);
            if iscell(analysisResult)
                filteredResult = cell(1, numel(analysisResult));
                parfor channelIndex = 1 : numel(analysisResult)
                    if (~isempty(analysisResult(channelIndex)))
                        filteredResult{channelIndex} = Solve(self, analysisResult{channelIndex});
                    end
                    
                    if mod(channelIndex, 10) == 0
                        progress = channelIndex / numberOfChannels;
                        UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                            UserInterfacePkg.StatusChangeEventData('Assigning deflections...', progress, self));
                    end
                end
            else
                filteredResult = self.Solve(analysisResult);
            end
            filterSettings = [];
            
            progress = 1;
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Done', progress, self));
        end
        
        function templateMatchingResult = SpatialDetection(self, ecgData, templateMatchingResult, radius, velocityThresholds)
            if nargin < 5
                velocityThresholds = [0.2, 2];
                if nargin < 4
                    radius = sqrt(2 * ecgData.ComputeMinimumSpacing()^2 + 1e3*eps);
                end
            end
            
            persistent figureHandle;
            
            numberOfChannels = ecgData.GetNumberOfChannels();
            
            electrodeNeighbors = ecgData.GetElectrodeNeighbors(1:numberOfChannels, radius);
            distanceMatrix = squareform(pdist(ecgData.ElectrodePositions)) / 1000;
            allDeflectionTimes = cell(numberOfChannels, 1);
            allDeflectionSlopes = cell(numberOfChannels, 1);
            allDeflectionAmplitudes = cell(numberOfChannels, 1);
            allDeflectionWeights = cell(numberOfChannels, 1);
            for electrodeIndex = 1:numberOfChannels
                deflectionIndices = templateMatchingResult{electrodeIndex}.templatePeakIndices;
                allDeflectionTimes{electrodeIndex} = self.Time(deflectionIndices);
                allDeflectionAmplitudes{electrodeIndex} = templateMatchingResult{electrodeIndex}.templatePeakAmplitudes;
                allDeflectionSlopes{electrodeIndex} = allDeflectionAmplitudes{electrodeIndex} ./...
                    (templateMatchingResult{electrodeIndex}.templatePeakDurations(:) / templateMatchingResult{electrodeIndex}.samplingFrequency);
                allDeflectionWeights{electrodeIndex} = allDeflectionAmplitudes{electrodeIndex} .* allDeflectionSlopes{electrodeIndex};
                electrodeNeighbors(electrodeIndex, electrodeIndex) = false;
            end
            
            for electrodeIndex = 1:numberOfChannels
                electrodeTimes = allDeflectionTimes{electrodeIndex};
                electrodeWeights = allDeflectionWeights{electrodeIndex};
                deflectionWeights = zeros(size(electrodeTimes));
                neighborTimes = allDeflectionTimes(electrodeNeighbors(:, electrodeIndex));
                neighborDistance = distanceMatrix(electrodeNeighbors(:, electrodeIndex), electrodeIndex);
                electrodeNeighborWeights = allDeflectionWeights(electrodeNeighbors(:, electrodeIndex));
                
                incomingConduction = zeros(numel(neighborTimes), 1);
                outgoingConduction = zeros(numel(neighborTimes), 1);
                for deflectionIndex = 1:numel(electrodeTimes)
                    for neighborIndex = 1:numel(neighborTimes)
                        neighborWeights = electrodeNeighborWeights{neighborIndex};
                        neighborVelocities = neighborDistance(neighborIndex) ./ (electrodeTimes(deflectionIndex) - neighborTimes{neighborIndex});
                        incomingDeflections = neighborVelocities <= -velocityThresholds(1) & neighborVelocities >= -velocityThresholds(end);
                        if any(incomingDeflections)
                            incomingConduction(neighborIndex) = max(neighborWeights(incomingDeflections));
                        end
                        
                        outgoingDeflections = neighborVelocities >= velocityThresholds(1) & neighborVelocities <= velocityThresholds(end);
                        if any(outgoingDeflections)
                            outgoingConduction(neighborIndex) = max(neighborWeights(outgoingDeflections));
                        end
                    end
                    deflectionWeights(deflectionIndex) = abs(sum(incomingConduction) + sum(outgoingConduction)) * electrodeWeights(deflectionIndex);
                end
                lambda = 1;
                deflectionWeights = lambda * deflectionWeights;
                
                analysisResult = templateMatchingResult{electrodeIndex};
                deflectionIndices = analysisResult.templatePeakIndices;
                deflectionAmplitudes = analysisResult.templatePeakAmplitudes;
                deflectionSlopes = deflectionAmplitudes(:) ./...
                    (analysisResult.templatePeakDurations(:) / analysisResult.samplingFrequency);
                deflectionTimes = self.Time(deflectionIndices);
                
                fitMean = self.Fit.Beta(2);
                fitSigma = self.Fit.Beta(3);
                fitFunctionHandle = @(x) exp(-0.5 * ((x - fitMean)./fitSigma).^2) ./ (sqrt(2*pi) .* fitSigma);
                
                intervalProbabilityThreshold = eps;
                
                numberOfDeflections = numel(deflectionIndices);
                connectivityMatrix = sparse(numberOfDeflections + 2, numberOfDeflections + 2);
                % connections from beginning of the signal
                for deflectionIndex = 1:numberOfDeflections
                    if deflectionTimes(deflectionIndex) < fitMean
                        intervalProbability = fitFunctionHandle(fitMean);
                    else
                        intervalProbability = fitFunctionHandle(deflectionTimes(deflectionIndex));
                    end
                    if intervalProbability < intervalProbabilityThreshold, break; end
                    
                    connectivityMatrix(1, deflectionIndex + 1) = -(log(intervalProbability) * (deflectionWeights(deflectionIndex)) / 2);
                end
                
                % connections between deflections
                for deflectionIndex = 1:numberOfDeflections
                    deflectionWeight = deflectionWeights(deflectionIndex);
                    for nextDeflectionIndex = (deflectionIndex + 1):numberOfDeflections
                        interval = deflectionTimes(nextDeflectionIndex) - deflectionTimes(deflectionIndex);
                        intervalProbability = fitFunctionHandle(interval);
                        if intervalProbability < intervalProbabilityThreshold
                            if interval > fitMean,
                                break;
                            else
                                continue;
                            end
                        end
                        
                        nextDeflectionWeight = deflectionWeights(nextDeflectionIndex);
                        connectivityMatrix(deflectionIndex + 1, nextDeflectionIndex + 1) = -(log(intervalProbability) *...
                            ((deflectionWeight) / 2 + (nextDeflectionWeight) / 2));
                    end
                end
                
                % connections to the end of the signal
                for deflectionIndex = numberOfDeflections:-1:1
                    if self.Time(end) - deflectionTimes(deflectionIndex) < fitMean
                        intervalProbability = fitFunctionHandle(fitMean);
                    else
                        intervalProbability = fitFunctionHandle(self.Time(end) - deflectionTimes(deflectionIndex));
                    end
                    if intervalProbability < intervalProbabilityThreshold, break; end
                    
                    connectivityMatrix(deflectionIndex + 1, end) = -(log(intervalProbability) * ((deflectionWeights(deflectionIndex)) / 2));
                end
                
                % shortest path between beginning and end
                [distance, path] = graphshortestpath(connectivityMatrix, 1, numberOfDeflections + 2, 'method', 'Acyclic');
                intrinsicDeflectionIndices = path(2:(end - 1)) - 1;
                
                intrinsicDeflections = false(size(deflectionIndices));
                intrinsicDeflections(intrinsicDeflectionIndices) = true;
                
                templateMatchingResult{electrodeIndex}.PrimaryDeflections = intrinsicDeflections;
                
                visualizeResults = false;
                if visualizeResults
                    if isempty(figureHandle)
                        figureHandle = figure('name', 'Shortest path assignment', 'numberTitle', 'off');
                    end
                    axesHandles = NaN(4, 1);
                    
                    axesHandles(1) = subplot(4,1,1, 'parent', figureHandle);
                    plot(self.Time, ecgData.Data(:, electrodeIndex));
                    hold(axesHandles(1), 'on');
                    stem(axesHandles(1), deflectionTimes(intrinsicDeflections), deflectionAmplitudes(intrinsicDeflections), 'r');
                    hold(axesHandles(1), 'off');
                    title(axesHandles(1), 'Amplitude');
                    
                    axesHandles(2) = subplot(4,1,2, 'parent', figureHandle);
                    stem(axesHandles(2), deflectionTimes, deflectionAmplitudes);
                    hold(axesHandles(2), 'on');
                    stem(axesHandles(2), deflectionTimes(intrinsicDeflections), deflectionAmplitudes(intrinsicDeflections), 'r');
                    hold(axesHandles(2), 'off');
                    title(axesHandles(2), 'Amplitude');
                    
                    axesHandles(3) = subplot(4,1,3, 'parent', figureHandle);
                    stem(axesHandles(3), deflectionTimes, deflectionSlopes);
                    hold(axesHandles(3), 'on');
                    stem(axesHandles(3), deflectionTimes(intrinsicDeflections), deflectionSlopes(intrinsicDeflections), 'r');
                    hold(axesHandles(3), 'off');
                    title(axesHandles(3), 'Slope');
                    
                    axesHandles(4) = subplot(4,1,4, 'parent', figureHandle);
                    stem(axesHandles(4), deflectionTimes, deflectionWeights);
                    hold(axesHandles(4), 'on');
                    stem(axesHandles(4), deflectionTimes(intrinsicDeflections), deflectionWeights(intrinsicDeflections), 'r');
                    hold(axesHandles(4), 'off');
                    title(axesHandles(4), 'Absolute degree');
                    
                    linkaxes(axesHandles, 'x');
                    pause;
                end
            end
        end
    end
    
    methods (Access = private)
        function analysisResult = Solve(self, analysisResult)
            switch self.AssignmentAlgorithm
                case AlgorithmPkg.DeflectionAssignmentCalculator.ASSIGNMENT_ALGORITHMS{1}
                    intrinsicDeflections = self.LocalSearchAssignment(analysisResult);
                case AlgorithmPkg.DeflectionAssignmentCalculator.ASSIGNMENT_ALGORITHMS{2}
                    intrinsicDeflections = self.SmallestIntervalAssignment(analysisResult);
                case AlgorithmPkg.DeflectionAssignmentCalculator.ASSIGNMENT_ALGORITHMS{3}
                    intrinsicDeflections = self.SmallestAmplitudeAssignment(analysisResult);
                case AlgorithmPkg.DeflectionAssignmentCalculator.ASSIGNMENT_ALGORITHMS{4}
                    intrinsicDeflections = self.AmplitudeSlopeAssignment(analysisResult);
                case AlgorithmPkg.DeflectionAssignmentCalculator.ASSIGNMENT_ALGORITHMS{5}
                    intrinsicDeflections = self.ShortestPathAssignment(analysisResult);
                otherwise
                    intrinsicDeflections = self.LocalSearchAssignment(analysisResult);
            end
            analysisResult.PrimaryDeflections = intrinsicDeflections;
        end
        
        function intrinsicDeflections = LocalSearchAssignment(self, analysisResult)
            deflectionIndices = analysisResult.templatePeakIndices;
            switch self.InitialDeflectionAssignment
                case self.InitialDeflectionAssignments{1}
                    initialPrimaryIndices = true(1, numel(deflectionIndices));
                    fixedPrimaryIndices = false(1, numel(deflectionIndices));
                case self.InitialDeflectionAssignments{2}
                    initialPrimaryIndices = (rand(1, numel(deflectionIndices)) > 0.5);
                    fixedPrimaryIndices = false(1, numel(deflectionIndices));
                case self.InitialDeflectionAssignments{3}
                    initialPrimaryIndices = analysisResult.mostLikeliIndices;
                    fixedPrimaryIndices = analysisResult.mostLikeliIndices;
            end
            deflectionTimes = deflectionIndices / analysisResult.samplingFrequency;
            
            intrinsicDeflections = self.DetermineOptimumDeflectionDistribution(deflectionTimes,...
                initialPrimaryIndices, fixedPrimaryIndices, self.Fit, 1);
        end
        
        function intrinsicDeflections = SmallestIntervalAssignment(self, analysisResult)
            deflectionIndices = analysisResult.templatePeakIndices;
            
            fitFunctionHandle = self.Fit.GetFunctionHandle();
            
            currentDeflections = deflectionIndices;
            improving = true;
            while improving
                deflectionTimes = currentDeflections / analysisResult.samplingFrequency;
                currentIntervals = diff(deflectionTimes);
                currentLikelihoods = fitFunctionHandle(currentIntervals);
                
                [sortedIntervals, arrayIndex] = sort(currentIntervals);
                for sortedIntervalIndex = 1:numel(sortedIntervals)
                    intervalIndex = arrayIndex(sortedIntervalIndex);
                    smallestInterval = currentIntervals(intervalIndex);
                    firstDeflectionIndex = intervalIndex;
                    secondDeflectionIndex = intervalIndex + 1;
                    
                    if firstDeflectionIndex > 1
                        previousInterval = currentIntervals(intervalIndex - 1);
                        newInterval = previousInterval + smallestInterval;
                        firstLikelihoodChange = log(fitFunctionHandle(newInterval)) / 2 - ...
                            sum(log(currentLikelihoods([intervalIndex - 1 intervalIndex]))) / 3;
                    else
                        firstLikelihoodChange = -Inf;   %CHANGE
                    end
                    
                    if secondDeflectionIndex < numel(currentDeflections)
                        nextInterval = currentIntervals(intervalIndex + 1);
                        newInterval = smallestInterval + nextInterval;
                        secondLikelihoodChange = log(fitFunctionHandle(newInterval)) / 2 - ...
                            sum(log(currentLikelihoods([intervalIndex intervalIndex + 1]))) / 3;
                    else
                        secondLikelihoodChange = -Inf;  %CHANGE
                    end
                    
                    [bestLikelihoodChange, bestPosition] = max([firstLikelihoodChange secondLikelihoodChange]);
                    if bestLikelihoodChange > 0
                        improving= true;
                        if bestPosition == 1
                            currentDeflections(firstDeflectionIndex) = [];
                        else
                            currentDeflections(secondDeflectionIndex) = [];
                        end
                        break;
                    else
                        improving = false;
                    end
                end
            end
            
            intrinsicDeflections = ismember(deflectionIndices, currentDeflections);
        end
        
        function intrinsicDeflections = SmallestAmplitudeAssignment(self, analysisResult)
            deflectionIndices = analysisResult.templatePeakIndices;
            deflectionAmplitudes = analysisResult.templatePeakAmplitudes;
            
            fitFunctionHandle = self.Fit.GetFunctionHandle();
            
            currentDeflections = deflectionIndices;
            improving = true;
            while improving
                deflectionTimes = currentDeflections / analysisResult.samplingFrequency;
                currentAmplitudes = deflectionAmplitudes(ismember(deflectionIndices, currentDeflections));
                
                
                currentIntervals = diff(deflectionTimes);
                %                 numberOfIntervals = numel(currentIntervals);
                currentLikelihoods = fitFunctionHandle(currentIntervals);
                %                 currentLikelihood = sum(log(currentLikelihoods)) / numberOfIntervals;
                
                [sortedAmplitudes, arrayIndex] = sort(currentAmplitudes);
                for sortedAmplitudeIndex = 1:numel(sortedAmplitudes)
                    deflectionIndex = arrayIndex(sortedAmplitudeIndex);
                    
                    if deflectionIndex > 1 && deflectionIndex < numel(currentAmplitudes)
                        firstInterval = currentIntervals(deflectionIndex - 1);
                        secondInterval = currentIntervals(deflectionIndex);
                        newInterval = firstInterval + secondInterval;
                        
                        likelihoodChange = log(fitFunctionHandle(newInterval)) / 2 - ...
                            sum(log(currentLikelihoods([deflectionIndex - 1 deflectionIndex]))) / 3;
                    elseif deflectionIndex == 1
                        firstInterval = currentIntervals(deflectionIndex);
                        secondInterval = firstInterval;
                        newInterval = firstInterval + secondInterval;
                        likelihoodChange = log(fitFunctionHandle(newInterval)) / 2 - ...
                            2 * log(currentLikelihoods(deflectionIndex)) / 3;
                    else
                        secondInterval = currentIntervals(deflectionIndex - 1);
                        firstInterval = secondInterval;
                        newInterval = firstInterval + secondInterval;
                        likelihoodChange = log(fitFunctionHandle(newInterval)) / 2 - ...
                            2 * log(currentLikelihoods(deflectionIndex - 1)) / 3;
                    end
                    
                    if likelihoodChange > 0
                        improving = true;
                        currentDeflections(deflectionIndex) = [];
                        break;
                    else
                        improving = false;
                    end
                end
            end
            
            intrinsicDeflections = ismember(deflectionIndices, currentDeflections);
        end
        
        function intrinsicDeflections = AmplitudeSlopeAssignment(self, analysisResult)
            deflectionIndices = analysisResult.templatePeakIndices;
            deflectionAmplitudes = analysisResult.templatePeakAmplitudes;
            deflectionSlopes = deflectionAmplitudes(:) ./ analysisResult.templatePeakDurations(:);
            
            fitFunctionHandle = self.Fit.GetFunctionHandle();
            fitMean = self.Fit.Beta(2);
            
            currentDeflections = deflectionIndices;
            improving = true;
            while improving
                deflectionTimes = currentDeflections / analysisResult.samplingFrequency;
                currentAmplitudes = deflectionAmplitudes(ismember(deflectionIndices, currentDeflections));
                currentSlopes = deflectionSlopes(ismember(deflectionIndices, currentDeflections));
                
                currentIntervals = diff(deflectionTimes);
                %                 numberOfIntervals = numel(currentIntervals);
                currentLikelihoods = fitFunctionHandle(currentIntervals);
                %                 currentLikelihood = sum(log(currentLikelihoods)) / numberOfIntervals;
                
                [sortedAmplitudes, arrayIndex] = sort(currentAmplitudes .* currentSlopes);
                for sortedAmplitudeIndex = 1:numel(sortedAmplitudes)
                    deflectionIndex = arrayIndex(sortedAmplitudeIndex);
                    
                    if deflectionIndex > 1 && deflectionIndex < numel(currentAmplitudes)
                        firstInterval = currentIntervals(deflectionIndex - 1);
                        secondInterval = currentIntervals(deflectionIndex);
                        newInterval = firstInterval + secondInterval;
                        
                        firstIntervalLikelihood = currentLikelihoods(deflectionIndex - 1);
                        secondIntervalLikelihood = currentLikelihoods(deflectionIndex);
                    elseif deflectionIndex == 1
                        firstInterval = currentIntervals(deflectionIndex);
                        secondInterval = firstInterval;
                        newInterval = firstInterval + secondInterval;
                        
                        firstIntervalLikelihood = currentLikelihoods(deflectionIndex);
                        secondIntervalLikelihood = firstIntervalLikelihood;
                    else
                        secondInterval = currentIntervals(deflectionIndex - 1);
                        firstInterval = secondInterval;
                        newInterval = firstInterval + secondInterval;
                        
                        secondIntervalLikelihood = currentLikelihoods(deflectionIndex - 1);
                        firstIntervalLikelihood = secondIntervalLikelihood;
                    end
                    
                    newIntervalLikelihood = fitFunctionHandle(newInterval);
                    likelihoodChange = log(newIntervalLikelihood) / 2 - ...
                        (log(firstIntervalLikelihood) + log(secondIntervalLikelihood)) / 3;
                    
                    if newIntervalLikelihood == 0
                        if (firstIntervalLikelihood == 0 && secondIntervalLikelihood == 0) &&...
                                (firstInterval < fitMean && secondInterval < fitMean)
                            likelihoodChange = 1;
                        end
                    end
                    
                    if likelihoodChange > 0
                        improving = true;
                        currentDeflections(deflectionIndex) = [];
                        break;
                    else
                        improving = false;
                    end
                end
            end
            
            intrinsicDeflections = ismember(deflectionIndices, currentDeflections);
        end
        
        function intrinsicDeflections = ShortestPathAssignment(self, analysisResult)
            persistent figureHandle;
            deflectionIndices = analysisResult.templatePeakIndices;
            deflectionAmplitudes = analysisResult.templatePeakAmplitudes;
            deflectionSlopes = deflectionAmplitudes(:) ./...
                (analysisResult.templatePeakDurations(:) / analysisResult.samplingFrequency);
            deflectionTimes = self.Time(deflectionIndices);
            
            lambda = 1;
            
            deflectionWeights = lambda * (deflectionAmplitudes + deflectionSlopes);
            %             deflectionWeights = deflectionWeights / max(deflectionWeights);
            
            fitMean = self.Fit.Beta(2);
            fitSigma = self.Fit.Beta(3);
            fitFunctionHandle = @(x) exp(-0.5 * ((x - fitMean)./fitSigma).^2) ./ (sqrt(2*pi) .* fitSigma);
            
            intervalProbabilityThreshold = eps;
            
            numberOfDeflections = numel(deflectionIndices);
            connectivityMatrix = sparse(numberOfDeflections + 2, numberOfDeflections + 2);
            % connections from beginning of the signal
            for deflectionIndex = 1:numberOfDeflections
                if deflectionTimes(deflectionIndex) < fitMean
                    intervalProbability = fitFunctionHandle(fitMean);
                else
                    intervalProbability = fitFunctionHandle(deflectionTimes(deflectionIndex));
                end
                if intervalProbability < intervalProbabilityThreshold, break; end
                
                connectivityMatrix(1, deflectionIndex + 1) = -(log(intervalProbability) * (deflectionWeights(deflectionIndex)) / 2);
            end
            
            % connections between deflections
            for deflectionIndex = 1:numberOfDeflections
                deflectionWeight = deflectionWeights(deflectionIndex);
                for nextDeflectionIndex = (deflectionIndex + 1):numberOfDeflections
                    interval = deflectionTimes(nextDeflectionIndex) - deflectionTimes(deflectionIndex);
                    intervalProbability = fitFunctionHandle(interval);
                    if intervalProbability < intervalProbabilityThreshold
                        if interval > fitMean,
                            break;
                        else
                            continue;
                        end
                    end
                    
                    nextDeflectionWeight = deflectionWeights(nextDeflectionIndex);
                    connectivityMatrix(deflectionIndex + 1, nextDeflectionIndex + 1) = -(log(intervalProbability) *...
                        ((deflectionWeight) / 2 + (nextDeflectionWeight) / 2));
                end
            end
            
            % connections to the end of the signal
            for deflectionIndex = numberOfDeflections:-1:1
                if self.Time(end) - deflectionTimes(deflectionIndex) < fitMean
                    intervalProbability = fitFunctionHandle(fitMean);
                else
                    intervalProbability = fitFunctionHandle(self.Time(end) - deflectionTimes(deflectionIndex));
                end
                if intervalProbability < intervalProbabilityThreshold, break; end
                
                connectivityMatrix(deflectionIndex + 1, end) = -(log(intervalProbability) * ((deflectionWeights(deflectionIndex)) / 2));
            end
            
            % shortest path between beginning and end
            [distance, path] = graphshortestpath(connectivityMatrix, 1, numberOfDeflections + 2, 'method', 'Acyclic');
            intrinsicDeflectionIndices = path(2:(end - 1)) - 1;
            
            intrinsicDeflections = false(size(deflectionIndices));
            intrinsicDeflections(intrinsicDeflectionIndices) = true;
            
            visualizeResults = false;
            if visualizeResults
                if isempty(figureHandle)
                    figureHandle = figure('name', 'Shortest path assignment', 'numberTitle', 'off');
                end
                axesHandles = NaN(3, 1);
                
                axesHandles(1) = subplot(3,1,1, 'parent', figureHandle);
                stem(axesHandles(1), deflectionTimes, deflectionAmplitudes);
                hold(axesHandles(1), 'on');
                stem(axesHandles(1), deflectionTimes(intrinsicDeflections), deflectionAmplitudes(intrinsicDeflections), 'r');
                hold(axesHandles(1), 'off');
                title(axesHandles(1), 'Amplitude');
                
                axesHandles(2) = subplot(3,1,2, 'parent', figureHandle);
                stem(axesHandles(2), deflectionTimes, deflectionSlopes);
                hold(axesHandles(2), 'on');
                stem(axesHandles(2), deflectionTimes(intrinsicDeflections), deflectionSlopes(intrinsicDeflections), 'r');
                hold(axesHandles(2), 'off');
                title(axesHandles(2), 'Slope');
                
                axesHandles(3) = subplot(3,1,3, 'parent', figureHandle);
                stem(axesHandles(3), deflectionTimes, deflectionWeights);
                hold(axesHandles(3), 'on');
                stem(axesHandles(3), deflectionTimes(intrinsicDeflections), deflectionWeights(intrinsicDeflections), 'r');
                hold(axesHandles(3), 'off');
                title(axesHandles(3), 'Slope * Amplitude');
                
                linkaxes(axesHandles, 'x');
                pause;
            end
        end
    end
    
    methods (Static)
        function [optimumDistribution, optimumLikelihood] = DetermineOptimumDeflectionDistribution(deflectionTimes,...
                initialDeflections, fixedDeflections, fit, n)
            
            iRandomLikelihoods = zeros(1, n);
            iRandomDeflections = false(n, numel(deflectionTimes));
            
            fitFunctionHandle = fit.GetFunctionHandle();
            numberOfTotalDeflections = numel(initialDeflections);
            
            count = 0;
            for iRandom = 1 : n
                maxIterations = 1000;
                
                currentDeflections = initialDeflections;
                currentDeflectionIndices = find(currentDeflections);
                
                %% start local search algorithm
                currentIntervals = diff(deflectionTimes(currentDeflections));
                numberOfIntervals = numel(currentIntervals);
                currentLikelihoods = fit.YValues(currentIntervals);
                yy = currentLikelihoods(currentLikelihoods ~= 0);
                currentLikelihood = sum(log(yy)) / numel(currentLikelihoods);
                
                while count < maxIterations
                    
                    % Add
                    addCandidates = find(~currentDeflections);
                    addLikelihood = zeros(1, numel(addCandidates));
                    addDeflections = false(numel(addCandidates), numel(currentDeflections));
                    for i = 1:numel(addCandidates)
                        deflections = currentDeflections;
                        deflections(addCandidates(i)) = true;
                        addDeflections(i, :) = deflections;
                        
                        originalIntervalIndex = find(currentDeflectionIndices < addCandidates(i), 1, 'last');
                        if ~isempty(originalIntervalIndex)
                            if originalIntervalIndex <= numberOfIntervals
                                previousDeflectionIndex = currentDeflectionIndices(originalIntervalIndex);
                                nextDeflectionIndex = currentDeflectionIndices(originalIntervalIndex + 1);
                                newInterval1 = deflectionTimes(addCandidates(i)) - deflectionTimes(previousDeflectionIndex);
                                newInterval2 = deflectionTimes(nextDeflectionIndex) - deflectionTimes(addCandidates(i));
                                newLikelihood = (numberOfIntervals * currentLikelihood...
                                    - log(currentLikelihoods(originalIntervalIndex))...
                                    + sum(log(fitFunctionHandle([newInterval1 newInterval2])))) / ...
                                    (numberOfIntervals + 1);
                            else
                                previousDeflectionIndex = currentDeflectionIndices(end);
                                newInterval = deflectionTimes(addCandidates(i)) - deflectionTimes(previousDeflectionIndex);
                                newLikelihood = (numberOfIntervals * currentLikelihood + log(fitFunctionHandle(newInterval)) / (numberOfIntervals + 1));
                            end
                        else
                            nextDeflectionIndex = currentDeflectionIndices(1);
                            newInterval = deflectionTimes(nextDeflectionIndex) - deflectionTimes(addCandidates(i));
                            newLikelihood = (numberOfIntervals * currentLikelihood + log(fitFunctionHandle(newInterval)) / (numberOfIntervals + 1));
                        end
                        
                        addLikelihood(i) = newLikelihood;
                    end
                    bestAddLikelihood = max(addLikelihood);
                    
                    % Remove
                    removalCandidates = currentDeflectionIndices;
                    removeLikelihood = zeros(1, numel(removalCandidates));
                    removeDeflections = false(numel(removalCandidates), numel(currentDeflections));
                    for i = 1:numel(removalCandidates)
                        if fixedDeflections(removalCandidates(i)), continue; end
                        
                        deflections = currentDeflections;
                        deflections(removalCandidates(i)) = false;
                        removeDeflections(i,:) = deflections;
                        
                        secondIntervalIndex = i;
                        if secondIntervalIndex > 1 && secondIntervalIndex <= numberOfIntervals
                            newInterval = currentIntervals(secondIntervalIndex) + currentIntervals(secondIntervalIndex - 1);
                            newLikelihood = (numberOfIntervals * currentLikelihood - ...
                                log(currentLikelihoods(secondIntervalIndex)) - log(currentLikelihoods(secondIntervalIndex - 1)) +...
                                log(fitFunctionHandle(newInterval))) / (numberOfIntervals - 1);
                        elseif secondIntervalIndex == 1
                            newLikelihood = (numberOfIntervals * currentLikelihood - log(currentLikelihoods(secondIntervalIndex))) / (numberOfIntervals - 1);
                        else
                            newLikelihood = (numberOfIntervals * currentLikelihood - log(currentLikelihoods(secondIntervalIndex - 1))) / (numberOfIntervals - 1);
                        end
                        
                        removeLikelihood(i) = newLikelihood;
                        %                             cycles = diff(deflectionTimes(removeDeflections(k,:)));
                        %                             y = fit.YValues(cycles/1e3);
                        %                             yy = y(y~=0);
                        %                             removeLikelihood(k) = sum(log(yy))/numel(y);
                        %                             k = k + 1;
                    end
                    removeLikelihood = removeLikelihood(~fixedDeflections(removalCandidates));
                    removeDeflections = removeDeflections(~fixedDeflections(removalCandidates), :);
                    bestRemoveLikelihood = max(removeLikelihood);
                    
                    % Switch
                    switchCandidates = currentDeflectionIndices;
                    switchedForwardLikelihood = NaN(1, numel(switchCandidates));
                    switchedBackwardLikelihood = switchedForwardLikelihood;
                    switchedForwardDeflections = false(numel(switchCandidates), numel(currentDeflections));
                    switchedBackwardDeflections = switchedForwardDeflections;
                    for i = 1:numel(switchCandidates)
                        if fixedDeflections(switchCandidates(i)), continue; end
                        
                        % Forward
                        deflections = currentDeflections;
                        if (switchCandidates(i) < numberOfTotalDeflections)
                            if ~currentDeflections(switchCandidates(i) + 1)
                                deflections(switchCandidates(i)) = false;
                                deflections(switchCandidates(i) + 1) = true;
                                
                                cycles = diff(deflectionTimes(deflections));
                                y = fitFunctionHandle(cycles);
                                yy = y(y~=0);
                                switchedForwardLikelihood(i) = sum(log(yy)) / numel(y);
                                switchedForwardDeflections(i, :) = deflections;
                            end
                        end
                        
                        % Backward
                        deflections = currentDeflections;
                        if (switchCandidates(i) > 1)
                            if ~currentDeflections(switchCandidates(i) - 1)
                                deflections(switchCandidates(i) - 1) = true;
                                deflections(switchCandidates(i)) = false;
                                
                                cycles = diff(deflectionTimes(deflections));
                                y = fitFunctionHandle(cycles);
                                yy = y(y~=0);
                                switchedBackwardLikelihood(i) = sum(log(yy)) / numel(y);
                                switchedBackwardDeflections(i, :) = deflections;
                            end
                        end
                    end
                    switchedLikelihood = [switchedForwardLikelihood(:); switchedBackwardLikelihood(:)];
                    switchedDeflections = [switchedForwardDeflections; switchedBackwardDeflections];
                    validSwitches = ~isnan(switchedLikelihood);
                    switchedLikelihood = switchedLikelihood(validSwitches);
                    switchedDeflections = switchedDeflections(validSwitches, :);
                    
                    bestSwitchLikelihood = max(switchedLikelihood);
                    
                    % Perform add/remove/switch
                    bestLikelihood = max([bestAddLikelihood bestRemoveLikelihood bestSwitchLikelihood]);
                    if (bestLikelihood > currentLikelihood)
                        if (bestAddLikelihood == bestLikelihood)
                            % add
                            [currentLikelihood, i] = max(addLikelihood);
                            currentDeflections = addDeflections(i,:);
                            %fprintf('Add\r\n');
                        elseif (bestRemoveLikelihood == bestLikelihood)
                            % remove
                            [currentLikelihood, i] = max(removeLikelihood);
                            currentDeflections = removeDeflections(i,:);
                            %fprintf('Remove\r\n');
                        elseif (bestSwitchLikelihood == bestLikelihood)
                            % switch
                            [currentLikelihood, i] = max(switchedLikelihood);
                            currentDeflections = switchedDeflections(i, :);
                            %fprintf('Replace\r\n');
                        end
                    else
                        break;
                    end
                    
                    % update solution
                    currentDeflectionIndices = find(currentDeflections);
                    currentIntervals = diff(deflectionTimes(currentDeflections));
                    numberOfIntervals = numel(currentIntervals);
                    currentLikelihoods = fitFunctionHandle(currentIntervals);
                    
                    count = count + 1;
                end
                disp([num2str(count) 'Iterations performed']);
                iRandomLikelihoods(iRandom) = currentLikelihood;
                iRandomDeflections(iRandom, :) = currentDeflections;
            end
            [optimumLikelihood, iMax] = max(iRandomLikelihoods);
            optimumDistribution = iRandomDeflections(iMax, :);
        end
        
        function cycleLengthFit = ComputeSignalCycleLengthDistribution(ecgData, dfPrior)
            if nargin < 2
                dfPrior = NaN;
            end
            % compute the overall dominant frequency
            samplingFrequency = ecgData.SamplingFrequency; 
            nyquistFrequency = samplingFrequency / 2;
            numberOfChannels = ecgData.GetNumberOfChannels();
            numberOfSamples = ecgData.GetNumberOfSamples();
            
            [bHigh, aHigh] = butter(2, [40, 250] / nyquistFrequency);
            [bLow, aLow] = butter(2, 20 / nyquistFrequency, 'low');
            
            filteredSignals = filtfilt(bHigh, aHigh, ecgData.Data);
            filteredSignals = abs(filteredSignals);
            filteredSignals = [zeros(100, numberOfChannels); filteredSignals; zeros(100, numberOfChannels)];
            filteredSignals = filtfilt(bLow, aLow, filteredSignals);
            filteredSignals = filteredSignals(101:(end-100), :);
            channelDF = NaN(numberOfChannels, 1);
            parfor channelIndex = 1:numberOfChannels
                channelData = filteredSignals(:, channelIndex);
                % compute DF
                nFFT = 2^14;
                [frequencyPower, frequencies] = pwelch(channelData, [], [], nFFT, samplingFrequency);
                [peakValues, peakIndices] = findpeaks(frequencyPower);
                if isnan(dfPrior)
                    minimalAFFrequency = 1;
                    maximalAFFrequency = 20;
                    validDFFrequencies = frequencies >= minimalAFFrequency &...
                        frequencies < maximalAFFrequency;
                    peakDFValues = peakValues(validDFFrequencies(peakIndices));
                    peakDFIndices = peakIndices(validDFFrequencies(peakIndices));
                    [~, maxPosition] = max(peakDFValues);
                else
                    [~, maxPosition] = min(abs(frequencies(peakIndices) - dfPrior));
                    peakDFIndices = peakIndices;
                end
                channelDF(channelIndex) = frequencies(peakDFIndices(maxPosition));
            end
            
            medianAFCL = median(1 ./ channelDF(~isnan(channelDF)));
            sdAFCL = std(1 ./ channelDF(~isnan(channelDF)));
            if sdAFCL < (medianAFCL  / 100)
                sdAFCL = medianAFCL  / 100;
            end
            
            cycleLengthFit = HistogramFitCollection.FitTypeWithParameters(...
                HistogramFitCollection.AvailableFitFunctions{1},...
                [1, medianAFCL, sdAFCL]);
        end
    end
end