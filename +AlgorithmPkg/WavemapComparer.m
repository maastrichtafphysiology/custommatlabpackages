classdef WavemapComparer < handle
    properties (Access = private)
        Waves1
        Waves2
        ElectrodePositions1
        ElectrodePositions2
    end
    
    methods
        function self = WavemapComparer()
            
        end
        
        function SetData(self, waves1, waves2, electrodePositions1, electrodePositions2)
            self.Waves1 = waves1;
            self.Waves2 = waves2;
            self.ElectrodePositions1 = electrodePositions1;
            self.ElectrodePositions2 = electrodePositions2;
        end
        
        function result = Compare(self)
            % number of waves
            result.waves1.numberOfWaves = numel(self.Waves1);
            result.waves2.numberOfWaves = numel(self.Waves2);
            
            % Wave size
            result.waves1.waveSizes = AlgorithmPkg.WavemapComparer.ComputeWaveSize(self.Waves1);
            result.waves2.waveSizes = AlgorithmPkg.WavemapComparer.ComputeWaveSize(self.Waves2);
            
            % Conduction velocity
            result.waves1.waveVelocities = AlgorithmPkg.WavemapComparer.ComputeWaveVelocity(self.Waves1, self.ElectrodePositions1);
            result.waves2.waveVelocities = AlgorithmPkg.WavemapComparer.ComputeWaveVelocity(self.Waves2, self.ElectrodePositions2);
            
            % AFCL
            result.waves1.AFCL = AlgorithmPkg.WavemapComparer.ComputeAFCL(self.Waves1, self.ElectrodePositions1);
            result.waves2.AFCL = AlgorithmPkg.WavemapComparer.ComputeAFCL(self.Waves2, self.ElectrodePositions2);
            
            % (Non-)peripheral waves
            result.waves1.numberOfBT = AlgorithmPkg.WavemapComparer.ComputeNumberOfBT(self.Waves1);
            result.waves2.numberOfBT = AlgorithmPkg.WavemapComparer.ComputeNumberOfBT(self.Waves2);
        end
    end
    
    methods (Static)
        function result = ComputeWaveCharacteristics(waves, positions)
            result.numberOfWaves = numel(waves);
            result.waveSizes = AlgorithmPkg.WavemapComparer.ComputeWaveSize(waves);
            result.waveVelocities = AlgorithmPkg.WavemapComparer.ComputeWaveVelocity(waves, positions);
            result.AFCL = AlgorithmPkg.WavemapComparer.ComputeAFCL(waves, positions);
            result.numberOfBT = AlgorithmPkg.WavemapComparer.ComputeNumberOfBT(waves);
        end
        
        function waveSize = ComputeWaveSize(waveData)
            waveSize = NaN(size(waveData));
            for waveIndex = 1:numel(waveData)
                waveSize(waveIndex) = waveData(waveIndex).Size();
            end
        end
        
        function waveVelocity = ComputeWaveVelocity(waveData, electrodePositions)
            velocityCalculator = AlgorithmPkg.VelocityCalculator();
            
            distanceMatrix = pdist(electrodePositions);
            distanceMatrix = distanceMatrix(distanceMatrix ~= 0);
            minimumSpacing = min(distanceMatrix);
            settings = ApplicationSettings.Instance;
            settings.VelocityComputation.radius = sqrt(2 * (minimumSpacing + 1e3 * eps)^2);
            settings.VelocityComputation.hierarchical = false;
            
            waveVelocity = cell(size(waveData));
            for waveIndex = 1:numel(waveData)
                [xVelocity, yVelocity] = velocityCalculator.ComputeActivationWavefront(...
                    waveData(waveIndex).Members(:, 3),...
                    electrodePositions(waveData(waveIndex).Members(:, 1), :),...
                    1:waveData(waveIndex).Size());
                velocities = sqrt(xVelocity.^2 + yVelocity.^2);
                velocities(isnan(velocities) | isinf(velocities)) = [];
                waveVelocity{waveIndex} = velocities(:);
            end
            
            waveVelocity = vertcat(waveVelocity{:});
        end
        
        function cycleLength = ComputeAFCL(waveData, electrodePositions)
            intervals = [];
            for electrodeIndex = 1:size(electrodePositions)
                electrodeActivations = [];
                for waveIndex = 1:numel(waveData)
                    electrodeMembers = waveData(waveIndex).Members(:, 1) == electrodeIndex;
                    electrodeActivations = [electrodeActivations;...
                        waveData(waveIndex).Members(electrodeMembers, 3)]; %#ok<AGROW>
                end
                electrodeActivations = sort(electrodeActivations);
                intervals = [intervals; diff(electrodeActivations)]; %#ok<AGROW>
            end
            cycleLength = median(intervals);
        end
        
        function numberOfBT = ComputeNumberOfBT(waveData)
            numberOfBT = ~[waveData.Peripheral];
%             numberOfBT = 0;
%             for waveIndex = 1:numel(waveData)
%                 if ~waveData(waveIndex).Peripheral
%                     numberOfBT = numberOfBT + 1;
%                 end
%             end
        end
    end
end