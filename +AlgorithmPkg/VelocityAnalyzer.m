classdef VelocityAnalyzer < handle
    properties
        SupranormalThreshold
    end
 
    properties(Constant)
       ADDITIONAL_INFORMATION_TEMPLATE = struct(...
                'vx', [],...
                'vy', [],...
                'method', '',...
                'ellipse',[],...
                'meanVectorsX',[],...
                'meanVectorsY',[],...
                'xcomponents', [],...
                'ycomponents', [], ...
                'supranormalThreshold', []);

      ADDITIONAL_PREFERENTIALITY_INFORMATION_TEMPLATE = struct(...
                'vx', [],...
                'vy', [],...
                'vxNormalized', [],...
                'vyNormalized', [],...
                'vxFinal', [], ...
                'vyFinal', [], ...
                'normalized', [], ...
                'supranormalThreshold', []);
     
    end
    
    methods
        function self = VelocityAnalyzer()
            self.SupranormalThreshold = Inf;
        end
        
        function preferentialVelocities = ComputePreferentialVelocities(self, electrodeVelocities, bidirectional)
            if nargin < 3
                bidirectional = true;
            end
            
            numberOfElectrodes = numel(electrodeVelocities);
            additionalInformationStruct(numberOfElectrodes, 1) =...
                AlgorithmPkg.VelocityAnalyzer.ADDITIONAL_PREFERENTIALITY_INFORMATION_TEMPLATE;
          
            preferentialVelocities = struct(...
                'angle', NaN(numberOfElectrodes, 1),...
                'value', NaN(numberOfElectrodes, 1),...
                'additionalInformation', additionalInformationStruct);
            
            progressPosition = 0;
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Computing preferentiality', progressPosition, self));
                     
            for electrodeIndex = 1:numberOfElectrodes
                [preferentialAngle, preferentiality] = ...
                    self.ComputeElectrodePreferentialVelocity(electrodeVelocities(electrodeIndex), bidirectional);
                preferentialVelocities.angle(electrodeIndex) = preferentialAngle;
                preferentialVelocities.value(electrodeIndex) = preferentiality;
                preferentialVelocities.additionalInformation(electrodeIndex).vx = electrodeVelocities(electrodeIndex).xVelocity;
                preferentialVelocities.additionalInformation(electrodeIndex).vy = electrodeVelocities(electrodeIndex).yVelocity;
                
%                 progressPosition = electrodeIndex / numberOfElectrodes;
%                 UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
%                     UserInterfacePkg.StatusChangeEventData('Computing preferentiality', progressPosition, self));
            end
            progressPosition = 1;
                UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                    UserInterfacePkg.StatusChangeEventData('', progressPosition, self));
        end
            
        function anisotropyStruct = ComputeElectrodesAnisotropy(self, electrodeVelocities)
            numberOfElectrodes = numel(electrodeVelocities);
            additionalInformationStruct(numberOfElectrodes, 1) =...
                AlgorithmPkg.VelocityAnalyzer.ADDITIONAL_INFORMATION_TEMPLATE;
            
            progressPosition = 0;
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Computing anisotropy', progressPosition, self));
            
            electrodeAngles = NaN(numberOfElectrodes, 1);
            electrodeValues = NaN(numberOfElectrodes, 1);
            electrodeInformation = additionalInformationStruct;
            
            parfor electrodeIndex = 1:numberOfElectrodes
                [angle, index, ellipseParameters] = self.ComputeElectrodeAnisotropy(...
                    electrodeVelocities(electrodeIndex));              
                electrodeAngles(electrodeIndex) = angle;
                electrodeValues(electrodeIndex) = index;
                electrodeInformation(electrodeIndex).vx = electrodeVelocities(electrodeIndex).xVelocity;
                electrodeInformation(electrodeIndex).vy = electrodeVelocities(electrodeIndex).yVelocity;
                electrodeInformation(electrodeIndex).ellipse = ellipseParameters;
            end
            
            anisotropyStruct = struct(...
                'angle', electrodeAngles,...
                'value', electrodeValues,...
                'additionalInformation', electrodeInformation);
            
            progressPosition = 1;
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('', progressPosition, self));
        end
    end
    
    methods (Access = protected)
        function [preferentialAngle, preferentiality] = ComputeElectrodePreferentialVelocity(self, electrodeVelocities, bidirectional)
            if nargin < 3
                bidirectional = true;
            end
           
            vx = electrodeVelocities.xVelocity;
            vy = electrodeVelocities.yVelocity;
            V = sqrt( vx.^2 + vy.^2 );
            validPositions = (~isnan(V) & ~isinf(V) & V < self.SupranormalThreshold);
            
            if any(validPositions)
                angles = atan2(vy(validPositions), vx(validPositions));
                if bidirectional
                    angles = mod(angles(:), pi) * 2;
                    [circularMean, circularVariance] = AlgorithmPkg.VelocityAnalyzer.CircularMeanAndVariance(angles);
                    preferentialAngle = mod(circularMean / 2, pi);
                else
                    [circularMean, circularVariance] = AlgorithmPkg.VelocityAnalyzer.CircularMeanAndVariance(angles);
                    preferentialAngle = circularMean;
                end
                angleVariance = circularVariance;
                preferentiality = (1 - angleVariance);
            else
                preferentialAngle = NaN;
                preferentiality = NaN;
            end
        end
        
        function [anisotropyAngle, anisotropyIndex, ellipseParameters] = ComputeElectrodeAnisotropy(self, electrodeVelocities)
            anisotropyAngle = NaN;
            anisotropyIndex = NaN;
            ellipseParameters = [];

            % compute the anisotropy
            vx = electrodeVelocities.xVelocity;
            vy = electrodeVelocities.yVelocity;
            V = sqrt(vx.^2 + vy.^2);
            validPositions = ~(isnan(V) | isinf(V)) & V <= self.SupranormalThreshold;
            vx = vx(validPositions);
            vy = vy(validPositions);
            
            if ~any(validPositions)
               return; 
            end

            %fit an ellipse directly on to the velocities
            ellipseParameters = AlgorithmPkg.VelocityAnalyzer.FitCentroidEllipse(vx, vy ,...
                'supranormalThreshold', 1.5);
            theta = ellipseParameters(5);
            
            if ellipseParameters(1) >= ellipseParameters(2)
                anisotropyAngle = mod(theta, 2 * pi);
                anisotropyIndex = abs(ellipseParameters(1) / ellipseParameters(2));
            else
                anisotropyAngle = mod(theta + pi / 2, 2 * pi);
                anisotropyIndex = abs(ellipseParameters(2) / ellipseParameters(1));
            end
        end    
    end
    
    methods (Static)
        function CreatePreferentialVelocityPlot(plotHandle, preferentialVelocities, indexRange)
            if nargin < 3
                indexRange = [min(preferentialVelocities.index(:)) max(preferentialVelocities.index(:))];
            end
            preferentialityColors = hot(256);
            mapSize = size(preferentialVelocities.index);
            hold(plotHandle, 'on');
            colorIndices = ceil((preferentialVelocities.index - indexRange(1)) /...
                (indexRange(2) - indexRange(1)) *...
                (size(preferentialityColors, 1)- 1)) + 1;
            colorData = ind2rgb(colorIndices, preferentialityColors);
            surfaceHandle = pcolor(plotHandle, NaN(mapSize + 1));     
            set(surfaceHandle, 'CData', colorData);
            
            
            arrowColor = [0 1 1];
            [xGrid yGrid] = meshgrid(1:mapSize(2), 1:mapSize(1));
            arrows = quiver(plotHandle, xGrid + .5, yGrid + .5, preferentialVelocities.X, preferentialVelocities.Y, .5);
            set(arrows, 'color', arrowColor);
            arrows = quiver(plotHandle, xGrid + .5, yGrid + .5, -preferentialVelocities.X, -preferentialVelocities.Y, .5);
            set(arrows, 'color', arrowColor);
            
            hold(plotHandle, 'off');
            set(plotHandle, 'XTick', (1:mapSize(2)) + .5, 'YTick', (1:mapSize(1)) + .5);
            set(plotHandle, 'XTickLabel', 1:mapSize(2), 'YTickLabel', 1:mapSize(1));
            axis(plotHandle, 'image');
            box(plotHandle, 'on');
        end
        
        function CreateAnisotropyPlot(plotHandle, anisotropyStruct, indexRange, callbackFunction)
            if nargin < 3
                indexRange = [min(anisotropyStruct.index(:)) max(anisotropyStruct.index(:))];
            end
            colors = hot(256);
            mapSize = size(anisotropyStruct.index);
            hold(plotHandle, 'on');
            anisotropyValues = anisotropyStruct.index;
            
            anisotropyValues(anisotropyValues > indexRange(2)) = indexRange(2);
            anisotropyValues(anisotropyValues < indexRange(1)) = indexRange(1);
            
            
            colorIntensities = (anisotropyValues - indexRange(1)) / ...
                abs(indexRange(2) - indexRange(1));
            colorIndices = ceil(colorIntensities * size(colors, 1)) + 1;
            colorData = ind2rgb(colorIndices, colors);
            surfaceHandle = pcolor(plotHandle, NaN(mapSize + 1));     
            set(surfaceHandle, 'CData', colorData);
%             set(surfaceHandle, 'HitTest', 'off');
            if nargin > 3
               set(surfaceHandle, 'ButtonDownFcn', callbackFunction); 
            end
            
            [xGrid, yGrid] = meshgrid(1:mapSize(2), 1:mapSize(1));
            
            arrrowColor = [0 1 1];
            
            [X, Y] = VelocityAnalyzer.angles2Vectors(anisotropyStruct.angle);
            arrows = quiver(plotHandle, xGrid + .5, yGrid + .5, X, Y, .5);
            set(arrows, 'color', arrrowColor);
            arrows = quiver(plotHandle, xGrid + .5, yGrid + .5, -X, -Y, .5);
            set(arrows, 'color', arrrowColor);
            
            hold(plotHandle, 'off');
            set(plotHandle, 'XTick', (1:mapSize(2)) + .5, 'YTick', (1:mapSize(1)) + .5);
            set(plotHandle, 'XTickLabel', 1:mapSize(2), 'YTickLabel', 1:mapSize(1));
            axis(plotHandle, 'image');
            box(plotHandle, 'on');
        end
                            
        function [bestSolution, meanVectorsX, meanVectorsY] = FitCentroidEllipse(X, Y, varargin)
            %FIT_CENTROID_ELLIPSE fits an ellipse to data points (X,Y) centered at the
            %origin (0,0)
            supranormalThreshold = 1.5;
            useConstraints = true;
            useBinning = true;
            numberOfBins = 12;
            numberOfInitialSolutions = 3;
            
            for i = 1:numel(varargin)
                if ~isscalar(varargin{i}) && ~ischar(varargin{i})
                   continue;
                end
                
                switch varargin{i}
                    case 'supranormal'
                        supranormalThreshold = varargin{i+1};
                    case 'supranormalThreshold'
                        supranormalThreshold = varargin{i+1};
                    case 'useConstraints'
                        useConstraints = varargin{i+1};
                    case 'useBinning'
                        useBinning = varargin{i+1};
                    case 'numberOfBins'
                        numberOfBins = varargin{i+1};
                    case 'optimizationDistanceMethod'
                        optimizationDistanceMethod = varargin{i+1};
                end
            end
            
            if isempty(X) || isempty(Y)
               bestSolution = zeros(1, 5);
               meanVectorsX = [];
               meanVectorsY = [];
               return;
            end
            
            if useBinning
                %make histogram
                angleBins = linspace(-pi, pi, numberOfBins + 1);
                [binCount, binMembers] = histc(atan2(Y, X), angleBins);

                meanVectorsX = NaN(numberOfBins, 1);
                meanVectorsY = NaN(numberOfBins, 1);

                for bin = 1:numberOfBins
                    if binCount(bin) > 0
                        meanVectorsX(bin) = sum(X(binMembers == bin)) / binCount(bin);
                        meanVectorsY(bin) = sum(Y(binMembers == bin)) / binCount(bin);        
                    end
                end
                meanVectorsX = meanVectorsX(~isnan(meanVectorsX));
                meanVectorsY = meanVectorsY(~isnan(meanVectorsY));

                X = meanVectorsX;
                Y = meanVectorsY;
            end
            options = optimset('Display', 'off');
            [majorAxes, minorAxes, angles, errors, localMinima] =...
                AlgorithmPkg.VelocityAnalyzer.ComputeAnisotropyLocalMinima(X, Y, supranormalThreshold);
            minimalErrors = errors(localMinima);
            minimalMajorAxes = majorAxes(localMinima);
            minimalMinorAxes = minorAxes(localMinima);
            minimalAngles = angles(localMinima);
            
            [sortedMinimalErrors, sortIndex] = sort(minimalErrors);
            bestSolution = zeros(1, 5);
            bestSolutionError = Inf;
            for localMinimumIndex = 1:(min([numel(sortedMinimalErrors), numberOfInitialSolutions]))
                minPosition = sortIndex(localMinimumIndex);
                sizeEstimate = [minimalMajorAxes(minPosition) minimalMinorAxes(minPosition) minimalAngles(minPosition)];
                
                %optimization bounds
                lowerBounds = [0 0 -Inf];
                upperBounds = [supranormalThreshold supranormalThreshold Inf];
                A = [];
                b = [];
                Aeq = [];
                beq = [];
                
                %algebraic distance
                if useConstraints
                    % estimation
                    options.Algorithm = 'interior-point';
                    [sizeEstimate, resnorm] =...
                        fmincon( ...
                        @(v) AlgorithmPkg.VelocityAnalyzer.ComputeLocalSpaceEllipseDistanceCentered(v, X, Y, 'square-sum', true),...
                        sizeEstimate,...
                        A, b,... %A*x <= b
                        Aeq, beq,... %Aeq*x = beq
                        lowerBounds, upperBounds,... %lb <= x <= ub
                        [],... %nonlcon
                        options);
                else
                    [sizeEstimate, resnorm] =...
                        lsqnonlin( @(v) AlgorithmPkg.VelocityAnalyzer.ComputeLocalSpaceEllipseDistanceCentered(v, X, Y),...
                        sizeEstimate,...
                        [], [], options);
                end
                % correct the rotation angle
                sizeEstimate(3) = mod( sizeEstimate(3), 2*pi );
                if resnorm < bestSolutionError
                    bestSolution = [sizeEstimate(1) sizeEstimate(2) 0 0 sizeEstimate(3)];
                end
            end
        end
        
        function d = computeEllipseDistance(v, X, Y, varargin)
            %COMPUTE_ELLIPSE_DISTANCE computes the distances of points to an ellipse
            %centered at the origin
            a = v(1);
            b = v(2);
            theta = v(3);
            T = [cos(theta) -sin(theta); sin(theta) cos(theta)];
            d = zeros(size(X));
            for i = 1:length(X)
                alpha = atan2(Y(i), X(i)) - theta;
                x = a * cos(alpha);
                y = b * sin(alpha);
                d1 = T * [x; y];
                d2 = [X(i); Y(i)];
                d(i) = norm(d2) - norm(d1);
            end
            
            for i = 1:numel(varargin)
                if ~isscalar(varargin{i}) && ~ischar(varargin{i})
                   continue;
                end
               
                switch varargin{i}
                    case 'square-sum'
                        if varargin{i+1}
                            d = sum(d.^2);
                        end
                end
            end
        end
        
        function d = ComputeAngleEllipseDistance(ellipseData, X, Y)
           %ellipseData has the format [majorAxis minorAxis centerX centerY angle]
           
           %Compute the distance of a point to an ellipse, by computing the
           %point of the ellipse, that has the same angle wrt. the center
           %than the point in question and determining the distance between
           %the two points. Note that this is not the optimal solution.
           
           d = NaN(size(X));
           majorAxis = ellipseData(1);
           minorAxis = ellipseData(2);
           ellipseAngle = ellipseData(5);
               
           for dataIndex = 1:numel(X)
               angleToCenter = atan2(Y(dataIndex) - ellipseData(4), X(dataIndex) - ellipseData(3));
               angleToCenter = mod(angleToCenter + 2 * pi, 2 * pi);
               relativeAngleToCenter = angleToCenter - ellipseAngle;
                        
               ellipseX = ellipseData(3) + ...
                   majorAxis * cos(ellipseAngle) * cos(relativeAngleToCenter) - minorAxis * sin(ellipseAngle) * sin(relativeAngleToCenter);
               ellipseY = ellipseData(4) + ...
                   majorAxis * sin(ellipseAngle) * cos(relativeAngleToCenter) + minorAxis * cos(ellipseAngle) * sin(relativeAngleToCenter);
               
               d(dataIndex) = sqrt((X(dataIndex) - ellipseX)^2 + (Y(dataIndex) - ellipseY)^2);
           end
           
        end
        
        function d = ComputeCenteredAngleEllipseDistance(ellipseData, X, Y)
            %ellipseData has the format [majorAxis minorAxis angle]
            d = VelocityAnalyzer.ComputeAngleEllipseDistance(...
                [ellipseData(1) ellipseData(2) 0 0 ellipseData(3)], X, Y);           
        end
        
        function d = ComputeLocalSpaceEllipseDistance(ellipseData, X, Y, varargin)
           %ellipseData has the format [majorAxis minorAxis centerX centerY
           %angle]
%            d = NaN(size(X));
           majorAxis = ellipseData(1);
           minorAxis = ellipseData(2);
           ellipseAngle = ellipseData(5);
           
           
           %reduce the problem to the case of finding the distance to a
           %centered un-rotated ellipse
           X = X - ellipseData(3);
           Y = Y - ellipseData(4);          
           rotationAngle = - ellipseAngle;
           RotatedX = cos(rotationAngle) * X - sin(rotationAngle) * Y;
           RotatedY = sin(rotationAngle) * X + cos(rotationAngle) * Y;
           
           
%            %algebraic distance
%            A = minorAxis^2;
%            B = 0;
%            C = majorAxis^2;
%            D = 0;
%            E = 0;
%            F = -((majorAxis * minorAxis)^2);
%            
%            d = RotatedX.^2 + RotatedY.^2 * C + F;

          %unitcircle normalized distance
          d = (RotatedX ./ majorAxis).^2 + (RotatedY ./ minorAxis).^2 - 1;
            

   
         
%           d = sqrt((RotatedX ./ majorAxis).^2 + (RotatedY ./ minorAxis).^2 - 1);
            
            
            for i = 1:numel(varargin)
                if ~isscalar(varargin{i}) && ~ischar(varargin{i})
                   continue;
                end
               
                switch varargin{i}
                    case 'square-sum'
                        if varargin{i+1}
                            d = sum(d.^2);
                        end
                end
            end
        end
        
        function d = ComputeLocalSpaceEllipseDistanceCentered(ellipseData, X, Y, varargin)
           %ellipseData has the format [majorAxis minorAxis angle]
           d = AlgorithmPkg.VelocityAnalyzer.ComputeLocalSpaceEllipseDistance(...
               [ellipseData(1) ellipseData(2) 0 0 ellipseData(3)], X, Y);
            for i = 1:numel(varargin)
                switch varargin{i}
                    case 'square-sum'
                        if varargin{i+1}
                            d = sum(d.^2);
                        end
                end
            end
        end
        
        function d = ComputeLocalSpaceEllipseDistanceCenteredFixedAngle(ellipseData, angle, X, Y, varargin)
            %ellipse data has the format [majorAxis minorAxis]
            d = VelocityAnalyzer.ComputeLocalSpaceEllipseDistance(...
                [ellipseData(1) ellipseData(2) 0 0 angle], X, Y);    
            
            for i = 1:numel(varargin)
                if ~isscalar(varargin{i}) && ~ischar(varargin{i})
                   continue;
                end
               
                switch varargin{i}
                    case 'square-sum'
                        if varargin{i+1}
                            d = sum(d.^2);
                        end
                end
            end
        end
        
        function d = ComputeAngleDifference(pointAngle, X, Y, varargin)
           if pointAngle < 0
               pointAngleMirrored = pointAngle + pi;
           else
               pointAngleMirrored = pointAngle - pi;
           end
           
           errorMethod = 'none';
           for i = 1:numel(varargin)
                if ~isscalar(varargin{i}) && ~ischar(varargin{i})
                   continue;
                end
                
               switch varargin{i}
                    case 'errorMethod'
                        errorMethod = varargin{i+1};
               end
           end
           
           velocityAngles = atan2(Y, X);
           
           d1 = velocityAngles - pointAngle;
           %points that are at the opposite of the angle spectrum are
           %actually close
           d1(d1 > pi) = d1(d1 > pi) - 2 * pi;
           d1(d1 < -pi) = d1(d1 < -pi) + 2 * pi;
           d1 = abs(d1);
           
           d2 = velocityAngles - pointAngleMirrored;
           d2(d2 > pi) = d2(d2 > pi) - 2 * pi;
           d2(d2 < -pi) = d2(d2 < -pi) + 2 * pi;
           d2 = abs(d2);
           
           d = min(d1, d2);
           
           switch errorMethod
               case 'none'
                   return;
               case 'rmse'
                   d = sqrt(mean(d.^2));
               case 'least-squares'
                   d = sum(d.^2);
               case 'absolute-differences'
                   d = sum(d);
               case 'mean'
                   d = mean(d);
               otherwise
                   return;
           end
        end
        
        function [X Y] = angles2Vectors(angles, varargin)
            X = ones(size(angles));
            Y = tan(angles);
            
            L = sqrt( X.^2 + Y.^2);
            X = X ./ L;
            Y = Y ./ L;
        end
        
        function [X Y] = ComputeEllipsePoints(majorAxis, minorAxis, ellipseAngle, centerX, centerY, no_points)
           %Computes a set amount of equally distributed points on an ellipse for plotting purposes 
           
           if nargin < 6 
              no_points = 42; 
           end
           
           if nargin < 5
               centerY = 0;
           end
           
           if nargin < 4
               centerX = 0;
           end
           
           if no_points < 1
               return;
           end
           maxAngle = 2*pi;
           angles = 0:maxAngle / (no_points):maxAngle; 
           angles = angles(1:(numel(angles)));
           
          
           X = zeros(1, numel(angles));
           Y = zeros(1, numel(angles));
           
           for i = 1:numel(angles)
               X(i) = centerX + ...
                   majorAxis * cos(ellipseAngle) * cos(angles(i)) - minorAxis * sin(ellipseAngle) * sin(angles(i));
               Y(i) = centerY + ...
                   majorAxis * sin(ellipseAngle) * cos(angles(i)) + minorAxis * cos(ellipseAngle) * sin(angles(i));
           end
        end
        
        function ShowEllipseFitErrorSurface()
            majorAxis = 3;
            minorAxis = 2;
            angle = pi / 4;
            noiseMean = 0;
            noiseStd = 0;
            dataPoints = 32;
            
            [ellipseX ellipseY] = VelocityAnalyzer.ComputeEllipsePoints(majorAxis, minorAxis, angle, 0, 0, dataPoints);
                
            ellipseX = ellipseX + noiseMean + noiseStd * randn(size(ellipseX));
            ellipseY = ellipseY + noiseMean + noiseStd * randn(size(ellipseY));
            
            [MA, MI] = meshgrid(1:.1:5, 1:.1:5);
            
            Z = NaN(size(MA));
            
            errorFunction = 1;
            
            for i = 1:size(Z, 1)
               for j = 1:size(Z, 2)
                    currentMajorAxis = MA(i, j);
                    currentMinorAxis = MI(i, j);
                    
                    switch errorFunction
                        case 1 
                            distances = VelocityAnalyzer.computeEllipseDistance(...
                                [currentMajorAxis currentMinorAxis angle],...
                                ellipseX, ellipseY);
                        case 2 
                            distances = VelocityAnalyzer.ComputeAngleEllipseDistance(...
                                [currentMajorAxis currentMinorAxis 0 0 angle],...
                                ellipseX, ellipseY);
                        case 3
                            distances = VelocityAnalyzer.ComputeLocalSpaceEllipseDistance(...
                                [currentMajorAxis currentMinorAxis 0 0 angle], ellipseX, ellipseY);
                    end


                    
%                     Z(i, j) = sum(distances.^2);
                    Z(i, j) = sqrt(mean(distances.^2));
               end
            end
            
            axesHandle = gca;
            
            surf(axesHandle,...
                MA,MI,Z);
            
            zLim = get(axesHandle, 'ZLim');
            zLim(1) = 0;
            set(axesHandle, 'ZLim', zLim);
            hold on;
            contour(axesHandle,...
                MA,MI,Z,30);
            
            line(...
                'parent', axesHandle,...
                'XData', [majorAxis majorAxis],...
                'YData', [minorAxis minorAxis],...
                'ZData', zLim,...
                'LineStyle', '-',...
                'LineWidth', 2,...
                'Color', [0 1 0]);
            
            minColumns = min(Z);
            [minValue minMajorIndex] = min(minColumns);
            [minValue minMinorIndex] = min(Z(:, minMajorIndex));
                    
%             initialPoint = [MA(minYIndex, minXIndex) MI(minYIndex, minXIndex)];
            optimalMajorAxis = MA(minMinorIndex, minMajorIndex);
            optimalMinorAxis = MI(minMinorIndex, minMajorIndex);
            
            line(...
                'parent', axesHandle,...
                'XData', [optimalMajorAxis optimalMajorAxis],...
                'YData', [optimalMinorAxis optimalMinorAxis],...
                'ZData', zLim,...
                'LineStyle', '-',...
                'LineWidth', 2,...
                'Color', [1 0 0]);
            
            xLabelText = get(axesHandle, 'XLabel');
            set(xLabelText, 'String', 'Major Axis Length');
            yLabelText = get(axesHandle, 'YLabel');
            set(yLabelText, 'String', 'Minor Axis Length');
            zLabelText = get(axesHandle, 'ZLabel');
            set(zLabelText, 'String', 'RMSE');
            
        end
               
        function [X, Y, Z, errors, localMinima] = ComputeAnisotropyLocalMinima(ellipseX, ellipseY, supranormalThreshold)
            majorAxisRange = .2:.2:supranormalThreshold;
            minorAxisRange = .2:.2:supranormalThreshold;
            angleRange = 0:(pi / 4):(pi);
            
            [X Y Z] = meshgrid(majorAxisRange, minorAxisRange, angleRange);
            
            errors = NaN(size(X));
            
            for majorAxisIndex = 1:numel(majorAxisRange)
                for minorAxisIndex = 1:(min(majorAxisIndex, numel(minorAxisRange)))
                   for angleIndex = 1:numel(angleRange)          
                      currentError = ...
                          AlgorithmPkg.VelocityAnalyzer.ComputeLocalSpaceEllipseDistanceCentered(...
                            [ X(minorAxisIndex, majorAxisIndex, angleIndex) ...
                              Y(minorAxisIndex, majorAxisIndex, angleIndex) ...
                              Z(minorAxisIndex, majorAxisIndex, angleIndex)], ...
                            ellipseX, ellipseY,...
                            'square-sum', true);
                        
                      currentError = sqrt(currentError / numel(ellipseX));  
                      errors(minorAxisIndex, majorAxisIndex, angleIndex) = currentError;
                   end
                end
            end
            localMinima = AlgorithmPkg.VelocityAnalyzer.ComputeLocalMinima3D(errors);
        end
        
        function localMinima = ComputeLocalMinima3D(matrix)
            localMinima = false(size(matrix));
            [sizeX, sizeY, sizeZ] = size(matrix);
            edgeMatrix = Inf(size(matrix) + 2);
            
            edgeMatrix(2:(end-1), 2:(end-1), 2:(end-1)) = matrix;
            
            validPoints = ~isnan(matrix);
            for xIndex = 1:sizeX
                for yIndex = 1:sizeY
                    for zIndex = 1:sizeZ
                        if validPoints(xIndex, yIndex, zIndex)
                            neigborhoodValues = edgeMatrix(...
                                xIndex:(xIndex + 2), yIndex:(yIndex + 2), zIndex:(zIndex + 2));
                            neigborhoodValues = neigborhoodValues(~isnan(neigborhoodValues));
                            if all(neigborhoodValues >= matrix(xIndex, yIndex, zIndex))
                                localMinima(xIndex, yIndex, zIndex) = true;
                            end
                        end
                   end
                end
            end

        end
        
        function [differenceMean, differenceVariance, mappedDifferences] = ComputeCircularDifferenceMeanAndVariance(data1, data2, absoluteDifference)
            if nargin < 3
                absoluteDifference = false;
            end
            
            validData = ~(isnan(data1) | isnan(data2));
            if any(validData)
                differences = circ_dist(data2, data1);
                mappedDifferences = differences;
                mappedDifferences(differences > (pi / 2)) = differences(differences > (pi / 2)) - pi;
                mappedDifferences(differences < -(pi / 2)) = differences(differences < -(pi / 2)) + pi;
                
                if absoluteDifference
                    mappedDifferences = abs(mappedDifferences);
                    differenceMean = mean(mappedDifferences);
                    differenceVariance = var(mappedDifferences);
                else
                    differenceMean = AlgorithmPkg.VelocityAnalyzer.CircularMeanAndVariance(2 * mappedDifferences) / 2;
                    [~, differenceVariance] = AlgorithmPkg.VelocityAnalyzer.CircularMeanAndVariance(mappedDifferences);
                end
            else
                differenceMean = NaN;
                differenceVariance = NaN;
            end
        end
        
        function [circularMean, circularVariance] = CircularMeanAndVariance(angles)
            r = sum(exp(1i * angles));
            circularMean = angle(r);
            rLength = abs(r)./ sum(ones(size(angles)));
            circularVariance = 1 - rLength;
        end
    end
end
