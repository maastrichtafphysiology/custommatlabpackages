classdef VelocityCalculator < handle
    properties
        NormalConductionThreshold   % Block conduction threshold
        Method                      % CV computation methods
        FullArray                   % (T/F): all activations or activations exceeding
                                    % minimal normal conduction threshold (NormalConductionThreshold)
        Radius                      % maximum distance from reference electrode in mm
        Hierarchical
        CheckUniquePositions
        UseParallel
    end
    
    methods
        function self = VelocityCalculator()
            self.NormalConductionThreshold = 0.2;
            self.Method = 'Planar';
            self.FullArray = false;
            self.Radius = sqrt(2 * 2.5^2);
            self.Hierarchical = false;
            self.CheckUniquePositions = true;
            self.UseParallel = false;
        end
        
        function [xVelocity, yVelocity] = ComputeActivationWavefront(self, activations, positions, indices)
            % COMPUTEACTIVATIONWAVEFRONT computes 2D velocity vector by fitting a function through the
            % activation times at a (grid of) electrodes:
            % - Planar: a plane
            % - Biquadratic: a quadratic surface
            % - Min_Max: vector from minimum to maximum activation time
            % INPUT:
            % - uses ApplicationSettings.Instance.VelocityComputation
            % settings (set before calling this function):
            % - activations: array of activation times (in ms) for each electrode
            % - positions: electrode positions (2D or 3D)
            % - indices: indices of electrodes for which to compute
            % velocity (all if missing)
            % OUTPUT:
            % - xVelocity & yVelocity: x,y elements of 2D velocity vectors
            
            if nargin < 4
                indices = 1:size(positions, 1);
            end
            
            % retrieve settings from ApplicationSettings singleton
            self.Method = ApplicationSettings.Instance.VelocityComputation.method;
            self.Radius = ApplicationSettings.Instance.VelocityComputation.radius;
            self.FullArray = ApplicationSettings.Instance.VelocityComputation.fullArray;
            self.NormalConductionThreshold = ApplicationSettings.Instance.VelocityComputation.conductionBlock;
            self.Hierarchical = ApplicationSettings.Instance.VelocityComputation.hierarchical;
            
            [xVelocity, yVelocity] = self.Compute2DActivationWavefront(activations, positions, indices);
        end
        
        function [xVelocity, yVelocity] = Compute2DActivationWavefront(self, activations, positions, indices)
            % COMPUTE2DACTIVATIONWAVEFRONT computes 2D velocity vector by fitting a function through the
            % activation times at a (grid of) electrodes:
            % - Planar: a plane
            % - Biquadratic: a quadratic surface
            % - Min_Max: vector from minimum to maximum activation time
            % - activations: array of activation times (in ms) for each electrode
            % - positions: electrode positions (2D or 3D)
            % - indices (optional): indices of electrodes for which to compute
            % velocity (all if missing)
            % OUTPUT:
            % - xVelocity & yVelocity: x,y elements of 2D velocity vectors
            
            if nargin < 4
                indices = 1:size(positions, 1);
            end
            
            switch self.Method
                case 'Planar'
                    fhandle = @self.ComputePlaneVelocity;
                case 'Biquadratic'
                    fhandle = @self.ComputeBiquadraticVelocity;
                case 'Min_Max'
                    fhandle = @self.ComputeMinMaxVelocity;
                otherwise
                    fhandle = @self.ComputePlaneVelocity;
            end
            
            xVelocity = zeros(size(indices));
            yVelocity = xVelocity;
            
            fullArray = self.FullArray;
            radius = self.Radius;
            
            if self.UseParallel
                parfor positionIndex = 1:numel(indices)
                    currentPosition = positions(indices(positionIndex), : );
                    currentTime = activations(indices(positionIndex));
                    positionDifferences = bsxfun(@minus, positions, currentPosition);
                    distances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
                    validPositions = distances <= radius;
                    
                    if fullArray
                        validActivations = ~isnan(activations);
                    else
                        validActivations = self.GetValidActivations(activations, positions, currentPosition, currentTime);
                    end
                    
                    validIndices = validPositions & validActivations;
                    
                    [xVelocity(positionIndex), yVelocity(positionIndex)] =...
                        fhandle(activations(validIndices), positions(validIndices, :));
                end
            else
                for positionIndex = 1:numel(indices)
                    currentPosition = positions(indices(positionIndex), : );
                    currentTime = activations(indices(positionIndex));
                    positionDifferences = bsxfun(@minus, positions, currentPosition);
                    distances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
                    validPositions = distances <= radius;
                    
                    if fullArray
                        validActivations = ~isnan(activations);
                    else
                        validActivations = self.GetValidActivations(activations, positions, currentPosition, currentTime);
                    end
                    
                    validIndices = validPositions & validActivations;
                    
                    [xVelocity(positionIndex), yVelocity(positionIndex)] =...
                        fhandle(activations(validIndices), positions(validIndices, :));
                end
            end
        end
    end
    
    methods (Access = private)
        function [xVelocity, yVelocity] = ComputeMinMaxVelocity(self, activations, positions)
            % COMPUTEMINMAXVELOCITY determine maximum and minimum grid value and return
            % the corresponding direction
            [maxValue, maxIndex] = max(activations);
            maxPosition = positions(maxIndex, :);
            [minValue, minIndex] = min(activations);
            minPosition = positions(minIndex, :);
            
            if isnan(maxValue)
                xVelocity = NaN;
                yVelocity = NaN;
                return;
            end
            
            timeDifference = maxValue - minValue;
            distance = norm((maxPosition - minPosition), 2);
            if 0 == timeDifference || 0 == distance
                xVelocity = NaN;
                yVelocity = NaN;
                return;
            end
            velocity = distance / timeDifference;
            
            distanceVector = maxPosition - minPosition;
            alpha = atan2(distanceVector(1), distanceVector(2));
            xVelocity = cos(alpha) * velocity;
            yVelocity = sin(alpha) * velocity;
        end
        
        function [xVelocity, yVelocity] = ComputePlaneVelocity(self, activations, positions)
            %COMPUTE_PLANE_VELOCITY computes the x- and y-velocity of a certain matrix
            %based on a plane fitted onto the data
            xVelocity = NaN;
            yVelocity = NaN;
            
            rows = numel(activations);
            switch rows
                case {0,1}
                    return;
                case 2
                    if self.Hierarchical
                        [xVelocity, yVelocity] = self.ComputeMinMaxVelocity(activations, positions);
                    end
                otherwise
                    Z = activations;
                    X = zeros(rows, 3);
                    X(:, 1) = 1;
                    X(:, 2:3) = positions(:, 1:2);
                    if 1e+10 < cond(X' * X)
                        if self.Hierarchical
                            [xVelocity, yVelocity] = self.ComputeMinMaxVelocity(activations, positions);
                        end
                        return;
                    end
                    A = X \ Z;
                    r = norm([A(2) A(3)]);
                    if 1e-1 < r
                        alpha = atan2(A(3), A(2));
                        diagonalVelocity = 1 / r;
                        xVelocity = cos(alpha) * diagonalVelocity;
                        yVelocity = sin(alpha) * diagonalVelocity;
                    else
                        xVelocity = Inf;
                        yVelocity = Inf;
                    end
            end
        end
        
        function [xVelocity, yVelocity] = ComputeBiquadraticVelocity(self, activations, positions)
            %COMPUTE_BIQUADRATIC_VELOCITY computes the x- and y-velocity based on a
            %biquadratic plane fitted on the data
            
            xVelocity = NaN;
            yVelocity = NaN;
            
            rows = numel(activations);
            switch rows
                case {0, 1}
                    return;
                case 2
                    if self.Hierarchical
                        [xVelocity, yVelocity] = self.ComputeMinMaxVelocity(activations, positions);
                    end
                case {3, 4, 5}
                    if self.Hierarchical
                        [xVelocity, yVelocity] = self.ComputePlaneVelocity(activations, positions);
                    end
                otherwise
                    Z = activations;
                    X = zeros(rows, 6);
                    X(:, 1) = 1;
                    X(:, 2:3) = positions(:, 1:2);
                    X(:, 4) = positions(:, 1) .* positions(:, 2);
                    X(:, 5) = positions(:, 1).^2;
                    X(:, 6) = positions(:, 2).^2;
                    if 1e+15 < cond(X' * X)
                        if self.Hierarchical
                            [xVelocity, yVelocity] = self.ComputePlaneVelocity(activations, positions);
                        end
                        return;
                    end
                    A = X \ Z;
                    % compute the center of the osculating circle (a, b)
                    dydx = -A(2) / A(3);
                    d2yd2x = -(2 * A(5) + 2 * A(6) * dydx^2) / A(3);
                    a = -dydx * (1 + dydx^2) / d2yd2x;
                    b = (1 + dydx^2) / d2yd2x;
                    % compute the velocity
                    alpha = atan2(b, a);
                    Dxy = 1;
                    x1 = cos(alpha) * Dxy;
                    y1 = sin(alpha) * Dxy;
                    x2 = -cos(alpha) * Dxy;
                    y2 = -sin(alpha) * Dxy;
                    t1 = A(2) * x1 + A(3) * y1 + A(4) * x1 * y1 + A(5) * x1^2 + A(6) * y1^2;
                    t2 = A(2) * x2 + A(3) * y2 + A(4) * x2*y2 + A(5) * x2^2 + A(6) * y2^2;
                    velocity = (2 * Dxy) / (t1 - t2);
                    xVelocity = cos(alpha) * velocity;
                    yVelocity = sin(alpha) * velocity;
            end
        end
        
        function validActivations = GetValidActivations(self, activations, positions, referencePosition, referenceTime)
            positionDifferences = bsxfun(@minus, positions, referencePosition);
            distances = sqrt(...
                positionDifferences(:, 1).^2 +...
                positionDifferences(:, 2).^2 +...
                positionDifferences(:, 3).^2);
            
            activationDifference = activations - referenceTime;
            
            localVelocity = abs(distances ./ activationDifference);
            localVelocity(distances == 0 & activationDifference == 0) = Inf;
            localVelocity(distances == 0 & activationDifference ~= 0) = NaN;
            
            validVelocities = (localVelocity > self.NormalConductionThreshold);
            
            validActivations = validVelocities & ~isnan(activations);
            
            % select unique (closest) activation for each position
            if ~any(validActivations), return; end
            
            if self.CheckUniquePositions
                [uniquePositions, ~, groupIndex] = unique(positions(validActivations, :), 'rows');
                numberOfPositions = numel(find(validActivations));
                numberOfUniquePositions = size(uniquePositions, 1);
                if numberOfUniquePositions == numberOfPositions, return; end
                
                selectedActivationDifference = activationDifference(validActivations);
                selectedPositionIndices = find(validActivations);
                for positionIndex = 1:numberOfUniquePositions
                    currentPositionIndices = find(groupIndex == positionIndex);
                    if numel(currentPositionIndices) > 1
                        validActivations(selectedPositionIndices(currentPositionIndices)) = false;
                        [~, minimalPosition] = min(abs(selectedActivationDifference(currentPositionIndices)));
                        validActivations(selectedPositionIndices(currentPositionIndices(minimalPosition))) = true;
                    end
                end
            end
        end
    end
end