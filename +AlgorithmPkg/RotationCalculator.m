classdef RotationCalculator < handle
    
    methods
        function self = RotationCalculator()
        end
    end
    
    methods (Static)
        % trajectories and rotation
        function [componentData, rotationData, componentSummary, rotationSummary] = ComputeActivationRotation(ecgData, thresholds)
            minimumTrajectoryLength = thresholds.minimumTrajectoryLength;
            minimumTrajectoryAFCLDuration = thresholds.minimumTrajectoryAFCLDuration;
            maximumRotationOverlap = thresholds.maximumRotationOverlap;
            minimalConductionVelocity = thresholds.velocity;
            maximumNumberOfEndPoints = thresholds.maximumNumberOfEndPoints;
            
            componentSummary = struct(...
                'size', [],...
                'startingPoints', [],...
                'numberOfSamples', NaN,...
                'recordingLength', NaN,...
                'AFCL', NaN);
            
            rotationSummary = struct(...
                'scores', [],...
                'length', [],...
                'rotationLength', [],...
                'range', [],...
                'rotationRange', [],...
                'duration', [],...
                'rotationDuration', [],...
                'selfIntersecting', [],...
                'componentID', []);
            
            afcl = cellfun(@(x) mean(diff(x)), ecgData.Activations);
            minimumTrajectoryDuration = round(median(afcl(~isnan(afcl))) * minimumTrajectoryAFCLDuration);
            
            componentSummary.numberOfSamples = ecgData.GetNumberOfSamples();
            componentSummary.AFCL = afcl;
            timeRange = ecgData.GetTimeRange();
            componentSummary.recordingLength = timeRange(end) - timeRange(1);
            
            minimumSpacing = ecgData.ComputeMinimumSpacing();
            neighborRadius = sqrt(2 * (minimumSpacing + 1e3 * eps)^2);
            
            wavemapCalculator = AlgorithmPkg.WavemapCalculator();
            wavemapCalculator.NeighborRadius = neighborRadius;
            wavemapCalculator.ConductionThreshold = minimalConductionVelocity;
            
            disp('Calculating activation components...');
            [components, sources] =...
                wavemapCalculator.DetectActivationComponents(ecgData);
            components = components(vertcat(components.Size) >= minimumTrajectoryLength);
            componentData = components;
            
            numberOfComponents = numel(components);
            rotorTrajectories = cell(numberOfComponents, 1);
            fileStartingPoints = NaN(numel(numberOfComponents), 1);
            componentSummary.size = vertcat(components.Size);
            reverseStr = '';
            for componentIndex = 1:numberOfComponents
                msg = sprintf('Component %d of %d', componentIndex, numberOfComponents);
                fprintf([reverseStr, msg]);
                reverseStr = repmat(sprintf('\b'), 1, length(msg));
                
                fileStartingPoints(componentIndex) = numel(sources{componentIndex});
                radius = sqrt(2 * minimumSpacing^2) + 1e3 * eps;
                component = components(componentIndex);
                if isempty(component.ConnectivityMatrix)
                    component.SetConnectivityMatrix(self.ConductionBlockThreshold, radius);
                end
                
                %                     [trajectories, startPointWaveMembers] =...
                %                         AlgorithmPkg.WavemapAnalyzer.ComputeWaveTrajectories(component, minimumTrajectoryLength, minimumTrajectoryDuration);
                %                     trajectories =...
                %                         AlgorithmPkg.WavemapAnalyzer.ComputeComponentWaveTrajectories(...
                %                         component, minimumTrajectoryLength, minimumTrajectoryDuration, maximumNumberOfEndPoints);
                trajectories =...
                    AlgorithmPkg.WavemapAnalyzer.ComputeComponentMainTrajectories(...
                    component, minimumTrajectoryLength, minimumTrajectoryDuration);
                
                if isempty(trajectories), continue; end
                
                numberOfTrajectories = numel(trajectories);
                componentScores = NaN(numberOfTrajectories, 1);
                trajectoryData = struct(...
                    'electrodeIndices', [],...
                    'activations', [],...
                    'range', [],...
                    'score', [],...
                    'rotationPoints', [],...
                    'rotationRange', [],...
                    'centroid', [],...
                    'selfIntersecting', [],...
                    'componentID', []);
                
                %                     parfor trajectoryIndex = 1:numberOfTrajectories
                for trajectoryIndex = 1:numberOfTrajectories
                    [~, curvature] = AlgorithmPkg.WavemapAnalyzer.ComputeTrajectoryAngle(component, trajectories{trajectoryIndex}, minimumTrajectoryDuration);
                    [componentScores(trajectoryIndex), curvature, rotationPoints, centroid] = AlgorithmPkg.WavemapAnalyzer.ComputeCurvatureScore(curvature);
                    
                    trajectoryMembers = component.Members(trajectories{trajectoryIndex}, :);
                    trajectoryData(trajectoryIndex).electrodeIndices = trajectoryMembers(:, 1);
                    trajectoryData(trajectoryIndex).activations = trajectoryMembers(:, 3);
                    trajectoryData(trajectoryIndex).range = [curvature.time(1), curvature.time(end)];
                    trajectoryData(trajectoryIndex).score = componentScores(trajectoryIndex);
                    trajectoryData(trajectoryIndex).rotationPoints = rotationPoints;
                    trajectoryData(trajectoryIndex).rotationRange = [min(rotationPoints(:, 3)), max(rotationPoints(:, 3))];
                    trajectoryData(trajectoryIndex).centroid = centroid;
                    trajectoryData(trajectoryIndex).selfIntersecting = ~isempty(curvature.intersections);
                    trajectoryData(trajectoryIndex).componentID = component.ID;
                end
                
                rotationRanges = vertcat(trajectoryData.rotationRange);
                selfIntersecting = vertcat(trajectoryData.selfIntersecting);
                %                     rotors = ((abs(componentScores) >= 1) & selfIntersecting) &...
                %                         ((rotationRanges(:, end) - rotationRanges(:, 1)) >= minimumTrajectoryDuration);
                rotors = ((abs(componentScores) >= 1)) &...
                    ((rotationRanges(:, end) - rotationRanges(:, 1)) >= minimumTrajectoryDuration);
                if any(rotors)
                    componentRotors = trajectoryData(rotors);
                    numberOfRotors = numel(componentRotors);
                    rotorTrajectories{componentIndex} = componentRotors;
                    rotationScores = abs(vertcat(componentRotors.score));
                    principalRotors = false(size(componentRotors));
                    
                    rotorLeft = true;
                    remainingRotorIndices = 1:numberOfRotors;
                    while rotorLeft
                        % find rotor with maximum rotation
                        [maxRotorValue, maxRotorPosition] = max(rotationScores(remainingRotorIndices));
                        principalRotors(remainingRotorIndices(maxRotorPosition)) = true;
                        
                        % compute overlap with other rotors
                        maxRotorPoints = componentRotors(remainingRotorIndices(maxRotorPosition)).rotationPoints;
                        maxRotorCentroid = mean(maxRotorPoints);
                        maxRotorRange = [min(maxRotorPoints(:, 3)), max(maxRotorPoints(:, 3))];
                        remainingRotorIndices(maxRotorPosition) = [];
                        remainingRotors = componentRotors(remainingRotorIndices);
                        rotationOverlap = zeros(size(remainingRotorIndices));
                        centroidXYDistance = zeros(size(remainingRotorIndices));
                        centroidOverlap = false(size(remainingRotorIndices));
                        parfor rotorIndex = 1:numel(remainingRotorIndices)
                            remainingRotorPoints = remainingRotors(rotorIndex).rotationPoints;
                            remainingRotorCentroid = mean(remainingRotorPoints);
                            remainingRotorRange = [min(remainingRotorPoints(:, 3)), max(remainingRotorPoints(:, 3))];
                            overlap = ismember(maxRotorPoints, remainingRotorPoints, 'rows');
                            rotationOverlap(rotorIndex) = numel(find(overlap)) /...
                                min([size(maxRotorPoints, 1), size(remainingRotorPoints, 1)]);
                            
                            centroidXYDistance(rotorIndex) = sqrt(sum((maxRotorCentroid([1, 2]) - remainingRotorCentroid([1, 2])).^2));
                            
                            if (remainingRotorCentroid(3) >= maxRotorRange(1) && remainingRotorCentroid(3) <= maxRotorRange(2)) ||...
                                    (maxRotorCentroid(3) >= remainingRotorRange(1) && maxRotorCentroid(3) <= remainingRotorRange(2))
                                centroidOverlap(rotorIndex) = true;
                            end
                        end
                        remainingRotorIndices = remainingRotorIndices(...
                            ~((centroidXYDistance <= 1.5 * neighborRadius & centroidOverlap) | rotationOverlap >= maximumRotationOverlap));
                        
                        if isempty(remainingRotorIndices)
                            rotorLeft = false;
                        end
                    end
                    rotorTrajectories{componentIndex} = componentRotors(principalRotors)';
                end
            end
            componentSummary.startingPoints = fileStartingPoints;
            
            rotationData = vertcat(rotorTrajectories{:});
            if ~isempty(rotationData)
                rotationSummary.scores = vertcat(rotationData.score);
                rotationSummary.length = vertcat(cellfun(@(x) numel(x), {rotationData.electrodeIndices}));
                rotationSummary.rotationLength =  vertcat(cellfun(@(x) size(x, 1), {rotationData.rotationPoints}));
                rotationSummary.range = vertcat(rotationData.range);
                rotationSummary.rotationRange = vertcat(rotationData.rotationRange);
                rotationSummary.duration = rotationSummary.range(:, 2) - rotationSummary.range(:, 1);
                rotationSummary.rotationDuration = rotationSummary.rotationRange(:, 2) - rotationSummary.rotationRange(:, 1);
                rotationSummary.selfIntersecting = vertcat(rotationData.selfIntersecting);
                rotationSummary.componentID = vertcat(rotationData.componentID);
            end
            
            fprintf('\n');
        end
        
        function [components, trajectoryData] = ComputeActivationTrajectories(ecgData, thresholds)
            minimumTrajectoryLength = thresholds.minimumTrajectoryLength;
            minimalConductionVelocity = thresholds.velocity;
            
            minimumSpacing = ecgData.ComputeMinimumSpacing();
            neighborRadius = sqrt(2 * (minimumSpacing + 1e3 * eps)^2);
            
            wavemapCalculator = AlgorithmPkg.WavemapCalculator();
            wavemapCalculator.NeighborRadius = neighborRadius;
            wavemapCalculator.ConductionThreshold = minimalConductionVelocity;
            
            disp('Calculating activation components...');
            [components, sources] =...
                wavemapCalculator.DetectActivationComponents(ecgData);
            components = components(vertcat(components.Size) >= minimumTrajectoryLength);
            
            numberOfComponents = numel(components);
            trajectoryData = cell(numberOfComponents, 1);
            
            fileStartingPoints = NaN(numel(numberOfComponents), 1);
            reverseStr = '';
            for componentIndex = 1:numberOfComponents
                msg = sprintf('Component %d of %d', componentIndex, numberOfComponents);
                fprintf([reverseStr, msg]);
                reverseStr = repmat(sprintf('\b'), 1, length(msg));
                
                fileStartingPoints(componentIndex) = numel(sources{componentIndex});
                radius = sqrt(2 * minimumSpacing^2) + 1e3 * eps;
                component = components(componentIndex);
                if isempty(component.ConnectivityMatrix)
                    component.SetConnectivityMatrix(self.ConductionBlockThreshold, radius);
                end
                
                trajectories = AlgorithmPkg.WavemapAnalyzer.ComputeComponentWaveTrajectories(component, minimumTrajectoryLength);
                
                if isempty(trajectories), continue; end
                
                numberOfTrajectories = numel(trajectories);
                
                componentTrajectoryData = struct(...
                    'electrodeIndices', [],...
                    'activations', [],...
                    'range', [],...
                    'componentID', [],...
                    'componentIndices', []);
                
                for trajectoryIndex = 1:numberOfTrajectories
                    trajectoryMembers = component.Members(trajectories{trajectoryIndex}, :);
                    componentTrajectoryData(trajectoryIndex).electrodeIndices = trajectoryMembers(:, 1);
                    componentTrajectoryData(trajectoryIndex).activations = trajectoryMembers(:, 3);
                    componentTrajectoryData(trajectoryIndex).componentID = component.ID;
                    componentTrajectoryData(trajectoryIndex).componentIndices = trajectories{trajectoryIndex};
                end
                
                trajectoryData{componentIndex} = componentTrajectoryData;
            end
        end
        
        function trajectoryDevianceData = ComputeActivationTrajectoryDeviance(ecgData, thresholds)
            [components, trajectoryData] = AlgorithmPkg.RotationCalculator.ComputeActivationTrajectories(ecgData, thresholds);
            
            electrodePositions = ecgData.ElectrodePositions;
            numberOfComponents = numel(components);
            trajectoryDevianceData = cell(numberOfComponents, 1);
            
            afcl = cellfun(@(x) mean(diff(x)), ecgData.Activations);
            minimumTrajectoryDuration = round(median(afcl(~isnan(afcl))) * thresholds.minimumTrajectoryAFCLDuration);
            nonIntersecting = thresholds.nonIntersecting;
            
            %             parfor componentIndex = 1:numberOfComponents
            for componentIndex = 1:numberOfComponents
                currentComponent = components(componentIndex);
                trajectories = trajectoryData{componentIndex};
                
                numberOfTrajectories = numel(trajectories);
                trajectoryDeviance = NaN(numberOfTrajectories, 1);
                for trajectoryIndex = 1:numberOfTrajectories
                    if nonIntersecting
                        [~, curvature] = AlgorithmPkg.WavemapAnalyzer.ComputeTrajectoryAngle(currentComponent,...
                            trajectories(trajectoryIndex).componentIndices, minimumTrajectoryDuration);
                        if ~isempty(curvature.intersections)
                            continue;   % ignore self-intersecting trajectories
                        end
                    end
                    
                    trajectoryElectrodeIndices = trajectories(trajectoryIndex).electrodeIndices;
                    trajectoryPositions = electrodePositions(trajectoryElectrodeIndices, :);
                    
                    straightLineLength = norm(trajectoryPositions(end, :) - trajectoryPositions(1, :));
                    positionDifferences = trajectoryPositions(2:end, :) - trajectoryPositions(1:(end-1), :);
                    trajectoryLength = sum(sqrt(sum(positionDifferences.^2, 2)));
                    trajectoryDeviance(trajectoryIndex) = trajectoryLength / straightLineLength - 1;
                end
                trajectoryDevianceData{componentIndex} = trajectoryDeviance(~isnan(trajectoryDeviance));
            end
        end
        
        function flowLines = ComputeFlowLines()
        end
        
        % batch functions
        function [componentData, rotationData, componentSummary, rotationSummary, filenames] = ComputeBatchActivationRotation(foldername, thresholds)
            listing = dir(fullfile(foldername, '*_DD.mat'));
            filenames = {listing.name};
            numberOfFiles = numel(listing);
            
            componentData = cell(numberOfFiles, 1);
            rotationData = cell(numberOfFiles, 1);
            componentSummary = cell(numberOfFiles, 1);
            rotationSummary = cell(numberOfFiles, 1);
            
            for fileIndex = 1:numel(listing)
                disp(['ComputeActivationRotation: file ' num2str(fileIndex) ' of ' num2str(numel(listing))]);
                filename = fullfile(foldername, listing(fileIndex).name);
                
                try
                    ddData = load(filename);
                catch ME
                    disp(ME);
                    disp(filename);
                    continue;
                end
                
                [assignedDeflections, reAssigned] = AlgorithmPkg.RotationCalculator.AssignIntrinsicDeflections(ddData);
                if reAssigned
                    % save assigned deflections
                    intrinsicDeflectionResult = assignedDeflections; %#ok<NASGU>
                    save(filename, 'intrinsicDeflectionResult', '-append');
                end
                ddData.ecgData.SetActivations(assignedDeflections);
                
                [fileComponentData, fileRotationData, fileComponentSummary, fileRotationSummary] =...
                    AlgorithmPkg.RotationCalculator.ComputeActivationRotation(ddData.ecgData, thresholds);
                
                componentSummary{fileIndex} = fileComponentSummary;
                rotationSummary{fileIndex} = fileRotationSummary;
                
                componentData{fileIndex} = fileComponentData;
                rotationData{fileIndex} = fileRotationData;
                
                clear ddData assignedDeflections;
            end
        end
        
        function [trajectoryDevianceData, filenames] = ComputeBatchActivationTrajectoryDeviance(foldername, thresholds)
            listing = dir(fullfile(foldername, '*_DD.mat'));
            filenames = {listing.name};
            numberOfFiles = numel(listing);
            
            trajectoryDevianceData = cell(numberOfFiles, 1);
            
            for fileIndex = 1:numberOfFiles
                disp(['ComputeActivationRotation: file ' num2str(fileIndex) ' of ' num2str(numel(listing))]);
                filename = fullfile(foldername, listing(fileIndex).name);
                
                try
                    ddData = load(filename);
                catch ME
                    disp(ME);
                    disp(filename);
                    continue;
                end
                
                [assignedDeflections, reAssigned] = AlgorithmPkg.RotationCalculator.AssignIntrinsicDeflections(ddData);
                if reAssigned
                    % save assigned deflections
                    intrinsicDeflectionResult = assignedDeflections; %#ok<NASGU>
                    save(filename, 'intrinsicDeflectionResult', '-append');
                end
                ddData.ecgData.SetActivations(assignedDeflections);
                
                trajectoryDevianceData{fileIndex} = AlgorithmPkg.RotationCalculator.ComputeActivationTrajectoryDeviance(ddData.ecgData, thresholds);
            end
        end
        
        % helper functions
        function [intrinsicDeflectionData, reAssigned] = AssignIntrinsicDeflections(ddData, recompute)
            if nargin < 2
                recompute = false;
            end
            
            if isfield(ddData, 'intrinsicDeflectionResult') && ~isempty(ddData.intrinsicDeflectionResult) && ~recompute
                intrinsicDeflectionData = ddData.intrinsicDeflectionResult;
                reAssigned = false;
                return;
            end
            
            cycleLengthFit = ddData.deflectionData.Fit;
            deflectionCalculator = AlgorithmPkg.DeflectionAssignmentCalculator(cycleLengthFit);
            deflectionCalculator.Time = ddData.ecgData.GetTimeRange();
            deflectionCalculator.AssignmentAlgorithm = AlgorithmPkg.DeflectionAssignmentCalculator.ASSIGNMENT_ALGORITHMS{4};
            
            templateMatchingAnalysisResult = ddData.deflectionData.AnalysisResults;
            intrinsicDeflectionData = deflectionCalculator.Apply(templateMatchingAnalysisResult);
            reAssigned = true;
        end
    end
end