classdef WavemapAnalyzer < handle
    methods
        function self = WavemapAnalyzer()
        end
    end
    
    methods (Static)
        function result = ComputeWaveCharacteristics(waves, electrodePositions, minimumWaveSize, neighborRadius)
            if nargin < 4
                neighborRadius = sqrt(1.6^2 + 1.6^2);
                if nargin < 3
                    minimumWaveSize = 3;
                end
            end
            waveSizes = AlgorithmPkg.WavemapAnalyzer.ComputeWaveSize(waves);
            validWaves = waveSizes >= minimumWaveSize;
            waves = waves(validWaves);
            
            result.numberOfWaves = numel(waves);
            result.numberOfBT = numel(find(~[waves.Peripheral]));
            result.waveSizes = waveSizes(validWaves);
            [result.waveVelocity, result.waveTortuosity, result.electrodeVelocity, result.electrodeTortuosity] =...
                AlgorithmPkg.WavemapAnalyzer.ComputeWaveDistributions(waves, electrodePositions, neighborRadius);
            [~, result.AFCL] = AlgorithmPkg.WavemapAnalyzer.ComputeAFCL(waves, electrodePositions);
        end
        
        function [waveVelocity, waveTortuosity, electrodeVelocity, electrodeTortuosity] =...
                ComputeWaveDistributions(waves, electrodePositions, neighborRadius)
            
            velocityCalculator = AlgorithmPkg.VelocityCalculator();
            
            computeTortuosity = true;
            if nargout < 2
                computeTortuosity = false;
            end
            
            waveVelocity = cell(size(waves));
            waveTortuosity = cell(size(waves));
            
            if isempty(electrodePositions)
                if isempty(waves)
                    return;
                else
                    electrodePositions = waves(1).ElectrodePositions;
                end
            end
            
            electrodeVelocity(size(electrodePositions, 1), 1) = struct(...
                'xVelocity', [], 'yVelocity', [], 'activationIndex', []);
            electrodeTortuosity = cell(size(electrodePositions, 1), 1);
            
            for waveIndex = 1:numel(waves)
                [xVelocity, yVelocity] = velocityCalculator.ComputeActivationWavefront(...
                    waves(waveIndex).Members(:, 3),...
                    electrodePositions(waves(waveIndex).Members(:, 1), :),...
                    1:waves(waveIndex).Size());
                velocities = sqrt(xVelocity.^2 + yVelocity.^2);
                for memberIndex = 1:waves(waveIndex).Size()
                    electrodeIndex = waves(waveIndex).Members(memberIndex, 1);
                    activationIndex = waves(waveIndex).Members(memberIndex, 2);
                    electrodeVelocity(electrodeIndex).xVelocity = [electrodeVelocity(electrodeIndex).xVelocity;...
                        xVelocity(memberIndex)];
                    electrodeVelocity(electrodeIndex).yVelocity = [electrodeVelocity(electrodeIndex).yVelocity;...
                        yVelocity(memberIndex)];
                    electrodeVelocity(electrodeIndex).activationIndex = [electrodeVelocity(electrodeIndex).activationIndex;...
                        activationIndex];
                end
                
                velocities(isnan(velocities) | isinf(velocities)) = [];
                waveVelocity{waveIndex} = velocities(:);
                
                if computeTortuosity
                    differences = AlgorithmPkg.WavemapAnalyzer.ComputeLocalDirectionDifferences(xVelocity, yVelocity,...
                        electrodePositions(waves(waveIndex).Members(:, 1), :), neighborRadius);
                    waveTortuosity{waveIndex} = differences;
                    for memberIndex = 1:waves(waveIndex).Size()
                        electrodeIndex = waves(waveIndex).Members(memberIndex, 1);
                        electrodeTortuosity{electrodeIndex} = [electrodeTortuosity{electrodeIndex};...
                            differences(memberIndex)];
                    end
                end
            end
            
            waveVelocity = vertcat(waveVelocity{:});
            waveTortuosity = vertcat(waveTortuosity{:});
        end
        
        function directionDifferences = ComputeLocalDirectionDifferences(xVelocity, yVelocity, positions, radius)
            directionDifferences = NaN(size(xVelocity));
            directionDifferences = directionDifferences(:);
            directions = atan2(yVelocity, xVelocity);
            validPositions = ~isnan(directions);
            for positionIndex = 1:size(positions, 1)
                if validPositions(positionIndex)
                    neighbors = AlgorithmPkg.WavemapCalculator.GetElectrodeNeighbors(positions,...
                        xVelocity, positionIndex, radius);
                    
                    differences = (180/pi)* (mod(mod(directions(positionIndex) -...
                        directions(neighbors.positions), 2*pi) + 3*pi, 2*pi) - pi);
                    
                    differences(neighbors.positions == positionIndex) = [];
                    differences(isnan(differences) | isinf(differences)) = [];
                    if ~isempty(differences)
                        directionDifferences(positionIndex) = median(differences);
                    end
                else
                    continue;
                end
            end
        end
        
        function result = ComputeWindowedWaveParameters(waves, electrodePositions, windowLength, windowShift, minimumWaveSize)
            if nargin < 5
                minimumWaveSize = 3;
            end
            
            waveRanges = NaN(numel(waves), 2);
            for waveIndex = 1:numel(waves)
                waveRanges(waveIndex, 1) = min(waves(waveIndex).Members(:, 3));
                waveRanges(waveIndex, 2) = max(waves(waveIndex).Members(:, 3));
            end
            
            waveSizes = AlgorithmPkg.WavemapAnalyzer.ComputeWaveSize(waves);
            largeWaves = waveSizes >= minimumWaveSize;
            
            windowSampleLength = round(windowLength * 1000);
            windowSampleShift = round(windowShift * 1000);
            maxActivation = ceil(max(waveRanges(:, 2)) / windowSampleShift) *  windowSampleShift;
            
            windowStart = 0:windowSampleShift:maxActivation;
            windowEnd = (windowSampleLength):windowSampleShift:maxActivation;
            numberOfWindows = numel(windowEnd);
            windowStart = windowStart(1:numberOfWindows);
            
            resultStruct = struct(...
                'values', NaN(numberOfWindows, 1),...
                'mean', NaN,...
                'median', NaN,...
                'std', NaN);
            
            result = struct(...
                'windowCenter', windowStart + (windowEnd - windowStart) / 2,...
                'numberOfWaves', resultStruct,...
                'waveSize', resultStruct,...
                'numberOfBT', resultStruct,...
                'AFCL', resultStruct);
            
            for windowIndex = 1:numberOfWindows
                validWaves = waveRanges(:, 1) >= windowStart(windowIndex) & waveRanges(:, 2) <= windowEnd(windowIndex);
                validWaves = validWaves & largeWaves(:);
                
                result.AFCL.values(windowIndex)= AlgorithmPkg.WavemapAnalyzer.ComputeAFCL(waves(validWaves), electrodePositions);
                result.numberOfWaves.values(windowIndex) = numel(find(validWaves)) / (1000 * windowLength / result.AFCL.values(windowIndex));
                result.numberOfBT.values(windowIndex) = numel(find(~[waves(validWaves).Peripheral])) / (1000 * windowLength / result.AFCL.values(windowIndex));
                result.waveSize.values(windowIndex) = mean(waveSizes(validWaves));
            end
            
            result.numberOfWaves.mean = mean(result.numberOfWaves.values);
            result.numberOfBT.mean = mean(result.numberOfBT.values);
            result.waveSize.mean = mean(result.waveSize.values);
            result.AFCL.mean = mean(result.AFCL.values);
            
            result.numberOfWaves.median = median(result.numberOfWaves.values);
            result.numberOfBT.median = median(result.numberOfBT.values);
            result.waveSize.median = median(result.waveSize.values);
            result.AFCL.median = median(result.AFCL.values);
            
            result.numberOfWaves.std = std(result.numberOfWaves.values);
            result.numberOfBT.std = std(result.numberOfBT.values);
            result.waveSize.std = std(result.waveSize.values);
            result.AFCL.std = std(result.AFCL.values);
        end
        
        function waveSize = ComputeWaveSize(waves)
            waveSize = NaN(size(waves));
            for waveIndex = 1:numel(waves)
                waveSize(waveIndex) = waves(waveIndex).Size();
            end
        end
        
        function [cycleLength, intervals] = ComputeAFCL(waves, electrodePositions)
            intervals = [];
            cycleLength = [];
            
            if isempty(electrodePositions)
                if isempty(waves)
                    return;
                else
                    electrodePositions = waves(1).ElectrodePositions;
                end
            end
            
            for electrodeIndex = 1:size(electrodePositions)
                electrodeActivations = [];
                for waveIndex = 1:numel(waves)
                    electrodeMembers = waves(waveIndex).Members(:, 1) == electrodeIndex;
                    electrodeActivations = [electrodeActivations;...
                        waves(waveIndex).Members(electrodeMembers, 3)]; %#ok<AGROW>
                end
                electrodeActivations = sort(electrodeActivations);
                intervals = [intervals; diff(electrodeActivations)]; %#ok<AGROW>
            end
            cycleLength = median(intervals);
        end
        
        function result = ComputeWindowedFractionation(deflections, windowLength, windowShift)
            windowSampleLength = round(windowLength * 1000);
            windowSampleShift = round(windowShift * 1000);
            
            maxDeflectionIndex = 1;
            for channelIndex = 1:numel(deflections)
                maxDeflectionIndex = max([maxDeflectionIndex; deflections{channelIndex}.templatePeakIndices]);
            end
            maxActivation = ceil(maxDeflectionIndex / windowSampleShift) *  windowSampleShift;
            
            windowStart = 0:windowSampleShift:maxActivation;
            windowEnd = (windowSampleLength):windowSampleShift:maxActivation;
            numberOfWindows = numel(windowEnd);
            windowStart = windowStart(1:numberOfWindows);
            
            result = struct(...
                'windowCenter', windowStart + (windowEnd - windowStart) / 2,...
                'FI', NaN(numberOfWindows, 1),...
                'ChannelFI', NaN(numel(deflections), numberOfWindows));
            
            for windowIndex = 1:numberOfWindows
                for channelIndex = 1:numel(deflections)
                    deflectionIndices = deflections{channelIndex}.templatePeakIndices;
                    validDeflections = deflectionIndices >= windowStart(windowIndex) & deflectionIndices <= windowEnd(windowIndex);
                    intrinsicDeflections = deflections{channelIndex}.PrimaryDeflections;
                    farFieldDeflections = ~intrinsicDeflections;
                    result.ChannelFI(channelIndex, windowIndex) = numel(find(farFieldDeflections & validDeflections)) /...
                        numel(find(intrinsicDeflections & validDeflections));
                end
                result.FI(windowIndex) = median(result.ChannelFI(:, windowIndex));
            end
        end
        
        function result = ComputeAreaBTRate(dcmData, areas, borderCorrection)
            if nargin < 3
                borderCorrection = false;
            end
            electrodeBTCount = dcmData.ComputeBTCount();
            electrodePositions = dcmData.ElectrodePositions;
            
            numberOfAreas = size(areas, 1);
            result = NaN(numberOfAreas, 1);
            tolerance = 1e3 * eps;
            for areaIndex = 1:numberOfAreas
                area = areas(areaIndex, :);
                validElectrodes = electrodePositions(:, 1) >= (area(1) - tolerance) &...
                    electrodePositions(:, 1) <= (area(1) + area(3) + tolerance) &...
                    electrodePositions(:, 2) >= (area(2) - tolerance) &...
                    electrodePositions(:, 2) <= (area(2) + area(4) + tolerance);
                
                if ~any(validElectrodes), continue; end
                
                if borderCorrection
                    try
                        validElectrodeIndices = find(validElectrodes);
                        edgeElectrodeIndices = convhull(electrodePositions(validElectrodes, 1), electrodePositions(validElectrodes, 2));
                        validElectrodes(validElectrodeIndices(edgeElectrodeIndices)) = false;
                    catch ME
                        disp(ME);
                    end
                end
                
                areaElectrodeBTCount = electrodeBTCount(validElectrodes);
                result(areaIndex) = sum(areaElectrodeBTCount) / numel(find(validElectrodes));
            end
        end
        
        function [entropy, electrodeBTCount, nonUniform, pValue] = ComputeBTEntropy(dcmData)
            edgeElectrodes = dcmData.GetEdgeElectrodesWaveBased();
            electrodeBTCount = dcmData.ComputeBTCount();
            
            electrodeBTCount = electrodeBTCount(~edgeElectrodes);
            totalCount = sum(electrodeBTCount(:));
            pdfValues = electrodeBTCount ./ totalCount;
            logProbabilities = log(pdfValues);
            logProbabilities(pdfValues == 0) = 0;
            entropy = -pdfValues' * logProbabilities;
            
            expectedCounts = (totalCount / numel(electrodeBTCount)) * ones(size(electrodeBTCount));
            [nonUniform, pValue] = chi2gof(1:numel(electrodeBTCount),...
                'frequency', electrodeBTCount,...
                'expected', expectedCounts,...
                'eMin', 0);
        end
        
        function repetition = ComputeBTRepetition(dcmData, radius)
            if nargin < 2
                radius = 0;
            end
            btWaves = dcmData.Waves(~([dcmData.Waves.Peripheral]));
            earliestWaveActivationTime = NaN(numel(btWaves), 1);
            waveStartingElectrodes = cell(numel(btWaves), 1);
            for waveIndex = 1:numel(btWaves)
                startingPoints = btWaves(waveIndex).GetAllStartingPoints();
                earliestWaveActivationTime(waveIndex) = min(startingPoints(:, 3));
                validBTElectrodes = startingPoints(:, 3) == earliestWaveActivationTime(waveIndex);
                waveStartingElectrodes{waveIndex} = startingPoints(validBTElectrodes, 1);
            end
            
            [sortedTimes, sortedOrder] = sort(earliestWaveActivationTime); %#ok<ASGLU>
            waveStartingElectrodes = waveStartingElectrodes(sortedOrder);
            repetition = zeros(numel(btWaves), 1);
            currentRepetition = 1;
            for waveIndex = 1:(numel(btWaves) - 1)
                radiusMembers = dcmData.GetElectrodeNeighbors(waveStartingElectrodes{waveIndex}, radius);
                radiusMembers = find(any(radiusMembers, 2));
                
                if any(ismember(radiusMembers, waveStartingElectrodes{waveIndex + 1}))
                    currentRepetition = currentRepetition + 1;
                else
                    repetition(currentRepetition) = repetition(currentRepetition) + 1;
                    currentRepetition = 1;
                end
            end
            maxRepetition = find(repetition, 1, 'last');
            repetition = repetition(1:maxRepetition);
        end
        
        function [trajectories, startingPointIndices, startPointWaveMembers] = ComputeWaveTrajectories(wave, minimumLength, minimumDuration)
            if nargin < 3
                minimumDuration = 1;
                if nargin < 2
                    minimumLength = 2;
                end
            end
            if ~any(wave.ConnectivityMatrix),
                trajectories = {};
                startPointWaveMembers = false(wave.Size);
                return;
            end
            % determine isolated end regions: reverse wave connectivity graph
            wave.ConnectivityMatrix = wave.ConnectivityMatrix';
            [~, endPointIndices] = wave.ComputeAllStartingPoints();
            endPointconnectivityMatrix = wave.ConnectivityMatrix(endPointIndices, endPointIndices);
%             [numberOfEndPoints, pointEndIndex] = graphconncomp(endPointconnectivityMatrix, 'weak', true);
            % change for > R2022a
            [pointEndIndex, componentSize] = conncomp(digraph(endPointconnectivityMatrix), 'Type', 'weak');
            numberOfEndPoints = numel(componentSize);
            %
            
            mergedEndPointIndices = cell(numberOfEndPoints, 1);
            for pointIndex = 1:numberOfEndPoints
                endPointMembers = pointEndIndex == pointIndex;
                mergedEndPointIndices{pointIndex} = endPointIndices(endPointMembers);
            end
            endPointIndices = mergedEndPointIndices;
            wave.ConnectivityMatrix = wave.ConnectivityMatrix';
            
            % determine isolated starting regions
            [~, startingPointIndices] = wave.ComputeAllStartingPoints();
            startPointconnectivityMatrix = wave.ConnectivityMatrix(startingPointIndices, startingPointIndices);
%             [numberOfStartingPoints, pointStartIndex] = graphconncomp(startPointconnectivityMatrix, 'weak', true);
            % change for > R2022a            
            [pointStartIndex, componentSize] = conncomp(digraph(startPointconnectivityMatrix), 'Type', 'weak');
            numberOfStartingPoints = numel(componentSize);
            %
            
            mergedStartingPointIndices = cell(numberOfStartingPoints, 1);
            for pointIndex = 1:numberOfStartingPoints
                startPointMembers = pointStartIndex == pointIndex;
                mergedStartingPointIndices{pointIndex} = startingPointIndices(startPointMembers);
            end
            startingPointIndices = mergedStartingPointIndices;
            
            if nargout > 2
                % determine reachable activations for each starting point
                startPointWaveMembers = false(wave.Size, numberOfStartingPoints);
                for pointIndex = 1:numberOfStartingPoints
                    memberIndices = graphtraverse(wave.ConnectivityMatrix, startingPointIndices{pointIndex}(1));
                    startPointWaveMembers(memberIndices, pointIndex) = true;
                    startPointWaveMembers(startingPointIndices{pointIndex}, pointIndex) = false;
                end
            end
            
            % compute shortest paths from all starting regions to all end regions
            trajectories = cell(numberOfStartingPoints, numberOfEndPoints);
            conductionMatrix = wave.LocalConduction();
            graphWeights = 1 ./ conductionMatrix(wave.ConnectivityMatrix) + eps;
            
            parfor startIndex = 1:numberOfStartingPoints
                currentStartIndices = startingPointIndices{startIndex};
                for endIndex = 1:numberOfEndPoints
                    currentEndPointIndices = endPointIndices{endIndex};
                    currentPaths = cell(numel(currentStartIndices), numel(currentEndPointIndices));
                    startTime = wave.Members(currentStartIndices(1), 3);
                    endTime = wave.Members(currentEndPointIndices(1), 3);
                    disconnectedPoints = false;
                    if (endTime - startTime) >= minimumDuration
                        for currentStartIndex = 1:numel(currentStartIndices)
                            for currentEndIndex = 1:numel(currentEndPointIndices)
                                [distance, path] = graphshortestpath(wave.ConnectivityMatrix,...
                                    currentStartIndices(currentStartIndex),...
                                    currentEndPointIndices(currentEndIndex),...
                                    'weights', graphWeights);
                                
                                if isinf(distance),
                                    disconnectedPoints = true;
                                    break;
                                end
                                currentPaths{currentStartIndex, currentEndIndex} = path;
                            end
                            if disconnectedPoints
                                break;
                            end
                        end
                    end
                    pathLengths = cellfun(@numel, currentPaths);
                    [~, minPosition] = min(pathLengths(:));
                    trajectories(startIndex, endIndex) = currentPaths(minPosition);
                end
            end
            trajectoryLength = cellfun(@numel, trajectories);
            validTrajectories = trajectoryLength >= minimumLength;
            trajectories = trajectories(validTrajectories);
        end
        
        function [trajectories, startingPointIndices, startPointWaveMembers] = ComputeComponentWaveTrajectories(...
                component, minimumLength, minimumDuration, maximumNumberOfEndPoints)
            if nargin < 4
                maximumNumberOfEndPoints = Inf;
                if nargin < 3
                    minimumDuration = 1;
                    if nargin < 2
                        minimumLength = 2;
                    end
                end
            end
            
            if ~any(component.ConnectivityMatrix)
                trajectories = {};
                startPointWaveMembers = false(component.Size);
                return;
            end
            % determine isolated end regions: reverse component connectivity graph
            component.ConnectivityMatrix = component.ConnectivityMatrix';
            [endPoints, endPointIndices] = component.ComputeAllStartingPoints();
            endTimes = endPoints(:, 3);
            endPointconnectivityMatrix = component.ConnectivityMatrix(endPointIndices, endPointIndices);
%             [numberOfEndPoints, pointEndIndex] = graphconncomp(endPointconnectivityMatrix, 'weak', true);
            % change for > R2022a            
            [pointEndIndex, componentSize] = conncomp(digraph(endPointconnectivityMatrix), 'Type', 'weak');
            numberOfEndPoints = numel(componentSize);
            %

            mergedEndPointIndices = cell(numberOfEndPoints, 1);
            mergedEndTimes = NaN(numberOfEndPoints, 1);
            for pointIndex = 1:numberOfEndPoints
                endPointMembers = pointEndIndex == pointIndex;
                mergedEndPointIndices{pointIndex} = endPointIndices(endPointMembers);
                mergedEndTimes(pointIndex) = min(endTimes(endPointMembers));
            end
            component.ConnectivityMatrix = component.ConnectivityMatrix';
            
            % determine isolated starting regions
            [startingPoints, startingPointIndices] = component.ComputeAllStartingPoints();
            startTimes = startingPoints(:, 3);
            startPointconnectivityMatrix = component.ConnectivityMatrix(startingPointIndices, startingPointIndices);
%             [numberOfStartingPoints, pointStartIndex] = graphconncomp(startPointconnectivityMatrix, 'weak', true);
            % change for > R2022a            
            [pointStartIndex, componentSize] = conncomp(digraph(startPointconnectivityMatrix), 'Type', 'weak');
            numberOfStartingPoints = numel(componentSize);
            %

            mergedStartingPointIndices = cell(numberOfStartingPoints, 1);
            mergedStartTimes = NaN(numberOfStartingPoints, 1);
            for pointIndex = 1:numberOfStartingPoints
                startPointMembers = pointStartIndex == pointIndex;
                mergedStartingPointIndices{pointIndex} = startingPointIndices(startPointMembers);
                mergedStartTimes(pointIndex) = min(startTimes(startPointMembers));
            end
            startingPointIndices = mergedStartingPointIndices;
            
            if nargout > 2
                % determine reachable activations for each starting point
                startPointWaveMembers = false(component.Size, numberOfStartingPoints);
                for pointIndex = 1:numberOfStartingPoints
                    memberIndices = graphtraverse(component.ConnectivityMatrix, startingPointIndices{pointIndex}(1));
                    startPointWaveMembers(memberIndices, pointIndex) = true;
                    startPointWaveMembers(startingPointIndices{pointIndex}, pointIndex) = false;
                end
            end
            
            % compute shortest paths from all starting regions to all end regions
            trajectories = cell(numberOfStartingPoints, 1);
            reverseStr = '';
            for startIndex = 1:numberOfStartingPoints
                msg = sprintf(' -- Starting point %d of %d', startIndex, numberOfStartingPoints);
                fprintf([reverseStr, msg]);
                reverseStr = repmat(sprintf('\b'), 1, length(msg));
                    
                currentStartIndices = startingPointIndices{startIndex};
                startTime = mergedStartTimes(startIndex);
                memberIndices = graphtraverse(component.ConnectivityMatrix, startingPointIndices{startIndex}(1));
                
                trajectoryDurations = endTimes - startTime;
                endPointMembers = ismember(endPointIndices, memberIndices) & trajectoryDurations >= minimumDuration;
                endPointMemberGroupIndex = pointEndIndex(endPointMembers);
                mergedEndPointGroupIndices = unique(endPointMemberGroupIndex);
                numberOfCurrentEndPoints = numel(mergedEndPointGroupIndices);
                
                if numberOfCurrentEndPoints == 0 
                    trajectories{startIndex} = cell(0, 1); 
                    continue;
                end
                
                % select only N longest trajectories (maximumNumberOfEndPoints)
                if ~(isinf(maximumNumberOfEndPoints) || numberOfCurrentEndPoints <= maximumNumberOfEndPoints)
                    endPointMemberDurations = mergedEndTimes(mergedEndPointGroupIndices) - startTime;
                    [~, sortedIndex] = sort(endPointMemberDurations, 'descend');
                    mergedEndPointGroupIndices = mergedEndPointGroupIndices(sortedIndex(1:maximumNumberOfEndPoints));
                    numberOfCurrentEndPoints = maximumNumberOfEndPoints;
                end
                startPointTrajectories = cell(numberOfCurrentEndPoints, 1);
                
                % create a temporal wave from the current starting point
                waveConnectivityMatrix = component.ConnectivityMatrix(memberIndices, memberIndices);
                startingPointWave = AlgorithmPkg.Wave(startIndex, component.ElectrodePositions);
                startingPointWave.Members = component.Members(memberIndices, :);
                startingPointWave.ConnectivityMatrix = waveConnectivityMatrix;
                waveConductionMatrix = startingPointWave.LocalConduction();
                waveGraphWeights = 1 ./ waveConductionMatrix(waveConnectivityMatrix) + eps;
                
                
                parfor endIndex = 1:numberOfCurrentEndPoints
                    currentEndPointIndices = mergedEndPointIndices{mergedEndPointGroupIndices(endIndex)};
                    currentPaths = cell(numel(currentStartIndices), numel(currentEndPointIndices));
                    
                    for currentStartIndex = 1:numel(currentStartIndices)
                        waveStartIndex = find(currentStartIndices(currentStartIndex) == memberIndices);
                        for currentEndIndex = 1:numel(currentEndPointIndices)
                            waveEndIndex = find(currentEndPointIndices(currentEndIndex) == memberIndices);
                            [~, path] = graphshortestpath(waveConnectivityMatrix,...
                                waveStartIndex,...
                                waveEndIndex,...
                                'weights', waveGraphWeights);
                            
                            currentPaths{currentStartIndex, currentEndIndex} = memberIndices(path);
                        end
                    end
                    pathLengths = cellfun(@numel, currentPaths);
                    [~, minPosition] = min(pathLengths(:));
                    startPointTrajectories{endIndex} = currentPaths{minPosition};
                end
                trajectories{startIndex} = startPointTrajectories;
            end
            trajectories = cellfun(@(x) x(cellfun(@numel, x) >= minimumLength), trajectories, 'uniformOutput', false);
            trajectories = vertcat(trajectories{:});
            
            fprintf(reverseStr);
        end
        
        function trajectories = ComputeComponentMainTrajectories(...
                component, minimumLength, minimumDuration)
            if nargin < 3
                minimumDuration = 1;
                if nargin < 2
                    minimumLength = 2;
                end
            end
            
            if ~any(component.ConnectivityMatrix)
                trajectories = {};
                return;
            end
            
            % determine isolated end regions: reverse component connectivity graph
            component.ConnectivityMatrix = component.ConnectivityMatrix';
            [endPoints, endPointIndices] = component.ComputeAllStartingPoints();
            endTimes = endPoints(:, 3);
            endPointconnectivityMatrix = component.ConnectivityMatrix(endPointIndices, endPointIndices);
%             [numberOfEndPoints, pointEndIndex] = graphconncomp(endPointconnectivityMatrix, 'weak', true);
            % change for > R2022a    
            [pointEndIndex, componentSize] = conncomp(digraph(endPointconnectivityMatrix), 'Type', 'weak');
            numberOfEndPoints = numel(componentSize);
            %

            mergedEndPointIndices = cell(numberOfEndPoints, 1);
            mergedEndTimes = NaN(numberOfEndPoints, 1);
            for pointIndex = 1:numberOfEndPoints
                endPointMembers = pointEndIndex == pointIndex;
                mergedEndPointIndices{pointIndex} = endPointIndices(endPointMembers);
                mergedEndTimes(pointIndex) = min(endTimes(endPointMembers));
            end
            component.ConnectivityMatrix = component.ConnectivityMatrix';
            
            % determine isolated starting regions
            [startingPoints, startingPointIndices] = component.ComputeAllStartingPoints();
            startTimes = startingPoints(:, 3);
            [~, sortedIndex] = sort(startTimes, 'ascend');
            startTimes = startTimes(sortedIndex);
            startingPointIndices = startingPointIndices(sortedIndex);
            startPointconnectivityMatrix = component.ConnectivityMatrix(startingPointIndices, startingPointIndices);
%             [numberOfStartingPoints, pointStartIndex] = graphconncomp(startPointconnectivityMatrix, 'weak', true);
            % change for > R2022a 
            [pointStartIndex, componentSize] = conncomp(digraph(startPointconnectivityMatrix), 'Type', 'weak');
            numberOfStartingPoints = numel(componentSize);
            %
            
            mergedStartingPointIndices = cell(numberOfStartingPoints, 1);
            mergedStartTimes = NaN(numberOfStartingPoints, 1);
            for pointIndex = 1:numberOfStartingPoints
                startPointMembers = pointStartIndex == pointIndex;
                mergedStartingPointIndices{pointIndex} = startingPointIndices(startPointMembers);
                mergedStartTimes(pointIndex) = min(startTimes(startPointMembers));
            end
            
            % Search procedure:
            % 1. connect the first starting point to the last reachable end point
            % 2. remove all starting points that are reachable from this end
            % point
            % 3. go to 1) and repeat until there are no starting points left
            
            visitedStartingPoints = false(size(mergedStartingPointIndices));
            visitedEndPoints = false(size(mergedEndPointIndices));
            candidateStartingPointIndices = mergedStartingPointIndices;
            trajectories = cell(numberOfStartingPoints, 1);
            currentTrajectoryIndex = 0;
            reverseStr = '';
            nextStartIndex = 1;
            while ~isempty(nextStartIndex)
                startIndex = nextStartIndex;
                
                msg = sprintf(' -- Starting point %d of %d', startIndex, numberOfStartingPoints);
                fprintf([reverseStr, msg]);
                reverseStr = repmat(sprintf('\b'), 1, length(msg));
                
                currentStartIndices = mergedStartingPointIndices{startIndex};
                startTime = mergedStartTimes(startIndex);
                memberIndices = graphtraverse(component.ConnectivityMatrix, mergedStartingPointIndices{startIndex}(1));
                
                trajectoryDurations = endTimes - startTime;
                endPointMembers = ismember(endPointIndices, memberIndices) & trajectoryDurations >= minimumDuration;
                endPointMemberGroupIndex = pointEndIndex(endPointMembers);
                mergedEndPointGroupIndices = unique(endPointMemberGroupIndex);
                mergedEndPointGroupIndices = mergedEndPointGroupIndices(~visitedEndPoints(mergedEndPointGroupIndices));
                numberOfCurrentEndPoints = numel(mergedEndPointGroupIndices);
                
                if numberOfCurrentEndPoints == 0 
                    visitedStartingPoints(startIndex) = true;
                    nextStartIndex = find(~visitedStartingPoints, 1, 'first');
                    continue;
                else
                    currentTrajectoryIndex = currentTrajectoryIndex + 1;
                end
                
                % select only the endpoint with the longest trajectory (in
                % time)
                endPointMemberDurations = mergedEndTimes(mergedEndPointGroupIndices) - startTime;
                [~, maxDurationIndex] = max(endPointMemberDurations);
                mergedEndPointGroupIndex = mergedEndPointGroupIndices(maxDurationIndex);
                
                % mark all end points as visited
                visitedEndPoints(mergedEndPointGroupIndices) = true;
                
                % mark all starting point that can reach this end point as
                % visited
                endPointmemberIndices = graphtraverse(component.ConnectivityMatrix', mergedEndPointIndices{mergedEndPointGroupIndex}(1));
                startingPointMembers = ismember(startingPointIndices, endPointmemberIndices);
                startPointMemberGroupIndex = pointStartIndex(startingPointMembers);
                mergedStartingPointGroupIndices = unique(startPointMemberGroupIndex);
                visitedStartingPoints(mergedStartingPointGroupIndices) = true;
                
                % create a temporal wave from the current starting point to
                % determine the shortest path to the end point
                waveConnectivityMatrix = component.ConnectivityMatrix(memberIndices, memberIndices);
                startingPointWave = AlgorithmPkg.Wave(startIndex, component.ElectrodePositions);
                startingPointWave.Members = component.Members(memberIndices, :);
                startingPointWave.ConnectivityMatrix = waveConnectivityMatrix;
                waveConductionMatrix = startingPointWave.LocalConduction();
                waveGraphWeights = 1 ./ waveConductionMatrix(waveConnectivityMatrix) + eps;
                
                currentEndPointIndices = mergedEndPointIndices{mergedEndPointGroupIndex};
                currentPaths = cell(numel(currentStartIndices), numel(currentEndPointIndices));
                
                for currentStartIndex = 1:numel(currentStartIndices)
                    waveStartIndex = find(currentStartIndices(currentStartIndex) == memberIndices);
                    for currentEndIndex = 1:numel(currentEndPointIndices)
                        waveEndIndex = find(currentEndPointIndices(currentEndIndex) == memberIndices);
                        [~, path] = graphshortestpath(waveConnectivityMatrix,...
                            waveStartIndex,...
                            waveEndIndex,...
                            'weights', waveGraphWeights);
                        
                        currentPaths{currentStartIndex, currentEndIndex} = memberIndices(path);
                    end
                end
                pathLengths = cellfun(@numel, currentPaths);
                [~, minPosition] = min(pathLengths(:));
                trajectories{currentTrajectoryIndex} = currentPaths(minPosition);
                
                nextStartIndex = find(~visitedStartingPoints, 1, 'first');
            end
            trajectories = trajectories(1:currentTrajectoryIndex);
            trajectories = cellfun(@(x) x(cellfun(@numel, x) >= minimumLength), trajectories, 'uniformOutput', false);
            trajectories = vertcat(trajectories{:});
            
            fprintf(reverseStr);
        end
        
        function length = ComputeTrajectoryLength(trajectoryPositions)            
            positionDifferences = trajectoryPositions(2:end, :) - trajectoryPositions(1:(end-1), :);
            length = sum(sqrt(sum(positionDifferences.^2, 2)));
        end
        
        function [splineFit, curvature] = ComputeTrajectoryCurvature(wave, trajectory)
            trajectoryData = wave.Members(trajectory, :);
            electrodePositions = wave.ElectrodePositions(trajectoryData(:, 1), :);
            activations = trajectoryData(:, 3);
            
            % find and average double points (equal values of activations)
            [uniqueZValues indexZ, indexUnique] = unique(activations);
            uniqueX = NaN(numel(uniqueZValues), 1);
            uniqueY = NaN(numel(uniqueZValues), 1);
            uniqueZ = NaN(numel(uniqueZValues), 1);
            
            for valueIndex = 1:numel(uniqueZValues)
                points = (indexUnique == valueIndex);
                
                uniqueX(valueIndex) = mean(electrodePositions(points, 2));
                uniqueY(valueIndex) = mean(electrodePositions(points, 1));
                uniqueZ(valueIndex) = mean(activations(points));
            end
            uniqueXYZ = [uniqueX, uniqueY, uniqueZ];
            splineFit = cscvn(uniqueXYZ');
            interpolatedValues = linspace(splineFit.breaks(1), splineFit.breaks(end), 100);
            splineValues = fnval(splineFit, interpolatedValues);
            
            splineDerivative = fnder(splineFit);
            interpolatedValues = linspace(splineDerivative.breaks(1), splineDerivative.breaks(end), 100);
            derivatives = fnval(splineDerivative, interpolatedValues);
            angles = atan2(derivatives(2, :), derivatives(1, :));
            
            curvature = struct(...
                'time', splineValues(3, :),...
                'breaks', splineDerivative.breaks,...
                'angles', angles);
        end
        
        function [points, curvature] = ComputeTrajectoryAngle(wave, trajectory, minimumIntersectionInterval)
            trajectoryData = wave.Members(trajectory, :);
            electrodePositions = wave.ElectrodePositions(trajectoryData(:, 1), :);
            activations = trajectoryData(:, 3);
            
            % find and average double points (equal values of activations)
            [uniqueZValues, indexZ, indexUnique] = unique(activations);
            %             uniqueX = NaN(numel(uniqueZValues), 1);
            %             uniqueY = NaN(numel(uniqueZValues), 1);
            uniqueXY = NaN(numel(uniqueZValues), 2);
            
            numberOfPoints = numel(uniqueZValues);
            uniqueZ = uniqueZValues(:);
            for valueIndex = 1:numberOfPoints
                points = (indexUnique == valueIndex);
                
                if numel(find(points)) > 1
                    uniqueXY(valueIndex, :) = mean(electrodePositions(points, [2, 1]), 1);
                    %                 uniqueX(valueIndex) = mean(electrodePositions(points, 2));
                    %                 uniqueY(valueIndex) = mean(electrodePositions(points, 1));
                else
                    uniqueXY(valueIndex, :) = electrodePositions(points, [2, 1]);
                end
            end
            %             points = [uniqueX, uniqueY, uniqueZ]';
            %             angles = atan2(-diff(uniqueX), diff(uniqueY));
            points = [uniqueXY, uniqueZ]';
            
            % remove self-intersections with short duration
            intervalFound = true;
            while intervalFound
                [x0, y0, iout, jout] = AlgorithmPkg.WavemapAnalyzer.ComputeSelfIntersections(points(1, :)', points(2, :)');
                validPoints = ~(isnan(iout) |isnan(jout));
                x0 = x0(validPoints);
                y0 = y0(validPoints);
                iout = iout(validPoints);
                jout = jout(validPoints);
                if isempty(x0)
                    intervalFound = false;
                else
%                     disp([x0, y0]);
                    startIndex = floor(iout);
                    endIndex = floor(jout);
                    dT = [diff(points(3, :)), 0];
                    
                    startTime = points(3, startIndex) + dT(startIndex) .* (iout - startIndex)';
                    endTime = points(3, endIndex) + dT(endIndex) .* (jout - endIndex)';
                    timeDifference = endTime - startTime;
                    [minimalTimeDifferences, minimalIndex] = min(timeDifference);
                    if minimalTimeDifferences < minimumIntersectionInterval
                        newPoint = [x0(minimalIndex), y0(minimalIndex), mean([startTime(minimalIndex), endTime(minimalIndex)])];
                        newStartIndex = startIndex(minimalIndex);
                        newEndIndex = endIndex(minimalIndex) + 1;
                        
                        coincidesWithStart = iout(minimalIndex) - startIndex(minimalIndex) == 0;
                        if coincidesWithStart
                            newStartIndex = startIndex(minimalIndex) - 1;
                        end
                        
                        coincidesWithEnd = jout(minimalIndex) - endIndex(minimalIndex) == 0;
                        if coincidesWithEnd
                            newEndIndex = endIndex(minimalIndex) + 1;
                        end    
                                                                          
                        points = [points(:, 1:newStartIndex), newPoint', points(:, newEndIndex:end)];
                    else
                        intervalFound = false;
                    end
                end
            end
                    
            angles = atan2(-diff(points(1, :)), diff(points(2, :)))';
            
            curvature = struct(...
                'points', points',...
                'time', points(3, 1:(end-1))',...
                'breaks', [],...
                'angles', angles,...
                'intersections', [x0, y0, iout, jout]);
        end
        
        function uncertainty = ComputeTrajectoryUncertainty(trajectory, startPointWaveMembers)
            numberOfTrajectoryOverlap = sum(startPointWaveMembers(trajectory, :), 2);
            uncertainty = sum(numberOfTrajectoryOverlap > 1) / numel(numberOfTrajectoryOverlap);
        end
        
        function [score, correctedCurvature, rotationPoints, centroid] = ComputeCurvatureScore(curvature)
            angles = curvature.angles;
            for angleIndex = 2:numel(angles)
                angleDifference = angles(angleIndex) - angles(angleIndex - 1);
                if  angleDifference >= pi
                    angles(angleIndex:end) = angles(angleIndex:end) - 2 * pi;
                end
                
                if angleDifference <= -pi
                    angles(angleIndex:end) = angles(angleIndex:end) + 2 * pi;
                end
            end
            
            %             angles = angles - angles(1);
            %             score = ((angles(1:end-1) + angles(2:end)) / 2) * diff(curvature.time)';
            
            angles = angles - angles(1);
            [maxValue, maxPosition] = max(angles);
            [minValue, minPosition] = min(angles);
            scoreRange = sort([minPosition, maxPosition]);
            scoreRange(end) = scoreRange(end) + 1;
            rotationScore = (maxValue - minValue) / (2 * pi);
            intersectionScore = double(~isempty(curvature.intersections));
            if intersectionScore > rotationScore
                scoreRange = [floor(curvature.intersections(1, 3)), ceil(curvature.intersections(end, 4))];
            end
            score = max([rotationScore, intersectionScore]);
            rotationPoints = curvature.points(scoreRange(1):scoreRange(end), :);
            centroid = mean(rotationPoints, 1);
            
            correctedCurvature = curvature;
            correctedCurvature.angles = angles;
        end
    end
    
    methods (Static, Access = private)
        function [x0, y0, iout, jout] = ComputeSelfIntersections(x, y)
            %INTERSECTIONS Intersections of curves.
            %   Computes the (x,y) locations where two curves intersect. 
            
            % Theory of operation:
            %
            % Given two line segments, L1 and L2,
            %
            %   L1 endpoints:  (x1(1),y1(1)) and (x1(2),y1(2))
            %   L2 endpoints:  (x2(1),y2(1)) and (x2(2),y2(2))
            %
            % we can write four equations with four unknowns and then solve them.  The
            % four unknowns are t1, t2, x0 and y0, where (x0,y0) is the intersection of
            % L1 and L2, t1 is the distance from the starting point of L1 to the
            % intersection relative to the length of L1 and t2 is the distance from the
            % starting point of L2 to the intersection relative to the length of L2.
            %
            % So, the four equations are
            %
            %    (x1(2) - x1(1))*t1 = x0 - x1(1)
            %    (x2(2) - x2(1))*t2 = x0 - x2(1)
            %    (y1(2) - y1(1))*t1 = y0 - y1(1)
            %    (y2(2) - y2(1))*t2 = y0 - y2(1)
            %
            % Rearranging and writing in matrix form,
            %
            %  [x1(2)-x1(1)       0       -1   0;      [t1;      [-x1(1);
            %        0       x2(2)-x2(1)  -1   0;   *   t2;   =   -x2(1);
            %   y1(2)-y1(1)       0        0  -1;       x0;       -y1(1);
            %        0       y2(2)-y2(1)   0  -1]       y0]       -y2(1)]
            %
            % Let's call that A*T = B.  We can solve for T with T = A\B.
            %
            % Once we have our solution we just have to look at t1 and t2 to determine
            % whether L1 and L2 intersect.  If 0 <= t1 < 1 and 0 <= t2 < 1 then the two
            % line segments cross and we can include (x0,y0) in the output.
            %
            % In principle, we have to perform this computation on every pair of line
            % segments in the input data.  This can be quite a large number of pairs so
            % we will reduce it by doing a simple preliminary check to eliminate line
            % segment pairs that could not possibly cross.  The check is to look at the
            % smallest enclosing rectangles (with sides parallel to the axes) for each
            % line segment pair and see if they overlap.  If they do then we have to
            % compute t1 and t2 (via the A\B computation) to see if the line segments
            % cross, but if they don't then the line segments cannot cross.  In a
            % typical application, this technique will eliminate most of the potential
            % line segment pairs.
            
            % Force all inputs to be column vectors.
            x = x(:);
            y = y(:);
            
            % Compute number of line segments in each curve and some differences we'll
            % need later.
            n1 = length(x) - 1;
            xy1 = [x, y];
            dxy1 = diff(xy1);
            
            % Determine the combinations of i and j where the rectangle enclosing the
            % i'th line segment of curve 1 overlaps with the rectangle enclosing the
            % j'th line segment of curve 2.
            [i, j] = find(repmat(min(x(1:end-1), x(2:end)), 1, n1) <= ...
                repmat(max(x(1:end-1), x(2:end)).', n1, 1) & ...
                repmat(max(x(1:end-1), x(2:end)), 1, n1) >= ...
                repmat(min(x(1:end-1), x(2:end)).', n1, 1) & ...
                repmat(min(y(1:end-1), y(2:end)), 1, n1) <= ...
                repmat(max(y(1:end-1), y(2:end)).', n1, 1) & ...
                repmat(max(y(1:end-1), y(2:end)), 1, n1) >= ...
                repmat(min(y(1:end-1), y(2:end)).', n1, 1));
            
            % Force i and j to be column vectors, even when their length is zero, i.e.,
            % we want them to be 0-by-1 instead of 0-by-0.
            i = reshape(i, [], 1);
            j = reshape(j, [], 1);
            
            % Find segments pairs which have at least one vertex = NaN and remove them.
            % This line is a fast way of finding such segment pairs.  We take
            % advantage of the fact that NaNs propagate through calculations, in
            % particular subtraction (in the calculation of dxy1 and dxy2, which we
            % need anyway) and addition.
            % At the same time we can remove redundant combinations of i and j in the
            % case of finding intersections of a line with itself.
            remove = isnan(sum(dxy1(i, :) + dxy1(j, :), 2)) | j <= i + 1;
            i(remove) = [];
            j(remove) = [];
            
            % Initialize matrices.  We'll put the T's and B's in matrices and use them
            % one column at a time.  AA is a 3-D extension of A where we'll use one
            % plane at a time.
            n = length(i);
            T = zeros(4, n);
            AA = zeros(4, 4, n);
            AA([1, 2], 3, :) = -1;
            AA([3, 4], 4, :) = -1;
            AA([1, 3], 1, :) = dxy1(i,:).';
            AA([2, 4], 2, :) = dxy1(j,:).';
            B = -[x(i), x(j), y(i), y(j)].';
            
            % Loop through possibilities.  Trap singularity warning and then use
            % lastwarn to see if that plane of AA is near singular.  Process any such
            % segment pairs to determine if they are colinear (overlap) or merely
            % parallel.  That test consists of checking to see if one of the endpoints
            % of the curve 2 segment lies on the curve 1 segment.  This is done by
            % checking the cross product
            %
            %   (x1(2),y1(2)) - (x1(1),y1(1)) x (x2(2),y2(2)) - (x1(1),y1(1)).
            %
            % If this is close to zero then the segments overlap.
            
            % If the robust option is false then we assume no two segment pairs are
            % parallel and just go ahead and do the computation.  If A is ever singular
            % a warning will appear.  This is faster and obviously you should use it
            % only when you know you will never have overlapping or parallel segment
            % pairs.
            
            
            overlap = false(n,1);
            warning('off', 'backtrace');
            warning_state = warning('off','MATLAB:singularMatrix');
            warning_state = warning('off','MATLAB:nearlySingularMatrix');
            % Use try-catch to guarantee original warning state is restored.
            try
                lastwarn('');
                for k = 1:n
                    T(:, k) = AA(:,:, k) \ B(:, k);
                    [~, last_warn] = lastwarn;
                    lastwarn('')
                    if strcmp(last_warn,'MATLAB:singularMatrix') || strcmp(last_warn,'MATLAB:nearlySingularMatrix')
                        % Force in_range(k) to be false.
                        T(1,k) = NaN;
                        % Determine if these segments overlap or are just parallel.
                        overlap(k) = rcond([dxy1(i(k), :); xy1(j(k), :) - xy1(i(k), :)]) < eps;
                    end
                end
                warning(warning_state);
            catch err
                warning(warning_state);
                rethrow(err);
            end
            % Find where t1 and t2 are between 0 and 1 and return the corresponding
            % x0 and y0 values.
            in_range = (T(1, :) >= 0 & T(2, :) >= 0 & T(1, :) <= 1 & T(2, :) <= 1).';
            % For overlapping segment pairs the algorithm will return an
            % intersection point that is at the center of the overlapping region.
            if any(overlap)
                ia = i(overlap);
                ja = j(overlap);
                % set x0 and y0 to middle of overlapping region.
                T(3, overlap) = (max(min(x(ia), x(ia + 1)), min(x(ja), x(ja + 1))) + ...
                    min(max(x(ia), x(ia + 1)), max(x(ja), x(ja + 1)))).' / 2;
                T(4, overlap) = (max(min(y(ia), y(ia + 1)), min(y(ja), y(ja + 1))) + ...
                    min(max(y(ia), y(ia + 1)), max(y(ja), y(ja + 1)))).' / 2;
                selected = in_range | overlap;
            else
                selected = in_range;
            end
            xy0 = T(3:4, selected).';
            
%             % Remove duplicate intersection points.
%             [xy0, index] = unique(xy0, 'rows');
            index = (1:size(xy0, 1))';
            x0 = xy0(:, 1);
            y0 = xy0(:, 2);
            
            % Compute how far along each line segment the intersections are.
            if nargout > 2
                sel_index = find(selected);
                sel = sel_index(index);
                if ~isempty(sel)
                iout = i(sel) + T(1, sel).';
                jout = j(sel) + T(2, sel).';
                else
                    iout = [];
                    jout = [];
                end
            end
        end
    end
end