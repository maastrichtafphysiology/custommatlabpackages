classdef WavemapCalculator < handle
    properties
        ConductionThreshold
        DiscontinuousConductionThreshold
        NeighborRadius
        MergeSearchRadius
        MergePhaseShiftThreshold
        MergeOverlapThreshold
        
        WaveMerging
        StartPointElimination
        WaveConductionLikelihoodImprovement
        
        DetectionMethodIndex
    end
    
    properties (Access = private)
        EcgData
        VelocityDistribution
        ElectrodeActivations
        ElectrodePositions
        DeflectionData
        MembershipMatrix
        DetectedWaves
    end
    
    properties (Constant)
        METHODS = {'Start-point based', 'Conduction based'}
    end
    
    methods
        function self = WavemapCalculator()
            self.ConductionThreshold = 0.2;
            self.DiscontinuousConductionThreshold = 0.05;
            self.NeighborRadius = sqrt(1.5^2 + 1.5^2);
            self.MergeSearchRadius = 2 * self.NeighborRadius;
            self.MergePhaseShiftThreshold = 1 / self.ConductionThreshold;
            self.MergeOverlapThreshold = 0.75;
            
            self.WaveMerging = false;
            self.StartPointElimination = false;
            self.WaveConductionLikelihoodImprovement = true;
            
            self.DetectionMethodIndex = 2;
        end
        
        function [detectedWaves, membershipMatrix, startingPointMatrix] = DetectPacemapWaves(self, ecgData, activations)
            self.EcgData = ecgData;
            self.ElectrodeActivations = activations;
            
            [detectedWaves, membershipMatrix, startingPointMatrix] = self.DetectConductionBasedWaves();
            
            for waveIndex = 1:numel(detectedWaves)
                detectedWaves(waveIndex).SetConnectivityMatrix(self.ConductionThreshold, self.NeighborRadius);
            end
            
            edgeElectrodes = self.EcgData.GetEdgeElectrodes();
            AlgorithmPkg.WavemapCalculator.SetWaveType(detectedWaves, edgeElectrodes);
            
            detectedWaves = AlgorithmPkg.WavemapCalculator.SortWaves(detectedWaves);
        end
        
        function [detectedWaves, membershipMatrix, startingPointMatrix] = DetectWaves(self, ecgData, deflectionData)
            self.EcgData = ecgData;
            self.DeflectionData = deflectionData;
            
            self.ElectrodeActivations = AlgorithmPkg.WavemapCalculator.GetActivationData(ecgData, deflectionData);
            
            applicationSettings = ApplicationSettings.Instance();
            applicationSettings.VelocityComputation.conductionBlock = self.ConductionThreshold;
            
            switch self.DetectionMethodIndex
                case 1
                    [detectedWaves, membershipMatrix, startingPointMatrix] = self.DetectStartPointBasedWaves();
                case 2
                    [detectedWaves, membershipMatrix, startingPointMatrix] = self.DetectConductionBasedWaves();
            end
            
            
            for waveIndex = 1:numel(detectedWaves)
                detectedWaves(waveIndex).SetConnectivityMatrix(self.ConductionThreshold, self.NeighborRadius);
            end
            
            edgeElectrodes = self.EcgData.GetEdgeElectrodes();
            AlgorithmPkg.WavemapCalculator.SetWaveType(detectedWaves, edgeElectrodes);
            
            detectedWaves = AlgorithmPkg.WavemapCalculator.SortWaves(detectedWaves);
            self.DetectedWaves = detectedWaves;
            self.MembershipMatrix = membershipMatrix;
        end
        
        function [improvedWaves, membershipMatrix, deflectionData] = ImproveWaves(self)
            if isempty(self.EcgData)
                improvedWaves = {};
                membershipMatrix = [];
                return;
            end
            improvedWaves = self.DetectedWaves;
            membershipMatrix = self.MembershipMatrix;
            deflectionData = self.DeflectionData;
            
            if self.WaveMerging
                [improvedWaves, membershipMatrix] =...
                    self.MergeWaves(improvedWaves, self.EcgData.ElectrodePositions, self.ElectrodeActivations, membershipMatrix);
            end
            
            if self.StartPointElimination
                [improvedWaves, membershipMatrix, startingPointMatrix, deflectionData] =...
                    self.EliminateStartingPoints(improvedWaves,...
                    self.EcgData.ElectrodePositions, self.ElectrodeActivations,...
                    membershipMatrix, deflectionData);
            end
            
            if self.WaveConductionLikelihoodImprovement
                [improvedWaves, deflectionData] = self.ImproveWaveConductionLikelihood(improvedWaves, self.EcgData.ElectrodePositions, deflectionData);
            end
            
            edgeElectrodes = self.EcgData.GetEdgeElectrodes();
            AlgorithmPkg.WavemapCalculator.SetWaveType(improvedWaves, edgeElectrodes);
            
            self.DetectedWaves = improvedWaves;
            self.DeflectionData = deflectionData;
            self.ElectrodeActivations = AlgorithmPkg.WavemapCalculator.GetActivationData(self.EcgData, deflectionData);
            self.MembershipMatrix = AlgorithmPkg.WavemapCalculator.GetMemberships(improvedWaves, self.ElectrodeActivations);
        end
        
        function [detectedComponents, startingPoints, startPointComponentMembers] = DetectWaveComponents(self, ecgData, deflectionData)
            self.EcgData = ecgData;
            self.DeflectionData = deflectionData;
            
            self.ElectrodeActivations = AlgorithmPkg.WavemapCalculator.GetActivationData(ecgData, deflectionData);
            
            detectedComponents = self.DetectConnectedActivations();
            
            startingPoints = cell(size(detectedComponents));
            startPointComponentMembers = cell(size(detectedComponents));
            for componentIndex = 1:numel(detectedComponents)
                [startingPoints{componentIndex}, startPointComponentMembers{componentIndex}] =...
                    AlgorithmPkg.WavemapCalculator.GetComponentStartingPoints(detectedComponents(componentIndex));
            end
        end
        
        function [detectedComponents, startingPoints, startPointComponentMembers] = DetectActivationComponents(self, ecgData)
            self.EcgData = ecgData;
            computestartPointMembers = false;
            if nargout > 2
                computestartPointMembers = true;
            end
            
            self.ElectrodeActivations = ecgData.Activations;
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Computing connectivity...', 0, self));
            
            detectedComponents = self.DetectConnectedActivations();
            
            startingPoints = cell(size(detectedComponents));
            startPointComponentMembers = cell(size(detectedComponents));
            parfor componentIndex = 1:numel(detectedComponents)
                if computestartPointMembers
                    [startingPoints{componentIndex}, startPointComponentMembers{componentIndex}] =...
                        AlgorithmPkg.WavemapCalculator.GetComponentStartingPoints(detectedComponents(componentIndex));
                else
                    startingPoints{componentIndex} =...
                        AlgorithmPkg.WavemapCalculator.GetComponentStartingPoints(detectedComponents(componentIndex));
                end
            end
        end
        
        function [components, activationData, activationComponentIndex] = GetConnectedActivations(self, ecgData)
            self.EcgData = ecgData;
            self.ElectrodeActivations = ecgData.Activations;
            [components, activationData, activationComponentIndex] = self.DetectConnectedActivations();
        end
        
        function delete(self)
            self.DetectedWaves = [];
            self.EcgData = [];
        end
    end
    
    methods (Access = private)
        function [detectedWaves, membershipMatrix, startingPointMatrix] = DetectStartPointBasedWaves(self)
            % Deterministic wave mapping
            [deterministicWaves membershipMatrix startingPointMatrix] =...
                self.DetectDeterministicWaves(self.ElectrodeActivations, self.EcgData.ElectrodePositions);
            
            % Distribution computation
            self.VelocityDistribution =...
                self.ComputeWaveDistributions(deterministicWaves, self.EcgData.ElectrodePositions);
            
            % Probabilistic wave mapping
            [detectedWaves membershipMatrix startingPointMatrix] =...
                self.DetectProbabilisticWaves(self.ElectrodeActivations,...
                self.EcgData.ElectrodePositions,...
                deterministicWaves,...
                membershipMatrix,...
                startingPointMatrix);
        end
        
        function [detectedWaves, membershipMatrix, startingPointMatrix] = DetectConductionBasedWaves(self)
            
            % determine (weakly) connected activations
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Computing connectivity...', 0, self));
            detectedWaves = self.DetectConnectedActivations();
            
            % segment waves
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Segmenting waves...', 0, self));
            
            segmentedWaves = cell(size(detectedWaves));
            if isempty(self.MergeOverlapThreshold)
                self.MergeOverlapThreshold = 0.75;
            end
            
            if self.MergeOverlapThreshold > 0
                for waveIndex = 1:numel(detectedWaves)
                    segmentedWaves{waveIndex} = self.SegmentWave(detectedWaves(waveIndex));
                end
                detectedWaves = horzcat(segmentedWaves{:});
            end
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Segmenting waves...Done', 1, self));
            
            % set wave IDs, memberships
            for waveIndex = 1:numel(detectedWaves)
                detectedWaves(waveIndex).ID = waveIndex;
            end
            
            membershipMatrix = AlgorithmPkg.WavemapCalculator.GetMemberships(detectedWaves, self.ElectrodeActivations);
            
            startingPointMatrix = AlgorithmPkg.WavemapCalculator.GetStartingPoints(detectedWaves, self.ElectrodeActivations);
            
            numberOfElectrodes = self.EcgData.GetNumberOfChannels();
            for electrodeIndex = 1:numberOfElectrodes
                membershipMatrix{electrodeIndex}(isnan(membershipMatrix{electrodeIndex})) = Inf;
            end
            
             % Distribution computation
            self.VelocityDistribution =...
                self.ComputeWaveDistributions(detectedWaves, self.EcgData.ElectrodePositions);
            
            % Probabilistic wave mapping
            [detectedWaves, membershipMatrix, startingPointMatrix] =...
                self.DetectProbabilisticWaves(self.ElectrodeActivations,...
                self.EcgData.ElectrodePositions,...
                detectedWaves,...
                membershipMatrix,...
                startingPointMatrix);
        end
        
        function [detectedWaves, activationData, activationComponentIndex] = DetectConnectedActivations(self)
            numberOfActivations = numel([self.ElectrodeActivations{:}]);
            numberOfElectrodes = self.EcgData.GetNumberOfChannels();
            electrodePositions = self.EcgData.ElectrodePositions;
            maximumNumberOfConnections = 9 * numberOfActivations;
            
            activationData = NaN(numberOfActivations, 3);
            numberOfActivationsPerElectrode = cellfun(@numel, self.ElectrodeActivations);
            electrodeActivationStartIndex = cumsum(numberOfActivationsPerElectrode) - numberOfActivationsPerElectrode;
            electrodeIndices = cell(numberOfElectrodes, 1);
            activationIndices = cell(numberOfElectrodes, 1);
            for electrodeIndex = 1:numberOfElectrodes
                electrodeIndices{electrodeIndex} = electrodeIndex * ones(numberOfActivationsPerElectrode(electrodeIndex), 1);
                activationIndices{electrodeIndex} = (1:numberOfActivationsPerElectrode(electrodeIndex))';
            end
            activationData(:, 1) = vertcat(electrodeIndices{:});
            activationData(:, 2) = vertcat(activationIndices{:});
            activationData(:, 3) = [self.ElectrodeActivations{:}];
            
            electrodeConductionIndices = cell(numberOfElectrodes, 1);
            allElectrodeActivations = self.ElectrodeActivations;
            neighborRadius = self.NeighborRadius;
            conductionThreshold = self.ConductionThreshold;
            
            parfor electrodeIndex = 1:numberOfElectrodes
                neighbors = AlgorithmPkg.WavemapCalculator.GetElectrodeNeighbors(...
                    electrodePositions, allElectrodeActivations,...
                    electrodeIndex, neighborRadius);
                electrodeActivations = allElectrodeActivations{electrodeIndex};
                currentElectrodeConductionIndices = cell(numel(electrodeActivations), 1);
                for activationIndex = 1:numel(electrodeActivations)
                    activationGlobalIndex = electrodeActivationStartIndex(electrodeIndex) + activationIndex;
                    
%                     [neighborActivationsBefore, activationIndicesBefore] =...
%                         cellfun(@(x)...
%                         AlgorithmPkg.WavemapCalculator.FindFirstActivationIndexBeforeCurrent(...
%                         x, electrodeActivations(activationIndex)), neighbors.activations);
%                     
%                     neighborConductionBefore = neighbors.distances ./...
%                         (electrodeActivations(activationIndex) - neighborActivationsBefore);
%                     validNeighborsBefore = (neighborConductionBefore >= conductionThreshold);
                    
                    [neighborActivations, activationIndices] =...
                        cellfun(@(x)...
                        AlgorithmPkg.WavemapCalculator.FindFirstActivationIndexAfterCurrent(...
                        x, electrodeActivations(activationIndex)), neighbors.activations);
                    
                    neighborConduction = neighbors.distances ./...
                        (neighborActivations - electrodeActivations(activationIndex));
                    validNeighbors = (neighborConduction >= conductionThreshold);
                    currentElectrodeConductionIndices{activationIndex} = [activationGlobalIndex * ones(numel(find(validNeighbors)), 1),...
                        electrodeActivationStartIndex(neighbors.positions(validNeighbors)) + activationIndices(validNeighbors)];
                end
                electrodeConductionIndices{electrodeIndex} = vertcat(currentElectrodeConductionIndices{:});
            end
            electrodeConductionIndices = vertcat(electrodeConductionIndices{:});
            
            % determine connected components as initial waves
            connectivityGraph = sparse(electrodeConductionIndices(:, 1), electrodeConductionIndices(:, 2), true,...
                numberOfActivations, numberOfActivations, maximumNumberOfConnections);
            
%             [numberOfComponents, activationComponentIndex] = graphconncomp(connectivityGraph, 'weak', true);
            % change for > R2022a
            [activationComponentIndex, componentSize] = conncomp(digraph(connectivityGraph), 'Type', 'weak');
            numberOfComponents = numel(componentSize);
            %
            
            detectedWaves = AlgorithmPkg.Wave.empty(numberOfComponents, 0);
            for componentIndex = 1:numberOfComponents
                componentMembers = (activationComponentIndex == componentIndex);
                
                waveMembers = activationData(componentMembers, :);
                detectedWaves(componentIndex) = AlgorithmPkg.Wave(componentIndex, electrodePositions);
                detectedWaves(componentIndex).Members = waveMembers;
                detectedWaves(componentIndex).ConnectivityMatrix = connectivityGraph(componentMembers, componentMembers);
            end
        end
        
        function segmentedWave = SegmentWave(self, wave)
           [startingPoints, startingPointIndices] = wave.GetAllStartingPoints();
           numberOfStartingPoints = size(startingPoints, 1);
           
           if numberOfStartingPoints == 1
               segmentedWave = wave;
               return;
           end
           
           % merge adjacent starting points
           startPointconnectivityMatrix = wave.ConnectivityMatrix(startingPointIndices, startingPointIndices);
%            [numberOfStartingPoints, pointStartIndex] = graphconncomp(startPointconnectivityMatrix, 'weak', true);
           % change for > R2022a
           [pointStartIndex, componentSize] = conncomp(digraph(startPointconnectivityMatrix), 'Type', 'weak');
           numberOfStartingPoints = numel(componentSize);
           %
           
           if numberOfStartingPoints == 1, 
               segmentedWave = wave;
               return;
           end
           
           mergedStartingPointIndices = cell(numberOfStartingPoints, 1);
           for pointIndex = 1:numberOfStartingPoints
               startPointMembers = pointStartIndex == pointIndex;
               mergedStartingPointIndices{pointIndex} = startingPointIndices(startPointMembers);
           end
           startingPointIndices = mergedStartingPointIndices;
           
           % determine reachable activations for each starting point
           startPointWaveMembers = false(wave.Size, numberOfStartingPoints);
           for pointIndex = 1:numberOfStartingPoints
%                memberIndices = graphtraverse(wave.ConnectivityMatrix, startingPointIndices{pointIndex}(1));
               % Change for > R2022a
               memberIndices = bfsearch(digraph(wave.ConnectivityMatrix), startingPointIndices{pointIndex}(1));
               %
               startPointWaveMembers(memberIndices, pointIndex) = true;
               startPointWaveMembers(startingPointIndices{pointIndex}, pointIndex) = false;
           end
           
           % determine partial waves
           exclusiveStartPointWaveMembers = false(wave.Size, numberOfStartingPoints);
           for pointIndex = 1:numberOfStartingPoints
               otherStartingPoints = true(1, numberOfStartingPoints);
               otherStartingPoints(pointIndex) = false;
               exclusiveStartPointWaveMembers(:, pointIndex) =...
                   startPointWaveMembers(:, pointIndex) & ~any(startPointWaveMembers(:, otherStartingPoints), 2);
           end
           
           % Wave merging based on overlap
           % (1) Remove connected components from overlap graph to make it acyclic
           acyclic = false;
           while ~acyclic
               waveOverlap = AlgorithmPkg.WavemapCalculator.ComputeWaveOverlapMatrix(startPointWaveMembers);
               completeOverlapGraph = waveOverlap > self.MergeOverlapThreshold;
%                [numberOfStartingPoints, pointStartIndex] = graphconncomp(sparse(completeOverlapGraph));
               % change for > R2022a
               [pointStartIndex, componentSize] = conncomp(digraph(sparse(completeOverlapGraph)));
               numberOfStartingPoints = numel(componentSize);
               %
               
               if numberOfStartingPoints == 1,
                   segmentedWave = wave;
                   return;
               end
               
               if numberOfStartingPoints == size(startPointWaveMembers, 2)
                   break;
               end
               
               mergedStartingPointIndices = cell(numberOfStartingPoints, 1);
               mergedStartPointWaveMembers = false(wave.Size, numberOfStartingPoints);
               for pointIndex = 1:numberOfStartingPoints
                   startPointMembers = pointStartIndex == pointIndex;
                   mergedStartingPointIndices{pointIndex} = vertcat(startingPointIndices{startPointMembers});
                   mergedStartPointWaveMembers(:, pointIndex) = any(startPointWaveMembers(:, startPointMembers), 2);
                   mergedExclusiveStartPointWaveMembers(:, pointIndex) = any(exclusiveStartPointWaveMembers(:, startPointMembers), 2);
               end
               startingPointIndices = mergedStartingPointIndices;
               startPointWaveMembers = mergedStartPointWaveMembers;
               exclusiveStartPointWaveMembers = mergedExclusiveStartPointWaveMembers;
           end
           
           % (2) Determine reachable waves from <100% overlapped waves
           wavesMerged = true;
           while wavesMerged
               waveOverlap = AlgorithmPkg.WavemapCalculator.ComputeWaveOverlapMatrix(startPointWaveMembers);
               completeWaveOverlapGraph = (waveOverlap > self.MergeOverlapThreshold)';
               starterWaves = sum(completeWaveOverlapGraph, 1) == 1;
               if ~all(starterWaves)
                   completeWaveOverlapGraph = sparse(completeWaveOverlapGraph);
                   starterWaveIndices = find(starterWaves);
                   independentWaves = true(numberOfStartingPoints, 1);
                   numberOfStartWaves = numel(starterWaveIndices);
                   waveOverlapMembers = false(numberOfStartingPoints, numberOfStartWaves);
                   for waveIndex = 1:numberOfStartWaves
%                        memberIndices = graphtraverse(completeWaveOverlapGraph, starterWaveIndices(waveIndex));
                       % Change for > R2002a
                       memberIndices = bfsearch(digraph(completeWaveOverlapGraph), starterWaveIndices(waveIndex));
                       %
                       waveOverlapMembers(memberIndices, waveIndex) = true;
                   end
                   
                   for waveIndex = 1:numberOfStartWaves
                       otherStartingPoints = true(1, numberOfStartWaves);
                       otherStartingPoints(waveIndex) = false;
                       exclusiveWaveOverlap =...
                           waveOverlapMembers(:, waveIndex) & ~any(waveOverlapMembers(:, otherStartingPoints), 2);
                       exclusiveWaveOverlap(starterWaveIndices(waveIndex)) = false;
                       if ~any(exclusiveWaveOverlap), continue; end
                       
                       exclusiveWaveOverlap(starterWaveIndices(waveIndex)) = true;
                       startingPointIndices{starterWaveIndices(waveIndex)} = vertcat(startingPointIndices{exclusiveWaveOverlap});
                       startPointWaveMembers(:, starterWaveIndices(waveIndex)) = any(startPointWaveMembers(:, exclusiveWaveOverlap), 2);
                       exclusiveStartPointWaveMembers(:, starterWaveIndices(waveIndex)) = any(exclusiveStartPointWaveMembers(:, exclusiveWaveOverlap), 2);
                       
                       exclusiveWaveOverlap(starterWaveIndices(waveIndex)) = false;
                       independentWaves(exclusiveWaveOverlap) = false;
                   end
                   numberOfStartingPoints = numel(find(independentWaves));
                   if all(independentWaves), break; end
                   
                   startingPointIndices = startingPointIndices(independentWaves);
                   startPointWaveMembers = startPointWaveMembers(:, independentWaves);
                   exclusiveStartPointWaveMembers = exclusiveStartPointWaveMembers(:, independentWaves);
               else
                   wavesMerged = false;
               end
           end
           
           % Create segmented wave
           segmentedWave = AlgorithmPkg.Wave.empty(numberOfStartingPoints, 0);
           for pointIndex = 1:numberOfStartingPoints
               segmentedWave(pointIndex) = AlgorithmPkg.Wave(pointIndex, self.EcgData.ElectrodePositions);
               segmentedWave(pointIndex).Members =...
                   [wave.Members(startingPointIndices{pointIndex}, :);...
                   wave.Members(exclusiveStartPointWaveMembers(:, pointIndex), :)];
               segmentedWave(pointIndex).StartingPoints = wave.Members(startingPointIndices{pointIndex}, :);
               segmentedWave(pointIndex).StartingPointIndices = startingPointIndices{pointIndex};
               allWaveMembers = [startingPointIndices{pointIndex}; find(exclusiveStartPointWaveMembers(:, pointIndex))];
               segmentedWave(pointIndex).ConnectivityMatrix = wave.ConnectivityMatrix(allWaveMembers, allWaveMembers);
           end
        end
        
        function [detectedWaves, membershipMatrix, startingPointMatrix] = DetectDeterministicWaves(self, electrodeActivations, electrodePositions)
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Deterministic wave detection...', 0, self));
            
            uniqueActivations = sort(unique([electrodeActivations{:}]));
            
            membershipMatrix = cell(size(electrodeActivations));
            startingPointMatrix = cell(size(electrodeActivations));
            for position = 1:numel(electrodeActivations)
                membershipMatrix{position} = NaN(size(electrodeActivations{position}));
                startingPointMatrix{position} = NaN(size(electrodeActivations{position}));
            end
            
            detectedWaves = AlgorithmPkg.Wave.empty(1, 0);
            numberOfActivations = numel(uniqueActivations);
            for activationIndex = 1:numberOfActivations
                nextActivationTime = uniqueActivations(activationIndex);
                
                activationIndices = cellfun(@(x) find(x == nextActivationTime), electrodeActivations, 'uniformOutput', false);
                nextElectrodes = find(cellfun(@(x) ~isempty(x), activationIndices));
                
                electrodeAssignedInPreviousStep = true;
                while ~isempty(nextElectrodes)
                    assignedElectrodes = false(numel(nextElectrodes), 1);
                    for electrodeIndex = 1:numel(nextElectrodes)
                        neighbors = AlgorithmPkg.WavemapCalculator.GetElectrodeNeighbors(...
                            electrodePositions, electrodeActivations,...
                            nextElectrodes(electrodeIndex),...
                            self.NeighborRadius);
                        
                        [neighbors.activations memberships] =...
                            cellfun(@(x, y)...
                            AlgorithmPkg.WavemapCalculator.FindFirstActivationBeforeCurrent(x, y, nextActivationTime),...
                            neighbors.activations, membershipMatrix(neighbors.positions));
                        
                        neighborConduction = neighbors.distances ./...
                            (nextActivationTime - neighbors.activations);
                        candidateWaves = [];
                        if any(any(neighborConduction >= self.ConductionThreshold))
                            candidateWaves = unique(memberships(neighborConduction >= self.ConductionThreshold));
                            candidateWaves = candidateWaves(~isnan(candidateWaves));
                        end
                        
                        if isempty(candidateWaves)
                            if ~electrodeAssignedInPreviousStep || numel(nextElectrodes) == 1
                                [isochroneNeighbors isochronePositions] = ismember(nextElectrodes, neighbors.positions);
                                isochroneNeighbors(electrodeIndex) = false;
                                isochroneWaveID = unique(memberships(isochronePositions(isochroneNeighbors)));
                                if any(isochroneNeighbors) && all(~isnan(isochroneWaveID))
                                    waveID = isochroneWaveID;
                                    detectedWaves(isochroneWaveID).Add(nextElectrodes(electrodeIndex),...
                                        activationIndices{nextElectrodes(electrodeIndex)},...
                                        nextActivationTime);
                                else
                                    waveID = numel(detectedWaves) + 1;
                                    newWave = AlgorithmPkg.Wave(waveID, electrodePositions);
                                    newWave.Add(nextElectrodes(electrodeIndex),...
                                        activationIndices{nextElectrodes(electrodeIndex)},...
                                        nextActivationTime);
                                    detectedWaves = [detectedWaves newWave]; %#ok<*AGROW>
                                end
                                
                                activationPosition = electrodeActivations{nextElectrodes(electrodeIndex)} == nextActivationTime;
                                startingPointMatrix{nextElectrodes(electrodeIndex)}(activationPosition) = waveID;
                                membershipMatrix{nextElectrodes(electrodeIndex)}(activationPosition) = waveID;
                                
                                assignedElectrodes(electrodeIndex) = true;
                            end
                        else
                            if numel(candidateWaves) == 1 && ~isinf(candidateWaves)
                                detectedWaves(candidateWaves).Add(nextElectrodes(electrodeIndex),...
                                    activationIndices{nextElectrodes(electrodeIndex)},...
                                    nextActivationTime);
                                waveID = detectedWaves(candidateWaves).ID;
                                
                                activationPosition = electrodeActivations{nextElectrodes(electrodeIndex)} == nextActivationTime;
                                membershipMatrix{nextElectrodes(electrodeIndex)}(activationPosition) = waveID;
                            else
                                activationPosition = electrodeActivations{nextElectrodes(electrodeIndex)} == nextActivationTime;
                                membershipMatrix{nextElectrodes(electrodeIndex)}(activationPosition) = Inf;
                            end
                            assignedElectrodes(electrodeIndex) = true;
                        end
                    end
                    if any(assignedElectrodes)
                        electrodeAssignedInPreviousStep = true;
                        nextElectrodes(assignedElectrodes) = [];
                    else
                        electrodeAssignedInPreviousStep = false;
                    end
                end

                if mod(nextActivationTime, 100) < 1
                    UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                        UserInterfacePkg.StatusChangeEventData({'Deterministic wave detection'; ['Time position: ' num2str(nextActivationTime)]},...
                        activationIndex / numberOfActivations, self));
                end
            end
        end
        
        function [detectedWaves, membershipMatrix, startingPointMatrix] =...
                DetectProbabilisticWaves(self, electrodeActivations, electrodePositions, deterministicWaves,...
                membershipMatrix, startingPointMatrix)
            
            allActivations = [electrodeActivations{:}];
            allMemberships = [membershipMatrix{:}];
            allActivations = allActivations(isinf(allMemberships));
            uniqueActivations = sort(unique(allActivations));
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Probabilistic wave detection...', 0, self));
            
            detectedWaves = deterministicWaves;
            numberOfActivations = numel(uniqueActivations);
            for activationIndex = 1:numberOfActivations
                nextActivationTime = uniqueActivations(activationIndex);
                
                activationIndices = cellfun(@(x, y) find(x == nextActivationTime & isinf(y)), electrodeActivations, membershipMatrix, 'uniformOutput', false);
                nextElectrodes = find(cellfun(@(x) ~isempty(x), activationIndices));
                
                electrodeAssignedInPreviousStep = true;
                while ~isempty(nextElectrodes)
                    assignedElectrodes = false(numel(nextElectrodes), 1);
                    for electrodeIndex = 1:numel(nextElectrodes)
                        neighbors = AlgorithmPkg.WavemapCalculator.GetElectrodeNeighbors(...
                            electrodePositions, electrodeActivations,...
                            nextElectrodes(electrodeIndex),...
                            self.NeighborRadius);
                        
                        [neighbors.activations, memberships] =...
                            cellfun(@(x, y)...
                            AlgorithmPkg.WavemapCalculator.FindFirstActivationBeforeCurrent(x, y, nextActivationTime),...
                            neighbors.activations, membershipMatrix(neighbors.positions));
                        
                        neighborConduction =...
                            neighbors.distances ./ (nextActivationTime - neighbors.activations);
                        candidateWaves = [];
                        if any(any(neighborConduction >= self.ConductionThreshold))
                            candidateWaves = unique(memberships(neighborConduction >= self.ConductionThreshold));
                            candidateWaves = candidateWaves(~isnan(candidateWaves) & ~isinf(candidateWaves));
                        end
                        
                        if isempty(candidateWaves)
                            if ~electrodeAssignedInPreviousStep || numel(nextElectrodes) == 1
                                waveID = numel(detectedWaves) + 1;
                                newWave = AlgorithmPkg.Wave(waveID, electrodePositions);
                                newWave.Add(nextElectrodes(electrodeIndex),...
                                    activationIndices{nextElectrodes(electrodeIndex)},...
                                    nextActivationTime);
                                detectedWaves = [detectedWaves newWave];
                                
                                activationPosition = electrodeActivations{nextElectrodes(electrodeIndex)} == nextActivationTime;
                                startingPointMatrix{nextElectrodes(electrodeIndex)}(activationPosition) = waveID;
                                membershipMatrix{nextElectrodes(electrodeIndex)}(activationPosition) = waveID;
                                
                                assignedElectrodes(electrodeIndex) = true;
                            end
                        else
                            if numel(candidateWaves) == 1
                                detectedWaves(candidateWaves).Add(nextElectrodes(electrodeIndex),...
                                    activationIndices{nextElectrodes(electrodeIndex)},...
                                    nextActivationTime);
                                waveID = detectedWaves(candidateWaves).ID;
                                
                                activationPosition = electrodeActivations{nextElectrodes(electrodeIndex)} == nextActivationTime;
                                membershipMatrix{nextElectrodes(electrodeIndex)}(activationPosition) = waveID;
                            else
                                waveIndex = self.AssignElectrodeActivationToWave(detectedWaves(candidateWaves),...
                                    electrodePositions, electrodeActivations,...
                                    nextElectrodes(electrodeIndex), nextActivationTime);
                                selectedWave = candidateWaves(waveIndex);
                                for waveIndex = 1:numel(selectedWave)
                                    detectedWaves(selectedWave(waveIndex)).Add(nextElectrodes(electrodeIndex),...
                                        activationIndices{nextElectrodes(electrodeIndex)},...
                                        nextActivationTime);
                                    waveID = detectedWaves(selectedWave(waveIndex)).ID;
                                end
                                
                                activationPosition = electrodeActivations{nextElectrodes(electrodeIndex)} == nextActivationTime;
                                membershipMatrix{nextElectrodes(electrodeIndex)}(activationPosition) = waveID;
                            end
                            assignedElectrodes(electrodeIndex) = true;
                        end
                    end
                    if any(assignedElectrodes)
                        electrodeAssignedInPreviousStep = true;
                        nextElectrodes(assignedElectrodes) = [];
                    else
                        electrodeAssignedInPreviousStep = false;
                    end
                end
                
%                 if mod(nextActivationTime, 100) < 1
%                     UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
%                         UserInterfacePkg.StatusChangeEventData({'Probabilistic wave detection'; ['Time position: ' num2str(nextActivationTime)]},...
%                         activationIndex / numberOfActivations, self));
%                 end
            end
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                        UserInterfacePkg.StatusChangeEventData('Done', 1, self));
        end
        
        function [improvedWaves, membershipMatrix, startingPointMatrix, deflectionData] =...
                EliminateStartingPoints(self, detectedWaves, electrodePositions, electrodeActivations, membershipMatrix, deflectionData)
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Starting point elimination...', 0, self));
            
            time = 1000 * self.EcgData.GetTimeRange();
            improvedWaves = detectedWaves;
            startingPointMatrix = [];
            
            numberOfWaves = numel(detectedWaves);
            
            waveLink = NaN(size(detectedWaves));
            for waveIndex = 1:numberOfWaves
                if mod(waveIndex, 100) == 0
                    UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                        UserInterfacePkg.StatusChangeEventData({'Starting point elimination'; ['Wave ID: ' num2str(currentWave.ID)]},...
                        waveIndex / numberOfWaves, self));
                end
                
                currentWave = detectedWaves(waveIndex);
                waveStartingPointElectrode = currentWave.Members(1, 1);
                startingPointDeflectionIndex = currentWave.Members(1, 2);
                startingPointTime = currentWave.Members(1, 3);
                
                neighbors = AlgorithmPkg.WavemapCalculator.GetElectrodeNeighbors(...
                            electrodePositions, electrodeActivations,...
                            waveStartingPointElectrode,...
                            self.NeighborRadius);
                        
                % Maximum shift back in time
                [laterActivations laterMemberships] =...
                    cellfun(@(x, y)...
                    AlgorithmPkg.WavemapCalculator.FindFirstActivationAfterCurrent(x, y, startingPointTime),...
                    neighbors.activations, membershipMatrix(neighbors.positions));
                
                waveMembers = (laterMemberships == currentWave.ID);
                maxActivationDifference = neighbors.distances(waveMembers) / self.ConductionThreshold;
                if startingPointDeflectionIndex > 1
                    if numel(find(waveMembers)) > 1
                        maxActivationDifference(maxActivationDifference == 0) = max(maxActivationDifference);
                    else
                        maxActivationDifference(maxActivationDifference == 0) =...
                            startingPointTime - electrodeActivations{waveStartingPointElectrode}(startingPointDeflectionIndex - 1);
                    end
                else
                    if numel(find(waveMembers)) > 1
                        maxActivationDifference(maxActivationDifference == 0) = max(maxActivationDifference);
                    else
                        maxActivationDifference(maxActivationDifference == 0) = Inf;
                    end
                end
                maxActivationShiftBack = max(maxActivationDifference - (laterActivations(waveMembers) - startingPointTime));
                maxActivationShiftForward = min(laterActivations(waveMembers) - startingPointTime);
                
                % Candidate deflections
                farFieldDeflections = ~deflectionData{waveStartingPointElectrode}.PrimaryDeflections;
                farFieldDeflectionIndices = deflectionData{waveStartingPointElectrode}.templatePeakIndices(farFieldDeflections);
                farFieldDeflectionTimes = time(farFieldDeflectionIndices);
                candidateDeflections = (startingPointTime - farFieldDeflectionTimes) < maxActivationShiftBack &...
                    (startingPointTime - farFieldDeflectionTimes) > maxActivationShiftForward;
                if ~any(candidateDeflections), continue; end
                candidateDeflectionTimes = farFieldDeflectionTimes(candidateDeflections);
                
                % Candidate waves
                [neighbors.activations memberships] =...
                    cellfun(@(x, y)...
                    AlgorithmPkg.WavemapCalculator.FindFirstActivationBeforeCurrent(x, y, startingPointTime),...
                    neighbors.activations, membershipMatrix(neighbors.positions));
                
                activationDifferences = bsxfun(@minus, candidateDeflectionTimes, neighbors.activations(~waveMembers));
                candidateWaveConductions = bsxfun(@rdivide, neighbors.distances(~waveMembers), activationDifferences);
                [neighborIndices, deflectionIndices] = find(candidateWaveConductions > self.ConductionThreshold);
                if isempty(neighborIndices), continue; end
                
                % 'Best' wave to link to 
                probabilities = zeros(size(candidateDeflectionTimes));
                waveIndices = NaN(size(candidateDeflectionTimes));
                candidateMemberships = memberships(~waveMembers);
                for deflectionIndex = 1:numel(candidateDeflectionTimes)
                    deflectionNeigborIndices = unique(neighborIndices(deflectionIndices == deflectionIndex));
                    if isempty(deflectionNeigborIndices), continue; end
                    
                    candidateWavesIndices = unique(candidateMemberships(deflectionNeigborIndices));
                    candidateWaves = detectedWaves(candidateWavesIndices);
                    [bestWaveIndex probabilities(deflectionIndex)] =...
                        AssignElectrodeActivationToWave(self, candidateWaves,...
                        electrodePositions, electrodeActivations,...
                        waveStartingPointElectrode, candidateDeflectionTimes(deflectionIndex));
                    waveIndices(deflectionIndex) = candidateWavesIndices(bestWaveIndex);
                end
                [maxValue maxPosition] = max(probabilities);
                waveLink(waveIndex) = waveIndices(maxPosition);
                
                % update deflections
                primaryDeflectionIndices = find(deflectionData{waveStartingPointElectrode}.PrimaryDeflections);
                farFieldDeflectionIndices = find(~deflectionData{waveStartingPointElectrode}.PrimaryDeflections);
                candidateDeflectionIndices = farFieldDeflectionIndices(candidateDeflections);
                deflectionData{waveStartingPointElectrode}.PrimaryDeflections(primaryDeflectionIndices(startingPointDeflectionIndex)) = false;
                deflectionData{waveStartingPointElectrode}.PrimaryDeflections(candidateDeflectionIndices(maxPosition)) = true;
                detectedWaves(waveIndex).Members(1, 3) = candidateDeflectionTimes(maxPosition);
            end
            
            for waveIndex = numel(detectedWaves):(-1):1
                linkedWaveIndex = waveLink(waveIndex);
                if isnan(linkedWaveIndex), continue; end
                
                currentWave = detectedWaves(waveIndex);
                linkedWave = detectedWaves(linkedWaveIndex);
                
                for waveMemberIndex = 1:currentWave.Size()
                    linkedWave.Add(currentWave.Members(waveMemberIndex, 1),...
                        currentWave.Members(waveMemberIndex, 2),...
                        currentWave.Members(waveMemberIndex, 3));
                end
                
                membershipMatrix = cellfun(@(x) AlgorithmPkg.WavemapCalculator.ChangeMembership(x, currentWave.ID, linkedWave.ID), membershipMatrix,...
                    'uniformOutput', false);
            end
            
            % re-number waves
            improvedWaves = detectedWaves(isnan(waveLink));
            for waveIndex = 1:numel(improvedWaves)
                membershipMatrix = cellfun(@(x) AlgorithmPkg.WavemapCalculator.ChangeMembership(x, improvedWaves(waveIndex).ID, waveIndex),...
                    membershipMatrix, 'uniformOutput', false);
                improvedWaves(waveIndex).ID = waveIndex;
            end
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                        UserInterfacePkg.StatusChangeEventData('Done', 1, self));
        end
        
        function [mergedWaves, membershipMatrix] =...
                MergeWaves(self, waves, electrodePositions, electrodeActivations, membershipMatrix)
            
            %TEMP
            self.MergeSearchRadius = 2 * self.NeighborRadius;
            self.MergePhaseShiftThreshold = 1 / self.ConductionThreshold;
            %
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Merging waves...', 0, self));
            
            startingPoints = AlgorithmPkg.WavemapCalculator.GetStartingPoints(waves, electrodeActivations);
            
            numberOfWaves = numel(waves);
            waveLinkGraph = sparse(numel(waves), numel(waves));
            for waveIndex = 1:numberOfWaves
                currentWave = waves(waveIndex);
                waveStartingPoints = currentWave.GetAllStartingPoints();
                for startIndex = 1:size(waveStartingPoints, 1)
                    neighbors = AlgorithmPkg.WavemapCalculator.GetElectrodeNeighbors(...
                        electrodePositions, electrodeActivations,...
                        waveStartingPoints(startIndex, 1),...
                        self.MergeSearchRadius);
                    
                    startingPointTime = waveStartingPoints(startIndex, 3);
                    
                    [laterActivations laterMemberships activationIndex] =...
                        cellfun(@(x, y)...
                        AlgorithmPkg.WavemapCalculator.FindFirstActivationAfterCurrent(x, y, startingPointTime),...
                        neighbors.activations, membershipMatrix(neighbors.positions));
                    
                    otherWaveMembers = find(laterMemberships ~= currentWave.ID & ~isnan(laterMemberships));
                    otherStartingPoints = false(size(otherWaveMembers));
                    for pointIndex = 1:numel(otherWaveMembers)
                        otherStartingPoints(pointIndex) =...
                            startingPoints{neighbors.positions(otherWaveMembers(pointIndex))}(activationIndex(otherWaveMembers(pointIndex)));
                    end
                    
                    otherStartingPointElectrodes = neighbors.positions(otherWaveMembers(otherStartingPoints));
                    if isempty(otherStartingPointElectrodes), continue; end
                    otherStartingPointActivations = laterActivations(otherWaveMembers(otherStartingPoints));
                    otherStartingPointWaveIDs = laterMemberships(otherWaveMembers(otherStartingPoints));
                    otherStartingPointDistances = neighbors.distances(otherWaveMembers(otherStartingPoints));
                    
                    linkedWaves = (otherStartingPointActivations - startingPointTime) ./ otherStartingPointDistances <= self.MergePhaseShiftThreshold;
                    if any(linkedWaves)
                        linkedWavesIDs = otherStartingPointWaveIDs(linkedWaves);
                        waveLinkGraph(linkedWavesIDs, waveIndex) = 1; %#ok<SPRIX>
                    end
                end
            end
            
%             [numberOfComponents componentIndices] = graphconncomp(waveLinkGraph, 'directed', false);
            % change for > R2022a
            [componentIndices, componentSize] = conncomp(digraph(waveLinkGraph), 'Type', 'weak');
            numberOfComponents = numel(componentSize);
            %
            mergedWaves = AlgorithmPkg.Wave.empty(numberOfComponents, 0);
            for componentIndex = 1:numberOfComponents
                componentMembers = find(componentIndices == componentIndex);
                mergedWaves(componentIndex) = AlgorithmPkg.Wave(componentIndex, electrodePositions);
                for memberIndex = 1:numel(componentMembers)
                    currentWave = waves(componentMembers(memberIndex));
                    for waveMemberIndex = 1:currentWave.Size()
                        mergedWaves(componentIndex).Add(currentWave.Members(waveMemberIndex, 1),...
                            currentWave.Members(waveMemberIndex, 2),...
                            currentWave.Members(waveMemberIndex, 3));
                    end 
                end
            end
            
            membershipMatrix = AlgorithmPkg.WavemapCalculator.GetMemberships(mergedWaves, electrodeActivations);
        end
        
        function [waves, deflectionData] = ImproveWaveConductionLikelihood(self, waves, electrodePositions, deflectionData)
            % for each wave, for each member, try to find a secondary deflection that improves the wave conduction likelihood (CV
            % and tortuosity)
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Improving wave conduction likelihood...', 0, self));
            
            time = 1000 * self.EcgData.GetTimeRange();
            numberOfWaves = numel(waves);
            
            for waveIndex = 1:numel(waves)
                currentWave = waves(waveIndex);
                if mod(waveIndex, 10) == 0
                    UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                        UserInterfacePkg.StatusChangeEventData({'Improving wave conduction likelihood'; ['Wave ID: ' num2str(currentWave.ID)]},...
                        waveIndex / numberOfWaves, self));
                end
                
                if currentWave.Size < 2, continue; end
                
                for memberIndex = 1:currentWave.Size()
                    electrodeIndex = currentWave.Members(memberIndex, 1);
                    deflectionIndex = currentWave.Members(memberIndex, 2);
                    activationTime = currentWave.Members(memberIndex, 3);
                    primaryDeflectionIndices = find(deflectionData{electrodeIndex}.PrimaryDeflections);
                    
                    % look for surrounding wave members
                    waveMemberPositionDifferences = bsxfun(@minus, electrodePositions(electrodeIndex, :), electrodePositions(currentWave.Members(:, 1), :));
                    waveMemberDistances = sqrt(waveMemberPositionDifferences(:, 1).^2 +...
                        waveMemberPositionDifferences(:, 2).^2 + waveMemberPositionDifferences(:, 3).^2);
                    surroundingWaveMembers = waveMemberDistances <= self.NeighborRadius;
                    surroundingWaveMembers(memberIndex) = false;
                    if ~any(surroundingWaveMembers), continue; end
                    
                    % determine maximum shift backward and forward
                    surroundingActivationTimes = currentWave.Members(surroundingWaveMembers, 3);
                    surroundingDistances = waveMemberDistances(surroundingWaveMembers);
                    
                    minActivation = max(surroundingActivationTimes - surroundingDistances / self.ConductionThreshold);
                    maxActivation = min(surroundingActivationTimes + surroundingDistances / self.ConductionThreshold);
                    
                    % determine candidate deflections
                    electrodeDeflectionTimes = time(deflectionData{electrodeIndex}.templatePeakIndices);
                    candidateDeflections = electrodeDeflectionTimes <= maxActivation &...
                        electrodeDeflectionTimes >= minActivation;
                    if numel(find((candidateDeflections))) < 2, continue; end
                    candidateDeflectionIndices = find(candidateDeflections);
                    candidateDeflectionTimes = electrodeDeflectionTimes(candidateDeflections);
                    
                    % determine 'best' deflection
                    conductionProbability = zeros(size(candidateDeflectionTimes));
                    directionDifferenceProbability = zeros(size(candidateDeflectionTimes));
                    for candididateDeflectionIndex = 1:numel(candidateDeflectionTimes)
                        currentWave.Members(memberIndex, 3) = candidateDeflectionTimes(candididateDeflectionIndex);
                        
                        velocityCalculator = AlgorithmPkg.VelocityCalculator();
                        
                        waveElectrodeActivations = currentWave.Members(:, 3);
                        waveElectrodePositions = electrodePositions(currentWave.Members(:, 1), :);
                        
                        [xVelocity yVelocity] = velocityCalculator.ComputeActivationWavefront(waveElectrodeActivations, waveElectrodePositions);
                        
                        electrodeConduction = sqrt(xVelocity(memberIndex)^2 + yVelocity(memberIndex)^2);
                        conductionProbability(candididateDeflectionIndex) = gampdf(electrodeConduction,...
                            self.VelocityDistribution.velocity(1), self.VelocityDistribution.velocity(2));
                        
                        directionDifferences = AlgorithmPkg.WavemapCalculator.ComputeLocalDirectionDifferences(xVelocity, yVelocity,...
                            waveElectrodePositions, self.NeighborRadius);
                        directionDifference = directionDifferences(memberIndex);
                        
                        directionDifferenceProbability(candididateDeflectionIndex) = normpdf(directionDifference,...
                            self.VelocityDistribution.direction(1), self.VelocityDistribution.direction(2));
                    end
                    [sortedProbability deflectionIndices] = sort(conductionProbability .* directionDifferenceProbability, 'descend');
                    if sortedProbability(1) > 0
                        bestDeflectionIndex = deflectionIndices(1);
                        currentWave.Members(memberIndex, 3) = candidateDeflectionTimes(bestDeflectionIndex);
                        deflectionData{electrodeIndex}.PrimaryDeflections(primaryDeflectionIndices(deflectionIndex)) = false;
                        deflectionData{electrodeIndex}.PrimaryDeflections(candidateDeflectionIndices(bestDeflectionIndex)) = true;
                    else
                        currentWave.Members(memberIndex, 3) = activationTime;
                    end
                end
            end
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Done', 1, self));
        end
        
        function [waveIndex, probability] = AssignElectrodeActivationToWave(self, candidateWaves,...
                electrodePositions, electrodeActivations,...
                electrodeIndex, electrodeActivation)
            
            waveConductionProbability = zeros(1, numel(candidateWaves));
            waveDirectionDifferenceProbability = zeros(1, numel(candidateWaves));
            
            velocityCalculator = AlgorithmPkg.VelocityCalculator();
            
            neighbors = AlgorithmPkg.WavemapCalculator.GetElectrodeNeighbors(electrodePositions,...
                        electrodeActivations, electrodeIndex, self.NeighborRadius);
                
            neighbors.activations =...
                    cellfun(@(x) AlgorithmPkg.WavemapCalculator.FindFirstActivationBeforeCurrent(x, x, electrodeActivation), neighbors.activations);
                
            for waveIndex = 1:numel(candidateWaves)
                waveMembers = candidateWaves(waveIndex).Members(:, 1);
                [isNeighbor, waveElectrodePosition] = ismember(waveMembers, neighbors.positions);
                
                activationGrid = NaN(size(neighbors.activations));
                activationGrid(waveElectrodePosition(isNeighbor)) = candidateWaves(waveIndex).Members(isNeighbor, 3);
                isMember = (activationGrid == neighbors.activations);
                if any(isMember(:))
                    indicesToCompute = [find(isNeighbor); numel(isNeighbor) + 1];
                    waveElectrodeActivations = [candidateWaves(waveIndex).Members(:, 3); electrodeActivation];
                    waveElectrodePositions = [electrodePositions(candidateWaves(waveIndex).Members(:, 1), :); electrodePositions(electrodeIndex, :)];
                    
                    [xVelocity, yVelocity] = velocityCalculator.ComputeActivationWavefront(waveElectrodeActivations, waveElectrodePositions, indicesToCompute);
                    
                    electrodeConduction = sqrt(xVelocity(end)^2 + yVelocity(end)^2);
                    waveConductionProbability(waveIndex) = gampdf(electrodeConduction,...
                        self.VelocityDistribution.velocity(1), self.VelocityDistribution.velocity(2));
                    
                    waveElectrodePositions = waveElectrodePositions(indicesToCompute, :);
                    directionDifferences = AlgorithmPkg.WavemapCalculator.ComputeLocalDirectionDifferences(xVelocity, yVelocity,...
                        waveElectrodePositions, self.NeighborRadius);
                    directionDifference = directionDifferences(end);
                    
                    waveDirectionDifferenceProbability(waveIndex) = normpdf(directionDifference,...
                        self.VelocityDistribution.direction(1), self.VelocityDistribution.direction(2));
                end
            end
            [sortedProbability, waveIndices] = sort(waveConductionProbability .* waveDirectionDifferenceProbability, 'descend');
            waveIndex = waveIndices(1);
            probability = sortedProbability(1);
        end
        
        function velocityDistribution = ComputeWaveDistributions(self, waves, electrodePositions)
            velocityThreshold = 1.5;
            minimalWaveSize = 4;
            plotDistributions = false;
            minimalConductionThreshold = self.ConductionThreshold;
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Wave distribution computation...', 0, self));
            
            waveVelocities = [];
            directionDifferences = [];
            numberOfWaves = numel(waves);
            parfor waveIndex = 1:numberOfWaves
                applicationSettings = ApplicationSettings.Instance();
                applicationSettings.VelocityComputation.conductionBlock = minimalConductionThreshold;
                
                if waves(waveIndex).Size() > minimalWaveSize
                    [currentWaveVelocities, currentDirectionDifferences] =...
                        self.ComputeWaveVelocities(waves(waveIndex), electrodePositions);
                    velocities = sqrt(currentWaveVelocities.vx.^2 + currentWaveVelocities.vy.^2);
                    currentDirectionDifferences(isnan(velocities) | isinf(velocities) | velocities > velocityThreshold) = [];
                    velocities(isnan(velocities) | isinf(velocities) | velocities > velocityThreshold) = [];
                    waveVelocities = [waveVelocities velocities];
                    currentDirectionDifferences(isnan(currentDirectionDifferences) | isinf(currentDirectionDifferences)) = [];
                    directionDifferences = [directionDifferences currentDirectionDifferences];
                end
                
%                 if mod(waveIndex, 50) == 0
%                     UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
%                         UserInterfacePkg.StatusChangeEventData({'Wave distribution computation'; ['Wave index: ' num2str(waveIndex)]},...
%                         waveIndex / numberOfWaves, self));
%                 end
            end
            
            [lambda, nEvent] = AlgorithmPkg.WavemapCalculator.FitErlangDistribution(waveVelocities);
            gammaEstimate = gamfit(waveVelocities);
            [mu, sigma] = normfit(directionDifferences);
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Wave distribution computation...Done', 1, self));
            
            if plotDistributions
                velocityFigure = figure('Name', 'Velocity distribution', 'numberTitle', 'off');
                
                bivariatePlot = subplot(2, 2, [1 2], 'Parent', velocityFigure);
                hold(bivariatePlot, 'on');
                scatter(bivariatePlot, waveVelocities, directionDifferences);
                [correlation pValues] = corrcoef([waveVelocities; directionDifferences]');
                correlationText = 'No correlation detected';
                if pValues(1, 2) < 0.05
                    correlationText = 'Significant correlation detected';
                end
                title(bivariatePlot, {'Velocity / difference distribution'; correlationText});
                xlabel(bivariatePlot, 'velocity (mm/ms)');
                ylabel(bivariatePlot, 'direction difference (degrees)');
                hold(bivariatePlot, 'off');
                
                velocityPlot = subplot(2,2,3, 'Parent', velocityFigure);
                hold(velocityPlot, 'on');
                hist(waveVelocities, 200);
                [bincounts, binpositions] = hist(waveVelocities, 200);
                binwidth = binpositions(2) - binpositions(1);
                histarea = binwidth * sum(bincounts);
                x = 0:0.001:binpositions(end);
                %y = erlang(x, lambda, nEvent);
                y = gampdf(x, gammaEstimate(1), gammaEstimate(2));
                plot(velocityPlot, x, histarea * y, 'r', 'LineWidth', 2);
                title(velocityPlot, 'Velocity distribution');
                xlabel(velocityPlot, 'velocity (mm/ms)');
                hold(velocityPlot, 'off');
                
                differencePlot = subplot(2,2,4, 'Parent', velocityFigure);
                hold(differencePlot, 'on');
                hist(directionDifferences, 200);
                [bincounts, binpositions] = hist(directionDifferences, 200);
                binwidth = binpositions(2) - binpositions(1);
                histarea = binwidth * sum(bincounts);
                x = (-180):0.01:180;
                y = normpdf(x, mu, sigma);
                plot(differencePlot, x, histarea * y, 'r', 'LineWidth', 2);
                title(differencePlot, 'Direction difference distribution');
                xlabel(differencePlot, 'direction difference (degrees)');
                hold(differencePlot, 'off');
            end
            
            velocityDistribution = struct(...
                'velocity', [lambda nEvent],...
                'direction', [mu, sigma]);
        end
        
        function [waveVelocities, directionDifferences] = ComputeWaveVelocities(self, wave, electrodePositions)
            velocityCalculator = AlgorithmPkg.VelocityCalculator();
            [xVelocity, yVelocity] = velocityCalculator.ComputeActivationWavefront(wave.Members(:, 3),...
                electrodePositions(wave.Members(:, 1), :), 1:wave.Size());
            
            waveVelocities = struct('vx', xVelocity, 'vy', yVelocity);
            directionDifferences = AlgorithmPkg.WavemapCalculator.ComputeLocalDirectionDifferences(xVelocity, yVelocity,...
                electrodePositions(wave.Members(:, 1), :), self.NeighborRadius);
        end
    end
    
    methods (Static)
        function [activation, membership, activationIndex]= FindFirstActivationBeforeCurrent(activations, memberships, currentActivation)
            position = find(activations <= currentActivation, 1, 'last');
            
            if isempty(position)
                membership = NaN;
                activation = NaN;
                activationIndex = NaN;
            else
                membership = memberships(position);
                activation = activations(position);
                activationIndex = position;
            end
        end
        
        function [activation, membership, activationIndex] = FindFirstActivationAfterCurrent(activations, memberships, currentActivation)
            position = find(activations >= currentActivation, 1, 'first');
            
            if isempty(position)
                membership = NaN;
                activation = NaN;
                activationIndex = NaN;
            else
                membership = memberships(position);
                activation = activations(position);
                activationIndex = position;
            end
        end
        
        function [activation, activationIndex]= FindFirstActivationIndexBeforeCurrent(activations, currentActivation)
            position = find(activations <= currentActivation, 1, 'last');
            
            if isempty(position)
                activation = NaN;
                activationIndex = NaN;
            else
                activation = activations(position);
                activationIndex = position;
            end
        end
        
        function [activation, activationIndex] = FindFirstActivationIndexAfterCurrent(activations, currentActivation)
            position = find(activations >= currentActivation, 1, 'first');
            
            if isempty(position)
                activation = NaN;
                activationIndex = NaN;
            else
                activation = activations(position);
                activationIndex = position;
            end
        end
        
        function activation = FindFirstUnAssignedActivationAfterCurrent(activations, memberships, currentActivation)
            position = find((activations > currentActivation) & isinf(memberships), 1, 'first');
            
            if isempty(position)
                activation = NaN;
            else
                activation = activations(position);
            end
        end
        
        function [lambda, nEvent] = FitErlangDistribution(observations)
            gammaEstimate = gamfit(observations);
            nEvent = round(gammaEstimate(1));
            lambda = 1 / gammaEstimate(2);
        end
        
        function directionDifferences = ComputeLocalDirectionDifferences(xVelocity, yVelocity, positions, radius)
            directionDifferences = NaN(size(xVelocity));
            directions = atan2(yVelocity, xVelocity);
            validPositions = ~isnan(directions);
            for positionIndex = 1:size(positions, 1)
                if validPositions(positionIndex)
                    neighbors = AlgorithmPkg.WavemapCalculator.GetElectrodeNeighbors(positions,...
                        xVelocity, positionIndex, radius);
                    
                    differences = (180/pi)* (mod(mod(directions(positionIndex) -...
                        directions(neighbors.positions), 2*pi) + 3*pi, 2*pi) - pi);
                    
                    differences(neighbors.positions == positionIndex) = [];
                    differences(isnan(differences) | isinf(differences)) = [];
                    directionDifferences(positionIndex) = mean(differences);
                else
                    continue;
                end
            end
        end
        
        function neighbors = GetElectrodeNeighbors(electrodePositions, electrodeActivations, electrodeIndex, radius)
            electrodePosition = electrodePositions(electrodeIndex, : );
            positionDifferences = bsxfun(@minus, electrodePositions, electrodePosition);
            electrodeDistances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
            validNeigbors = electrodeDistances <= radius;
            
            neighbors = struct(...
                'positions', find(validNeigbors),...
                'activations', {electrodeActivations(validNeigbors)},...
                'distances', electrodeDistances(validNeigbors));
        end
        
        function memberships = ChangeMembership(memberships, oldValue, newValue)
            memberships(memberships == oldValue) = newValue;
        end
        
        function SetWaveType(waves, edgeElectrodes)
            for waveIndex = 1:numel(waves)
                if(waves(waveIndex).IsPeripheral(edgeElectrodes))
                    waves(waveIndex).Peripheral = true;
                else
                    waves(waveIndex).Peripheral = false;
                end
            end
        end
        
        function electrodeActivations = GetActivationData(ecgData, deflectionData)
            electrodeActivations = cell(ecgData.GetNumberOfChannels(), 1);
            timeRange = ecgData.GetTimeRange() * 1000;
            for channelIndex = 1:ecgData.GetNumberOfChannels()    
                    activationIndices = deflectionData{channelIndex}.templatePeakIndices(deflectionData{channelIndex}.PrimaryDeflections);                
                electrodeActivations{channelIndex} = timeRange(activationIndices);
            end
        end
        
        function memberships = GetMemberships(waves, electrodeActivations)
            memberships = cell(size(electrodeActivations));
            for electrodeIndex = 1:numel(memberships)
                memberships{electrodeIndex} = NaN(size(electrodeActivations{electrodeIndex}));
            end
            
            for waveIndex = 1:numel(waves)
                currentWave = waves(waveIndex);
                for memberIndex = 1:currentWave.Size()
                    memberships{currentWave.Members(memberIndex, 1)}(currentWave.Members(memberIndex, 2)) = currentWave.ID;
                end
            end
        end
        
        function startingPoints = GetStartingPoints(waves, electrodeActivations)
            startingPoints = cell(size(electrodeActivations));
            for electrodeIndex = 1:numel(startingPoints)
                startingPoints{electrodeIndex} = false(size(electrodeActivations{electrodeIndex}));
            end
            
            for waveIndex = 1:numel(waves)
                waveStartingPoints = waves(waveIndex).ComputeAllStartingPoints();
                for startIndex = 1:size(waveStartingPoints, 1)
                    startingPoints{waveStartingPoints(startIndex, 1)}(waveStartingPoints(startIndex, 2)) = true;
                end
            end
        end
        
        function sortedWaves = SortWaves(waves)
            waveMinimumStart = NaN(numel(waves), 1);
            for waveIndex = 1:numel(waves)
               waveStartingPoints = waves(waveIndex).GetAllStartingPoints();
               waveMinimumStart(waveIndex) = min(waveStartingPoints(:, 3));
            end
            
            [sortedStartingPoints sortIndex] = sort(waveMinimumStart); %#ok<ASGLU>
            sortedWaves = waves(sortIndex);
        end
        
        function waveOverlap = ComputeWaveOverlapMatrix(startPointWaveMembers)
            numberOfWaves = size(startPointWaveMembers, 2);
            waveOverlap = zeros(numberOfWaves);
           for pointIndex = 1:numberOfWaves
               waveOverlap(pointIndex, :) = sum(bsxfun(@and, startPointWaveMembers, startPointWaveMembers(:, pointIndex)) /...
                   sum(startPointWaveMembers(:, pointIndex)));
           end
        end
        
        function [startingPoints, startPointComponentMembers] = GetComponentStartingPoints(component)
            % determine isolated starting regions
            [startingPoints, startingPointIndices] = component.ComputeAllStartingPoints();
            startPointconnectivityMatrix = component.ConnectivityMatrix(startingPointIndices, startingPointIndices);
%             [numberOfStartingPoints, pointStartIndex] = graphconncomp(startPointconnectivityMatrix, 'weak', true);
            % change for > R2022a
            [pointStartIndex, componentSize] = conncomp(digraph(startPointconnectivityMatrix), 'Type', 'weak');
            numberOfStartingPoints = numel(componentSize);
            %
            
            mergedStartingPointIndices = cell(numberOfStartingPoints, 1);
            for pointIndex = 1:numberOfStartingPoints
                startPointMembers = pointStartIndex == pointIndex;
                mergedStartingPointIndices{pointIndex} = startingPointIndices(startPointMembers);
            end
            startingPointIndices = mergedStartingPointIndices;
            
            if nargout > 1
                % determine reachable activations for each starting point
                startPointComponentMembers = false(component.Size, numberOfStartingPoints);
                for pointIndex = 1:numberOfStartingPoints
%                     memberIndices = graphtraverse(component.ConnectivityMatrix, startingPointIndices{pointIndex}(1));
                    % Change for > R2022a
                    memberIndices = bfsearch(digraph(component.ConnectivityMatrix), startingPointIndices{pointIndex}(1));
                    %
                    startPointComponentMembers(memberIndices, pointIndex) = true;
                    startPointComponentMembers(startingPointIndices{pointIndex}, pointIndex) = false;
                end
            end
        end

        function [endPoints, endPointIndices] = GetComponentEndPoints(component)
            % determine isolated end regions: reverse component connectivity graph
            component.ConnectivityMatrix = component.ConnectivityMatrix';
            [endPoints, endPointIndices] = component.ComputeAllStartingPoints();
            endTimes = endPoints(:, 3);
            endPointconnectivityMatrix = component.ConnectivityMatrix(endPointIndices, endPointIndices);
            [pointEndIndex, componentSize] = conncomp(digraph(endPointconnectivityMatrix), 'Type', 'weak');
            numberOfEndPoints = numel(componentSize);

            mergedEndPointIndices = cell(numberOfEndPoints, 1);
            mergedEndTimes = NaN(numberOfEndPoints, 1);
            for pointIndex = 1:numberOfEndPoints
                endPointMembers = pointEndIndex == pointIndex;
                mergedEndPointIndices{pointIndex} = endPointIndices(endPointMembers);
                mergedEndTimes(pointIndex) = min(endTimes(endPointMembers));
            end
            
            % restore component starting points
            component.ConnectivityMatrix = component.ConnectivityMatrix';
            component.ComputeAllStartingPoints();
        end
    end
end