classdef TemplateMatchingOptimizationFilter < UserInterfacePkg.IStatusChange & AlgorithmPkg.DeflectionDetectionResultsFilter
    properties
        AvailableParameters
        OptimizationProtocol
        Steps
    end
    methods
        function self = TemplateMatchingOptimizationFilter(availableParameters, optimizationProtocol)
            self.OptimizationProtocol = optimizationProtocol;
            self.AvailableParameters = availableParameters;
            self.Steps = 10;
        end
        
        function [filteredResult, filterSettings] = Apply(self, analysisResult)
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Optimizing filter settings...', 0, self));
            
            if iscell(analysisResult)
                filteredResult = cell(1, numel(analysisResult));
                filterSettings = cell(1, numel(analysisResult));
                for i = 1 : numel(analysisResult)
                    [filteredResult{i}, filterSettings{i}] = self.ApplySingle(analysisResult{i});
                    if mod(i, 50) == 0
                        progress = i / numel(analysisResult);
                        UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                            UserInterfacePkg.StatusChangeEventData('Optimizing filter settings', progress, self));
                    end
                end
            else
                [filteredResult, filterSettings] = self.ApplySingle(analysisResult);
            end
            
            progress = 1;
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Done', progress, self));
        end
    end
    
    methods (Access = private)
        function DetectOptimumSetting(self, filterSetting, parameter, stepsize, analysisResult, percentage, count)
            cycleLengthCutoffValue = ApplicationSettings.Instance.DeflectionClustering.CycleLengthCutoff;
            if (count < 6)
                cycleLengths = []; %#ok<NASGU>
                cycleLengthFlag = 0;
                for iStep = 1:self.Steps
                    filterSetting.(parameter) = filterSetting.(parameter) + stepsize;
                    deflectionFilter = AlgorithmPkg.DeflectionFilter(filterSetting);
                    filterResults = deflectionFilter.Apply(analysisResult);
                    sampleFrequency = analysisResult.samplingFrequency;
                    cycleLengths = diff(double(filterResults.templatePeakIndices)) /sampleFrequency ;
                    percentageAboveCutoff = 100 * (numel(find(cycleLengths >  cycleLengthCutoffValue)) / numel(cycleLengths));
                    if (isempty(cycleLengths) || (max(cycleLengths) > cycleLengthCutoffValue && percentage == 0) || ...
                            percentageAboveCutoff > percentage)
                        cycleLengthFlag = 1;
                        break;
                    end
                end
                
                if (cycleLengthFlag == 1)
                    filterSetting.(parameter) = filterSetting.(parameter) - stepsize; % start value
                    stepsize = stepsize / self.Steps;
                    self.DetectOptimumSetting(filterSetting, parameter, stepsize, analysisResult, percentage, count + 1);
                end
            end
        end
        
        function [filteredResult, optimumFilterSettings] = ApplySingle(self, analysisResult)
            if (~isempty(analysisResult))
                optimumFilterSettings = self.DetermineOptimumFilterSettings(analysisResult);
                deflectionFilter = AlgorithmPkg.DeflectionFilter(optimumFilterSettings);
                filteredResult = deflectionFilter.Apply(analysisResult);
            else
                optimumFilterSettings = [];
                filteredResult = [];
            end
        end
        
        function filterSetting = DetermineOptimumFilterSettings(self, analysisResult)
            filterSetting = FilterSettings();

            % Set initial filtersetting and stepsize according to min/max (depending on optimization direction)
            for parameterIndex = 1 : numel(self.AvailableParameters)
                validValues = analysisResult.(self.AvailableParameters(parameterIndex).AnalysisResultName);
%                 peaks = analysisResult.templatePeakIndices;
%                 validValues = values(peaks);
                
                self.AvailableParameters(parameterIndex).Min = min(validValues);
                self.AvailableParameters(parameterIndex).Max = max(validValues);
                self.AvailableParameters(parameterIndex).StepSize = (self.AvailableParameters(parameterIndex).Max -...
                    self.AvailableParameters(parameterIndex).Min)/self.Steps;
                
                if strcmp(self.AvailableParameters(parameterIndex).Direction, 'increase');
                    filterSetting.(self.AvailableParameters(parameterIndex).FilterSettingName) = self.AvailableParameters(parameterIndex).Min -...
                        self.AvailableParameters(parameterIndex).StepSize;
                else
                    filterSetting.(self.AvailableParameters(parameterIndex).FilterSettingName) = self.AvailableParameters(parameterIndex).Max +...
                        self.AvailableParameters(parameterIndex).StepSize;
                    self.AvailableParameters(parameterIndex).StepSize = - self.AvailableParameters(parameterIndex).StepSize;
                end
            end
            %
            
            for parameterIndex = 1 : numel(self.OptimizationProtocol)
                if strcmp(self.OptimizationProtocol(parameterIndex).Action , 'Fixed')
                    filterSetting.(self.OptimizationProtocol(parameterIndex).Parameter) = self.OptimizationProtocol(parameterIndex).Value;
                else
                    self.DetectOptimumSetting(filterSetting, ...
                        self.OptimizationProtocol(parameterIndex).Parameter, ...
                        self.AvailableParameters(self.OptimizationProtocol(parameterIndex).ParameterIndex).StepSize, ...
                        analysisResult, ...
                        self.OptimizationProtocol(parameterIndex).Percentage, ...
                        1);
                    filterSetting.(self.OptimizationProtocol(parameterIndex).Parameter) = filterSetting.(self.OptimizationProtocol(parameterIndex).Parameter) * self.OptimizationProtocol(parameterIndex).Factor;
                end
            end
        end
    end
end

