# README #

This README contains information about the MATLAB package library *CustomMatlabPackages*

### What is this repository for? ###

* Algorithms and user interface components that form the backbone of the analysis software framework. Also contains the batch analysis modules
* Version 1.0

### How do I get set up? ###

* Add the folder CustomMatlabPackages to the MATLAB path
* Dependencies: None

### Questions / contact ###

* [Stef Zeemering](mailto:s.zeemering@maastrichtuniversity.nl)
