classdef PropertyMapPanel < UserInterfacePkg.CustomPanel
    properties
        EcgData
        DeflectionData
        
        PropertyData
        PropertyRanges
        
        WindowLength
        WindowOverlap
    end
    
    properties (Access = private)
        ControlPanel
        WindowSlider
        PropertyMap
        PropertyIndex
    end
    
    properties (Constant)
        PROPERTIES = {'Fractionation Index'};
    end
    
    methods
        function self = PropertyMapPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            
            self.PropertyRanges = [0 10];
            self.PropertyIndex = 1;
            
            self.WindowLength = 1;
            self.WindowOverlap = 0.5;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            self.CreatePropertyMapPanel();
        end
        
        function SetECGData(self, ecgData, intrinsicDeflectionData)
            self.EcgData = ecgData;
            self.DeflectionData = intrinsicDeflectionData;
            
            self.SetPropertyData();
            
            self.PropertyMap.Show();
        end
    end
    
    methods (Access = private)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .9 1 .1]);
            
            settingsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 .2 1 .8],...
                'title', 'Settings');
            
            uicontrol('parent', settingsPanel,...
                'units', 'normalized',...
                'position', [0 0 .3 1],...
                'style', 'popupmenu',...
                'string', AnalysisUIPkg.PropertyMapPanel.PROPERTIES,...
                'value', self.PropertyIndex,...
                'callback', @self.SetProperty);
            
            propertyRangePanel = uipanel('parent', settingsPanel,...
                'units', 'normalized',...
                'position', [.3 .1 .3 .8],...
                'title', 'Property range');
            uicontrol('parent', propertyRangePanel,...
                'units', 'normalized',...
                'position', [0 0 .5 1],...
                'style', 'edit',...
                'string', num2str(self.PropertyRanges(self.PropertyIndex, 1)),...
                'backgroundColor', [1 1 1],...
                'callback', @self.SetPropertyRangeStart);
            uicontrol('parent', propertyRangePanel,...
                'units', 'normalized',...
                'position', [.5 0 .5 1],...
                'style', 'edit',...
                'backgroundColor', [1 1 1],...
                'string', num2str(self.PropertyRanges(self.PropertyIndex, 2)),...
                'callback', @self.SetPropertyRangeEnd);
            
            windowSettingsPanel = uipanel('parent', settingsPanel,...
                'units', 'normalized',...
                'position', [.6 .1 .3 .8],...
                'title', 'Window length (s) & overlap (%)');
            uicontrol('parent', windowSettingsPanel,...
                'units', 'normalized',...
                'position', [0 0 .5 1],...
                'style', 'edit',...
                'backgroundColor', [1 1 1],...
                'string', num2str(self.WindowLength),...
                'callback', @self.SetWindowLength);
            uicontrol('parent', windowSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 0 .5 1],...
                'style', 'edit',...
                'backgroundColor', [1 1 1],...
                'string', num2str(self.WindowOverlap),...
                'callback', @self.SetWindowOverlap);
            
            self.WindowSlider = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.2 0 .7 .2],...
                'style', 'slider',...
                'callback', @self.WindowSliderCallback);
        end
        
        function CreatePropertyMapPanel(self)
            self.PropertyMap = ExtendedUIPkg.PropertyMapControl([0 0 1 .9]);
            self.PropertyMap.Create(self.ControlHandle);
        end
        
        function SetProperty(self, source, varargin)
            self.PropertyIndex = get(source, 'value');
            self.PropertyMap.SetFramePosition(1);
        end
        
        function SetPropertyRangeStart(self, source, varargin)
            self.PropertyRanges(self.PropertyIndex, 1) = str2double(get(source, 'string'));
            self.PropertyMap.SetDataRange(self.PropertyRanges(self.PropertyIndex, :));
        end
        
        function SetPropertyRangeEnd(self, source, varargin)
            self.PropertyRanges(self.PropertyIndex, 2) = str2double(get(source, 'string'));
            self.PropertyMap.SetDataRange(self.PropertyRanges(self.PropertyIndex, :));
        end
        
        function SetWindowLength(self, source, varargin)
            value = str2double(get(source, 'string'));
            time = self.EcgData.GetTimeRange();
            if value > 0 && value <= time(end)
                self.WindowLength = value;
                self.SetPropertyData();
            end
            set(source, 'string', num2str(self.WindowLength));
        end
        
        function SetWindowOverlap(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0 && value < 1
                self.WindowOverlap = value;
                self.SetPropertyData();
            end
            set(source, 'string', num2str(self.WindowOverlap));
        end
        
        function WindowSliderCallback(self, source, varargin)
            value = round(get(source, 'value'));
            self.PropertyMap.SetFramePosition(value);
        end
        
        function SetPropertyData(self)
            [data windowCenter] = self.GetPropertyData();
            if numel(windowCenter) > 1
                sliderStep = 1 / (numel(windowCenter) -1);
            else
                sliderStep = 0;
            end
            
            set(self.WindowSlider,...
                'min', 1,...
                'max', numel(windowCenter),...
                'sliderStep', [sliderStep, sliderStep],...
                'value', 1);
            self.PropertyData{1} = data;
            self.PropertyMap.SetDataRange(self.PropertyRanges(self.PropertyIndex, :));
            self.PropertyMap.SetData(self.PropertyData{1}, self.EcgData.ElectrodePositions);
            
            if numel(windowCenter) > 1
                propertyCorrelation = corrcoef(data);
                disp('Window correlation matrix: ');
                disp(propertyCorrelation);
            end
        end
        
        function [data windows] = GetPropertyData(self)
            switch self.PropertyIndex
                case 1
                    result = AlgorithmPkg.WavemapAnalyzer.ComputeWindowedFractionation(self.DeflectionData.AnalysisResults,...
                        self.WindowLength, (1 - self.WindowOverlap) * self.WindowLength);
                    data = result.ChannelFI;
                    windows = result.windowCenter;
                otherwise
                    data = NaN(size(self.EcgData.ElectrodeLabels));
                    windows = 1;
            end
        end
    end
end