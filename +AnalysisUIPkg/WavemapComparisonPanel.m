classdef WavemapComparisonPanel < UserInterfacePkg.CustomPanel
    properties (Access = private)        
        ControlPanel
        ResultPanel
        Comparer
    end
    
    methods
        function self = WavemapComparisonPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            self.Comparer = AlgorithmPkg.WavemapComparer();
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            self.CreateResultPanel();
        end
        
        function SetWavemapData(self, wavemapData1, wavemapData2, electrodePositions1, electrodePositions2)
            self.Comparer.SetData(wavemapData1, wavemapData2, electrodePositions1, electrodePositions2);
            self.ShowComparison();
        end
    end
    
    methods (Access = private)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .9 1 .1]);
        end
        
        function CreateResultPanel(self)
            self.ResultPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 0 1 .9]);
        end
        
        function ShowComparison(self)
            result = self.Comparer.Compare();
            
            delete(get(self.ResultPanel, 'children'));
            
            % number of waves
            plotHandle = subplot(2, 3, [1 4], 'parent', self.ResultPanel);
            hold(plotHandle, 'on');
            bar(plotHandle, 1, result.waves1.numberOfWaves, 'r');
            bar(plotHandle, 2, result.waves2.numberOfWaves, 'b');
            set(plotHandle, 'xTick', [1 2], 'xTickLabel', {'Data I', 'Data II'});
            title(plotHandle, 'Number of waves');
            hold(plotHandle, 'off');
            
            % wave size
            sizeBins = 1:max([result.waves1.waveSizes(:); result.waves2.waveSizes(:)]);
            plotHandle = subplot(2, 3, 2, 'parent', self.ResultPanel);
            [binCounts, binPositions] = hist(result.waves1.waveSizes, sizeBins);
            bar(plotHandle, binPositions, binCounts,...
                'barLayout', 'grouped',...
                'barWidth', 1,...
                'edgeColor', [0 0 0],...
                'faceColor', [1 0 0]);
            title(plotHandle, 'Wave sizes (Data I)');
            xlabel(plotHandle, 'size (# electrodes)')
            
            plotHandle = subplot(2, 3, 5, 'parent', self.ResultPanel);
            [binCounts, binPositions] = hist(result.waves2.waveSizes, sizeBins);
            bar(plotHandle, binPositions, binCounts,...
                'barLayout', 'grouped',...
                'barWidth', 1,...
                'edgeColor', [0 0 0],...
                'faceColor', [0 0 1]);
            title(plotHandle, 'Wave sizes (Data II)');
            xlabel(plotHandle, 'size (# electrodes)')
            
            % wave velocity
            velocityBins = 0:5:200;
            plotHandle = subplot(2, 3, 3, 'parent', self.ResultPanel);
            [binCounts, binPositions] = hist(100 * result.waves1.waveVelocities, velocityBins);
            bar(plotHandle, binPositions, binCounts,...
                'barLayout', 'grouped',...
                'barWidth', 1,...
                'edgeColor', [0 0 0],...
                'faceColor', [1 0 0]);
            title(plotHandle, 'Wave velocity (Data I)');
            xlabel(plotHandle, 'velocity (cm/s)')
            set(plotHandle, 'xLim', [0 200]);
            
            plotHandle = subplot(2, 3, 6, 'parent', self.ResultPanel);
            [binCounts, binPositions] = hist(100 * result.waves2.waveVelocities, velocityBins);
            bar(plotHandle, binPositions, binCounts,...
                'barLayout', 'grouped',...
                'barWidth', 1,...
                'edgeColor', [0 0 0],...
                'faceColor', [0 0 1]);
            title(plotHandle, 'Wave velocity (Data II)');
            xlabel(plotHandle, 'velocity (cm/s)')
            set(plotHandle, 'xLim', [0 200]);
        end
    end
end