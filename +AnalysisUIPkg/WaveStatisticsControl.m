classdef WaveStatisticsControl < UserInterfacePkg.CustomPanel
    properties (Access = private)
        WaveData
        RelevantPValues
        
        Preferentialities
        Anisotropies
        
        %% GUI controls
        MainPanelVisibleListener
        MouseMovementListener
        MouseUpListener
        
        WaveSizeModule
        WaveSizePlotPanel
        WaveWidthModule
        WaveWidthPlotPanel
        PreferentialityModule
        
        WidthPTextHandles
        WidthPLineHandles
        SizePTextHandles
        SizePLineHandles
        
        WidthPlotHandles
        SizePlotHandles
        
        WidthPlotLimits
        SizePlotLimits
        
        WidthHeterogenityText
        SizeHeterogenityText
        
        %Anisotropy-Bundle-Preferentiality Control
        ABPControl1
        ABPControl2
        CorrelationControl
        CorrelationListeners
        
        WaveInfoText
    end
    
    properties(Constant)
       P_TEXT_RELATIVE_HEIGHT = 0.8; 
    end
    
    methods
        function self = WaveStatisticsControl(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            
            self.RelevantPValues = [5, 50, 75, 90];
            self.Preferentialities = [];
            self.Anisotropies = [];
            self.WidthPlotLimits = zeros(2);
            self.SizePlotLimits = zeros(2);
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.BuildGUI();
        end
        
        function SetWaveData(self, waveData)
            self.WaveData = waveData;
        end
    end
    
    methods
        function BuildGUI(self)
            self.MainPanelVisibleListener = addlistener(self.ControlHandle, 'Visible', 'PostSet', @self.MainPanelVisibilitySwitchedCallback);
            self.MouseMovementListener = addlistener(UserInterfacePkg.MouseEventClass.Instance(), 'MouseMotion', @self.PlotUpkeep);
            self.MouseMovementListener.Enabled = false;
            self.MouseUpListener = addlistener(UserInterfacePkg.MouseEventClass.Instance(), 'MouseUp', @self.PlotUpkeep);
            self.MouseUpListener.Enabled = false;
            
            controlPanel = uipanel(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [.8 .5 .2 .5],...
                'UserData', 'WaveStatisticsControl.controlPanel');
            waveInfoPanel = uipanel(...
                'parent', controlPanel,...
                'units', 'normalized',...
                'position', [0 .2 1 .8],...
                'title', 'General info',...
                'UserData', 'WaveStatisticsControl.waveInfoPanel');
            self.WaveInfoText = uicontrol(...
                'style', 'text',...
                'parent', waveInfoPanel,...
                'units', 'normalized',...
                'position', [.1 .1 .8 .8],...
                'horizontalAlignment', 'left',...
                'fontName', 'fixedWidth',...
                'fontSize', 8);
            uicontrol(...
                'style', 'pushbutton',...
                'parent', controlPanel,...
                'units', 'normalized',...
                'position', [0 0 1 .1],...
                'string', 'Compute',...
                'callback', @self.ComputeStatistics);
            
            self.WaveSizeModule = uipanel(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [0 .5 .4 .5],...
                'title', 'Wave size',...
                'UserData', 'WaveStatisticsControl.WaveSizeModule');
            self.WaveWidthModule = uipanel(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [.4 .5 .4 .5],...
                'title', 'Wave width',...
                'UserData', 'WaveStatisticsControl.WaveWidthModule');
            self.WaveSizePlotPanel = uipanel(...
                'parent', self.WaveSizeModule,...
                'units', 'normalized',...
                'position', [0 0 .8 1],...
                'borderType', 'none',...
                'UserData', 'WaveStatisticsControl.WaveSizePlotPanel');
            self.WaveWidthPlotPanel = uipanel(...
                'parent', self.WaveWidthModule,...
                'units', 'normalized',...
                'position', [0 0 .8 1],...
                'borderType', 'none',...
                'UserData', 'WaveStatisticsControl.WaveWidthPlotPanel');
            
            self.WidthHeterogenityText = uicontrol(...
                'style', 'text',...
                'parent', self.WaveWidthModule,...
                'units', 'normalized',...
                'position', [.8 0 .2 1],...
                'horizontalAlignment', 'left',...
                'fontName', 'fixedWidth',...
                'fontSize', 8);
            self.SizeHeterogenityText = uicontrol(...
                'style', 'text',...
                'parent', self.WaveSizeModule,...
                'units', 'normalized',...
                'position', [.8 0 .2 1],...
                'horizontalAlignment', 'left',...
                'fontName', 'fixedWidth',...
                'fontSize', 8);
            
            self.PreferentialityModule = uipanel(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [0 0 .7 .5],...
                'title', '',...
                'borderType', 'none',...
                'UserData', 'WaveStatisticsControl.PreferentialityModule');

            self.ABPControl1 = PreferentialityAnisotropyBundleControl([0 0 0.5 1.0]);
            self.ABPControl1.Create(self.PreferentialityModule);
            
            self.ABPControl2 = PreferentialityAnisotropyBundleControl([0.5 0 0.5 1.0]);
            self.ABPControl2.Create(self.PreferentialityModule);
            
            self.CorrelationControl = CorrelatorControl([.7 0 .3 .5]);
            self.CorrelationControl.Create(self.ControlHandle);
            
            self.CorrelationListeners = event.listener.empty(2, 0);
            self.CorrelationListeners(1) = addlistener(self.ABPControl1, 'SelectionChanged', @self.SetCorrelationXData);
            self.CorrelationListeners(2) = addlistener(self.ABPControl2, 'SelectionChanged', @self.SetCorrelationYData);
        end
        
        function SetCorrelationXData(self, ~, eventData)
            self.CorrelationControl.SetXCorrelationData(eventData.CorrelationData);
        end
        
        function SetCorrelationYData(self, ~, eventData)
            self.CorrelationControl.SetYCorrelationData(eventData.CorrelationData);
        end
        
        function ComputeStatistics(self, varargin)
            if isempty(self.WaveData)
                return;
            end
            self.ClearHistogramLines();
            self.ShowWaveInfo();
            self.ShowWaveSizeStatistics();
            self.ShowWaveWidthStatistics();
            self.RefreshLines();
            self.RefreshTexts();
            self.ComputePreferentialityAndAnisotropy();
            self.ShowPreferentiality();
        end
        
        function ComputePreferentialityAndAnisotropy(self)
            velocityAnalyzer = VelocityAnalyzer();
            velocityAnalyzer.SupranormalThreshold = ParameterSettings.Instance().SupranormalThreshold;
            
            self.Preferentialities =...
                velocityAnalyzer.ComputePreferentialVelocities(...
                self.WaveData.ElectrodeMap,...
                self.WaveData.Velocities);
            
            self.Anisotropies = velocityAnalyzer.computeElectrodesAnisotropy(...
                self.WaveData.ElectrodeMap,...
                self.WaveData.Velocities);
            
            self.ABPControl1.SetData(...
                self.Preferentialities, self.Anisotropies, self.WaveData.Filename, self.WaveData.Pathname);
            self.ABPControl2.SetData(...
                self.Preferentialities, self.Anisotropies, self.WaveData.Filename, self.WaveData.Pathname);
        end
        
        function ShowWaveInfo(self)
            progressPosition = 0;
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Computing wave info', progressPosition, self));
                        
            waveIDs = self.WaveData.GetNonEmptyWaves();
            numberOfWaves = numel(waveIDs);
            
            [peripheralWaveIDs breakthroughWaveIDs discontinuousConductionWavesIDs] = ...
                self.WaveData.GetWaveTypes();
            numberOfPeripheralWaves = numel(waveIDs(ismember(waveIDs, peripheralWaveIDs)));
            numberOfBreakthroughWaves = numel(waveIDs(ismember(waveIDs, breakthroughWaveIDs)));
            numberOfDiscontinuousConductionWaves = numel(waveIDs(ismember(waveIDs, discontinuousConductionWavesIDs)));
            
            [epiWaveIDs endoWaveIDs] = self.WaveData.GetEpiAndEndoWaveIDs();
            numberOfEpiWaves = numel(waveIDs(ismember(waveIDs, epiWaveIDs)));
            numberOfPeripheralEpiWaves = numel(epiWaveIDs(ismember(epiWaveIDs, peripheralWaveIDs)));
            numberOfBreakthroughEpiWaves = numel(epiWaveIDs(ismember(epiWaveIDs, breakthroughWaveIDs)));
            numberOfDiscontinuousConductionEpiWaves = numel(epiWaveIDs(ismember(epiWaveIDs, discontinuousConductionWavesIDs)));
            
            numberOfEndoWaves = numel(waveIDs(ismember(waveIDs, endoWaveIDs)));
            numberOfPeripheralEndoWaves = numel(endoWaveIDs(ismember(endoWaveIDs, peripheralWaveIDs)));
            numberOfBreakthroughEndoWaves = numel(endoWaveIDs(ismember(endoWaveIDs, breakthroughWaveIDs)));
            numberOfDiscontinuousConductionEndoWaves = numel(endoWaveIDs(ismember(endoWaveIDs, discontinuousConductionWavesIDs)));
            
            waveInfoText = {...
                ['Total number of waves: ' num2str(numberOfWaves)];...
                ['Peripheral: ' num2str(numberOfPeripheralWaves) ' (' num2str(100 * numberOfPeripheralWaves / numberOfWaves, '%.1f') '%)'];...
                ['Breakthrough: ' num2str(numberOfBreakthroughWaves) ' (' num2str(100 * numberOfBreakthroughWaves / numberOfWaves, '%.1f') '%)'];...
                ['Discontinuous conduction: ' num2str(numberOfDiscontinuousConductionWaves) ' (' num2str(100 * numberOfDiscontinuousConductionWaves / numberOfWaves, '%.1f') '%)'];...
                '';...
                ['Epicardial: ' num2str(numberOfEpiWaves) ' (' num2str(100 * numberOfEpiWaves / numberOfWaves, '%.1f') '%)'];...
                ['Peripheral: ' num2str(numberOfPeripheralEpiWaves) ' (' num2str(100 * numberOfPeripheralEpiWaves / numberOfWaves, '%.1f') '%)'];...
                ['Breakthrough: ' num2str(numberOfBreakthroughEpiWaves) ' (' num2str(100 * numberOfBreakthroughEpiWaves / numberOfWaves, '%.1f') '%)'];...
                ['Discontinuous conduction: ' num2str(numberOfDiscontinuousConductionEpiWaves) ' (' num2str(100 * numberOfDiscontinuousConductionEpiWaves / numberOfWaves, '%.1f') '%)'];...
                '';...
                ['Endocardial: ' num2str(numberOfEndoWaves) ' (' num2str(100 * numberOfEndoWaves / numberOfWaves, '%.1f') '%)'];...
                ['Peripheral: ' num2str(numberOfPeripheralEndoWaves) ' (' num2str(100 * numberOfPeripheralEndoWaves / numberOfWaves, '%.1f') '%)'];...
                ['Breakthrough: ' num2str(numberOfBreakthroughEndoWaves) ' (' num2str(100 * numberOfBreakthroughEndoWaves / numberOfWaves, '%.1f') '%)'];...
                ['Discontinuous conduction: ' num2str(numberOfDiscontinuousConductionEndoWaves) ' (' num2str(100 * numberOfDiscontinuousConductionEndoWaves / numberOfWaves, '%.1f') '%)']};
            set(self.WaveInfoText, 'string', waveInfoText);
            
            progressPosition = 1;
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('', progressPosition, self));
        end
        
        function ShowWaveSizeStatistics(self)
            hold on
            
            progressPosition = 0;
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Computing wave size statistics', progressPosition, self));
            
            panelChildren = get(self.WaveSizePlotPanel, 'Children');
            if numel(panelChildren) > 0
                delete(panelChildren);
            end
            
            epiWavesPlotHandle = subplot(3,1,1, 'parent', self.WaveSizePlotPanel, 'UserData', 'epiSizePlotHandle');
            endoWavesPlotHandle = subplot(3,1,2, 'parent', self.WaveSizePlotPanel, 'UserData', 'endoSizePlotHandle');
            allWavesPlotHandle = subplot(3,1,3, 'parent', self.WaveSizePlotPanel, 'UserData', 'allSizePlotHandle');
           
            
            [waveIDs waveSizes] = self.WaveData.GetNonEmptyWaves();
            sizeBins = 1:max(waveSizes(:));
            
            [epiWaveIDs endoWaveIDs] = self.WaveData.GetEpiAndEndoWaveIDs();
            
            epiWaveSizes = waveSizes(ismember(waveIDs, epiWaveIDs));
            [count positions textHandles lineHandles] = ...
                WaveStatisticsControl.CreateHistogram(epiWavesPlotHandle, epiWaveSizes, sizeBins, self.RelevantPValues); %#ok<ASGLU>
            title(epiWavesPlotHandle, 'Epicardial waves');
            self.SizePTextHandles(1, :) = textHandles;
            self.SizePLineHandles(1, :) = lineHandles;
            self.SizePlotHandles(1) = epiWavesPlotHandle;
            
            endoWaveSizes = waveSizes(ismember(waveIDs, endoWaveIDs));
            [count positions textHandles lineHandles] = ...
                WaveStatisticsControl.CreateHistogram(endoWavesPlotHandle, endoWaveSizes, sizeBins, self.RelevantPValues); %#ok<ASGLU>
            title(endoWavesPlotHandle, 'Endocardial waves');
            self.SizePTextHandles(2, :) = textHandles;
            self.SizePLineHandles(2, :) = lineHandles;
            self.SizePlotHandles(2) = endoWavesPlotHandle;
            
            [count positions textHandles lineHandles] = ...
                WaveStatisticsControl.CreateHistogram(allWavesPlotHandle, waveSizes, sizeBins, self.RelevantPValues);     %#ok<ASGLU>
            title(allWavesPlotHandle, 'All waves');
            xlabel(allWavesPlotHandle, 'Size (number of electrodes)');
            self.SizePTextHandles(3, :) = textHandles;
            self.SizePLineHandles(3, :) = lineHandles;
            self.SizePlotHandles(3) = allWavesPlotHandle;
            
            linkaxes([allWavesPlotHandle, epiWavesPlotHandle, endoWavesPlotHandle ]);
            
            self.SizePlotLimits(1,:) = get(epiWavesPlotHandle, 'XLim');
            self.SizePlotLimits(2,:) = get(epiWavesPlotHandle, 'YLim');
            
            hEpi = WaveStatisticsControl.GetHeterogenityIndex(epiWaveSizes(~isnan(epiWaveSizes)));
            hEndo = WaveStatisticsControl.GetHeterogenityIndex(endoWaveSizes(~isnan(endoWaveSizes)));
            hAll = WaveStatisticsControl.GetHeterogenityIndex(waveSizes(~isnan(waveSizes)));
            
            WaveStatisticsControl.SetHeterogenityText(self.SizeHeterogenityText, hEpi, hEndo, hAll);

            progressPosition = 1;
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('', progressPosition, self));
        end       
        
        function ShowWaveWidthStatistics(self)
            hold on;
            
            progressPosition = 0;
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Computing wave width statistics', progressPosition, self));
            
            panelChildren = get(self.WaveWidthPlotPanel, 'Children');
            if numel(panelChildren) > 0
                delete(panelChildren);
            end
            
            waveIDs = self.WaveData.GetNonEmptyWaves();
            waveWidths = self.WaveData.GetWaveWidth(waveIDs);
            widthBins = 1:max(waveWidths(:));
            
            [epiWaveIDs endoWaveIDs] = self.WaveData.GetEpiAndEndoWaveIDs();
            
            epiWaveWidths = waveWidths(ismember(waveIDs, epiWaveIDs));
            epiWavesPlotHandle = subplot(3,1,1, 'parent', self.WaveWidthPlotPanel, 'UserData', 'epiWidthPlotHandle');
            [count positions textHandles lineHandles] = ...
                WaveStatisticsControl.CreateHistogram(epiWavesPlotHandle, epiWaveWidths, widthBins, self.RelevantPValues); %#ok<ASGLU>
            title(epiWavesPlotHandle, 'Epicardial waves');
            self.WidthPTextHandles(1, :) = textHandles;
            self.WidthPLineHandles(1, :) = lineHandles;
            self.WidthPlotHandles(1) = epiWavesPlotHandle;
            
            endoWaveWidths = waveWidths(ismember(waveIDs, endoWaveIDs));
            endoWavesPlotHandle = subplot(3,1,2, 'parent', self.WaveWidthPlotPanel, 'UserData', 'endoWidthPlotHandle');
            [count positions textHandles lineHandles] = ...
                WaveStatisticsControl.CreateHistogram(endoWavesPlotHandle, endoWaveWidths, widthBins, self.RelevantPValues); %#ok<ASGLU>
            title(endoWavesPlotHandle, 'Endocardial waves');
            self.WidthPTextHandles(2, :) = textHandles;
            self.WidthPLineHandles(2, :) = lineHandles;
            self.WidthPlotHandles(2) = endoWavesPlotHandle;
            
            allWavesPlotHandle = subplot(3,1,3, 'parent', self.WaveWidthPlotPanel, 'UserData', 'allWidthPlotHandle');
            [count positions textHandles lineHandles] = ...
                WaveStatisticsControl.CreateHistogram(allWavesPlotHandle, waveWidths, widthBins, self.RelevantPValues);     %#ok<ASGLU>
            title(allWavesPlotHandle, 'All waves');
            xlabel(allWavesPlotHandle, 'Width (number of electrodes)');
            self.WidthPTextHandles(3, :) = textHandles;
            self.WidthPLineHandles(3, :) = lineHandles;
            self.WidthPlotHandles(3) = allWavesPlotHandle;
            
            linkaxes([allWavesPlotHandle, epiWavesPlotHandle, endoWavesPlotHandle]);
            
            self.WidthPlotLimits(1,:) = get(epiWavesPlotHandle, 'XLim');
            self.WidthPlotLimits(2,:) = get(epiWavesPlotHandle, 'YLim');
            
            hEpi = WaveStatisticsControl.GetHeterogenityIndex(epiWaveWidths(~isnan(epiWaveWidths)));
            hEndo = WaveStatisticsControl.GetHeterogenityIndex(endoWaveWidths(~isnan(endoWaveWidths)));
            hAll = WaveStatisticsControl.GetHeterogenityIndex(waveWidths(~isnan(waveWidths)));
            
            WaveStatisticsControl.SetHeterogenityText(self.WidthHeterogenityText, hEpi, hEndo, hAll);
            
            progressPosition = 1;
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('', progressPosition, self));
        end    
        
        function ClearHistogramLines(self)
            self.WidthPTextHandles = [];
            self.WidthPLineHandles = [];
            self.SizePTextHandles = [];
            self.SizePLineHandles = [];

            self.WidthPlotHandles = [];
            self.SizePlotHandles = [];

            self.WidthPlotLimits = [];
            self.SizePlotLimits = [];
        end
        
        function ShowPreferentiality(self, varargin)
            if numel(self.Anisotropies) == 0
               self.ComputePreferentialityAndAnisotropy();
            end

            self.ABPControl1.Display();
            self.ABPControl2.Display();
            
        end
        
        function RefreshLines(self, varargin)
           if strcmp(get(self.ControlHandle, 'Visible'), 'off') || numel(self.WidthPlotHandles) == 0
               return;
           end
           
           yWidthLim = get(self.WidthPlotHandles(1), 'YLim');
           
           for i = 1:numel(self.WidthPLineHandles)
               set(self.WidthPLineHandles(i), 'YData', yWidthLim);
           end
           
           ySizeLim = get(self.SizePlotHandles(1), 'YLim');
           
           for i = 1:numel(self.SizePLineHandles)
               set(self.SizePLineHandles(i), 'YData', ySizeLim);
           end
           
        end
        
        function RefreshTexts(self, varargin)
          if strcmp(get(self.ControlHandle, 'Visible'), 'off') || numel(self.WidthPlotHandles) == 0
               return;
          end                   
                   
          yWidthLim = get(self.WidthPlotHandles(1), 'YLim');
          xWidthLim = get(self.WidthPlotHandles(1), 'XLim');
          ySizeLim = get(self.SizePlotHandles(1), 'YLim');
          xSizeLim = get(self.SizePlotHandles(1), 'XLim');
          
          for i = 1:numel(self.WidthPTextHandles)
              position = get(self.WidthPTextHandles(i), 'position');
              lineLength = abs(yWidthLim(2) - yWidthLim(1));
              position(2) = yWidthLim(2) - lineLength * (1 - WaveStatisticsControl.P_TEXT_RELATIVE_HEIGHT);
              set(self.WidthPTextHandles(i), 'position', position);
              
              if position(1) < xWidthLim(1) || position(1) > xWidthLim(2)
                  set(self.WidthPTextHandles(i), 'Visible', 'off');
              else
                  set(self.WidthPTextHandles(i), 'Visible', 'on');
              end
          end
          
          for i = 1:numel(self.SizePTextHandles)
              position = get(self.SizePTextHandles(i), 'position');
              lineLength = abs(ySizeLim(2) - ySizeLim(1));
              position(2) = ySizeLim(2) - lineLength * (1 - WaveStatisticsControl.P_TEXT_RELATIVE_HEIGHT);
              set(self.SizePTextHandles(i), 'position', position);
              
              if position(1) < xSizeLim(1) || position(1) > xSizeLim(2)
                  set(self.SizePTextHandles(i), 'Visible', 'off');
              else
                  set(self.SizePTextHandles(i), 'Visible', 'on');
              end
          end
        end
             
        function LimitPlotPan(self, varargin)
            if strcmp(get(self.ControlHandle, 'Visible'), 'off') || numel(self.WidthPlotHandles) == 0
               return;
            end  
            
            currentWidthXLim = get(self.WidthPlotHandles(1), 'XLim');
            currentWidthYLim = get(self.WidthPlotHandles(1), 'YLim');
            
            if currentWidthXLim(1) < self.WidthPlotLimits(1,1)
                %too far to the left
                currentWidth = abs(currentWidthXLim(2) - currentWidthXLim(1));
                self.SetWidthPlotWidths(self.WidthPlotLimits(1,1), self.WidthPlotLimits(1,1) + currentWidth);
            end
            
            if currentWidthXLim(2) > self.WidthPlotLimits(1,2)
                %too far to the right
                currentWidth = abs(currentWidthXLim(2) - currentWidthXLim(1));
                self.SetWidthPlotWidths(self.WidthPlotLimits(1,2) - currentWidth,  self.WidthPlotLimits(1,2));
            end
            
            if currentWidthYLim(1) < self.WidthPlotLimits(2,1)
                %too far down
                currentHeight = abs(currentWidthYLim(2) - currentWidthYLim(1));
                self.SetWidthPlotHeights(self.WidthPlotLimits(2,1), self.WidthPlotLimits(2,1) + currentHeight);
            end
            
            if currentWidthYLim(2) > self.WidthPlotLimits(2,2)
                %too far up
                currentHeight = abs(currentWidthYLim(2) - currentWidthYLim(1));
                self.SetWidthPlotHeights(self.WidthPlotLimits(2,2) - currentHeight, self.WidthPlotLimits(2,2));
            end
            
            currentSizeXLim = get(self.SizePlotHandles(1), 'XLim');
            currentSizeYLim = get(self.SizePlotHandles(1), 'YLim');
            
            if currentSizeXLim(1) < self.SizePlotLimits(1,1)
                %too far to the left
                currentWidth = abs(currentSizeXLim(2) - currentSizeXLim(1));
                self.SetSizePlotWidths(self.SizePlotLimits(1,1), self.SizePlotLimits(1,1) + currentWidth);
            end
            
            if currentSizeXLim(2) > self.SizePlotLimits(1,2)
                %too far to the right
                currentWidth = abs(currentSizeXLim(2) - currentSizeXLim(1));
                self.SetSizePlotWidths(self.SizePlotLimits(1,2) - currentWidth,  self.SizePlotLimits(1,2));
            end
            
            if currentSizeYLim(1) < self.SizePlotLimits(2,1)
                %too far down
                currentHeight = abs(currentSizeYLim(2) - currentSizeYLim(1));
                self.SetSizePlotHeights(self.SizePlotLimits(2,1), self.SizePlotLimits(2,1) + currentHeight);
            end
            
            if currentSizeYLim(2) > self.SizePlotLimits(2,2)
                %too far up
                currentHeight = abs(currentSizeYLim(2) - currentSizeYLim(1));
                self.SetSizePlotHeights(self.SizePlotLimits(2,2) - currentHeight, self.SizePlotLimits(2,2));
            end
                        
        end 
        
        function MainPanelVisibilitySwitchedCallback(self, varargin)
            visibility = get(self.ControlHandle, 'Visible');
            
            if strcmp(visibility, 'off')
               self.MouseMovementListener.Enabled = false;
               self.MouseUpListener.Enabled = false;
            else
               self.MouseMovementListener.Enabled = true;
               self.MouseUpListener.Enabled = true;
            end
            
        end
        
        function PlotUpkeep(self, varargin)
            if strcmp(get(self.ControlHandle, 'Visible'), 'off') || numel(self.WidthPlotHandles) == 0
               return;
            end  
                        
           self.LimitPlotPan();
           self.RefreshLines();
           self.RefreshTexts();
        end
        
        function SetWidthPlotHeights(self, min, max)
            for i = 1:numel(self.WidthPlotHandles)
                set(self.WidthPlotHandles(i), 'YLim',[min  max]);
            end
        end
        
        function SetWidthPlotWidths(self, min, max)
            for i = 1:numel(self.WidthPlotHandles)
                set(self.WidthPlotHandles(i), 'XLim',[min  max]);
            end
        end
        
        function SetSizePlotHeights(self, min, max)
            for i = 1:numel(self.SizePlotHandles)
                set(self.SizePlotHandles(i), 'YLim',[min  max]);
            end
        end
        
        function SetSizePlotWidths(self, min, max)
            for i = 1:numel(self.SizePlotHandles)
                set(self.SizePlotHandles(i), 'XLim',[min  max]);
            end
        end
    end
    
    methods (Static)
        function [binCounts, binPositions, pTextHandles, pLineHandles] = CreateHistogram(plotHandle, data, histogramBins, pValues, pRelativeHeight)
            if nargin < 5
                pRelativeHeight = WaveStatisticsControl.P_TEXT_RELATIVE_HEIGHT;
            end
            
            data = data(~isnan(data));
            
            [binCounts, binPositions] = hist(data , histogramBins);
            bar(plotHandle, binPositions, binCounts,...
                'barLayout', 'grouped',...
                'barWidth', 1,...
                'edgeColor', [0 0 0],...
                'faceColor', [0 0 0]);
            
            if nargin > 3
                pIndexes = WaveStatisticsControl.GetPValue(data, pValues);
                yLimits = get(plotHandle, 'yLim');

                for i = 1:numel(pIndexes)
                    pLineHandles(1, i) = line('xData', [pIndexes(i) pIndexes(i)], 'yData', yLimits,...
                        'parent', plotHandle, 'color', [1 0 0 ]); %#ok<AGROW>
                end
                
                yMax = max(yLimits);
                yMin = min(yLimits);
                yLength = yMax - yMin; 
                for i = 1:numel(pIndexes)
                   pTextHandles(1, i) = text('position',...
                       [pIndexes(i), yMax - yLength * (1 - pRelativeHeight)], ...
                       'String', ['p' num2str(pValues(i))],...
                       'parent', plotHandle,...
                       'color', [1 0 0]);  %#ok<AGROW>
                end
                
                
            end
            
        end
        
        function pvalue = GetPValue(data, p)
            
            if max(p) > 1
                p = p./ 100;
            end
            
            data = sort(data);
            pvalue = zeros(size(p));
            
            for i = 1:size(p, 1)
                for j = 1:size(p, 2)
                   pvalue(i, j) = data(ceil(numel(data) * p(i, j)));                 
                end 
            end
        end
        
        function heterogenity = GetHeterogenityIndex(data)
           pIndexi = [5 50 95];
           
           pValues = WaveStatisticsControl.GetPValue(data, pIndexi);
           
           heterogenity = (pValues(3) - pValues(1)) / pValues(2);
            
        end
              
        function SetHeterogenityText(uiText, hEpi, hEndo, hAll)
           
            heterogenityText = {...
            'Heterogenity Index';...
            '';...
            '';...
            '';...
            'Epicardial';...
            num2str(hEpi);...
            '';...
            '';...
            '';...
            '';...
            '';...
            '';...
            '';...
            '';...
            'Endocardial';...
            num2str(hEndo);...
            '';...
            '';...
            '';...
            '';...
            '';...
            '';...
            '';...
            '';...
            '';...
            '';...
            'Combined';...
            num2str(hAll)};
            
            set(uiText, 'string', heterogenityText);
        end
        
    end
end