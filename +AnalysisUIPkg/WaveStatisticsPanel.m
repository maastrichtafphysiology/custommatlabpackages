classdef WaveStatisticsPanel < UserInterfacePkg.CustomPanel
    properties (Access = private)
        ControlPanel
        ResultPanel
        
        Waves
        EcgData
        Deflections
    end
    
    properties (Constant)
        VIEWS = {'General statistics'}
    end
    
    methods
        function self = WaveStatisticsPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            self.CreateResultPanel();
        end
        
        function SetWavemapData(self, ecgData, waves, deflections)
            self.Waves = waves;
            self.EcgData = ecgData;
            self.Deflections = deflections;
            
            self.ShowGeneralStatistics();
        end
    end
    
    methods (Access = private)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .9 1 .1]);
        end
        
        function CreateResultPanel(self)
            self.ResultPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 0 1 .9]);
        end
        
        function ShowGeneralStatistics(self)
            if isempty(self.EcgData) || isempty(self.Waves), return; end
            
            
            % Computation
            waveSize = AlgorithmPkg.WavemapAnalyzer.ComputeWaveSize(self.Waves);
            
            neighborRadius = ApplicationSettings.Instance.VelocityComputation.radius;
            
            [waveVelocity waveTortuosity] = AlgorithmPkg.WavemapAnalyzer.ComputeWaveDistributions(self.Waves, self.EcgData.ElectrodePositions, neighborRadius);
            
            [cycleLength intervals] = AlgorithmPkg.WavemapAnalyzer.ComputeAFCL(self.Waves, self.EcgData.ElectrodePositions);
            
            deflectionFractionationIndices = AlgorithmPkg.AFComplexityCalculator.ComputeFractionation(self.Deflections);
            
            % Visualization
            pValues = [5 50 95];
            waveNumbersPanel = uipanel('parent', self.ResultPanel,...
                'units', 'normalized', ...
                'position', [0 .5 1 .5]);
            axesHandles = NaN(1, 2);
            
            axesHandles(1) = subplot(1,3,1, 'parent', waveNumbersPanel);
            numberOfWaves = numel(self.Waves);
            numberOfPeripheral = numel(find([self.Waves.Peripheral]));
            numberOfBT = numel(find(~[self.Waves.Peripheral]));
            pie(axesHandles(1), [numberOfPeripheral, numberOfBT], {'Peripheral', 'Breakthrough'});
            title(axesHandles(1), 'Wave types');
            text('units', 'normalized', 'position', [0 0 0], 'parent', axesHandles(1), 'string', ['Total: ', num2str(numberOfWaves),...
                ' , Peripheral: ', num2str(numberOfPeripheral),...
                ' , Breakthrough: ', num2str(numberOfBT)]);
            
            axesHandles(2) = subplot(1,3,2, 'parent', waveNumbersPanel);
            sizeBins = 1:1:max(waveSize);
            AnalysisUIPkg.WaveStatisticsPanel.CreateHistogram(axesHandles(2), waveSize, sizeBins, pValues);
            title(axesHandles(2), 'Wave Size');
            xlabel(axesHandles(2), 'size (number of electrodes)');
            
            axesHandles(3) = subplot(1,3,3, 'parent', waveNumbersPanel);
            FIBins = 0:0.5:max(deflectionFractionationIndices);
            AnalysisUIPkg.WaveStatisticsPanel.CreateHistogram(axesHandles(3), deflectionFractionationIndices, FIBins, pValues);
            title(axesHandles(3), 'Electrogram fractionation');
            xlabel(axesHandles(3), 'FI');
            
            waveVelocityPanel = uipanel('parent', self.ResultPanel,...
                'units', 'normalized', ...
                'position', [0 0 1 .5]);
            axesHandles = NaN(1, 3);
            
            axesHandles(1) = subplot(1,3,1, 'parent', waveVelocityPanel);
            afclBins = 0:1:300;
            AnalysisUIPkg.WaveStatisticsPanel.CreateHistogram(axesHandles(1), intervals, afclBins, pValues);
            title(axesHandles(1), 'Cycle length');
            xlabel(axesHandles(1), 'interval length (ms)');
            
            axesHandles(2) = subplot(1,3,2, 'parent', waveVelocityPanel);
            velocityBins = 0:1:150;
            AnalysisUIPkg.WaveStatisticsPanel.CreateHistogram(axesHandles(2), 100 * waveVelocity, velocityBins, pValues);
            title(axesHandles(2), 'Wave velocity');
            xlabel(axesHandles(2), 'velocity (cm/s)');
            
            axesHandles(3) = subplot(1,3,3, 'parent', waveVelocityPanel);
            tortuosityBins = -90:5:90;
            AnalysisUIPkg.WaveStatisticsPanel.CreateHistogram(axesHandles(3), waveTortuosity, tortuosityBins, pValues);
            title(axesHandles(3), 'Wave tortuosity');
            xlabel(axesHandles(3), 'tortuosity (degrees)');
        end
    end
    
    methods (Static)
        function [binCounts, binPositions] = CreateHistogram(plotHandle, data, histogramBins, pValues)
            data = data(~isnan(data));
            
            [binCounts, binPositions] = hist(data , histogramBins);
            bar(plotHandle, binPositions, binCounts,...
                'barLayout', 'grouped',...
                'barWidth', 1,...
                'edgeColor', [0 0 0],...
                'faceColor', [0 0 0]);
            
            if nargin > 3
                pIndexes = prctile(data, pValues);
                yLimits = get(plotHandle, 'yLim');
                
                for i = 1:numel(pIndexes)
                    line('xData', [pIndexes(i) pIndexes(i)], 'yData', yLimits,...
                        'parent', plotHandle, 'color', [1 0 0 ]);
                end
                
                for i = 1:numel(pIndexes)
                    text('position', [pIndexes(i), yLimits(end)], ...
                        'string', ['p' num2str(pValues(i))],...
                        'parent', plotHandle,...
                        'color', [1 0 0]);
                end
                
                
            end
            set(plotHandle, 'xLim', histogramBins([1 end]));
            
            clipboardMenu = uicontextmenu;
            set(plotHandle, 'UIContextMenu', clipboardMenu);
            uimenu(clipboardMenu, 'Label', 'Copy data to clipboard', 'callback', @(self, source) UtilityPkg.num2clip(data(:)));
        end
    end
end