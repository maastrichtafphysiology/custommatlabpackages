classdef UnidirectionalConductionMapControl < ExtendedUIPkg.ConductionMapControl

    methods
        function self = UnidirectionalConductionMapControl(position)
            self = self@ExtendedUIPkg.ConductionMapControl(position);
        end
    end
    
    methods (Access = protected)
        function SetFrame(self)
            SetFrame@ExtendedUIPkg.PropertyMapControl(self);
            
            hold(self.MapAxes, 'on');
            arrowColor = [0 0 0];
            arrows = quiver(self.MapAxes, self.Positions(:, 1), self.Positions(:, 2),...
                cos(self.Angles), sin(self.Angles), 0.5);
            set(arrows, 'color', arrowColor);
            set(arrows, 'HitTest', 'off');
            hold(self.MapAxes, 'off');
        end
    end
end

