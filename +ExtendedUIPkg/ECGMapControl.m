classdef ECGMapControl < ExtendedUIPkg.MapControl
    properties (Access = protected)
        EcgData
        ElectrodeAxesIndices
        LaplacianData
        ECGLines
        LaplacianLines
    end
    
    methods
        function self = ECGMapControl(position)
            self = self@ExtendedUIPkg.MapControl(position);
            self.TimeLag = 0.1;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
%             set(self.ControlHandle, 'backgroundColor', [1 1 1]);
        end
        
        function SetData(self, ecgData)
            self.EcgData = ecgData;
            self.Positions = ecgData.ElectrodePositions;
            
            self.InitializeAxes();
            
            self.SetFrame();
        end
        
        
        function SetLaplacianData(self, data)
            self.LaplacianData = data;
            self.SetFrame();
        end
    end
    
    methods (Access = protected)
        function InitializeAxes(self)
            maxPosition = max(self.Positions, [], 1);
            minPosition = min(self.Positions, [], 1);
            minimumSpacing = self.ComputeMinimumSpacing();
            
            xData = minPosition(1):minimumSpacing:maxPosition(1);
            yData = minPosition(2):minimumSpacing:maxPosition(2);
            axesSpacing = .005;
            axesWidth = 1 / numel(xData);
            axesHeight = 1 / numel(yData);
            
            validHandles = ishandle(self.MapAxes);
            if any(validHandles(:))
                delete(self.MapAxes(validHandles));
            end
            
            validHandles = ishandle(self.ECGLines);
            if any(validHandles(:))
                delete(self.ECGLines(validHandles));
            end
            
            validHandles = ishandle(self.LaplacianLines);
            if any(validHandles(:))
                delete(self.LaplacianLines(validHandles));
            end
            
            self.MapAxes = NaN(numel(xData), numel(yData));
            self.ECGLines = NaN(numel(xData), numel(yData));
            for xIndex = 1:numel(xData)
                for yIndex = 1:numel(yData)
                    self.MapAxes(xIndex, yIndex) =...
                        axes('parent', self.ControlHandle,...
                        'units', 'normalized',...
                        'position', [...
                        (xIndex - 1) * axesWidth,...
                        (yIndex - 1) * axesHeight,...
                        axesWidth - axesSpacing, axesHeight - axesSpacing],...
                        'xTickLabel', {}, 'yTickLabel', {},...
                        'color', [0 0 0]);
                    
                    self.ECGLines(xIndex, yIndex) = line('xData', [], 'yData', [],...
                        'color', [1 1 1],...
                        'parent', self.MapAxes(xIndex, yIndex));
                    
                    self.LaplacianLines(xIndex, yIndex) = line('xData', [], 'yData', [],...
                        'color', [0 1 0],...
                        'parent', self.MapAxes(xIndex, yIndex));
                end
            end
            
            [xMembers, xIndices] = ismember(self.Positions(:, 1), xData); %#ok<ASGLU>
            [yMembers, yIndices] = ismember(self.Positions(:, 2), yData); %#ok<ASGLU>
            self.ElectrodeAxesIndices = sub2ind([numel(yData), numel(xData)], yIndices, xIndices);
        end
        
        function SetFrame(self)
            time = self.EcgData.GetTimeRange();
            
            timePosition = self.FramePosition / self.EcgData.SamplingFrequency;
            timeRange = [(timePosition - self.TimeLag), timePosition];
            validPositions = time >= timeRange(1) & time <= timeRange(end);
            time = time(validPositions);
            data = self.EcgData.Data(validPositions, :);
            for electrodeIndex = 1:self.EcgData.GetNumberOfChannels()
                set(self.ECGLines(self.ElectrodeAxesIndices(electrodeIndex)),...
                    'xData', time, 'yData', data(:, electrodeIndex));
                
                if isempty(self.LaplacianData), continue; end
                
                set(self.LaplacianLines(self.ElectrodeAxesIndices(electrodeIndex)),...
                    'xData', time, 'yData', self.LaplacianData(validPositions, electrodeIndex));
            end
            set(self.MapAxes, 'xLim', timeRange);
        end
    end
end