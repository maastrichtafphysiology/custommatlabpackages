classdef GridWavemapControl < ExtendedUIPkg.WavemapControl
    properties (Access = protected)
        WavemapImage
        ElectrodeImageIndices
        ElectrodeTextHandles
        HighlightedElectrodeIndices
        MinimumSpacing
        
        XData
        YData
    end
    
    methods
        function self = GridWavemapControl(position)
            self = self@ExtendedUIPkg.WavemapControl(position);
        end
        
        function colorData = GetFrame(self, framePosition)
            if isempty(self.Waves), return; end

            timePosition = 1000 * framePosition / self.SamplingFrequency;
            activationRange = [(timePosition - self.TimeLag), timePosition];
            [activations, memberships, startingPoints] = self.GetRangeWaveActivations(activationRange);
            
            colors = ones(numel(activations), 3);
            validMemberships = ~isnan(memberships);
            colors(validMemberships, :) = self.WaveColors(memberships(validMemberships), :);
            
            cData = ones(size(get(self.WavemapImage, 'cData')));
            rData = ones(size(cData, 1), size(cData, 2));
            rData(self.ElectrodeImageIndices) = colors(:, 1);
            
            gData = ones(size(cData, 1), size(cData, 2));
            gData(self.ElectrodeImageIndices) = colors(:, 2);
            
            bData = ones(size(cData, 1), size(cData, 2));
            bData(self.ElectrodeImageIndices) = colors(:, 3);
            
            colorData = cat(3, rData, gData, bData);
        end
        
        function HighlightElectrodes(self, electrodeIndices)
            if isempty(self.ElectrodeTextHandles), return; end
            
            for electrodeIndex = 1:numel(self.HighlightedElectrodeIndices)
                if ishandle(self.ElectrodeTextHandles(self.HighlightedElectrodeIndices(electrodeIndex)))
                    set(self.ElectrodeTextHandles(self.HighlightedElectrodeIndices(electrodeIndex)),...
                        'edgeColor', 'none');
                end
            end
            
            for electrodeIndex = 1:numel(electrodeIndices)
                if ishandle(self.ElectrodeTextHandles(electrodeIndices(electrodeIndex)))
                    set(self.ElectrodeTextHandles(electrodeIndices(electrodeIndex)), 'edgeColor', [1 0 0]);
                end
            end
            self.HighlightedElectrodeIndices = electrodeIndices;
        end
    end
    
    methods (Access = protected)
        function SetStaticFrame(self)
            if isempty(self.Waves), return; end
            
            timePosition = 1000 * self.FramePosition / self.SamplingFrequency;
            activationRange = [(timePosition - self.TimeLag), timePosition];
            [activations, memberships, startingPoints] = self.GetRangeWaveActivations(activationRange);
            
            self.SetWaveFrame(activations, memberships, startingPoints);
            self.SetWaveLabels(activations, memberships, startingPoints);
        end
        
        function SetWaveFrame(self, activations, memberships, startingPoints)
            validHandles = ishandle(self.ElectrodeTextHandles);
            if any(validHandles)
                delete(self.ElectrodeTextHandles(validHandles));
            end
            
            colors = ones(numel(activations), 3);
            validMemberships = ~isnan(memberships);
            colors(validMemberships, :) = self.WaveColors(memberships(validMemberships), :);
            
            cData = ones(size(get(self.WavemapImage, 'cData')));
            rData = ones(size(cData, 1), size(cData, 2));
            rData(self.ElectrodeImageIndices) = colors(:, 1);
            
            gData = ones(size(cData, 1), size(cData, 2));
            gData(self.ElectrodeImageIndices) = colors(:, 2);
            
            bData = ones(size(cData, 1), size(cData, 2));
            bData(self.ElectrodeImageIndices) = colors(:, 3);
            
            cData = cat(3, rData, gData, bData);
            
%             alphaData = ones(size(cData, 1), size(cData, 2));
%             timePosition = 1000 * self.FramePosition / self.SamplingFrequency;
%             alphaData(self.ElectrodeImageIndices) = 1 - (timePosition - activations) / self.TimeLag;
%             set(self.WavemapImage, 'cData', cData, 'alphaData', alphaData); 

            set(self.WavemapImage, 'cData', cData); 
        end
        
        function SetWaveLabels(self, activations, memberships, startingPoints)
            for activationIndex = 1:numel(activations)
                if isnan(activations(activationIndex)), continue; end
                
                self.ElectrodeTextHandles(activationIndex) = text(...
                    'position', self.Positions(activationIndex, :),...
                    'string', num2str(activations(activationIndex), '%.1f'),...
                    'color', [0 0 0],...
                    'horizontalAlignment', 'center',...
                    'hitTest', 'off',...
                    'parent', self.MapAxes);
                
                if startingPoints(activationIndex)
                    set(self.ElectrodeTextHandles(activationIndex),...
                        'color', [1 1 1],...
                        'backgroundColor', [0 0 0],...
                        'margin', 1);
                end
            end
            
            for electrodeIndex = 1:numel(self.HighlightedElectrodeIndices)
                if ishandle(self.ElectrodeTextHandles(self.HighlightedElectrodeIndices(electrodeIndex)))
                    set(self.ElectrodeTextHandles(self.HighlightedElectrodeIndices(electrodeIndex)), 'edgeColor', [1 0 0]);
                end
            end
        end
        
        function InitializeAxes(self)
            maxPosition = max(self.Positions);
            minPosition = min(self.Positions);
            self.MinimumSpacing = self.ComputeMinimumSpacing();
            
            set(self.MapAxes,...
                'xlim', [minPosition(1) - self.MinimumSpacing, maxPosition(1) + self.MinimumSpacing],...
                'ylim', [minPosition(2) - self.MinimumSpacing, maxPosition(2) + self.MinimumSpacing]);
            set(self.MapAxes, 'XLimMode', 'manual', 'YLimMode', 'manual');
            
            self.XData = minPosition(1):self.MinimumSpacing:maxPosition(1);
            self.YData = minPosition(2):self.MinimumSpacing:maxPosition(2);
            colors = ones(numel(self.YData), numel(self.XData), 3);
            self.WavemapImage = image(...
                'xData', self.XData,...
                'yData', self.YData,...
                'cData', colors,...
                'hitTest', 'off',...
                'parent', self.MapAxes);
            
            tolerance = 1e3 * eps;
            [xMembers, xIndices] = UtilityPkg.ismemberf(self.Positions(:, 1), self.XData, 'tol', tolerance); %#ok<ASGLU>
            [yMembers, yIndices] = UtilityPkg.ismemberf(self.Positions(:, 2), self.YData, 'tol', tolerance); %#ok<ASGLU>
            self.ElectrodeImageIndices = sub2ind([numel(self.YData), numel(self.XData)], yIndices, xIndices);
            
            self.ElectrodeTextHandles = NaN(size(self.Positions, 1), 1);
            
            axis(self.MapAxes, 'image');
            set(self.MapAxes, 'yDir', 'reverse', 'color', [1 1 1]);
            
            set(self.MapAxes, 'LooseInset', get(self.MapAxes, 'TightInset'));
            
            self.DrawElectrodeGrid();
        end
        
        function DrawElectrodeGrid(self)
            minimumSpacing = self.ComputeMinimumSpacing();
            numberOfElectrodes = size(self.Positions, 1);
            electrodePatchHandles = NaN(numberOfElectrodes, 1);
            electrodePatchHandlesWhite = NaN(numberOfElectrodes, 1);
            
            for electrodeIndex = 1:size(self.Positions, 1)
                xData = [self.Positions(electrodeIndex, 1) - [minimumSpacing, minimumSpacing] / 2,...
                    self.Positions(electrodeIndex, 1) + [minimumSpacing, minimumSpacing] / 2];
                yData = [self.Positions(electrodeIndex, 2) - [minimumSpacing, -minimumSpacing] / 2,...
                    self.Positions(electrodeIndex, 2) + [minimumSpacing, -minimumSpacing] / 2];
                electrodePatchHandles(electrodeIndex) = patch(...
                    'xData', xData, 'yData', yData,...
                    'edgeColor', [0,0,0], 'edgeAlpha', 0.5, 'faceColor', 'none',...
                    'hitTest', 'off', 'parent', self.MapAxes);
                electrodePatchHandlesWhite(electrodeIndex) = patch(...
                    'xData', xData, 'yData', yData,...
                    'edgeColor', [1,1,1], 'lineStyle', ':', 'edgeAlpha', 0.5, 'faceColor', 'none',...
                    'hitTest', 'off', 'parent', self.MapAxes);
            end
        end
        
        function NotifyMapClick(self, clickPosition)
            timePosition = 1000 * self.FramePosition / self.SamplingFrequency;
            activationRange = [(timePosition - self.TimeLag), timePosition];
            activations = self.GetRangeWaveActivations(activationRange);
            time = activations(clickPosition) / 1000;
            
            notify(self, 'MapClicked', ExtendedUIPkg.MapClickedEventData(clickPosition, time));
        end
    end
end