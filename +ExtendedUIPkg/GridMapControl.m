classdef GridMapControl < ExtendedUIPkg.MapControl
    properties
        Data
    end
    
    properties (Access = protected)
        Image
        ElectrodeImageIndices
    end
    
    methods
        function self = GridMapControl(position)
            self = self@ExtendedUIPkg.MapControl(position);
        end
        
        function SetData(self, data, positions)
            self.Data = data;
            self.Positions = positions;
            
            if size(positions, 1) < 2, return; end
            
            self.InitializeAxes();
            
            self.SetFrame();
        end
    end
    
    methods (Access = protected)
        function InitializeAxes(self)
            delete(get(self.MapAxes, 'children'));
            maxPosition = max(self.Positions);
            minPosition = min(self.Positions);
            minimumSpacing = self.ComputeMinimumSpacing();
            
            set(self.MapAxes,...
                'xlim', [minPosition(1) - minimumSpacing, maxPosition(1) + minimumSpacing],...
                'ylim', [minPosition(2) - minimumSpacing, maxPosition(2) + minimumSpacing]);
            set(self.MapAxes, 'XLimMode', 'manual', 'YLimMode', 'manual');
            
            xData = minPosition(1):minimumSpacing:maxPosition(1);
            yData = minPosition(2):minimumSpacing:maxPosition(2);
            colors = ones(numel(yData), numel(xData), 3);
            self.Image = image(...
                'xData', xData,...
                'yData', yData,...
                'cData', colors,...
                'hitTest', 'off',...
                'parent', self.MapAxes);
            
            tolerance = 1e3 * eps;
            [xMembers, xIndices] = UtilityPkg.ismemberf(self.Positions(:, 1), xData, 'tol', tolerance); %#ok<ASGLU>
            [yMembers, yIndices] = UtilityPkg.ismemberf(self.Positions(:, 2), yData, 'tol', tolerance); %#ok<ASGLU>
            self.ElectrodeImageIndices = sub2ind([numel(yData), numel(xData)], yIndices, xIndices);
            
            axis(self.MapAxes, 'image');
            set(self.MapAxes, 'yDir', 'reverse');
            set(self.MapAxes, 'clim', [0 1]);
            
            set(self.MapAxes, 'LooseInset', get(self.MapAxes, 'TightInset'));
        end
        
        function SetFrame(self)
            if self.FramePosition > size(self.Data, 2), return; end
            
            colorData = self.GetFrame(self.FramePosition);
            
            set(self.Image, 'cData', colorData);
        end
        
        function CopyDataToClipboard(self, varargin)
           frameData = self.Data(:, self.FramePosition);
           sizeX = numel(get(self.Image, 'xData'));
           sizeY = numel(get(self.Image, 'yData'));
           dataMatrix = NaN(sizeY, sizeX);
           dataMatrix(self.ElectrodeImageIndices) = frameData;
           UtilityPkg.num2clip(dataMatrix);
        end
    end
end