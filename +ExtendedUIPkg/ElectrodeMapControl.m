classdef ElectrodeMapControl < ExtendedUIPkg.MapControl
    properties
        ElectrodeLabels
        ElectrodeMapImage
        ElectrodeMapImageIndices
        ElectrodeMapTextHandles
        HighlightedElectrodeIndices
    end
    
    
    methods
        function self = ElectrodeMapControl(position)
            self = self@ExtendedUIPkg.MapControl(position);
        end
        
        function SetData(self, data, positions)
            self.ElectrodeLabels = data;
            self.Positions = positions;
            
            self.InitializeAxes();
        end
        
        function HighlightElectrodes(self, electrodeIndices)
            if isempty(self.ElectrodeMapTextHandles), return; end
            
            for electrodeIndex = 1:numel(self.HighlightedElectrodeIndices)
               set(self.ElectrodeMapTextHandles(self.HighlightedElectrodeIndices(electrodeIndex)),...
                   'edgeColor', 'none');
            end
            
            for electrodeIndex = 1:numel(electrodeIndices)
                set(self.ElectrodeMapTextHandles(electrodeIndices), 'edgeColor', [1 0 0]);
            end
            self.HighlightedElectrodeIndices = electrodeIndices;
        end
    end
    
    methods (Access = protected)
        function SetFrame(self) %#ok<MANU>
            return;
        end
        
        function InitializeAxes(self)
            maxPosition = max(self.Positions);
            minPosition = min(self.Positions);
            minimumSpacing = self.ComputeMinimumSpacing();
            
            set(self.MapAxes,...
                'xlim', [minPosition(1) - minimumSpacing, maxPosition(1) + minimumSpacing],...
                'ylim', [minPosition(2) - minimumSpacing, maxPosition(2) + minimumSpacing]);
            set(self.MapAxes, 'XLimMode', 'manual', 'YLimMode', 'manual');
            
            xData = minPosition(1):minimumSpacing:maxPosition(1);
            yData = minPosition(2):minimumSpacing:maxPosition(2);
            colors = ones(numel(yData), numel(xData), 3);
            self.ElectrodeMapImage = image(...
                'xData', xData,...
                'yData', yData,...
                'cData', colors,...
                'hitTest', 'off',...
                'parent', self.MapAxes);
            
            tolerance = 1e3 * eps;
            [xMembers, xIndices] = UtilityPkg.ismemberf(self.Positions(:, 1), xData, 'tol', tolerance); %#ok<ASGLU>
            [yMembers, yIndices] = UtilityPkg.ismemberf(self.Positions(:, 2), yData, 'tol', tolerance); %#ok<ASGLU>
            self.ElectrodeMapImageIndices = sub2ind([numel(yData), numel(xData)], yIndices, xIndices);
            
            axis(self.MapAxes, 'image');
            set(self.MapAxes, 'yDir', 'reverse', 'color', [1 1 1]);
            
            set(self.MapAxes, 'LooseInset', get(self.MapAxes, 'TightInset'));
            
            validTextHandles = ishandle(self.ElectrodeMapTextHandles);
            if any(validTextHandles)
                delete(self.ElectrodeMapTextHandles(validTextHandles));
            end
            self.ElectrodeMapTextHandles = NaN(numel(self.ElectrodeLabels), 1);
            
            for electrodeIndex = 1:numel(self.ElectrodeLabels)
                self.ElectrodeMapTextHandles(electrodeIndex) = text(...
                    'position', self.Positions(electrodeIndex, :),...
                    'string', self.ElectrodeLabels{electrodeIndex},...
                    'color', [0 0 0],...
                    'horizontalAlignment', 'center',...
                    'hitTest', 'off',...
                    'parent', self.MapAxes);
            end
        end
    end
end