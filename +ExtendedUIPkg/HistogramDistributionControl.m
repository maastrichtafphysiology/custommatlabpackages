classdef HistogramDistributionControl < EcgMappingGuiPkg.HistogramControl
	properties
        ParameterEstimates
        ParameterEstimateStrings
		Pdf
	end
   
    methods
        function self = HistogramDistributionControl(position, label)
            self = self@EcgMappingGuiPkg.HistogramControl(position, label);
        end
	
	    function Show(self, bins, cycles, pdf, parameterEstimateStrings, parameterEstimates )
			self.ParameterEstimates = parameterEstimates;
            self.ParameterEstimateStrings = parameterEstimateStrings;
            self.Pdf = pdf;
			Show@EcgMappingGuiPkg.HistogramControl(self, bins, cycles);
        end
	
    end
	
	methods (Access = protected)
		function CreateHistogramAxes(self, axesPosition)
			self.HistogramAxes = EcgMappingGuiPkg.HistogramDistributionAxes(axesPosition, self.XLabel);
            self.AddCustomControl(self.HistogramAxes);
		end
		function UpdateGUI(self)
			self.HistogramAxes.Draw(self.Cycles, self.Bins, self.Pdf, self.ParameterEstimateStrings, self.ParameterEstimates);
        end
	end
end