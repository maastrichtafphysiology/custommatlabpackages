classdef ActivationMapControl < ExtendedUIPkg.MapControl
    properties
        Activations
        BubbleSize
        ActivationScatter
    end
    
    methods
        function self = ActivationMapControl(position)
            self = self@ExtendedUIPkg.MapControl(position);
            self.BubbleSize = 12^2;
        end
        
        function SetData(self, data, positions)
            self.Activations = data;
            self.Positions = positions;
            
            maxPosition = max(self.Positions);
            minPosition = min(self.Positions);
            minimumSpacing = self.ComputeMinimumSpacing();
            
            set(self.MapAxes,...
                'xlim', [minPosition(1) - minimumSpacing, maxPosition(1) + minimumSpacing],...
                'ylim', [minPosition(2) - minimumSpacing, maxPosition(2) + minimumSpacing]);
            set(self.MapAxes, 'XLimMode', 'manual', 'YLimMode', 'manual');
            color = [0 0 0];
            
            self.ActivationScatter = scatter(self.MapAxes,...
                self.Positions(:, 1),...
                self.Positions(:, 2),...
                self.BubbleSize, color, 'filled');
            
            axis(self.MapAxes, 'image');
            set(self.MapAxes, 'yDir', 'reverse');
            set(self.MapAxes, 'clim', [0 1]);
            
            self.SetFrame();
        end
    end
    
    methods (Access = protected)
        function SetFrame(self)
            timeLag = 200;
            timePosition = 1000 * self.FramePosition / self.SamplingFrequency;
            activationRange = [(timePosition - timeLag), timePosition];
            
            activations = cellfun(@(x) ExtendedUIPkg.ActivationMapControl.GetLastActivationWithinRange(x, activationRange), self.Activations);
            colors = (activations - (timePosition - timeLag)) / timeLag;
            colors(isnan(colors)) = 0;
            
            set(self.ActivationScatter, 'cData', colors);
        end
    end
    
    methods (Static)
        function activation = GetLastActivationWithinRange(activations, range)
            activationPosition = find(activations > range(1) & activations <= range(end), 1, 'last');
            if isempty(activationPosition)
                activation = NaN;
            else
                activation = activations(activationPosition);
            end
        end
    end
end