classdef AFComplexityHistogramControl < ExtendedUIPkg.HistogramControl
    properties (Access = private)
        IntrinsicCycles
    end
    
    methods
        function self = AFComplexityHistogramControl(position, histogramAxes)
            self = self@ExtendedUIPkg.HistogramControl(position, histogramAxes);
        end
        
        function Show(self, bins, allCycles, intrinsicCycles)
            Show@UserInterfacePkg.CustomPanel(self);
			self.Cycles = allCycles;
            self.IntrinsicCycles = intrinsicCycles;
			self.Bins = bins;

            self.HistogramAxes.Draw(self.Cycles, self.IntrinsicCycles, self.Bins);
            self.HistogramAxes.Show();
        end
    end
    
    methods (Access = protected)
        function CycleLengthCallback(self, varargin)
            CycleLengthStart = str2double(get(self.CycleLengthBinsEdit(1), 'string'));
            CycleLengthStep = str2double(get(self.CycleLengthBinsEdit(2), 'string'));
            CycleLengthEnd = str2double(get(self.CycleLengthBinsEdit(3), 'string'));
            if CycleLengthStart <= CycleLengthEnd
                self.Bins = CycleLengthStart:CycleLengthStep:CycleLengthEnd;
            end
            self.HistogramAxes.Draw(self.Cycles, self.IntrinsicCycles, self.Bins);
			set(self.CycleLengthBinsEdit(1), 'string', self.Bins(1));
			set(self.CycleLengthBinsEdit(2), 'string', self.Bins(2)-self.Bins(1));
			set(self.CycleLengthBinsEdit(3), 'string', self.Bins(end));
        end
    end
    
    methods (Static)
        function control = BasicAFComplexityHistogramControl(position, label)
            histogramAxes = ExtendedUIPkg.AFComplexityHistogramAxes(...
                ExtendedUIPkg.HistogramControl.HistogramAxesPosition, label);
            control = ExtendedUIPkg.AFComplexityHistogramControl(position, histogramAxes);
        end
    end
end