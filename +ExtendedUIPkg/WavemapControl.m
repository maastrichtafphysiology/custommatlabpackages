classdef WavemapControl < ExtendedUIPkg.MapControl
    properties
        Waves
        WaveRanges
        WaveColors
    end
    
    methods
        function self = WavemapControl(position)
            self = self@ExtendedUIPkg.MapControl(position);
            self.Resolution = 16;
            self.Colormap = zeros(self.Resolution, 3);
            colors = jet(self.Resolution);
            colorSeparation = 4;
            currentPosition = 1;
            for i = 1:colorSeparation
                currentColors = colors(i:colorSeparation:end, :);
                self.Colormap(currentPosition:(currentPosition + size(currentColors, 1) - 1), :) = currentColors;
                currentPosition = currentPosition + size(currentColors, 1);
            end
        end
        
        function SetData(self, data, positions)
            self.Positions = positions;
            self.Waves = data;
            
            self.DetermineWaveColors();
            self.SetWaveRanges();
            
            self.InitializeAxes();
            
            self.SetStaticFrame();
        end
        
        function MatchWaveColors(self, waves, colors)
            activationThreshold = 5;
            waveOverlap = zeros(numel(self.Waves), numel(waves));
            for waveIndex = 1:numel(self.Waves)
                for wave2Index = 1:numel(waves)
                    waveOverlap(waveIndex, wave2Index) = waves(wave2Index).Overlap(self.Waves(waveIndex), activationThreshold);
%                     waveOverlap(waveIndex, wave2Index) = waveOverlap(waveIndex, wave2Index) / waves(wave2Index).Size();
                end
            end
            
%             wavesWithOverlap = any(waveOverlap, 2);
%             waveOverlap = waveOverlap(wavesWithOverlap, :);
%             waves2WithOverlap = any(waveOverlap, 1);
%             waveOverlap = waveOverlap(:, waves2WithOverlap);
            
            noOverlap = waveOverlap == 0;
            costMatrix = -waveOverlap + max(waveOverlap(:)) + 1;
            costMatrix(noOverlap) = Inf;
            assignment = AlgorithmPkg.BipartiteAssignment(costMatrix);
            
            self.WaveColors(assignment > 0, :) = colors(assignment(assignment > 0), :);
            self.SetStaticFrame();
        end
    end
    
    methods (Access = protected, Abstract)
        SetWaveFrame(self, activations, memberships, startingPoints);
    end
    
    methods (Access = protected)
        function SetFrame(self)
            if isempty(self.Waves), return; end
            
            timePosition = 1000 * self.FramePosition / self.SamplingFrequency;
            activationRange = [(timePosition - self.TimeLag), timePosition];
            [activations, memberships, startingPoints] = self.GetRangeWaveActivations(activationRange);
            
            self.SetWaveFrame(activations, memberships, startingPoints);
        end
        
        function SetWaveRanges(self)
            self.WaveRanges = NaN(numel(self.Waves), 2);
            
            for waveIndex = 1:numel(self.Waves)
                currentWave = self.Waves(waveIndex);
                self.WaveRanges(waveIndex, 1) = min(currentWave.Members(:, 3));
                self.WaveRanges(waveIndex, 2) = max(currentWave.Members(:, 3));
            end
        end
        
        function DetermineWaveColors(self)
            if isempty(self.Waves), return; end
            numberOfWaves = max([self.Waves.ID]);
            waveColors = repmat(self.Colormap, ceil(numberOfWaves / self.Resolution), 1);
            self.WaveColors = waveColors(1:numberOfWaves, :);
        end
        
        function [activations, memberships, startingPoints] = GetRangeWaveActivations(self, range)
            activations = NaN(size(self.Positions, 1), 1);
            memberships = activations;
            startingPoints = false(size(memberships));
            
            validWaves = find(self.WaveRanges(:, 2) > range(1) & self.WaveRanges(:, 1) <= range(end));
            for waveIndex = 1:numel(validWaves)
                currentWave = self.Waves(validWaves(waveIndex));
                activationsWithinRange = (currentWave.Members(:, 3) > range(1) & currentWave.Members(:, 3) <= range(end));
                if any(activationsWithinRange)
                    activationElectrodes = currentWave.Members(activationsWithinRange, 1);
                    activations(activationElectrodes) = currentWave.Members(activationsWithinRange, 3);
%                     memberships(activationElectrodes) = currentWave.ID;
                    memberships(activationElectrodes) = validWaves(waveIndex);
                    
                    startingPoints(activationElectrodes) = false;
                    waveStartingPoints = currentWave.GetAllStartingPoints();
                    for startPointIndex = 1:size(waveStartingPoints, 1)
                        if ismember(waveStartingPoints(startPointIndex, 1), activationElectrodes);
                            startingPoints(waveStartingPoints(startPointIndex, 1)) = true;
                        end
                    end
                end
            end
        end
    end
end