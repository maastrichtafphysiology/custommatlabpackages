classdef MeshActivationMapControl < ExtendedUIPkg.MeshMapControl  
    methods
        function self = MeshActivationMapControl(position)
            self = self@ExtendedUIPkg.MeshMapControl(position);
            self.Colormap = flipud(jet(self.Resolution));
        end       
        
        function SetData(self, data, positions)
            self.Data = data;
            self.Positions = positions;
        end
        
        function colorData = GetFrame(self, framePosition)
            timePosition = 1000 * framePosition / self.SamplingFrequency;
            activationRange = [(timePosition - self.TimeLag), timePosition];
            
            activations = self.GetActivations(activationRange);
            relativeColors = (timePosition - activations) / self.TimeLag;
            validActivations = ~isnan(relativeColors);
            relativeColors = round(sqrt(relativeColors) * (self.Resolution - 1)) + 1;
            colorData = ones(numel(activations), 3);
            colorData(validActivations, :) = ind2rgb(relativeColors(validActivations), self.Colormap);
        end
    end
    
    methods (Access = protected)
        function SetFrame(self)
            colorData = self.GetFrame(self.FramePosition);
            
            set(self.MeshPatchHandles, 'FaceVertexCData', colorData, 'FaceColor', 'interp');
        end
        
        function SetColorBar(self)
            
        end
        
        function activations = GetActivations(self, activationRange)
            activations = cellfun(@(x) ExtendedUIPkg.GridActivationMapControl.GetLastActivationWithinRange(x, activationRange), self.Data);
        end
    end
end