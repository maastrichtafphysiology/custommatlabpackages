classdef ContourMapControl < ExtendedUIPkg.GridWavemapControl
    properties
        InterpolationFactor
        Range
    end
    
    properties (Access = protected)
        MapEditorFigure
        
        Contour
        ContourLabels
        ContourLabelVisibility
        ContourLabelColor
        ContourLineColor
        Colorbar
        ColorbarVisible
    end
    
    methods
        function self = ContourMapControl(position)
            self = self@ExtendedUIPkg.GridWavemapControl(position);
            
            self.InterpolationFactor = 4;
            self.ContourLabelVisibility = 'off';
            self.ContourLabelColor = [0 0 0];
            self.ContourLineColor = [0 0 0];
            self.ColorbarVisible = true;
        end
        
        function ShowIsochroneLabels(self, status)
            self.ContourLabelVisibility = status;
            validTextHandles = ishandle(self.ContourLabels);
            set(self.ContourLabels(validTextHandles), 'visible', status);
        end
        
        function SetContourLineColor(self, color)
            self.ContourLineColor = color;
            if ishandle(self.Contour)
                set(self.Contour, 'lineColor', color);
            end
        end
        
        function SetContourLabelColor(self, color)
            self.ContourLabelColor = color;
            validTextHandles = ishandle(self.ContourLabels);
            set(self.ContourLabels(validTextHandles), 'color', color);
        end
        
        function ShowColorbar(self, status)
            if ishandle(self.Colorbar)
                switch status
                    case 'on'
                        self.ColorbarVisible = true;
                        self.Colorbar = colorbar('peer', self.MapAxes,...
                            'location', 'eastOutSide');
                    case 'off'
                        self.ColorbarVisible = false;
                        colorbar(self.Colorbar, 'hide');
                end
            else
                switch status
                    case 'on'
                        self.ColorbarVisible = true;
                        self.Colorbar = colorbar('peer', self.MapAxes,...
                            'location', 'eastOutSide');
                end
            end
        end
    end
    
    methods (Access = protected)
        function SetWaveFrame(self, activations, memberships, startingPoints)
            validHandles = ishandle(self.ElectrodeTextHandles);
            if any(validHandles)
                delete(self.ElectrodeTextHandles(validHandles));
            end
            
            if ishandle(self.Contour)
                delete(self.Contour);
                validContourLabels = ishandle(self.ContourLabels);
                delete(self.ContourLabels(validContourLabels));
            end
            
            activationMap = NaN(numel(self.YData), numel(self.XData));
            activationMap(self.ElectrodeImageIndices) = activations;
            
            validPositions = ~isnan(activationMap);
            [YGrid XGrid] = meshgrid(self.YData, self.XData);
            x = XGrid(validPositions);
            y = YGrid(validPositions);
            interpolator = TriScatteredInterp(x, y, activationMap(validPositions), 'natural');
            
            newY = linspace(self.YData(1), self.YData(end), self.InterpolationFactor * size(activationMap, 2));
            newX = linspace(self.XData(1), self.XData(end), self.InterpolationFactor * size(activationMap, 1));
            [newYGrid newXGrid] = meshgrid(newY, newX);
            newMap = interpolator(newXGrid, newYGrid);
            
            if all(size(newMap) >= [2, 2])
                if isempty(self.Range)
                    maxMap = ceil(max(newMap(:)) / 5) * 5;
                    minMap = floor(min(newMap(:)) / 5) * 5;
                    contourLevels = minMap:5:maxMap;
                    [contourMatrix, contourGroup] = contourf(self.MapAxes, newY, newX, newMap, contourLevels);
                else
                    [contourMatrix, contourGroup] = contourf(self.MapAxes, newY, newX, newMap, self.Range);
                end
            else
                delete(get(self.MapAxes, 'children'));
                return;
            end
            
            set(self.MapAxes, 'yDir', 'reverse');
            axis(self.MapAxes, 'image');
            set(get(self.MapAxes, 'children'), 'hitTest', 'off');
            
            maxPosition = max(self.Positions);
            minPosition = min(self.Positions);
            set(self.MapAxes,...
                'xlim', [minPosition(1) - self.MinimumSpacing, maxPosition(1) + self.MinimumSpacing],...
                'ylim', [minPosition(2) - self.MinimumSpacing, maxPosition(2) + self.MinimumSpacing]);
            
            if ~isempty(self.Range)
                caxis(self.MapAxes, [self.Range(1) self.Range(end)]);
            end
            
            self.ContourLabels = clabel(contourMatrix, contourGroup,...
                    'fontSize', 12,....
                    'color', self.ContourLabelColor,...
                    'visible', self.ContourLabelVisibility);
            set(contourGroup, 'lineWidth', 1, 'lineColor', self.ContourLineColor);
            self.Contour = contourGroup;
                
            % map editor
            editMapMenu = uicontextmenu;
            set(self.MapAxes, 'UIContextMenu', editMapMenu);
            uimenu(editMapMenu, 'label', 'Edit map',...
                'callback', @self.EditMap);
        end
        
        function SetWaveLabels(self, activations, memberships, startingPoints)
            return;
        end
        
        function EditMap(self, varargin)
            if isobject(self.MapEditorFigure)
                delete(self.MapEditorFigure);
            end
            
            screenSize = get(0,'Screensize');
            figurePosition = [screenSize(3) / 5, screenSize(4) / 5, 3 * screenSize(3) / 5, 3 * screenSize(4) / 5];
            self.MapEditorFigure = UserInterfacePkg.CustomFigure(figurePosition);
            self.MapEditorFigure.Create();
            self.MapEditorFigure.Name = 'Map editor';
            
            mapEditorControl = MapEditorControl([0 0 1 1]);
            self.MapEditorFigure.AddCustomControl(mapEditorControl);
            mapEditorControl.SetContourMap(self);
            mapEditorControl.Show();
        end
    end
end