classdef ConductionMapControl < ExtendedUIPkg.PropertyMapControl
    properties
        Angles
    end
    
    methods
        function self = ConductionMapControl(position)
            self = self@ExtendedUIPkg.PropertyMapControl(position);
        end
        
        function Create(self, parentHandle)
            Create@ExtendedUIPkg.PropertyMapControl(self, parentHandle);
            
            uimenu(self.ContextMenu, 'Label', 'Copy angles to clipboard', 'callback', @self.CopyAnglesToClipboard);
        end
        
        function SetData(self, data, positions)
            self.Angles = data.angle;
            SetData@ExtendedUIPkg.PropertyMapControl(self, data.value, positions)
        end
    end
    
    methods (Access = protected)
        function SetFrame(self)
            SetFrame@ExtendedUIPkg.PropertyMapControl(self);
            
            hold(self.MapAxes, 'on');
            arrowColor = [0 0 0];
            arrows = quiver(self.MapAxes, self.Positions(:, 1), self.Positions(:, 2),...
                cos(self.Angles), sin(self.Angles), 0.5);
            set(arrows, 'color', arrowColor);
            set(arrows, 'HitTest', 'off');
            arrows = quiver(self.MapAxes,  self.Positions(:, 1), self.Positions(:, 2),...
                -cos(self.Angles), -sin(self.Angles), 0.5);
            set(arrows, 'color', arrowColor);
            set(arrows, 'HitTest', 'off');
            hold(self.MapAxes, 'off');
        end
        
        function CopyAnglesToClipboard(self, varargin)
           frameData = self.Angles(:, self.FramePosition);
           frameData = 180 * frameData / pi;
           sizeX = numel(get(self.Image, 'xData'));
           sizeY = numel(get(self.Image, 'yData'));
           dataMatrix = NaN(sizeY, sizeX);
           dataMatrix(self.ElectrodeImageIndices) = frameData;
           UtilityPkg.num2clip(dataMatrix);
        end
    end
end