classdef ECGGridMapControl < ExtendedUIPkg.ECGMapControl
    properties
        GridElectrodeIndices
        GridLabels
    end
    
    methods
        function self = ECGGridMapControl(position)
            self = self@ExtendedUIPkg.ECGMapControl(position);
            self.TimeLag = 0.2;
        end
        
        function SetGridElectrodes(self, gridElectrodeIndices)
            self.GridElectrodeIndices = gridElectrodeIndices;
            self.InitializeAxes();
            self.SetFrame();
        end
    end
    
    methods (Access = protected)
        function InitializeAxes(self)
            validHandles = ishandle(self.ECGLines);
            if any(validHandles(:))
                delete(self.ECGLines(validHandles));
            end
            
            validHandles = ishandle(self.LaplacianLines);
            if any(validHandles(:))
                delete(self.LaplacianLines(validHandles));
            end
            
            validHandles = ishandle(self.GridLabels);
            if any(validHandles(:))
                delete(self.GridLabels(validHandles));
            end
            
            validHandles = ishandle(self.MapAxes);
            if any(validHandles(:))
                delete(self.MapAxes(validHandles));
            end
            
            if isempty(self.GridElectrodeIndices), return; end
            
            maxPosition = max(self.Positions(self.GridElectrodeIndices, :), [], 1);
            minPosition = min(self.Positions(self.GridElectrodeIndices, :), [], 1);
            minimumSpacing = self.ComputeMinimumSpacing();
            
            xData = minPosition(1):minimumSpacing:maxPosition(1);
            yData = minPosition(2):minimumSpacing:maxPosition(2);
            axesSpacing = .005;
            axesWidth = 1 / numel(xData);
            axesHeight = 1 / numel(yData);
            
            self.MapAxes = NaN(numel(xData), numel(yData));
            self.ECGLines = NaN(numel(xData), numel(yData));
            if ~isempty(self.LaplacianData)
                self.LaplacianLines = NaN(numel(xData), numel(yData));
            end
            for xIndex = 1:numel(xData)
                for yIndex = 1:numel(yData)
                    self.MapAxes(xIndex, yIndex) =...
                        axes('parent', self.ControlHandle,...
                        'units', 'normalized',...
                        'position', [...
                        (xIndex - 1) * axesWidth,...
                        (numel(yData) - yIndex) * axesHeight,...
                        axesWidth - axesSpacing, axesHeight - axesSpacing],...
                        'xTickLabel', {}, 'yTickLabel', {},...
                        'color', [0 0 0]);
                    
                    self.ECGLines(xIndex, yIndex) = line('xData', [], 'yData', [],...
                        'color', [1 1 1],...
                        'parent', self.MapAxes(xIndex, yIndex));
                    
                    if isempty(self.LaplacianData), continue; end
                    self.LaplacianLines(xIndex, yIndex) = line('xData', [], 'yData', [],...
                        'color', [0 1 0],...
                        'parent', self.MapAxes(xIndex, yIndex));
                end
            end
%             linkaxes(self.MapAxes, 'x');
            
            tolerance = 1e3 * eps;
            [xMembers, xIndices] = UtilityPkg.ismemberf(self.Positions(self.GridElectrodeIndices, 1), xData, 'tol', tolerance); %#ok<ASGLU>
            [yMembers, yIndices] = UtilityPkg.ismemberf(self.Positions(self.GridElectrodeIndices, 2), yData, 'tol', tolerance); %#ok<ASGLU>
            self.ElectrodeAxesIndices = sub2ind([numel(xData), numel(yData)], xIndices, yIndices);
            
            self.GridLabels = NaN(numel(self.GridElectrodeIndices));
            for electrodeIndex = 1:numel(self.GridElectrodeIndices)
                self.GridLabels(electrodeIndex) = text(...
                        'units', 'normalized',...
                        'position', [0, 1],...
                        'string', self.EcgData.ElectrodeLabels{self.GridElectrodeIndices(electrodeIndex)},...
                        'verticalAlignment', 'top',...
                        'parent', self.MapAxes(self.ElectrodeAxesIndices(electrodeIndex)),...
                        'backgroundColor', [1 1 1],...
                        'edgeColor', [0 0 0]);
            end
        end
        
        function SetFrame(self)
            time = self.EcgData.GetTimeRange();
            
            timePosition = self.FramePosition / self.EcgData.SamplingFrequency;
            timeRange = [(timePosition - self.TimeLag / 2), (timePosition + self.TimeLag / 2)];
            validPositions = time >= timeRange(1) & time <= timeRange(end);
            time = time(validPositions);
            data = self.EcgData.Data(validPositions, self.GridElectrodeIndices);
            for electrodeIndex = 1:numel(self.GridElectrodeIndices)
                set(self.ECGLines(self.ElectrodeAxesIndices(electrodeIndex)),...
                    'xData', time, 'yData', data(:, electrodeIndex) / 1000);
                
                if isempty(self.LaplacianData), continue; end
                
                set(self.LaplacianLines(self.ElectrodeAxesIndices(electrodeIndex)),...
                    'xData', time, 'yData', self.LaplacianData(validPositions, self.GridElectrodeIndices(electrodeIndex)) / 1000);
            end
            maxY = max(data(:));
            minY = min(data(:));
            set(self.MapAxes, 'xLim', timeRange, 'yLim', [minY, maxY] / 1000);
        end
    end
end