classdef WaveContourMapControl < ExtendedUIPkg.ContourMapControl
    properties
        WaveIndex
    end
    
    methods
        function self = WaveContourMapControl(position)
            self = self@ExtendedUIPkg.ContourMapControl(position);
            self.WaveIndex = NaN;
            
            self.ContourLabelVisibility = 'on';
            self.InterpolationFactor = 1;
        end 
        
        function SetWaveIndex(self, waveIndex)
            self.WaveIndex = waveIndex;
            self.SetStaticFrame();
        end
    end
    
    methods (Access = protected)
        function SetStaticFrame(self)
            if isempty(self.Waves), return; end
            if isnan(self.WaveIndex), return; end
            
            activations = NaN(size(self.Positions, 1), 1);
            memberships = activations;
            startingPoints = false(size(memberships));
            
            currentWave = self.Waves(self.WaveIndex);
            waveActivations = currentWave.Members(:, 3);
            waveElectrodes = currentWave.Members(:, 1);
            waveStartingPoints = currentWave.GetAllStartingPoints();
            
            activations(waveElectrodes) = waveActivations;
            memberships(waveElectrodes) = currentWave.ID;
            startingPoints(waveStartingPoints(:, 1)) = true;
            
            self.SetWaveFrame(activations, memberships, startingPoints);
            self.SetWaveLabels(activations, memberships, startingPoints);
            
            [vx, vy] = voronoi(self.Positions(:, 1), self.Positions(:, 2));
            for vertexIndex = 1:size(vx, 2)
                line('xData', vx(:, vertexIndex), 'yData', vy(:, vertexIndex),...
                    'color', [.7 .7 .7],...
                    'parent', self.MapAxes);
            end
        end
        
        function SetWaveFrame(self, activations, memberships, startingPoints)
            validHandles = ishandle(self.ElectrodeTextHandles);
            if any(validHandles)
                delete(self.ElectrodeTextHandles(validHandles));
            end
            
            if ishandle(self.Contour)
                delete(self.Contour);
                validContourLabels = ishandle(self.ContourLabels);
                delete(self.ContourLabels(validContourLabels));
            end
            
            activationMap = NaN(numel(self.YData), numel(self.XData));
            activationMap(self.ElectrodeImageIndices) = activations;

            if self.InterpolationFactor > 1
                validPositions = ~isnan(activationMap);
                [YGrid XGrid] = meshgrid(self.YData, self.XData);
                x = XGrid(validPositions);
                y = YGrid(validPositions);
                interpolator = TriScatteredInterp(x, y, activationMap(validPositions), 'natural');
                
                newY = linspace(self.YData(1), self.YData(end), self.InterpolationFactor * size(activationMap, 2));
                newX = linspace(self.XData(1), self.XData(end), self.InterpolationFactor * size(activationMap, 1));
                [newYGrid newXGrid] = meshgrid(newY, newX);
                newMap = interpolator(newXGrid, newYGrid);
            else
                newMap = activationMap;
                newX = self.XData;
                newY = self.YData;
            end
            
            earliestActivation = min(newMap(:));
%             earliestActivation = 1982;
            newMap = newMap - earliestActivation;
            
            if all(size(newMap) >= [2, 2])
                if isempty(self.Range)
                    maxMap = ceil(max(newMap(:)) / 5) * 5;
                    minMap = floor(min(newMap(:)) / 5) * 5;
                    contourLevels = minMap:5:maxMap;
                    [contourMatrix, contourGroup] = contour(self.MapAxes, newX, newY, newMap, contourLevels);
                else
                    [contourMatrix, contourGroup] = contour(self.MapAxes, newX, newY, newMap, self.Range);
                end
            else
                delete(get(self.MapAxes, 'children'));
                return;
            end
            
            set(self.MapAxes, 'yDir', 'reverse');
            axis(self.MapAxes, 'image');
            set(get(self.MapAxes, 'children'), 'hitTest', 'off');
            
            maxPosition = max(self.Positions);
            minPosition = min(self.Positions);
            set(self.MapAxes,...
                'xlim', [minPosition(1) - self.MinimumSpacing, maxPosition(1) + self.MinimumSpacing],...
                'ylim', [minPosition(2) - self.MinimumSpacing, maxPosition(2) + self.MinimumSpacing]);
            
            if ~isempty(self.Range)
                caxis(self.MapAxes, [self.Range(1) self.Range(end)]);
            end
            
            if strcmp(self.ContourLabelVisibility, 'on')
                self.ContourLabels = clabel(contourMatrix, contourGroup,...
                    'fontSize', 12,....
                    'color', self.ContourLabelColor,...
                    'visible', self.ContourLabelVisibility);
            end
            set(contourGroup, 'lineWidth', 1, 'lineColor', self.ContourLineColor);
            self.Contour = contourGroup;
                
            % map editor
%             editMapMenu = uicontextmenu;
%             set(self.MapAxes, 'UIContextMenu', editMapMenu);
%             uimenu(editMapMenu, 'label', 'Edit map',...
%                 'callback', @self.EditMap);
        end
    end
end