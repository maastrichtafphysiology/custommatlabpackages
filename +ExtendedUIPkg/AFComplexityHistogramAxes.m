classdef AFComplexityHistogramAxes < ExtendedUIPkg.HistogramAxes 
    
    methods
        function self = AFComplexityHistogramAxes(position, label)
            self = self@ExtendedUIPkg.HistogramAxes(position, label);
        end
        
        function Draw(self, allValues, intrinsicValues, bins)
			self.Bins = bins;
			[self.BinCount, xout] = hist(allValues, bins);
            [intrinsicCount, xout] = hist(intrinsicValues, bins);
            barSeriesHandles = bar(self.ControlHandle,...
                xout, [self.BinCount(:) intrinsicCount(:)],...
                'barWidth', 1.7, 'barLayout', 'grouped');
            
            set(barSeriesHandles(1), 'faceColor', [0 0 0], 'edgeColor', [0 0 0]);
            set(barSeriesHandles(2), 'faceColor', [.7 .7 .7], 'edgeColor', [0 0 0]);
            
            self.XValues = xout;
            self.YValues = self.BinCount;
            axis(self.ControlHandle, 'tight');
            set(self.ControlHandle, 'xlim', [0 max(bins)]);
            xlabel(self.ControlHandle, self.XLabel)
            
            allDeflectionsMean = mean(allValues);
            allDeflectionsMedian = median(allValues);
            
            intrinsicDeflectionsMean = mean(intrinsicValues);
            intrinsicDeflectionsMedian = median(intrinsicValues);
            
            complexity = (numel(allValues) + 1) / (numel(intrinsicValues) + 1) - 1;
            
            textString = {'\bf Parameters:';...
                ['AF Complexity: ' num2str(complexity, '%.2f')];...
                ['Filtered deflections: ' num2str(numel(allValues) + 1, '%.0f')];...
                ['Filtered interval mean: ' num2str(1000 * allDeflectionsMean, '%.1f ms')];...
                ['Intrinsic deflections: ' num2str(numel(intrinsicValues) + 1, '%.0f')];...
                ['Intrinsic interval mean: ' num2str(1000 * intrinsicDeflectionsMean, '%.1f ms')]};
            
            text('parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [1 1],...
                'verticalAlignment', 'top',...
                'horizontalAlignment', 'right',...
                'edgeColor', [0 0 0],...
                'backgroundColor', [.8 .8 .8],...
                'fontWeight', 'bold',...
                'string', textString);
        end
    end
end