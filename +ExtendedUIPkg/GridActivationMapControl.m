classdef GridActivationMapControl < ExtendedUIPkg.MapControl
    properties
        Activations
        ActivationGridMatrix
        ActivationImage
        ElectrodeImageIndices
        ElectrodeTextHandles
        EmptyIndices
        HighlightedElectrodeIndices
        
        ElectrodePatchHandles
        ElectrodePatchHandlesWhite
    end
    
    methods
        function self = GridActivationMapControl(position)
            self = self@ExtendedUIPkg.MapControl(position);
            self.Activations = {};
            self.ActivationGridMatrix = [];
        end
        
        function SetData(self, data, positions)
            self.Activations = data;
            self.Positions = positions;
            
            self.InitializeAxes();
            self.DrawElectrodeGrid();
            
            self.SetStaticFrame();
        end

        function firstActivation = SetRelativeStaticFramePosition(self, framePosition, offset)
            self.FramePosition = framePosition;
            
            if nargin < 3
                firstActivation = self.SetRelativeStaticFrame();
            else
                firstActivation = self.SetRelativeStaticFrame(offset);
            end
        end
        
        function colorData = GetFrame(self, framePosition)
            timePosition = 1000 * framePosition / self.SamplingFrequency;
            activationRange = [(timePosition - self.TimeLag), timePosition];
            
            activations = cellfun(@(x) ExtendedUIPkg.GridActivationMapControl.GetLastActivationWithinRange(x, activationRange), self.Activations);
            relativeColors = (timePosition - activations) / self.TimeLag;
            validActivations = ~isnan(relativeColors);
            relativeColors = round(sqrt(relativeColors) * (self.Resolution - 1)) + 1;
            colors = ones(numel(activations), 3);
            colors(validActivations, :) = ind2rgb(relativeColors(validActivations), self.Colormap);
            
            cData = ones(size(get(self.ActivationImage, 'cData')));
            rData = ones(size(cData, 1), size(cData, 2));
            rData(self.ElectrodeImageIndices) = colors(:, 1);
            
            gData = ones(size(cData, 1), size(cData, 2));
            gData(self.ElectrodeImageIndices) = colors(:, 2);
            
            bData = ones(size(cData, 1), size(cData, 2));
            bData(self.ElectrodeImageIndices) = colors(:, 3);
            
            if any(self.EmptyIndices(:))
                rData(self.EmptyIndices) = .7;
                gData(self.EmptyIndices) = .7;
                bData(self.EmptyIndices) = .7;
            end
            
            colorData = cat(3, rData, gData, bData);
        end

        function colorData = GetRelativeFrame(self, activations)
            firstActivation = min(activations);
            lastActivation = max(activations);
            relativeColors = (activations - firstActivation) / (lastActivation - firstActivation);
            validActivations = ~isnan(relativeColors);
            relativeColors = round(sqrt(relativeColors) * (self.Resolution - 1)) + 1;
            colors = ones(numel(activations), 3);
            colors(validActivations, :) = ind2rgb(relativeColors(validActivations), self.Colormap);
            
            cData = ones(size(get(self.ActivationImage, 'cData')));
            rData = ones(size(cData, 1), size(cData, 2));
            rData(self.ElectrodeImageIndices) = colors(:, 1);
            
            gData = ones(size(cData, 1), size(cData, 2));
            gData(self.ElectrodeImageIndices) = colors(:, 2);
            
            bData = ones(size(cData, 1), size(cData, 2));
            bData(self.ElectrodeImageIndices) = colors(:, 3);
            
            if any(self.EmptyIndices(:))
                rData(self.EmptyIndices) = .7;
                gData(self.EmptyIndices) = .7;
                bData(self.EmptyIndices) = .7;
            end
            
            colorData = cat(3, rData, gData, bData);
        end
        
        function HighlightElectrodes(self, electrodeIndices)
            if isempty(self.ElectrodeTextHandles), return; end
            
            validElectrodeIndices = ishandle(self.ElectrodeTextHandles(self.HighlightedElectrodeIndices));
            self.HighlightedElectrodeIndices = self.HighlightedElectrodeIndices(validElectrodeIndices);
            for electrodeIndex = 1:numel(self.HighlightedElectrodeIndices)
               set(self.ElectrodeTextHandles(self.HighlightedElectrodeIndices(electrodeIndex)),...
                   'edgeColor', 'none');
            end
            
            validElectrodeIndices = ishandle(self.ElectrodeTextHandles(electrodeIndices));
            electrodeIndices = electrodeIndices(validElectrodeIndices);
            for electrodeIndex = 1:numel(electrodeIndices)
                set(self.ElectrodeTextHandles(electrodeIndices(electrodeIndex)), 'edgeColor', [1 0 0]);
            end
            self.HighlightedElectrodeIndices = electrodeIndices;
        end
    end
    
    methods (Access = protected)
        function InitializeAxes(self)
            maxPosition = max(self.Positions);
            minPosition = min(self.Positions);
            minimumSpacing = self.ComputeMinimumSpacing();
            
            set(self.MapAxes,...
                'xlim', [minPosition(1) - minimumSpacing, maxPosition(1) + minimumSpacing],...
                'ylim', [minPosition(2) - minimumSpacing, maxPosition(2) + minimumSpacing]);
            set(self.MapAxes, 'XLimMode', 'manual', 'YLimMode', 'manual');
            
            xData = minPosition(1):minimumSpacing:maxPosition(1);
            yData = minPosition(2):minimumSpacing:maxPosition(2);
            colors = ones(numel(yData), numel(xData), 3);
            
            if ishandle(self.ActivationImage)
                delete(self.ActivationImage);
            end
            self.ActivationImage = image(...
                'xData', xData,...
                'yData', yData,...
                'cData', colors,...
                'hitTest', 'off',...
                'parent', self.MapAxes);
            
            tolerance = 1e3 * eps;
            [xMembers, xIndices] = UtilityPkg.ismemberf(self.Positions(:, 1), xData, 'tol', tolerance); %#ok<ASGLU>
            [yMembers, yIndices] = UtilityPkg.ismemberf(self.Positions(:, 2), yData, 'tol', tolerance); %#ok<ASGLU>
            self.ElectrodeImageIndices = sub2ind([numel(yData), numel(xData)], yIndices, xIndices);
            
            self.EmptyIndices = true(numel(yData), numel(xData));
            self.EmptyIndices(self.ElectrodeImageIndices) = false;

            self.ActivationGridMatrix = NaN(numel(yData), numel(xData));
            
            validHandles = ishandle(self.ElectrodeTextHandles);
            if any(validHandles)
                delete(self.ElectrodeTextHandles(validHandles))
            end
            self.ElectrodeTextHandles = NaN(size(self.Positions, 1), 1);
            
            axis(self.MapAxes, 'image');
            set(self.MapAxes, 'yDir', 'reverse');
            set(self.MapAxes, 'clim', [0 1]);
            
            set(self.MapAxes, 'LooseInset', get(self.MapAxes, 'TightInset'));
        end
        
        function DrawElectrodeGrid(self)
            minimumSpacing = self.ComputeMinimumSpacing();
            numberOfElectrodes = size(self.Positions, 1);
            
            validHandles = ishandle(self.ElectrodePatchHandles);
            if any(validHandles)
                delete(self.ElectrodePatchHandles(validHandles))
            end
            self.ElectrodePatchHandles = NaN(numberOfElectrodes, 1);
            
            validHandles = ishandle(self.ElectrodePatchHandlesWhite);
            if any(validHandles)
                delete(self.ElectrodePatchHandlesWhite(validHandles))
            end
            self.ElectrodePatchHandlesWhite = NaN(numberOfElectrodes, 1);
            
            for electrodeIndex = 1:size(self.Positions, 1)
                xData = [self.Positions(electrodeIndex, 1) - [minimumSpacing, minimumSpacing] / 2,...
                    self.Positions(electrodeIndex, 1) + [minimumSpacing, minimumSpacing] / 2];
                yData = [self.Positions(electrodeIndex, 2) - [minimumSpacing, -minimumSpacing] / 2,...
                    self.Positions(electrodeIndex, 2) + [minimumSpacing, -minimumSpacing] / 2];
                self.ElectrodePatchHandles(electrodeIndex) = patch(...
                    'xData', xData, 'yData', yData,...
                    'edgeColor', [0,0,0], 'edgeAlpha', 0.5, 'faceColor', 'none',...
                    'hitTest', 'off', 'parent', self.MapAxes);
                self.ElectrodePatchHandlesWhite(electrodeIndex) = patch(...
                    'xData', xData, 'yData', yData,...
                    'edgeColor', [1,1,1], 'lineStyle', ':', 'edgeAlpha', 0.5, 'faceColor', 'none',...
                    'hitTest', 'off', 'parent', self.MapAxes);
            end
        end
        
        function SetFrame(self)
            validHandles = ishandle(self.ElectrodeTextHandles);
            if any(validHandles)
                delete(self.ElectrodeTextHandles(validHandles));
            end
            
            colorData = self.GetFrame(self.FramePosition);
            
            set(self.ActivationImage, 'cData', colorData);
        end
        
        function  SetStaticFrame(self)
            timePosition = 1000 * self.FramePosition / self.SamplingFrequency;
            activationRange = [(timePosition - self.TimeLag), timePosition];
            
            activations = cellfun(@(x) ExtendedUIPkg.GridActivationMapControl.GetLastActivationWithinRange(x, activationRange), self.Activations);

            self.ActivationGridMatrix(self.ElectrodeImageIndices) = activations;
            self.ActivationGridMatrix(self.EmptyIndices) = NaN;
            
            self.SetFrame();
            self.SetActivationLabels(activations);
        end

        function SetRelativeFrame(self, activations)
            validHandles = ishandle(self.ElectrodeTextHandles);
            if any(validHandles)
                delete(self.ElectrodeTextHandles(validHandles));
            end
            
            colorData = self.GetRelativeFrame(activations);
            
            set(self.ActivationImage, 'cData', colorData);
        end

        function firstActivation = SetRelativeStaticFrame(self, offset)
            timePosition = 1e3 * self.FramePosition / self.SamplingFrequency;

            if nargin < 2
                activations = cellfun(@(x) ExtendedUIPkg.GridActivationMapControl.GetClosestActivation(x, timePosition), self.Activations);
                activations = ExtendedUIPkg.GridActivationMapControl.FilterActivations(activations);

                firstActivation = min(activations);
                activations = activations - firstActivation + 1;
            else
                activations = cellfun(@(x) ExtendedUIPkg.GridActivationMapControl.GetClosestLaterActivation(x, offset), self.Activations);
                activations = activations - offset + 1;
                activations = ExtendedUIPkg.GridActivationMapControl.FilterActivations(activations);
                firstActivation = min(activations);
            end
            
            self.ActivationGridMatrix(self.ElectrodeImageIndices) = activations;
            self.ActivationGridMatrix(self.EmptyIndices) = NaN;

            self.SetRelativeFrame(activations);
            self.SetActivationLabels(activations);
        end
        
        function SetActivationLabels(self, activations)
            for activationIndex = 1:numel(activations)
                if isnan(activations(activationIndex)), continue; end
                
                self.ElectrodeTextHandles(activationIndex) = text(...
                    'position', self.Positions(activationIndex, :),...
                    'string', num2str(activations(activationIndex), '%.0f'),...
                    'color', [0 0 0],...
                    'horizontalAlignment', 'center',...
                    'hitTest', 'off',...
                    'parent', self.MapAxes);
            end
        end
        
        function NotifyMapClick(self, clickPosition)
            timePosition = 1000 * self.FramePosition / self.SamplingFrequency;
            activationRange = [(timePosition - self.TimeLag), timePosition];
            activations = cellfun(@(x) ExtendedUIPkg.GridActivationMapControl.GetLastActivationWithinRange(x, activationRange), self.Activations);
            time = activations(clickPosition) / 1000;
            
            notify(self, 'MapClicked', ExtendedUIPkg.MapClickedEventData(clickPosition, time));
        end

        function CopyDataToClipboard(self, varargin)
            dataMatrix = self.ActivationGridMatrix;
            UtilityPkg.num2clip(dataMatrix);

        end
    end
    
    methods (Static)
        function activation = GetLastActivationWithinRange(activations, range)
            activationPosition = find(activations > range(1) & activations <= range(end), 1, 'last');
            if isempty(activationPosition)
                activation = NaN;
            else
                activation = activations(activationPosition);
            end
        end

        function activation = GetClosestActivation(activations, reference)
            [~, activationPosition] = min(abs(activations -  reference));
            if isempty(activationPosition)
                activation = NaN;
            else
                activation = activations(activationPosition);
            end
        end

        function activation = GetClosestLaterActivation(activations, reference)
            activationPosition = find(activations > reference, 1, 'first' );
            if isempty(activationPosition)
                activation = NaN;
            else
                activation = activations(activationPosition);
            end
        end

        function activations = FilterActivations(activations)
            % filter for outliers
            activationQ1Q3 = prctile(activations, [25, 75]);
            activationIQR = activationQ1Q3(end) - activationQ1Q3(1);
            invalidActivations = activations < activationQ1Q3(1) - 1.5 *activationIQR |...
                activations > activationQ1Q3(end) + 1.5 *activationIQR;
            activations(invalidActivations) = NaN;
        end
    end
end