classdef ScatterWavemapControl < ExtendedUIPkg.WavemapControl
    properties (Access = private)
       BubbleSize
       WavemapScatter
    end
   
    methods
        function self = ScatterWavemapControl(position)
            self = self@ExtendedUIPkg.WavemapControl(position);
            self.BubbleSize = 12^2;
        end
    end
    
    methods (Access = protected)
        function SetWaveFrame(self, activations, memberships, startingPoints)
           timePosition = 1000 * self.FramePosition / self.SamplingFrequency;
           bubbleSizes = self.BubbleSize * (activations - (timePosition - self.TimeLag)) / self.TimeLag;
            
            colors = ones(numel(activations), 3);
            validMemberships = ~isnan(memberships);
            colors(validMemberships, :) = self.WaveColors(memberships(validMemberships), :);
            
            bubbleSizes(isnan(bubbleSizes)) = 1;
            
            set(self.WavemapScatter,...
                'sizeData', bubbleSizes, 'cData', colors); 
        end
        
        function InitializeAxes(self)
            maxPosition = max(self.Positions);
            minPosition = min(self.Positions);
            minimumSpacing = self.ComputeMinimumSpacing();
            
            set(self.MapAxes,...
                'xlim', [minPosition(1) - minimumSpacing, maxPosition(1) + minimumSpacing],...
                'ylim', [minPosition(2) - minimumSpacing, maxPosition(2) + minimumSpacing]);
            set(self.MapAxes, 'XLimMode', 'manual', 'YLimMode', 'manual');
            
            self.WavemapScatter = scatter(self.MapAxes,...
                self.Positions(:, 1),...
                self.Positions(:, 2), 'filled');
            
            axis(self.MapAxes, 'image');
            set(self.MapAxes, 'yDir', 'reverse');
        end
    end
end