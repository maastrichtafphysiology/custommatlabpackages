classdef AnimatedGridWavemapControl < ExtendedUIPkg.GridWavemapControl
    properties (Access = private)
        AnimationInterval
        AnimationData
        Playing
        AnimationButton
    end
    
    methods
        function self = AnimatedGridWavemapControl(position)
            self = self@ExtendedUIPkg.GridWavemapControl(position);
            self.Playing = false;
        end
        
        function Create(self, parentHandle)
            Create@ExtendedUIPkg.GridWavemapControl(self, parentHandle);
            
            self.AnimationButton = uicontrol(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [0 0 .05 .05],...
                'style', 'togglebutton',...
                'string', '>',...
                'callback', @self.ControlAnimation);
        end
    end
    
    methods (Access = protected)
        function SetStaticFrame(self)
            if isempty(self.Waves), return; end
            
            self.AnimationInterval = (self.FramePosition - round(self.TimeLag / 1000 * self.SamplingFrequency)):self.FramePosition;
            
            self.ComputeAnimationData();
        end
    end
    
    methods (Access = private)
        function ControlAnimation(self, varargin)
            set(self.AnimationButton, 'string', '||');
            
            framePosition = 1;
            numberOfFrames = numel(self.AnimationData);
            
            set(self.WavemapImage, 'eraseMode', 'none');
            while get(self.AnimationButton, 'value')
                set(self.WavemapImage, 'cData', self.AnimationData{framePosition});
                drawnow;
                framePosition = framePosition + 1;
                if framePosition > numberOfFrames
                    framePosition = 1;
                end
            end
            set(self.WavemapImage, 'eraseMode', 'normal');
            
            set(self.AnimationButton, 'string', '>');
        end
        
        function ComputeAnimationData(self)
            originalTimeLag = self.TimeLag;
            self.AnimationData = cell(size(self.AnimationInterval));
            for frameIndex = 1:numel(self.AnimationInterval);
                self.TimeLag = frameIndex / 1000 * self.SamplingFrequency;
                self.AnimationData{frameIndex} = GetFrame(self, self.AnimationInterval(frameIndex));
            end
            self.TimeLag = originalTimeLag;
        end
    end
end