classdef HistogramFitAxes < ExtendedUIPkg.HistogramAxes
    properties 
        FitCollection
        RegionSelector
        MouseIsDown
        TemplateMode
        Listeners
    end
    
    properties (Access = protected)
        FitLine
    end

    methods
        function self = HistogramFitAxes(position, label, fitCollection)
            self = self@ExtendedUIPkg.HistogramAxes(position,label);
            self.RegionSelector = UserInterfacePkg.TemplateSelector;
            self.TemplateMode = 'Initial';
            self.Listeners = event.listener.empty(0, 0);
            self.FitLine = [];
            self.FitCollection = fitCollection;
        end
        
        function Create(self, parentHandle)
            Create@ExtendedUIPkg.HistogramAxes(self, parentHandle);
            
            axesXLimits = get(self.ControlHandle, 'xlim');
            axesYLimits = get(self.ControlHandle, 'ylim');

            self.RegionSelector.Create(self.ControlHandle,...
                [axesXLimits(1) axesYLimits(1) .15  axesYLimits(2)]);
            self.RegionSelector.OnMatchTemplate = @self.MatchTemplate;
            self.RegionSelector.Hide();
            
            self.SelectTemplate();
            
            self.CreateMouseListeners();
        end
        
        function Draw(self, values, bins)
            Draw@ExtendedUIPkg.HistogramAxes(self, values, bins);
            axesXLimits = get(self.ControlHandle, 'xlim');
            axesYLimits = get(self.ControlHandle, 'ylim');
            
            self.RegionSelector.Create(self.ControlHandle,...
                [axesXLimits(1) axesYLimits(1) .15  axesYLimits(2)]);
            self.RegionSelector.OnMatchTemplate = @self.MatchTemplate;
            self.RegionSelector.Hide();

            self.SelectTemplate();
        end
        
        function SelectTemplate(self)
            self.InitializeTemplateSelection();
            self.TemplateMode = 'Placing';
            self.RegionSelector.Show();
        end
        
        function Show(self)
            if ~any(strcmpi(self.TemplateMode, {'Initial', 'Finished'}))
                self.RegionSelector.Show();
            end
            Show@ExtendedUIPkg.HistogramAxes(self);
        end
        
        function Hide(self)
            Hide@ExtendedUIPkg.HistogramAxes(self);
            self.RegionSelector.Hide();
        end
        
        function CreateMouseListeners(self)
            mouseEventSingleton = UserInterfacePkg.MouseEventClass.Instance();
            self.Listeners(end + 1) = addlistener(mouseEventSingleton, 'MouseMotion', @self.MouseMotionCallback);
            self.Listeners(end + 1) = addlistener(mouseEventSingleton, 'MouseDown', @self.MouseDownCallback);
            self.Listeners(end + 1) = addlistener(mouseEventSingleton, 'MouseUp', @self.MouseUpCallback);
        end
        
        function InitializeTemplateSelection(self)
            self.TemplateMode = 'Initial';
        end
        
        function MatchTemplate(self, varargin)
            xLim = [self.RegionSelector.Position(1)...
                self.RegionSelector.Position(1) + self.RegionSelector.Position(3)];

            xValues = self.XValues;
            yValues = self.YValues;
            selectedValues = or(self.XValues < xLim(1), self.XValues > xLim(2));
            yValues(selectedValues) = 0;

            self.FitCollection.PerformFit(xValues, yValues, xLim);
            disp(['Mean interval length: ' num2str(1000 * self.FitCollection.Beta(2)) ' ms']);

            fitXValues = xValues(1):.0001:xValues(end);
            if (~isempty(self.FitLine) && ishandle(self.FitLine))
                set(self.FitLine, 'XData', fitXValues, 'YData', self.FitCollection.YValues(fitXValues))
            else
                self.FitLine = line(fitXValues, self.FitCollection.YValues(fitXValues),...
                   'linewidth', 4,...
                   'color', 'green',...
                   'parent', self.ControlHandle,...
                   'lineSmoothing', 'on');
            end
        end
        
        function delete(self)
            if ~isempty(self.Listeners)
                delete(self.Listeners);
            end
        end
    end
    
    methods ( Access = private)
        function MouseMotionCallback(self, varargin)
            currentPoint = get(self.ControlHandle, 'currentPoint');
            
            xLimits = xlim(self.ControlHandle);
            yLimits = ylim(self.ControlHandle);
            if (currentPoint(1, 1) < xLimits(1) || currentPoint(1, 1) > xLimits(2)) ||...
                    (currentPoint(1, 2) < yLimits(1) || currentPoint(1, 2) > yLimits(2))
                return;
            end
            
            switch self.TemplateMode
                case {'Initial'}
                    return;
                case {'Placing', 'Moving'}
                    self.RegionSelector.SetXPosition(currentPoint(1, 1));
                case 'MovingStart'
                    self.RegionSelector.SetStart(currentPoint(1, 1));
                case 'MovingEnd'
                    self.RegionSelector.SetEnd(currentPoint(1, 1));
                case 'Finished'
                    return;
                otherwise
                    [templateSelected, selectedPart] =...
                        self.RegionSelector.GetSelected;
                    if templateSelected
                        switch selectedPart
                            case 'Center'
                                set(gcf, 'Pointer', 'hand');
                            case 'Start'
                                set(gcf, 'Pointer', 'left');
                            case 'End'
                                set(gcf, 'Pointer', 'right');
                        end
                    else
                        set(gcf, 'Pointer', 'arrow');
                    end
            end
        end
        
        function MouseDownCallback(self, varargin)
            switch self.TemplateMode
                case 'Placing'
                    self.TemplateMode = 'None';
                    self.RegionSelector.OnMove = @self.MoveTemplateCallback;
                    self.RegionSelector.OnMoveStart = @self.MoveTemplateStartCallback;
                    self.RegionSelector.OnMoveEnd = @self.MoveTemplateEndCallback;
            end
            self.MouseIsDown = true;
        end
        
        function MouseUpCallback(self, varargin)
            self.MouseIsDown = false;
            self.TemplateMode = 'None';
            self.MatchTemplate();
        end
        
        function MoveTemplateCallback(self, varargin)
            self.TemplateMode = 'Moving';
        end
        
        function MoveTemplateStartCallback(self, varargin)
            self.TemplateMode = 'MovingStart';
        end
        
        function MoveTemplateEndCallback(self, varargin)
            self.TemplateMode = 'MovingEnd';
        end
    end
end