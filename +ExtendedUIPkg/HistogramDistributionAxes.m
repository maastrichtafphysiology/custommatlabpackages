classdef HistogramDistributionAxes < EcgMappingGuiPkg.HistogramAxes
	properties (Access = private)
		PdfLine
        ParameterText
	end
 
    methods
        function self = HistogramDistributionAxes(position, label)
            self = self@EcgMappingGuiPkg.HistogramAxes(position, label);
			self.PdfLine = UserInterfacePkg.CustomLine([], []);
        end
		
		function Create(self, parentHandle)
            Create@EcgMappingGuiPkg.HistogramAxes(self, parentHandle);
			self.PdfLine.Create(self.ControlHandle);
        end
               
        function Draw(self, values, bins, pdf, parameterEstimates, parameterEstimateStrings)
            Draw@EcgMappingGuiPkg.HistogramAxes(self, values, bins);
			pdfLineXValues = (self.Bins(1):((self.Bins(end) - self.Bins(1)) / 100):self.Bins(end))';
            pdfLineYValues = self.HistogramArea * pdf(pdfLineXValues);
			delete(self.PdfLine);
			self.PdfLine = UserInterfacePkg.CustomLine(pdfLineXValues, pdfLineYValues);
            self.PdfLine.Create(self.ControlHandle);
			self.PdfLine.Color = [1 0 0];
			self.PdfLine.Width = 2;
            
            displayStrings(1) = strcat(parameterEstimateStrings(1),num2str(parameterEstimates(1)));
            displayStrings(2) = strcat(parameterEstimateStrings(2),num2str(parameterEstimates(2)));
            displayStrings(3) = strcat(parameterEstimateStrings(3),num2str(parameterEstimates(3)));
            displayStrings(4) = strcat(parameterEstimateStrings(4),num2str(self.HistogramArea * pdf(parameterEstimates(2))));
            displayStrings(5) = strcat(parameterEstimateStrings(5),num2str(parameterEstimates(3)));
            displayStrings(6) = strcat(parameterEstimateStrings(6),num2str(parameterEstimates(4)));
            displayStrings(7) = strcat(parameterEstimateStrings(6),num2str(parameterEstimates(5)));
                
            self.ParameterText = text('parent', self.ControlHandle, 'units', 'normalized', 'position', [0.7 0.5], 'string', displayStrings, 'HorizontalAlignment','left');
        end
		
    end
end