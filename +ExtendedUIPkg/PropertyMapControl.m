classdef PropertyMapControl < ExtendedUIPkg.GridMapControl
    properties
        DataRange
    end
    
    properties (Access = private)
        ColorBarAxes
        ColorBarImage
    end
    
    methods
        function self = PropertyMapControl(position)
            self = self@ExtendedUIPkg.GridMapControl(position);
            
            self.Colormap = flipud(jet(self.Resolution));
        end
        
        function Create(self, parentHandle)
            Create@ExtendedUIPkg.GridMapControl(self, parentHandle);
            
            set(self.MapAxes, 'outerPosition', [0 0 .95 1]);
            self.ColorBarAxes = axes(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'outerPosition', [.95 0 .05 1],...
                'yAxisLocation', 'right');
        end
        
        function SetDataRange(self, range)
            self.DataRange = range;
            
            self.SetFrame();
        end
        
        function colorData = GetFrame(self, framePosition)
            frameData = self.Data(:, framePosition);
            if isempty(frameData)
                colorData = [];
                return;
            end
            
            dataRange = self.DataRange;
            if isempty(self.DataRange)
                dataRange = [min(frameData) max(frameData)];
            end
            relativeColors = (frameData - dataRange(1)) / (dataRange(2) - dataRange(1));
            relativeColors(isnan(relativeColors)) = 1;
            relativeColors = round(relativeColors * (self.Resolution - 1)) + 1;
            colors = ind2rgb(relativeColors, self.Colormap);
            
            cData = ones(size(get(self.Image, 'cData')));
            rData = ones(size(cData, 1), size(cData, 2));
            rData(self.ElectrodeImageIndices) = colors(:, 1);
            
            gData = ones(size(cData, 1), size(cData, 2));
            gData(self.ElectrodeImageIndices) = colors(:, 2);
            
            bData = ones(size(cData, 1), size(cData, 2));
            bData(self.ElectrodeImageIndices) = colors(:, 3);
            
            colorData = cat(3, rData, gData, bData);
        end
    end
    
    methods (Access = protected)
        function SetFrame(self)
            SetFrame@ExtendedUIPkg.GridMapControl(self);
            
            self.SetColorBar();
        end
        
        function SetColorBar(self)
            delete(get(self.ColorBarAxes, 'children'));
            if isempty(self.Data), return; end
            
            frameData = self.Data(:, self.FramePosition);
            if all(isnan(frameData)), return; end
            
            dataRange = self.DataRange;
            if isempty(self.DataRange)
                dataRange = [min(frameData) max(frameData)];
            end
            dataTicks = linspace(dataRange(1), dataRange(end), 10)';
            relativeColors = (dataTicks - dataRange(1)) / (dataRange(2) - dataRange(1));
            relativeColors = round(relativeColors * (self.Resolution - 1)) + 1;
            colors = ind2rgb(relativeColors, self.Colormap);
            
            self.ColorBarImage = image(...
                'xData', 1,...
                'yData', dataTicks,...
                'cData', colors,...
                'parent', self.ColorBarAxes);
            set(self.ColorBarAxes, 'xTick', [], 'xTickLabel', {});
            axis(self.ColorBarAxes, 'tight');
        end
    end
end