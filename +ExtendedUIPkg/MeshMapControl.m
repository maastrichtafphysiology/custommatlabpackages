classdef MeshMapControl < ExtendedUIPkg.MapControl
    properties
        InitialView
        DefaultFaceColor
        BackgroundColor
    end
    
    properties (Access = protected)
        Mesh
        MeshPatchHandles
        SelectedVertexHandles
        
        Data
        DataRange
        
        ColorBarAxes
    end
    
    methods
        function self = MeshMapControl(position)
            self = self@ExtendedUIPkg.MapControl(position);
            self.InitialView = struct(...
                'view', [0, 0],...
                'light', [100, 0, 0]);
            self.DefaultFaceColor = [1,1,1];
            self.BackgroundColor = [0,0,0];
        end
        
        function Create(self, parentHandle)
            Create@ExtendedUIPkg.MapControl(self, parentHandle);
            
            set(self.ControlHandle, 'backgroundColor', self.BackgroundColor);
        end
        
        function SetData(self, data, positions)
            self.Data = data;
            self.Positions = positions;
            
            allData = data(~isnan(data) & ~isinf(data));
            self.DataRange = [min(allData); max(allData)];
            self.SetColorBar();
        end
        
        function SetDataRange(self, range)
            self.DataRange = range;
            
            self.SetFrame();
            self.SetColorBar();
        end
        
        function SetMesh(self, vertices, faces)
            self.Mesh.vertices = vertices;
            self.Mesh.faces = faces;
            
            self.InitializeAxes();
            
            self.SetFrame();
        end
        
        function colorData = GetFrame(self, framePosition)
            frameData = self.Data(framePosition, :);
            relativeColors = (frameData - self.DataRange(1, :)) ./ (self.DataRange(2, :) - self.DataRange(1, :));            
            relativeColors = round(relativeColors * (self.Resolution - 1)) + 1;
            colorData = squeeze(ind2rgb(relativeColors, self.Colormap));
        end
        
        function HighlightSelectedVertices(self, selectedVertices)          
            if ~isempty(self.SelectedVertexHandles)
                validHandles = ishandle(self.SelectedVertexHandles);
                delete(self.SelectedVertexHandles(validHandles));
            end
            
            selectedFaces = self.Mesh.faces(...
                ismember(self.Mesh.faces(:, 1), selectedVertices) &...
                ismember(self.Mesh.faces(:, 2), selectedVertices) &...
                ismember(self.Mesh.faces(:, 3), selectedVertices), :);
            selectedVertexPositions = self.Mesh.vertices(selectedVertices, :);
            recodedFaces = NaN(size(selectedFaces));
            for vertexIndex = 1:numel(selectedVertices)
                recodedFaces(selectedFaces == selectedVertices(vertexIndex)) = vertexIndex;
            end
                           
            selectedFaceColor = [1,1,1];
            
            self.SelectedVertexHandles = patch('parent', self.MapAxes,...
                'vertices', selectedVertexPositions, 'faces', recodedFaces,...
                'marker', 'o', 'markerEdgeColor', [0,0,0], 'markerFaceColor', [1,1,1],...
                'EdgeAlpha',0.1, 'FaceColor', selectedFaceColor, 'FaceAlpha', .5, 'FaceLighting', 'gouraud', 'AmbientStrength', .5,...
                'DiffuseStrength', .8,'SpecularStrength', 0,'SpecularExponent', 10, 'pickableParts', 'none');
        end
    end
    
    methods (Access = protected)
        function InitializeAxes(self)
            light('parent', self.MapAxes, 'position', self.InitialView.light, 'style', 'local');
            
            self.MeshPatchHandles = patch('parent', self.MapAxes, 'vertices', self.Mesh.vertices, 'faces', self.Mesh.faces,...
                'EdgeAlpha',0.1, 'FaceColor', self.DefaultFaceColor, 'FaceAlpha', 1, 'FaceLighting', 'gouraud', 'AmbientStrength', .5,...
                'DiffuseStrength', .8,'SpecularStrength', 0,'SpecularExponent', 10, 'BackFaceLighting', 'unlit',...
                'buttonDownFcn', @self.OnMapClick);
            
            view(self.MapAxes, self.InitialView.view);
            hold(self.MapAxes, 'on');
            axis(self.MapAxes, 'equal');
            axis(self.MapAxes, 'off');
        end
        
        function SetFrame(self)
            if self.FramePosition > size(self.Data, 1), return; end
            
            colorData = self.GetFrame(self.FramePosition);
            
            set(self.MeshPatchHandles, 'FaceVertexCData', colorData, 'FaceColor', 'interp');
        end
              
        function OnMapClick(self, patch, eventData)
            patchDifferences = bsxfun(@minus, patch.Vertices, eventData.IntersectionPoint);
            patchDistances = sqrt(patchDifferences(:, 1).^2 + patchDifferences(:, 2).^2 + patchDifferences(:, 3).^2);
            [~, minIndex] = min(patchDistances);
            self.NotifyMapClick(minIndex);
        end
        
        function SetColorBar(self)
            if ishandle(self.ColorBarAxes)
                delete(self.ColorBarAxes)
            end
            
            if isempty(self.DataRange) || numel(self.DataRange) > 2, return; end
            
            self.ColorBarAxes = axes(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'outerPosition', [.9, 0, .05, 1],...
                'yAxisLocation', 'right');
            
            dataTicks = linspace(self.DataRange(1), self.DataRange(end), 10)';
            relativeColors = (dataTicks - self.DataRange(1)) / (self.DataRange(2) - self.DataRange(1));
            relativeColors = round(relativeColors * (self.Resolution - 1)) + 1;
            colors = ind2rgb(relativeColors, self.Colormap);
            
            newTicks = linspace(self.DataRange(1), self.DataRange(end), 5);
            image(...
                'xData', 1,...
                'yData', dataTicks,...
                'cData', colors,...
                'parent', self.ColorBarAxes);
            set(self.ColorBarAxes, 'xTick', [], 'xTickLabel', {},...
                'yTick', newTicks,...
                'yTickLabel', cellstr(strcat(num2str(newTicks(:), '%.2f'))),...
                'yColor', [1, 1, 1]);
            axis(self.ColorBarAxes, 'tight');
        end
    end
end