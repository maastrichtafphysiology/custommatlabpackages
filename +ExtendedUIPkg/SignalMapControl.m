classdef SignalMapControl < ExtendedUIPkg.MapControl
    properties
        Signals
        DataMin
        DataMax
        SignalImage
    end
    
    methods
        function self = SignalMapControl(position)
            self = self@ExtendedUIPkg.MapControl(position);
        end
        
        function Create(self, parentHandle)
            Create@ExtendedUIPkg.MapControl(self, parentHandle);
        end
        
        function SetData(self, data, positions)
            self.Signals = data;
            self.Positions = positions;
            
            self.InitializeAxes();
            
            self.SetFrame();
        end
        
        function colorData = GetFrame(self, framePosition)
            frameImage = self.Signals(:, :, framePosition);
            frameImage = (frameImage - self.DataMin) ./ (self.DataMax - self.DataMin);
            frameImage(isnan(frameImage)) = 0;
            frameImage = round(frameImage * (self.Resolution - 1)) + 1;
            colorData = ind2rgb(frameImage, self.Colormap);
        end
    end
    
    methods (Access = protected)
        function InitializeAxes(self)
            [xSize, ySize, numberOfSamples] = size(self.Signals);
            
            self.SignalImage = imagesc(zeros(xSize, ySize), 'parent', self.MapAxes,...
                'CDataMapping', 'scaled');
            set(self.MapAxes, 'xlim', [1 ySize], 'ylim', [1 xSize]);
            axis(self.MapAxes, 'image');
            set(self.MapAxes, 'clim', [0 1]);
            
            self.DataMin = min(self.Signals, [], 3);
            self.DataMax = max(self.Signals, [], 3);
            
            set(self.MapAxes, 'LooseInset', get(self.MapAxes, 'TightInset'));
        end
        
        function SetFrame(self)
            frameImage = self.Signals(:, :, self.FramePosition);
            frameImage = (frameImage - self.DataMin) ./ (self.DataMax - self.DataMin);
            frameImage(isnan(frameImage)) = 0;
            frameImage = round(frameImage * (self.Resolution - 1)) + 1;
            cData = ind2rgb(frameImage, self.Colormap);
            
            set(self.SignalImage, 'CData', cData);
        end
    end
end