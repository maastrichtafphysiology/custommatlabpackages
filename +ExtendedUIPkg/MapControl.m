classdef MapControl < UserInterfacePkg.CustomPanel
    properties
        SamplingFrequency
        Positions
        FramePosition
        TimeLag
    end
    
    properties (SetAccess = protected)
        MapAxes
        Resolution
        Colormap
        ContextMenu
    end
    
    events
        MapClicked
    end
   
    methods
        function self = MapControl(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            self.FramePosition = 1;
            self.SamplingFrequency = 1e3;
            self.TimeLag = 100;
            self.Resolution = 256;
            self.Colormap = jet(self.Resolution);
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.MapAxes = axes(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'outerPosition', [0 0 1 1],...
                'buttonDownFcn', @self.OnMapClick);
            
            self.ContextMenu = uicontextmenu('parent', ancestor(self.MapAxes, 'figure'));
            set(self.MapAxes, 'UIContextMenu', self.ContextMenu);
            uimenu(self.ContextMenu, 'Label', 'Copy data to clipboard', 'callback', @self.CopyDataToClipboard);
            uimenu(self.ContextMenu, 'Label', 'Show in separate Matlab figure', 'callback', @self.ShowInFigure);
        end
        
        function SetFramePosition(self, framePosition)
            self.FramePosition = framePosition;
            
            self.SetFrame();
        end
        
        function SetStaticFramePosition(self, framePosition)
            self.FramePosition = framePosition;
            
            self.SetStaticFrame();
        end
        
        function SetTimeLag(self, timeLag)
            self.TimeLag = timeLag;
            
            if isempty(self.Positions), return; end
            
            self.SetStaticFrame();
        end
        
        function MaximizeAxes(self)
            set(self.MapAxes, 'Position', [0 0 1 1]);
            axis(self.MapAxes, 'off');
        end
        
        function SetColormap(self, newColormap, resolution)
            self.Resolution = resolution;
            self.Colormap = newColormap;
        end
        
        function SetZoom(self, status)
            zoom(self.MapAxes, status)
        end
        
        function SetPan(self, status)
            pan(self.MapAxes, status)
        end
        
        function SetRotate3D(self, rotateObject, status)
            setAllowAxesRotate(rotateObject, self.MapAxes, status);
        end
        
        function mapAxes = get.MapAxes(self)
            mapAxes = self.MapAxes; 
        end
    end
    
    methods (Abstract)
        SetData(self, data, positions);
    end
        
    methods (Access = protected, Abstract) 
        SetFrame(self);
        InitializeAxes(self);
    end
    
    methods (Access = protected)
        function SetStaticFrame(self)
            self.SetFrame();
        end
        
        function minimumSpacing = ComputeMinimumSpacing(self)
            % obsolete: use the ECGData function instead
            distanceMatrix = pdist(self.Positions);
            distanceMatrix = distanceMatrix(distanceMatrix ~= 0);
            minimumSpacing = min(distanceMatrix);
        end
        
        function OnMapClick(self, varargin)
            point = get(self.MapAxes, 'currentPoint');
            point = point(1, :);
            distances = bsxfun(@minus, self.Positions, point);
            distances = sqrt(distances(:, 1).^2 + distances(:, 2).^2 + distances(:, 3).^2);
            [minValue, minPosition] = min(distances); %#ok<ASGLU>
            self.NotifyMapClick(minPosition);
        end
        
        function NotifyMapClick(self, clickPosition)
            notify(self, 'MapClicked', ExtendedUIPkg.MapClickedEventData(clickPosition));
        end
        
        function CopyDataToClipboard(self, varargin) %#ok<INUSD>
            return;
        end
        
        function ShowInFigure(self, varargin)
            screenSize = get(0, 'Screensize');
            figurePosition = [screenSize(3) / 5, screenSize(4) / 5, 3 * screenSize(3) / 5, 3 * screenSize(4) / 5];
            figureHandle = figure('position', figurePosition);
            newHandle = copyobj(self.MapAxes, figureHandle);
            axis(newHandle, 'off');
        end
    end
end