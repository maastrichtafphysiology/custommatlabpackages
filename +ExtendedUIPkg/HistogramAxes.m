classdef HistogramAxes < UserInterfacePkg.CustomAxes
    properties 
        XLabelString
    end
    
    properties (GetAccess = public)
        XValues
        YValues
    end
    
    properties (Access = protected)
		Bins
		BinCount
        ColorMap
        MaxValue
        MinValue
    end
	
	properties (Dependent = true)
		HistogramArea
	end

    methods
        function self = HistogramAxes(position, label)
            self = self@UserInterfacePkg.CustomAxes(position);
            self.XLabelString = label;
			self.Bins = [];
			self.BinCount = 0;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomAxes(self, parentHandle);
        end
        
        function Draw(self, values, bins)
			self.Bins = bins;
			[self.BinCount, xout] = histc(values, bins);
            bar(self.ControlHandle, bins, self.BinCount);
%             plot(self.ControlHandle, xout, self.BinCount);
%             hist(self.ControlHandle, values, bins);
            self.XValues = (bins(1:(end - 1)) + diff(bins) / 2)';
            self.YValues = self.BinCount(1:(end-1));
%             set(self.ControlHandle, 'xlim', [0 max(bins)]);
            axis(self.ControlHandle, 'tight');
            xlabel(self.ControlHandle, self.XLabelString)
        end
		
		function value = get.HistogramArea(self)
			if numel(self.Bins > 1)
                value = self.BinCount(:)' * self.Bins(:);
			else
				value = 0;
			end
		end
    end
end