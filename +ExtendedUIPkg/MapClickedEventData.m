classdef MapClickedEventData < event.EventData
    properties
        Position
        Time
    end
    
    methods
        function self = MapClickedEventData(position, varargin)
            self.Position = position;
            self.Time = NaN;
            
            if nargin > 1
                self.Time = varargin{1};
            end
        end
    end
    
end