classdef HistogramControl < UserInterfacePkg.CustomPanel
    properties (Access = protected)
    	Bins
		Cycles
		CycleLengthBinsEdit
        HistogramAxes
        XLabel
    end
    
    properties (Constant, Access = protected)
        HistogramAxesPosition = [0.001 0.001 .98 .9];           
    end
    
    methods
        function self = HistogramControl(position, histogramAxes)
            parser = inputParser;
            parser.addRequired('histogramAxes', @(x) (isa(x, 'ExtendedUIPkg.HistogramAxes')));
            parser.parse(histogramAxes);
            
            self = self@UserInterfacePkg.CustomPanel(position);

            self.HistogramAxes = histogramAxes;
            self.Bins = 0:0.002:0.4;
            self.CycleLengthBinsEdit = zeros(1,3);
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            self.BuildGUI();
        end
        
        function set.Bins(self, value)
            if ~(isempty(value) || any(isnan(value)))
                self.Bins = value;
            end
        end

        function BuildGUI(self)
            editHeight = 0.1;
            self.CycleLengthBinsEdit(1) =...
                uicontrol('style', 'edit',...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [.35 .85 .1 editHeight],...
                'BackgroundColor', 'white',...
                'string', num2str(self.Bins(1)),...
                'callback', @self.CycleLengthCallback);
            self.CycleLengthBinsEdit(2) =...
                uicontrol('style', 'edit',...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [.50 .85 .1 editHeight],...
                'BackgroundColor', 'white',...
                'string', num2str(self.Bins(2)-...
                self.Bins(1)),...
                'callback', @self.CycleLengthCallback);
            self.CycleLengthBinsEdit(3) =...
                uicontrol('style', 'edit',...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [.65 .85 .1 editHeight],...
                'BackgroundColor', 'white',...
                'string', num2str(self.Bins(end)),...
                'callback', @self.CycleLengthCallback);
			
            self.AddCustomControl(self.HistogramAxes);
        end
        
        function Show(self, bins, cycles)
            Show@UserInterfacePkg.CustomPanel(self);
			self.Cycles = cycles;
            if ~isempty(bins)
                self.Bins = bins;
            end

            self.HistogramAxes.Draw(self.Cycles, self.Bins);
            self.HistogramAxes.Show();
        end
        
        function Hide(self)
            Hide@UserInterfacePkg.CustomPanel(self);
            self.HistogramAxes.Hide();
        end
        
        function delete(self)
            delete(self.HistogramAxes);
        end
    end
	
	methods (Access = protected)
		function CycleLengthCallback(self, varargin)
            CycleLengthStart = str2double(get(self.CycleLengthBinsEdit(1), 'string'));
            CycleLengthStep = str2double(get(self.CycleLengthBinsEdit(2), 'string'));
            CycleLengthEnd = str2double(get(self.CycleLengthBinsEdit(3), 'string'));
            if CycleLengthStart <= CycleLengthEnd
                self.Bins = CycleLengthStart:CycleLengthStep:CycleLengthEnd;
            end
            self.HistogramAxes.Draw(self.Cycles, self.Bins);
			set(self.CycleLengthBinsEdit(1), 'string', self.Bins(1));
			set(self.CycleLengthBinsEdit(2), 'string', self.Bins(2)-self.Bins(1));
			set(self.CycleLengthBinsEdit(3), 'string', self.Bins(end));
        end
    end
    
    methods (Static)
        function control = BasicHistogramControl(position, label)
            histogramAxes = ExtendedUIPkg.HistogramAxes(...
                ExtendedUIPkg.HistogramControl.HistogramAxesPosition, label);
            control = ExtendedUIPkg.HistogramControl(position, histogramAxes);
        end
    end
end

