classdef HistogramFitControl < ExtendedUIPkg.HistogramControl
    properties (Access = protected)
        FitCollection
    	FitFunctionSelector
    end
    
    methods
        function self = HistogramFitControl(position, histogramAxes, fitCollection)
            self = self@ExtendedUIPkg.HistogramControl(position, histogramAxes);
            self.FitCollection = fitCollection;
        end
        
        function Create(self, parentHandle)
            Create@ExtendedUIPkg.HistogramControl(self, parentHandle);
            
            self.FitFunctionSelector = uicontrol('style', 'popup',...
                'parent', self.ControlHandle,...
                'String',  HistogramFitCollection.AvailableFitFunctions,...
                'units', 'normalized',...
                'position', [0.01 0.85 0.14 .1],...
                'callback', @self.FitFunctionSelectionChanged);
        end
        
        function [type, beta] = GetFitTypeAndParameters(self)
            [type, beta] = self.FitCollection.GetFitTypeAndParameters();
        end
    end
	
	methods (Access = private)
		function FitFunctionSelectionChanged(self, varargin)
            self.FitCollection.CurrentFitFunction = HistogramFitCollection.AvailableFitFunctions{...
                get(self.FitFunctionSelector, 'value')};
            self.HistogramAxes.MatchTemplate();
        end
    end
    
    methods (Static)
        function control = BasicHistogramFitControl(position, label, bins)
            fitCollection = HistogramFitCollection();
            histogramAxes = ExtendedUIPkg.HistogramFitAxes(...
                ExtendedUIPkg.HistogramControl.HistogramAxesPosition, label, fitCollection);
            control = ExtendedUIPkg.HistogramFitControl(position, histogramAxes, fitCollection);
            if nargin > 2
                control.Bins = bins;
            end
        end
    end
end

