function Recomposed_Signal = get_recomposed_sinusoidal_signal( Data_Vector, Period, Electrogram_Type)
%   GET_RECOMPOSED_SINUSOIDAL_SIGNAL Summary of this function goes here
%   Electrogram_Type: 0 - unipolar
%   Electrogram_Type: 1 - bipolar


Recomposed_Signal = zeros(numel(Data_Vector),1);
Period = round(Period);

% create sinusoid
Sinusoid_Wavelet = zeros(Period+1,1);
for t=1:Period
Sinusoid_Wavelet(t) = sin( 2*pi*(t-Period/2)/Period);    
end

if Electrogram_Type== 0 % unipolar electrogram, slope driven recomposition
for t=2:numel(Recomposed_Signal)-1

    diff = Data_Vector(t+1) - Data_Vector(t-1);
    if diff < 0 
    for tt=-floor(Period/2):floor(Period/2)
    if t+tt>0 && t+tt<numel(Data_Vector)
    Recomposed_Signal(t+tt)= Recomposed_Signal(t+tt) + ...
        diff*Sinusoid_Wavelet(floor(tt+Period/2+1)); 
    end
    end
    end

end
end

if Electrogram_Type== 1 % bipolar electrogram, amplitude driven recomposition
for t=2:numel(Recomposed_Signal)-1
    for tt=-floor(Period/2):floor(Period/2)
    if t+tt>0 && t+tt<numel(Data_Vector)
    Recomposed_Signal(t+tt)= Recomposed_Signal(t+tt) + ...
        abs(Data_Vector(t))*Sinusoid_Wavelet(floor(tt+Period/2+1)); 
    end
    end
end
end

end

