function PeakIndices = get_deflection_positions_using_phase_transitions( Data_Vector, Period, Electrogram_Type )
%GET_DEFLECTION_POSITIONS_USING_PHASE_TRANSITIONS Summary of this function goes here
%   Detailed explanation goes here
%   Period: in samples, underlying cycle length in the electrogram
%   Electrogram_Type: 0 - unipolar
%   Electrogram_Type: 1 - bipolar

Phase = get_Hilbert_phase_using_sig_recomposition(Data_Vector, Period, Electrogram_Type);

Diff_vector = -diff(Phase);
Diff_vector = (Diff_vector>0).*Diff_vector; % to get only positive spikes
[Peaks,PeakIndices] = findpeaks(Diff_vector,'MINPEAKHEIGHT',0.5*range(Diff_vector));

end

