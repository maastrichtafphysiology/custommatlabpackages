function Phase = get_Hilbert_phase_using_sig_recomposition( Data_Vector, Period, Electrogram_Type )
%GET_HILBERT_HASE_USING_SIG_RECOMPOSITION Summary of this function goes here
%   Detailed explanation goes here
%   Electrogram_Type: 0 - unipolar
%   Electrogram_Type: 1 - bipolar

SIG = get_recomposed_sinusoidal_signal(Data_Vector,Period, Electrogram_Type );
h = hilbert(SIG);

Phase=atan2(real(h),-imag(h)); % this is correct 

end

