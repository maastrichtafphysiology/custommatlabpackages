classdef KeyEventClass < handle
    events
        KeyPressed
        KeyReleased
    end
    
    methods (Access = private)
        function self = KeyEventClass() 
            
        end
    end
    
    methods (Static)
        function self = Instance()
            persistent uniqueInstance;
            if isempty(uniqueInstance)
                uniqueInstance = UserInterfacePkg.KeyEventClass();
            end
            self = uniqueInstance;
        end
    end
    
    methods
        function NotifyKeyPressed(self, eventData, varargin)
            notify(self, 'KeyPressed', eventData);
        end
        
        function NotifyKeyReleased(self, eventData, varargin)
            notify(self, 'KeyReleased', eventData);
        end
    end
end