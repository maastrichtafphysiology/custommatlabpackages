classdef StatusEventClass < handle
    events
        StatusChange
    end
    
    methods (Access = private)
        function self = StatusEventClass() 
            
        end
    end
    
    methods (Static)
        function self = Instance()
            persistent uniqueInstance;
            if isempty(uniqueInstance)
                uniqueInstance = UserInterfacePkg.StatusEventClass();
            end
            self = uniqueInstance;
        end
    end
    
    methods
        function NotifyStatusChange(self, eventData, varargin)
            notify(self, 'StatusChange', eventData);
        end
    end
end