classdef IStatusChange < handle
    properties
        AbortNow 
    end
    events
        StatusChange
    end
end