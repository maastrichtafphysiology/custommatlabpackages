classdef CustomEditControl < UserInterfacePkg.CustomControl
    methods
        function self = CustomEditControl(position)
            self = self@UserInterfacePkg.CustomControl(position);
        end
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomControl(self, parentHandle);
            self.ControlHandle = uicontrol('style', 'edit',...
                'units', 'normalized',...
                'position', self.Position,...
                'string', self.Name,...
                'BackgroundColor', 'white');
        end
        
        
    end
end