classdef FileDialog < handle
    properties (GetAccess = public)
       Filename
       FilterIndex
       Pathname
    end
    
    properties
        InitialPath
        FilterSpec
        Title
    end
    
    
    methods
        function self = FileDialog()
            if ispc
                self.InitialPath = getenv('USERPROFILE'); 
            else
                self.InitialPath = [getenv('HOME') '/Documents'];
            end
            self.FilterSpec = {' *.*';'All Files'};
        end
        
        function set.InitialPath(self, path)
            self.InitialPath = path;
        end
        
        function set.FilterSpec(self, filterSpec)
            self.FilterSpec = filterSpec;
        end
        
        function set.Title(self, title)
            self.Title = title;
        end
    end
end
