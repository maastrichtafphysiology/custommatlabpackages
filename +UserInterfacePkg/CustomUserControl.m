classdef CustomUserControl < UserInterfacePkg.CustomControl
   properties
        CustomControlCollection;
    end
    
    methods       
        function self = CustomUserControl(position)
            self = self@UserInterfacePkg.CustomControl(position);
            self.CustomControlCollection = {};
        end
        function AddCustomControl(self, customControl)
            parser = inputParser;
            parser.addRequired('customControl', @(x) (isa(x, 'UserInterfacePkg.CustomUserControl')));
            parser.parse(customControl);
            self.CustomControlCollection{end + 1} = customControl;
            customControl.Create(self.ControlHandle);
        end
        
        function AddListener(self, source, eventName)
            for controlIndex = 1:numel(self.CustomControlCollection)
                self.CustomControlCollection{controlIndex}.AddListener(source, eventName);
            end
        end
    end 
end