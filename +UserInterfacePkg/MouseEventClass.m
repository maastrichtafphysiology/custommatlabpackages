classdef MouseEventClass < handle
    events
        MouseDown
        MouseUp
        MouseMotion
    end
    
    methods (Access = private)
        function self = MouseEventClass() 
            
        end
    end
    
    methods (Static)
        function self = Instance()
            persistent uniqueInstance;
            if isempty(uniqueInstance)
                uniqueInstance = UserInterfacePkg.MouseEventClass();
            end
            self = uniqueInstance;
        end
    end
    
    methods
        function NotifyMouseDown(self, varargin)
            notify(self, 'MouseDown');
        end
        
        function NotifyMouseUp(self, varargin)
            notify(self, 'MouseUp');
        end
        
        function NotifyMouseMotion(self, varargin)
            notify(self, 'MouseMotion');
        end
    end
end