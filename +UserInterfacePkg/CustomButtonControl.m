classdef CustomButtonControl < UserInterfacePkg.CustomControl
    methods
        function self = CustomButtonControl(position, callback)
            self = self@UserInterfacePkg.CustomControl(position);
            self.Callback = callback;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomControl(self, parentHandle);
            
            self.ControlHandle = uicontrol('style', 'pushbutton',...
                'parent', parentHandle,...
                'units', 'normalized',...
                'position', self.Position,...
                'string', self.Name,...
                'callback', self.Callback);
        end
    end
end