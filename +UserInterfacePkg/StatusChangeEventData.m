classdef StatusChangeEventData < event.EventData
    properties
        Message
        Object
        Progress
    end
    
    methods
        function self = StatusChangeEventData(message, progress, object)
            self.Message = message;
            self.Progress = progress;
            self.Object = object;
        end
    end
end

