classdef KeyPressedEventData < event.EventData
    
    properties
        Character;
        Modifier;
        Key;
    end
    
    methods
        function self = KeyPressedEventData(eventDataStruct)
            self.Character = eventDataStruct.Character;
            self.Modifier = eventDataStruct.Modifier;
            self.Key = eventDataStruct.Key;
        end
    end
    
end

