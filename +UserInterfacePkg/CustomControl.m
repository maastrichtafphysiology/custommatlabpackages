%> @brief CustomControl class: base GUI component class
classdef CustomControl < handle
    properties
        ControlHandle
        ParentHandle
        Callback
        Position
    end
    
    properties (Access = protected)
        Visible
    end
    
    properties (SetObservable)
        Name
    end
    
    methods
        function self = CustomControl(position)
            self.Position = position;
            self.Name = '';
            self.Callback = '';
            self.Visible = true;
            addlistener(self, 'Name', 'PostSet', @self.SetNameCallback);
        end
        
        function delete(self)
            if ishandle(self.ControlHandle)
                delete(self.ControlHandle);
            end
        end
        
        function Create(self, parentHandle)
            parser = inputParser;
            parser.addRequired('parentHandle', @(x) (ishandle(x) &&...
                any(strcmpi(get(x, 'type'), {'figure', 'uipanel', 'uitab'}))));
            parser.parse(parentHandle);
            self.ParentHandle = parentHandle;
        end
        
        function SetPosition(self, position)
            self.Position = position;
            if ishandle(self.ControlHandle)
                set(self.ControlHandle, 'position', position);
            end
        end
        
        function Show(self)
            for objectIndex = 1:numel(self)
                set(self(objectIndex).ControlHandle, 'visible', 'on');
                self(objectIndex).Visible = true;
            end
        end
        
        function Hide(self)
            for objectIndex = 1:numel(self)
                set(self(objectIndex).ControlHandle, 'visible', 'off');
                self(objectIndex).Visible = false;
            end
        end
        
        function Enable(self)
            for objectIndex = 1:numel(self)
                set(self(objectIndex).ControlHandle, 'enable', 'on');
            end
        end
        
        function Disable(self)
            for objectIndex = 1:numel(self)
                set(self(objectIndex).ControlHandle, 'enable', 'off');
            end
        end
    end
    
    methods (Access = protected)
        function SetNameCallback(self, varargin)
            set(self.ControlHandle, 'string', self.Name);
            pause(eps);
        end
    end
end