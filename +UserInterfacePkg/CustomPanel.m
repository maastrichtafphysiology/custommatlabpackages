classdef CustomPanel < UserInterfacePkg.CustomUserControl
    
    methods
        function self = CustomPanel(position)
            self = self@UserInterfacePkg.CustomUserControl(position);
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomUserControl(self, parentHandle);
            self.ControlHandle = uipanel('parent', parentHandle,...
                'units', 'normalized',...
                'position', self.Position,...
                'title', self.Name,...
                'visible', 'off');
        end
    end
    
    methods (Access = protected)
        function SetNameCallback(self, varargin)
            set(self.ControlHandle, 'title', self.Name);
            pause(eps);
        end
    end
end