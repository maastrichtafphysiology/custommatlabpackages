classdef StatusBar < UserInterfacePkg.CustomPanel
    properties (Access = protected)
        AbortButton
        AbortButtonPressed
        StatusLabel
        ProgressPatch
        ProgressText
    end
    
    properties (Dependent = true)
        StatusText
        Progress
    end
    
    methods
        function self = StatusBar(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            self.AbortButtonPressed = 0;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            self.BuildGUI();
        end
        
        function AddListener(self, source, eventName)
            if strcmp(eventName, 'StatusChange')
                addlistener(source, 'StatusChange', @self.StatusChange);
            end
        end
        
        function StatusChange(self, source, eventdata)
            self.StatusText = eventdata.Message;
            self.Progress = eventdata.Progress;
            drawnow update;
%             pause(eps);
            
            if (eventdata.Progress == 1)
                set(self.AbortButton, 'Enable', 'off');
                set(self.AbortButton, 'Backgroundcolor', [0.8314 0.8157 0.7843]);
            elseif (eventdata.Progress == 0)
                set(self.AbortButton, 'Enable', 'on');
                set(self.AbortButton, 'Backgroundcolor', 'red');
            end
            
            if (self.AbortButtonPressed)
               eventdata.Object.AbortNow = 1;
               self.AbortButtonPressed = 0;
            end
        end
        
        function set.StatusText(self, value)
            set(self.StatusLabel, 'string', value);
        end
        
        function set.Progress(self, value)
            if ~isempty(value)
            x = get(self.ProgressPatch, 'xData');
            x(3:4) = value;
            set(self.ProgressPatch, 'xData', x)
            
            set(self.ProgressText, 'string', [num2str(round(100 * value)) '% completed']);
            else
                set(self.ProgressPatch, 'xData', 0);
                set(self.ProgressText, 'string', '');
            end
        end
    end
    
    methods (Access = protected)
        function BuildGUI(self)
            set(self.ControlHandle,...
                'borderType', 'etchedin');
            self.StatusLabel = uicontrol(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [0.01 .05 .28 .9],...
                'style', 'text',...
                'HorizontalAlignment', 'left');
            
            axesHandle = axes(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [.3 .05 .65 .9],...
                'XLim', [0 1], 'YLim', [0 1],...
                'XTick', [], 'YTick', [],...
                'XColor', 'w', 'YColor', 'w');
            self.ProgressPatch = patch([0 0 0 0], [0 1 1 0], 'b',...
                'parent', axesHandle,...
                'edgeColor', 'none');
            self.ProgressText = text(.5, .5, '',...
                'parent', axesHandle,...
                'horizontalAlignment', 'center');
            self.AbortButton = uicontrol(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [0.95 .0 .05 1],...
                'string', 'Abort',...
                'enable', 'off',...
                'style', 'pushbutton',...
                'callback', @self.AbortCallback);
        end
        
        function AbortCallback(self, varargin)
           self.AbortButtonPressed = 1;
        end
    end
end