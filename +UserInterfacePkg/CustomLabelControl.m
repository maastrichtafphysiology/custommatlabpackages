classdef CustomLabelControl < UserInterfacePkg.CustomControl
    properties (Dependent)
        HorizontalAlignment
    end
    
    methods
        function self = CustomLabelControl(position)
            self = self@UserInterfacePkg.CustomControl(position);
        end
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomControl(self, parentHandle);
            self.ControlHandle = uicontrol(...
                'parent', parentHandle,...
                'style', 'text',...
                'units', 'normalized',...
                'position', self.Position,...
                'string', self.Name,...
                'HorizontalAlignment', 'center');
        end
        
        function set.HorizontalAlignment(self, value)
            set(self.ControlHandle, 'HorizontalAlignment', value);
        end
    end
end