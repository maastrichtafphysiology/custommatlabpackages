classdef ECGTablePanel < UserInterfacePkg.CustomPanel
    properties
        EcgData
        SelectedChannels
    end
    
    properties (Access = protected)
        ControlPanel
        ChannelPanel
        ChannelTable
        
        AxesPanel
        AxesHandles
    end
    
    properties (Constant)
        NUMBER_OF_DISPLAYED_CHANNELS = 3;
    end
    
    methods
        function self = ECGTablePanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            self.SelectedChannels = [];
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            self.CreateECGAxes();
        end
        
        function SetECGData(self, ecgData)
            self.EcgData = ecgData;
            
            if isempty(self.SelectedChannels)
                if self.EcgData.GetNumberOfChannels < UserInterfacePkg.ECGTablePanel.NUMBER_OF_DISPLAYED_CHANNELS
                    self.SelectedChannels = 1:self.EcgData.GetNumberOfChannels;
                else
                    self.SelectedChannels = 1:UserInterfacePkg.ECGTablePanel.NUMBER_OF_DISPLAYED_CHANNELS;
                end
            elseif numel(self.SelectedChannels) > self.EcgData.GetNumberOfChannels
                self.SelectedChannels = 1:self.EcgData.GetNumberOfChannels;
            end
            
            self.SetChannelList();
            self.ShowECGData();
        end
        
        function delete(self)
            self.EcgData = [];
        end
    end
    
    methods (Access = protected)
        function CreateECGAxes(self)
            self.AxesPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.2 0 .8 1]);
            
            uicontrol(...
                'parent', self.AxesPanel,...
                'units', 'normalized',...
                'position', [.95 0 .05 .05],...
                'style', 'pushbutton',...
                'string', 'Snapshot',...
                'callback', @self.Snapshot);
        end
        
        function CreateControlPanel(self)
            self.CreateChannelSelection();
        end
        
        function CreateChannelSelection(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 0 .2 1]);
            
            self.ChannelPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .2 1 .79],...
                'borderType', 'none');
            
            self.ChannelTable = uitable(...
                'parent', self.ChannelPanel,...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'backgroundColor', [1 1 1],...
                'cellEditCallback', @self.SetSelectedChannels,...
                'cellSelectionCallback', @self.SetMultipleSelectedChannels,...
                'interruptible', 'off',...
                'busyAction', 'queue');
        end
        
        function ShowECGData(self)
            time = self.EcgData.GetTimeRange();
            validHandles = ishandle(self.AxesHandles);
            if any(validHandles)
                delete(self.AxesHandles(validHandles));
            end
            self.AxesHandles = [];
            
            lineData = self.EcgData.Data(:, self.SelectedChannels);
            lineLabels = self.EcgData.ElectrodeLabels(self.SelectedChannels);
            
            if isempty(lineData), return; end
            
            set(self.AxesPanel, 'visible', 'off');
            self.AxesHandles = UserInterfacePkg.ECGTablePanel.TightSubplot(numel(lineLabels), 1, self.AxesPanel,...
                0.0025, [0.05, 0], [0, 0.05]);
            
            for lineIndex = 1:numel(lineLabels)
                lineColor = [0 0 0];
                line('xData', time, 'yData', lineData(:, lineIndex),...
                    'color', lineColor,...
                    'parent', self.AxesHandles(lineIndex),...
                    'lineSmoothing', 'on');
                text('parent', self.AxesHandles(lineIndex), 'string', lineLabels{lineIndex},...
                    'fontWeight', 'bold',...
                    'fontName', 'Helvetica',...
                    'fontSize', 11,...
                    'units', 'normalized', 'position', [0, 1, 0],...
                    'horizontalAlignment', 'left',...
                    'verticalAlignment', 'top',...
                    'interpreter', 'none');
                set(self.AxesHandles(lineIndex), 'xTick', [],...
                    'YAxisLocation', 'right', 'xLim', [time(1), time(end)]);
                %                 axis(self.AxesHandles(lineIndex), 'tight');
            end
            set(self.AxesHandles(end),'xTickMode', 'auto');
            xlabel(self.AxesHandles(end), 'time (seconds)');
            if numel(self.AxesHandles) > 1
                handleHandles = ishandle(self.AxesHandles);
                validHandles = isvalid(self.AxesHandles);
                if any(validHandles)
                    linkaxes(self.AxesHandles(validHandles), 'xy');
                end
            end
            set(self.AxesPanel, 'visible', 'on');
        end
        
        function SetChannelList(self)
            electrodeLabels = self.EcgData.ElectrodeLabels;
            channelSelected = false(size(electrodeLabels));
            channelSelected(self.SelectedChannels) = true;
            self.ChannelTable.Data = num2cell(channelSelected);
            self.ChannelTable.ColumnName = {'Show'};
            self.ChannelTable.RowName = electrodeLabels;
            self.ChannelTable.ColumnEditable = true;
        end
        
        function SetSelectedChannels(self, ~, cellEditData)
            selectedIndex = cellEditData.Indices;
            if selectedIndex(2) == 1
                tableData = self.ChannelTable.Data;
                self.SelectedChannels = find(vertcat(tableData{:, 1}));
                self.ShowECGData();
                return;
            end
        end
        
        function SetMultipleSelectedChannels(self, ~, cellSelectionData)
            selectedIndices = cellSelectionData.Indices;
            selectedChannels = selectedIndices(:, 1);
            if numel(selectedChannels) < 2, return; end
            
            if all(selectedIndices(:, 2) == 1)
                javaHandle = UtilityPkg.findjobj(self.ChannelTable);
                scrollPosition = javaHandle.getVerticalScrollBar.getValue;
                
                [channelFound, channelIndex] = ismember(selectedChannels, self.SelectedChannels);
                if numel(find(channelFound)) < numel(selectedChannels) - 1
                    self.SelectedChannels = unique([self.SelectedChannels; selectedChannels]);
                    self.ChannelTable.Data(self.SelectedChannels, 1) = {true};
                    drawnow;
                else
                    self.SelectedChannels(channelIndex(channelFound)) = [];
                    self.ChannelTable.Data(selectedChannels(channelFound), 1) = {false};
                    drawnow;
                end
                
                javaHandle.getVerticalScrollBar.setValue(scrollPosition);
                javaHandle.repaint;
                
                self.ShowECGData()
            end
        end
        
        function Snapshot(self, varargin)
            if ~any(ishandle(self.AxesHandles)), return; end
            
            screenSize = get(0, 'screensize');
            figurePosition = screenSize;
            figureHandle = figure('name', 'Snapshot', 'numberTitle', 'off',...
                'color', [1 1 1], 'outerPosition', figurePosition);
            
            timeRange = get(self.AxesHandles(1), 'xLim');
            lineData = self.EcgData.Data(:, self.SelectedChannels);
            lineLabels = self.EcgData.ElectrodeLabels(self.SelectedChannels);
            time = self.EcgData.GetTimeRange();
            visibleData = time >= timeRange(1) & time <= timeRange(end);
            axesHandles = NaN(numel(lineLabels), 1);
            
            for lineIndex = 1:numel(lineLabels)
                axesHandles(lineIndex) = subplot(numel(lineLabels), 1, lineIndex,...
                    'parent', figureHandle);
                lineColor = [0 0 0];
                line('xData', time(visibleData), 'yData', lineData(visibleData, lineIndex),...
                    'color', lineColor,...
                    'parent', axesHandles(lineIndex),...
                    'lineSmoothing', 'on',...
                    'lineWidth', 2);
                text('parent', axesHandles(lineIndex), 'string', lineLabels{lineIndex},...
                    'units', 'normalized', 'position', [-.05, .5, 0],...
                    'horizontalAlignment', 'right', 'fontWeight', 'demi');
                set(axesHandles(lineIndex), 'xLim', timeRange);
            end
            
            axis(axesHandles, 'off');
            linkaxes(axesHandles, 'x');
            set(axesHandles, {'looseInset'}, get(axesHandles, 'tightInset'));
            axis(axesHandles, 'tight');
        end
    end
    
    methods (Static)
        function axesHandles = TightSubplot(numberOfAxesRows, numberOfAxesColumns, parent, gap, heightMargins, widthMargins)
            
            % tight_subplot creates "subplot" axes with adjustable gaps and margins
            %
            % ha = tight_subplot(Nh, Nw, gap, marg_h, marg_w)
            %
            %   in:  Nh      number of axes in hight (vertical direction)
            %        Nw      number of axes in width (horizontaldirection)
            %        gap     gaps between the axes in normalized units (0...1)
            %                   or [gap_h gap_w] for different gaps in height and width
            %        marg_h  margins in height in normalized units (0...1)
            %                   or [lower upper] for different lower and upper margins
            %        marg_w  margins in width in normalized units (0...1)
            %                   or [left right] for different left and right margins
            %
            %  out:  ha     array of handles of the axes objects
            %                   starting from upper left corner, going row-wise as in
            %                   going row-wise as in
            %
            %  Example: ha = tight_subplot(3,2,[.01 .03],[.1 .01],[.01 .01])
            %           for ii = 1:6; axes(ha(ii)); plot(randn(10,ii)); end
            %           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')
            
            
            if nargin<4; gap = 0; end
            if nargin<5 || isempty(heightMargins); heightMargins = .05; end
            if nargin<6; widthMargins = .05; end
            
            if numel(gap)==1
                gap = [gap gap];
            end
            if numel(widthMargins)==1
                widthMargins = [widthMargins widthMargins];
            end
            if numel(heightMargins)==1
                heightMargins = [heightMargins heightMargins];
            end
            
            axesHeight = (1 - sum(heightMargins) - (numberOfAxesRows - 1) * gap(1)) / numberOfAxesRows;
            axesWidth = (1 - sum(widthMargins) - (numberOfAxesColumns - 1) * gap(2)) / numberOfAxesColumns;
            
            py = 1 - heightMargins(2) - axesHeight;
            
            axesHandles = gobjects(numberOfAxesRows * numberOfAxesColumns, 1);
            ii = 0;
            for ih = 1:numberOfAxesRows
                px = widthMargins(1);
                
                for ix = 1:numberOfAxesColumns
                    ii = ii+1;
                    axesHandles(ii) = axes('units','normalized', ...
                        'parent', parent,...
                        'position',[px, py, axesWidth, axesHeight]);
                    px = px + axesWidth + gap(2);
                end
                py = py - axesHeight - gap(1);
            end
        end
    end
end