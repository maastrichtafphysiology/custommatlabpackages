classdef CustomFigure < UserInterfacePkg.CustomUserControl
    
    events
        KeyPressed
        MouseDown
        MouseUp
        MouseMotion
        MouseScroll
        GUIResize
    end
    
    properties (Dependent)
        Colormap
    end
    
    methods
        function self = CustomFigure(position)
            self = self@UserInterfacePkg.CustomUserControl(position);
        end
        
        function Create(self)
            self.ControlHandle =...
                figure('position', self.Position,...
                'menuBar', 'none',...
                'color', get(0, 'defaultUicontrolBackgroundColor'),...
                'name', self.Name,...
                'numberTitle', 'off',...
                'renderer', 'OpenGL');
        end
        
        function HiddenCreate(self)
            self.ControlHandle =...
                figure('position', self.Position,...
                'menuBar', 'none',...
                'color', get(0, 'defaultUicontrolBackgroundColor'),...
                'name', self.Name,...
                'numberTitle', 'off',...
                'renderer', 'OpenGL',...
                'visible', 'off');
        end
        
        function AddListener(self, source, eventName)
            switch eventName
                case 'KeyPressed'
                    set(self.ControlHandle, 'KeyPressFcn', @self.KeyPressedCallback);
                case 'MouseDown'
                    set(self.ControlHandle, 'WindowButtonDownFcn', @self.MouseDownCallback);
                case 'MouseUp'
                    set(self.ControlHandle, 'WindowButtonUpFcn', @self.MouseUpCallback);
                case 'MouseMotion'
                    set(self.ControlHandle, 'WindowButtonMotionFcn', @self.MouseMotionCallback);
                case 'GUIResize'
                    set(self.ControlHandle, 'ResizeFcn', @(src,event)self.ResizeCallback);
                case 'MouseScroll'
                    set(self.ControlHandle, 'WindowScrollWheelFcn', @self.MouseScrollCallback);                    
            end
            AddListener@UserInterfacePkg.CustomUserControl(self, self, eventName);
        end
        
        function set.Colormap(self, value)
            set(self.ControlHandle, 'colormap', value);
        end
    end
    
    methods (Access = protected)
        function SetNameCallback(self, varargin)
            set(self.ControlHandle, 'name', self.Name);
            pause(eps);
        end
    end
    
    methods (Access = private)
        function KeyPressedCallback(self, source, eventData)
            notify(self, 'KeyPressed', EventPkg.KeyPressedEventData(eventData));
        end
        
        function MouseDownCallback(self, source, eventData)
            notify(self, 'MouseDown');
        end
        
        function MouseUpCallback(self, source, eventData)
            notify(self, 'MouseUp');
        end
        
        function MouseMotionCallback(self, source, eventData)
            notify(self, 'MouseMotion');
        end
        
        function MouseScrollCallback(self, source, eventData)
            notify(self, 'MouseScroll', EventPkg.MouseScrollEventData(eventData));
        end
        
        function ResizeCallback(self, source, eventData)
            notify(self, 'GUIResize');
        end
        
        function CloseRequest(self, varargin)
            selection = questdlg('Are you sure?',...
                'Close figure',...
                'Yes','No','Yes');
            switch selection,
                case 'Yes',
                    for i = 1 : 1 : numel(self.CustomControlCollection)
                        delete(self.CustomControlCollection{i});
                    end
                    delete(gcbf);
                case 'No'
                    return
            end
        end
    end
    
    methods (Static)
        function CheckSingleMenuItem(menuItemHandle)
            set(get(get(menuItemHandle, 'parent'), 'children'), 'checked', 'off');
            set(menuItemHandle, 'checked', 'on');
        end
    end
end