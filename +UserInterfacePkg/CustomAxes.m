classdef CustomAxes < UserInterfacePkg.CustomUserControl
    properties
        Children;
    end
    
    properties (Dependent = true)
        XLimits
        YLimits
        XLabel
        YLabel
        Title
        Grid
        XTickLabels
        YTickLabels
    end
    
    methods
        function self = CustomAxes(position)
            self = self@UserInterfacePkg.CustomUserControl(position);
            self.Children = [];
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomUserControl(self, parentHandle);
            self.ControlHandle =...
                axes('OuterPosition', self.Position,...
                'parent', parentHandle,...
                'busy', 'cancel',...
                'interruptible', 'on',...
                'units', 'normalized',...
                'box', 'on');
        end
        
        function value = get.XLimits(self)
            value = get(self.ControlHandle, 'xLim');
        end
        function set.XLimits(self, value)
            set(self.ControlHandle, 'xLim', value);
        end
        
        function value = get.YLimits(self)
            value = get(self.ControlHandle, 'yLim');
        end
        function set.YLimits(self, value)
            set(self.ControlHandle, 'yLim', value);
        end

        function set.XLabel(self, value)
            xlabel(self.ControlHandle, value);
        end
        function set.YLabel(self, value)
            ylabel(self.ControlHandle, value);
        end
        
        function set.Title(self, value)
            title(self.ControlHandle, value);
        end
        
        function set.Grid(self, value)
            grid(self.ControlHandle, value);
        end
        
        function set.XTickLabels(self, value)
            set(self.ControlHandle, 'xTickLabels', value);
        end
        function set.YTickLabels(self, value)
            set(self.ControlHandle, 'yTickLabels', value);
        end
    end
    
    methods (Abstract)
        Draw(self) 
    end
end