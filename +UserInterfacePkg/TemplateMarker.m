classdef TemplateMarker < handle

   properties (Access = public)
        StartPos
        NumbOfSamples
        Data
        LineHandle
    end    

    methods
        function self = TemplateMarker(startPos, numbOfSamples, data)
            self.StartPos = startPos;
            self.NumbOfSamples = numbOfSamples;
            self.Data = data;
            self.LineHandle = NaN;
        end
        
        function delete(self)
            if ishandle(self.LineHandle)
                delete(self.LineHandle);
            end
        end
        
        function Show(self)
            set(self.LineHandle, 'visible', 'on');                        
        end
        
        function Hide(self)
            set(self.LineHandle, 'visible', 'off');            
        end        
    end

end