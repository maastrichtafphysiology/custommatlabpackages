classdef OpenFileDialog < UserInterfacePkg.FileDialog
    methods
        function self = OpenFileDialog()
            self = self@UserInterfacePkg.FileDialog();
            self.Title = 'Open file';
        end
        
        function succes = Show(self)
            [self.Filename, self.Pathname, self.FilterIndex] = ...
                uigetfile(self.FilterSpec, self.Title, self.InitialPath);
            if isequal(self.Filename, 0)
                succes = false;
            else
                self.InitialPath = fullfile(self.Pathname, self.Filename);
                succes = true;
            end
        end
    end
end