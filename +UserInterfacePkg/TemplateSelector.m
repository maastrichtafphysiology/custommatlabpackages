classdef TemplateSelector < handle
    
    properties (Access = public)
        ParentHandle
        Position
    end
           
    properties (Access = protected)
        StartLine
        EndLine
        FillPatch
        
        ContextMenu
        MatchTemplateMenu
    end
    
    properties (Dependent)
        OnMove
        OnMoveStart
        OnMoveEnd
        
        OnMatchTemplate
    end
    
    methods
        function self = TemplateSelector()
            self.FillPatch = [];
            self.StartLine = [];
            self.EndLine = [];
        end
        
        function Create(self, parentHandle, position)
            self.Position = position;
            self.ParentHandle = parentHandle;
            self.FillPatch = patch(...
                [position(1), position(1), position(1) + position(3), position(1) + position(3)],...
                [position(2), position(2) + position(4), position(2) + position(4), position(2)],...
                [.7 .7 .7],...
                'parent', parentHandle,...
                'edgeColor', 'none',...
                'faceAlpha', .5);
            self.StartLine = line([position(1), position(1)],...
                [position(2), position(2) + position(4)],...
                'parent', parentHandle,...
                'color', [1 0 0],...
                'lineWidth', 2);
            self.EndLine = line([position(1) + position(3), position(1) + position(3)],...
                [position(2), position(2) + position(4)],...
                'parent', parentHandle,...
                'color', [1 0 0],...
                'lineWidth', 2);
        end
                
        function delete(self)
            if ishandle(self.StartLine)
                delete(self.StartLine);
            end
            if ishandle(self.EndLine)
                delete(self.EndLine);
            end
            if ishandle(self.FillPatch)
                delete(self.FillPatch);
            end
        end
        
        function Show(self)
            set(self.StartLine, 'visible', 'on');
            set(self.EndLine, 'visible', 'on');
            set(self.FillPatch, 'visible', 'on');
        end
        
        function Hide(self)
            set(self.StartLine, 'visible', 'off');
            set(self.EndLine, 'visible', 'off');
            set(self.FillPatch, 'visible', 'off');
        end
        
        function SetXPosition(self, xPosition)
            self.Position(1) = xPosition - self.Position(3) / 2;
            self.UpdatePosition;
        end
        
        function SetWidth(self, width)
            self.Position(3) = width;
            self.UpdatePosition;
        end
        
        function SetYPosition(self, yPosition)
            self.Position(2) = yPosition;
            self.UpdatePosition;
        end
        
        function SetHeight(self, height)
            self.Position(4) = height;
            self.UpdatePosition;
        end
        
        function SetStart(self, startPosition)
            if startPosition < (self.Position(1) + self.Position(3))
                self.Position(3) = self.Position(3) + (self.Position(1) - startPosition);
                self.Position(1) = startPosition;
                self.UpdatePosition;
            end
        end
        
        function SetEnd(self, endPosition)
            if endPosition > self.Position(1)
                self.Position(3) = endPosition - self.Position(1);
                self.UpdatePosition;
            end
        end
        
        function set.OnMove(self, callback)
            set(self.FillPatch, 'ButtonDownFcn', callback);
        end
        
        function set.OnMoveStart(self, callback)
            set(self.StartLine, 'ButtonDownFcn', callback);
        end
        
        function set.OnMoveEnd(self, callback)
            set(self.EndLine, 'ButtonDownFcn', callback);
        end
        
        function set.OnMatchTemplate(self, callback)
            set(self.MatchTemplateMenu, 'callback', callback);
        end
        
        function [result, location] = GetSelected(self)
            switch hittest(gcf)
                case self.FillPatch
                    result = true;
                    location = 'Center';    
                case self.StartLine
                    result = true;
                    location = 'Start';    
                case self.EndLine
                    result = true;
                    location = 'End';
                otherwise
                    result = false;
                    location = 'None';
            end
        end
    end
    
    methods (Access = protected)
        function UpdatePosition(self)
            position = self.Position;
            set(self.FillPatch,...
                'xdata', [position(1), position(1), position(1) + position(3), position(1) + position(3)],...
                'ydata', [position(2), position(2) + position(4), position(2) + position(4), position(2)]);
            set(self.StartLine,...
                'xdata', [position(1), position(1)],...
                'ydata', [position(2), position(2) + position(4)]);
            set(self.EndLine,...
                'xdata', [position(1) + position(3), position(1) + position(3)],...
                'ydata', [position(2), position(2) + position(4)]);
        end
    end
end