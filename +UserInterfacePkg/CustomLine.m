classdef CustomLine < handle
    properties (GetAccess = public)
        XData
        YData
    end
    
    properties (SetAccess = private)
        ControlHandle
        Name
    end

    properties (SetObservable)
        Width
    end
    
    properties (Dependent = true)
        Color
        LineStyle
        Marker
        Visible
        Parent
    end
    
    events
        LineSelected
    end
    
    methods              
        function self = CustomLine(xData, yData, varargin)
            parser = inputParser;
            parser.addRequired('xData', @isnumeric);
            parser.addRequired('yData', @isnumeric);
            parser.addParamValue('name', 'CustomLine', @ischar);
            
            parser.KeepUnmatched = true;
            parser.parse(xData, yData, varargin{:})
            
            self.XData = xData;
            self.YData = yData;
            self.Name = parser.Results.name;
            self.Width = 1;
            
            addlistener(self, 'Width', 'PostSet', @self.SetWidthCallback);
        end
        
        function delete(self)
            if ishandle(self.ControlHandle)
                delete(self.ControlHandle);
            end
        end
        
        function Create(self, parentHandle)
            self.ControlHandle =...
                line('XData', self.XData, ...
                'YData', self.YData, ...
                'DisplayName', self.Name, ...
                'LineWidth', self.Width,...
                'parent', parentHandle,...
                'visible', 'on',...
                'ButtonDownFcn', @self.LineSelectedCallback);
        end
        
        function UIContextMenu(self, menu)
            set(self.ControlHandle, 'uicontextmenu', menu);
        end
        
        function lineCopy = CopyToAxes(self, axesHandle)
            lineCopy = UserInterfacePkg.CustomLine(self.XData, self.YData, 'Name', self.Name);
            lineCopy.Create(axesHandle);
            lineCopy.Width = self.Width;
            lineCopy.Color = self.Color;
            lineCopy.LineStyle = self.LineStyle;
            lineCopy.Visible = self.Visible;
        end
        
        function set.Parent(self, parentHandle)
           set(self.ControlHandle, 'parent', parentHandle);
        end
        
        function value = get.Parent(self)
           value = get(self.ControlHandle, 'parent');
        end
        
        function Show(self)
            for objectIndex = 1:numel(self)
                self(objectIndex).Visible = true;
            end
        end     
        function Hide(self)
            for objectIndex = 1:numel(self)
                self(objectIndex).Visible = false;
            end
        end
        
        function SendToBack(self)
            for objectIndex = 1:numel(self)
                uistack(self(objectIndex).ControlHandle, 'bottom');
            end
        end       
        function BringToFront(self)
            for objectIndex = 1:numel(self)
                uistack(self(objectIndex).ControlHandle, 'top');
            end
        end
        
        function color = get.Color(self)
            color = get(self.ControlHandle, 'Color');
        end
        function set.Color(self, color)
            set(self.ControlHandle, 'Color', color);
        end
        
        function style = get.LineStyle(self)
            style = get(self.ControlHandle, 'LineStyle');
        end
        function set.LineStyle(self, style)
            set(self.ControlHandle, 'LineStyle', style);
        end
        
        function set.Visible(self, value)
            if value
                set(self.ControlHandle, 'visible', 'on');
            else
                set(self.ControlHandle, 'visible', 'off');
            end
        end     
        function value = get.Visible(self)
           if strcmp(get(self.ControlHandle, 'visible'), 'on')
               value = true;
           else
               value = false;
           end
        end
        
        function marker = get.Marker(self)
            marker = get(self.ControlHandle, 'marker');
        end
        function set.Marker(self, marker)
            set(self.ControlHandle, 'marker', marker);
        end
    end
    
    methods (Access = private)
        function LineSelectedCallback(self, varargin)
            switch get(gcbf,'SelectionType')
                case 'normal'
                button = 'Left';
                case 'alt'
                button = 'Right';
                otherwise
                button = 'Left';
            end
            notify(self, 'LineSelected', EventPkg.ButtonDownEventData(button));
        end
        
        function SetWidthCallback(self, varargin)
            set(self.ControlHandle, 'LineWidth', self.Width);
        end
    end
    
end