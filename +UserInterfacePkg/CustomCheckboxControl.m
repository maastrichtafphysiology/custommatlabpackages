classdef CustomCheckboxControl < UserInterfacePkg.CustomControl
    properties (Dependent = true)
        Checked
    end
    
    methods
        function self = CustomCheckboxControl(position, callback)
            self = self@UserInterfacePkg.CustomControl(position);
            self.Callback = callback;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomControl(self, parentHandle);
            self.ControlHandle = uicontrol('style', 'checkbox',...
                'units', 'normalized',...
                'position', self.Position,...
                'string', self.Name,...
                'min', 0,...
                'max', 1,...
                'value', 0,...
                'callback', self.Callback);
        end
        
        function set.Checked(self, checked)
            if checked
                set(self.ControlHandle, 'value', get(self.ControlHandle, 'max'));
            else
                set(self.ControlHandle, 'value', get(self.ControlHandle, 'min'));
            end
        end
        
        function checked = get.Checked(self)
            if get(self.ControlHandle, 'value') == get(self.ControlHandle, 'max')
                checked = true;
            else
                checked = true;
            end
        end
    end
end